#!/bin/bash

echo "Create .env from template ..."
if [ ! -f .env ]; then
  cp .env.example .env
  echo "... done"
else
  echo " ... already exists"
fi

echo "Load Docker Compose environment variables from '.env' file ..."
source .env
echo "... done"

echo ""
echo "Create directories for Docker temp data in '${DOCKER_DATA_DIR}' ..."
mkdir -p ${DOCKER_DATA_DIR}/{minio,sonarqube,tmp}
echo "... done"

echo ""
echo "Change permissions for '${DOCKER_TMP_DATA_DIR}' to 777 ..."
chmod -R 777 ${DOCKER_TMP_DATA_DIR}
echo "... done"
