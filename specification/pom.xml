<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <artifactId>webapp-parent</artifactId>
        <groupId>org.kinsecta.webapp</groupId>
        <version>3.3.0-SNAPSHOT</version>
    </parent>

    <artifactId>specification</artifactId>

    <name>KInsecta API Spec</name>
    <description>The API Specification and Models for the KInsecta Web App</description>

    <properties>
        <!-- Dependency Versions -->
        <swagger.annotations.version>1.6.14</swagger.annotations.version>
        <jackson.version>2.14.3</jackson.version>
        <springfox.version>3.0.0</springfox.version>
    </properties>

    <dependencies>
        <!-- GLOBALLY MANAGED DEPENDENCIES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-security</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-validation</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.data</groupId>
            <artifactId>spring-data-commons</artifactId>
        </dependency>
        <dependency>
            <groupId>javax.validation</groupId>
            <artifactId>validation-api</artifactId>
        </dependency>
        <dependency>
            <groupId>org.openapitools</groupId>
            <artifactId>jackson-databind-nullable</artifactId>
        </dependency>


        <!-- OPENAPI SPECIFIC DEPENDENCIES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
        <dependency>
            <groupId>io.swagger</groupId>
            <artifactId>swagger-annotations</artifactId>
            <version>${swagger.annotations.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-core</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-annotations</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.jaxrs</groupId>
            <artifactId>jackson-jaxrs-base</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.jaxrs</groupId>
            <artifactId>jackson-jaxrs-json-provider</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.datatype</groupId>
            <artifactId>jackson-datatype-jsr310</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.dataformat</groupId>
            <artifactId>jackson-dataformat-yaml</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-core</artifactId>
            <version>${springfox.version}</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <!-- OpenAPI CodeGenerator -->
            <plugin>
                <groupId>org.openapitools</groupId>
                <artifactId>openapi-generator-maven-plugin</artifactId>
                <version>5.3.1</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                        <!--
                            Configuration Options:
                            https://github.com/OpenAPITools/openapi-generator/blob/master/modules/openapi-generator-maven-plugin/README.md#general-configuration-parameters
                        -->
                        <configuration>
                            <inputSpec>${project.basedir}/src/main/resources/v1/openapi.yaml</inputSpec>

                            <!-- https://openapi-generator.tech/docs/generators/spring/ -->
                            <generatorName>spring</generatorName>
                            <library>spring-boot</library><!-- default: spring-boot -->

                            <!--
                                https://openapi-generator.tech/docs/templating
                                Download OpenApi Generator GitHub from the tag we're using here (ex. https://github.com/OpenAPITools/openapi-generator/tree/v5.3.1)
                                Get the mustache templates you want to override from 'modules/openapi-generator/src/main/resources/JavaSpring'
                                and place them in the following resources directory and change to your needs.

                                Our changes:
                                - api.mustache:
                                  - Add @PreAuthorize annotation for 'x-spring-security-roles' OpenAPI key
                                  - Add Javadocs @param for 'x-spring-paginated'
                                - beanValidation.mustache:
                                  - Add 'is not Nullable' check inside required
                            -->
                            <templateDirectory>${project.basedir}/src/main/resources/openapi-generator/templates/JavaSpring</templateDirectory>

                            <apiPackage>org.kinsecta.webapp.api.v1</apiPackage>
                            <modelPackage>org.kinsecta.webapp.api.v1.model</modelPackage>
                            <invokerPackage>org.kinsecta.webapp.api.v1.handler</invokerPackage>

                            <configOptions>
                                <basePackage>org.kinsecta.webapp.api.v1</basePackage>
                                <configPackage>org.kinsecta.webapp.api.v1.config</configPackage>

                                <!-- Use interfaceOnly pattern... -->
                                <interfaceOnly>true</interfaceOnly><!-- default: false -->
                                <!-- ... OR delegate pattern -->
                                <delegatePattern>false</delegatePattern><!-- default: false -->

                                <!-- Don't generate default interface methods which return HttpStatus.NOT_IMPLEMENTED -->
                                <skipDefaultInterface>false</skipDefaultInterface><!-- default: false -->

                                <!-- Use Java 8 LocalDate/LocalDateTime classes for dates -->
                                <dateLibrary>java8-localdatetime</dateLibrary><!-- default: threetenbp -->

                                <!--
                                    Boolean getter prefix

                                    Unfortunately we cannot use 'is' because Hibernate Bean Validator cannot handle
                                    annotations on non-getXXX getters for 'Boolean' types. See explanation at
                                    https://stackoverflow.com/a/48519909/1128689
                                    > In the Java Bean convention, "is" is a valid prefix only for a primitive boolean.
                                    > Here you have a Boolean so it should really be getXXX(). That's why HV ignores it.
                                    Matching source code: https://github.com/hibernate/hibernate-validator/blob/6.2/engine/src/main/java/org/hibernate/validator/internal/properties/DefaultGetterPropertySelectionStrategy.java#L67-L104
                                -->
                                <booleanGetterPrefix>get</booleanGetterPrefix><!-- default: get -->

                                <!-- Enable OpenAPI Jackson Nullable library for models -->
                                <openApiNullable>true</openApiNullable><!-- default: true -->

                                <!-- Use Bean Validation Impl. to perform BeanValidation -->
                                <useBeanValidation>true</useBeanValidation><!-- default: false -->
                                <performBeanValidation>true</performBeanValidation><!-- default: false -->

                                <!-- Use Optional<> classes for non-required request params -->
                                <useOptional>true</useOptional><!-- default: false -->
                            </configOptions>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
