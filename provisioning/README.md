# Deploy via Ansible

---

## Table Of Contents

[[_TOC_]]


---

## Requirements

### For deployment usage

If you just want to run deployments, all you need is...

* [Docker](https://www.docker.com)

Read the following sections _SSH Setup_, _Running from Ansible Docker Image_ and _Deploy_ for setup and usage.

### For Ansible development

If you want to change and develop the Ansible configs, you should consider installing Python 3 and Ansible locally...

* [Python 3](https://www.python.org/downloads/) (versions 3.5 and higher)
* [Ansible 4.x](https://github.com/ansible/ansible)
* and some more Python packages (see instructions below)...

Read the following sections _SSH Setup_, _Local Setup_ and _Deploy_ for setup and usage.

Optionally you can use Vagrant and Virtualbox for testing the Ansible scripts in a local container:

* [Virtualbox](https://www.virtualbox.org/)
* [Vagrant](https://www.vagrantup.com/)

Read the following section _Test local deployment with Vagrant_ for more details on this process.

### OS hints

Windows:

* use the _Windows Subsystem for Linux_ (WSL 2, setup instructions [[1]](https://dev.to/fharookshaik/get-started-with-wsl2-44) [[2]](https://yduman.github.io/blog/wsl2-setup/))
  * make sure to clone the repository in the WSL filesystem and not in the virtual Windows filesystem (doesn't support chmod for ansible correctly)

macOS:

* Consider using [Homebrew](https://brew.sh) as a package manager


---

## Docs & Tutorials

* [Ansible Docs](https://docs.ansible.com/)
* [Working with playbooks - Best Practices](https://docs.ansible.com/ansible/latest/user_guide/playbooks.html)

### Tutorials

* [An Ansible2 Tutorial](https://serversforhackers.com/c/an-ansible2-tutorial)
* [Ansible - A Beginner's Tutorial, Part 1](https://www.youtube.com/watch?v=icR-df2Olm8)
* [Ansible - A Beginner's Tutorial, Part 2](https://www.youtube.com/watch?v=pRZA9ymZXn0)


---

## SSH setup

Ansible runs mainly on a passwordless SSH authentication workflow. While it's not strictly required to use an SSH key and config, please consider doing so!

You need a user account on the target server and a working SSH key setup. Contact the project administrators for details.

### SSH config

Please add the hosts of this project, as found in the _inventory_, to your local SSH config file (usually located at `~/.ssh/config`).

```
# webapp-test.kinsecta.org
Host kinsecta-test 157.97.107.94
  Hostname 157.97.107.94
  User xxxxx
  IdentityFile ~/.ssh/id_ed25519_kinsecta

# webapp.kinsecta.org
Host kinsecta-prod 157.97.107.117
  Hostname 157.97.107.117
  User xxxxx
  IdentityFile ~/.ssh/id_ed25519_kinsecta
```

Note: The `Host` key accepts a whitespace-separated list of aliases for the hostname. If you only set a generic alias (e.g. `kinsecta-test`) you can only log into this server with `ssh kinsecta-test` but not with `ssh 157.97.107.94`. However, this is needed for Ansible which runs a `ssh user@IP` command and therefore the IP address must also be set in the hostname alias list!

### `sudo` password

If the SSH user you specified in your SSH config requires a password to become `root` on the host, you must run _every_ Ansible command with the `--ask-become-pass` / `-K` option and enter the user's sudo password manually when prompted.


---

## Running from Ansible Docker Image

Goldflam GmbH maintains a fork of [geerlingguy's Ubuntu 22.04 Ansible Docker image](https://github.com/geerlingguy/docker-ubuntu2204-ansible) over at their GitLab `tools` group: [https://gitlab.com/goldflam-gmbh/tools/docker-ubuntu2204-ansible](https://gitlab.com/goldflam-gmbh/tools/docker-ubuntu2204-ansible).

This image can be used as a 100% replacement for a local Ansible installation and contains all tools necessary to run the Ansible scripts in this repo.

NOTE: This does NOT work with local Vagrant/Virtualbox because Vagrant needs Ansible to be installed on the same host.

### Pull the container

You need your GitLab credentials to log into our GitLab container registry. If you have 2FA enabled on your GitLab account, you need to create a [Personal Access Token](https://gitlab.com/-/profile/personal_access_tokens) with _Scope_ `read_registry` and use this token as password.

```
docker login registry.gitlab.com
docker pull registry.gitlab.com/goldflam-gmbh/tools/docker-ubuntu2204-ansible:5.8.0
```

### Start the container

Run a detached container in the background and mount your current project directory to `/data` (the project dir! Not the `provisioning` subfolder! Because Ansible needs access to SQL fixtures and the `pom.xml`).

Because the _workdir_ is initially set to `/data` (and later overwritten and set to `/data/provisioning/` by the `docker exec` command) you can run ansible commands as if you were working locally.

```
export DOCKER_ID_ANSIBLE=$(docker run --detach --privileged --cgroupns=host \
    --volume=/sys/fs/cgroup:/sys/fs/cgroup:rw \
    --volume=$(pwd):/data:rw \
    registry.gitlab.com/goldflam-gmbh/tools/docker-ubuntu2204-ansible:5.8.0)
```

The container ID is saved in the env variable `$DOCKER_ID_ANSIBLE` and can be used for the next steps.

#### SSH Keys

If you want to deploy to the staging or production server, you need to mount your local `~/.ssh/` directory to the target host:

```
export DOCKER_ID_ANSIBLE=$(docker run --detach --privileged --cgroupns=host \
    --volume=/sys/fs/cgroup:/sys/fs/cgroup:rw \
    --volume=$(pwd):/data:rw --volume=$HOME/.ssh:/root/.ssh:ro \
    registry.gitlab.com/goldflam-gmbh/tools/docker-ubuntu2204-ansible:5.8.0)
```

If you haven't configured an SSH config file as described above, you must provide the SSH username and the path of the corresponding SSH Public Key in the _docker exec ansible-playbook_ command with `--user SSH_USER --private-key /root/.ssh/ID_XXXXXXX`.

### Install Ansible Galaxy roles

```
docker exec -w /data/provisioning/ --tty $DOCKER_ID_ANSIBLE env TERM=xterm ansible-galaxy install -r roles.yml
```

### Run `ansible` or `ansible-playbook`

Run `ansible`:

```
$ docker exec -w /data/provisioning/ --tty $DOCKER_ID_ANSIBLE env TERM=xterm ansible --version
ansible [core 2.12.6]
  config file = /data/provisioning/ansible.cfg
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/local/lib/python3.10/dist-packages/ansible
  ansible collection location = /data/provisioning
  executable location = /usr/local/bin/ansible
  python version = 3.10.4 (main, Apr  2 2022, 09:04:19) [GCC 11.2.0]
  jinja version = 3.1.2
  libyaml = True
```

Run `ansible-playbook`:

```
$ docker exec -w /data/provisioning/ --tty $DOCKER_ID_ANSIBLE env TERM=xterm ansible-playbook playbook.yml --limit staging
[WARNING]: Could not match supplied host pattern, ignoring: default

PLAY [default,ionos] **************************************************************************************************

TASK [Include vars for local 'VAGRANT' environment] ********************************************************************
skipping: [kinsecta-test]

TASK [Include vars for 'STAGING' environment] **************************************************************************
ok: [kinsecta-test]

...
```


---

## Local Setup

### Python 3

Make sure you have Python 3.8+ installed on your system.

On macOS, you can install it via Homebrew:

```
brew install python@3.10
```

### Ansible

The preferred way to install Ansible's latest release is via `pip`:

```
python3 -m pip install --user ansible
```

You can install Ansible (currently 5.8.0) and all required pip packages (Ansible dependencies) for this project with:

```
python3 -m pip install -r requirements.txt
```

Make sure you have the Python `bin/` directory on your `$PATH`, otherwise the `ansible` command isn't found.

### Install roles from Ansible Galaxy

Install the required roles from [Ansible Galaxy](https://galaxy.ansible.com/home) before the first run:

```
ansible-galaxy install -r roles.yml
```

### Secrets Management with Ansible Vault and Ansible Keepass Plugin

We use the open-source [Ansible Keepass Plugin](https://github.com/viczem/ansible-keepass) from [their own fork](https://github.com/goldflam-gmbh/ansible-keepass) to manage a project specific Keepass vault containing secrets, passwords, etc.

The plugin depends on the pip package `pykeepass`, which should already be installed by the pip requirements install above.

Now you only need to install the Ansible Keepass Plugin:

```
mkdir -p ~/.ansible/plugins/lookup
curl https://raw.githubusercontent.com/goldflam-gmbh/ansible-keepass/goldflam/keepass.py -o "${_}/keepass.py"
```

The Keepass project vault is placed in the Ansible root directory and is named `keepass-vault.kdbx`.

The Keepass vault password is stored in Goldflam's _"Goldflam shared"_ Keepass vault at `goldflam-shared/KInsecta/Keepass Vault` or can be requested by contacting the project team.

However, in order to run this Ansible play locally and in CI, it is necessary to provide the Keepass vault password in a way that Ansible can access it with its own tools. That's the point where [Ansible Vault](https://docs.ansible.com/ansible/latest/user_guide/vault.html) comes into play. We encrypt the Keepass vault password with _Ansible Vault_ on the CLI and store the variable and its encrypted content in the inventory vars file (`group_vars/all.yml`) so that Ansible can access and decrypt it with _Ansible Vault_.

#### Providing the Ansible Vault password

The Ansible Vault password can either be provided to an Ansible play via interactive CLI prompt (via `--ask-vault-pass` option) or via a non-interactive hidden password file (`.ansible/.vault-password`).

If you opt for the password file method, create the folder `.ansible` and a `.vault-password` file inside and fill in the Ansible Vault password for this project which is stored in Goldflam's _"Goldflam shared"_ Keepass vault at `goldflam-shared/KInsecta/Ansible Vault`.

The password file method is also used for GitLab CI where this file is created via a CI Environment Variable.

#### Encrypt the Keepass vault password

Encryption is done via CLI once the Keepass vault password changes:

```
ansible-vault encrypt_string '<KEEPASS-VAULT-PASSWORD>' --name 'keepass_psw'
```

The string is encrypted using the Ansible vault password provided via interactive CLI prompt or stored in the Ansible vault password file (see previous section).

The multiline string output of this command must be (re)placed manually in the inventory file.


---

## Test local deployment with Vagrant

You can use [Vagrant](https://www.vagrantup.com/docs) with Virtualbox to test the deployment process with a local Linux VM.

On macOS, the preferred way to install Virtualbox and Vagrant is via Homebrew:

```
brew install --cask virtualbox
brew install vagrant
```

### DNS for dev domains

For local dev domains, you need to install the `vagrant-dns` plugin:

```
vagrant plugin install vagrant-dns
vagrant dns --install
```

On macOS, this will create a file in `/etc/resolver/` per each tld defined in `Vagrantfile`, which tells macOS to resolve the TLD by using the nameserver given in this file. You will have to rerun `--install` every time a tld is added.

For more details see the `vagrant-dns` plugin page: https://github.com/BerlinVagrant/vagrant-dns/

For this project, we use the `.devel` tld as configured in `vars/vagrant.yml` and `Vagrantfile`. If you want to use a different tld, please change the domains in all Ansible config files and also in the `Vagrantfile`.

### Initial Deploy

To initially deploy to the virtual machine, run the following command from the `provisioning/` directory:

```
vagrant dns --start
vagrant up
```

You can then access the frontend in your local browser at http://kinsecta.devel (or `http://192.168.56.10:8090`) and the API at http://api.kinsecta.devel (or `http://192.168.56.10:8080`).

You can log in to the vagrant box with `vagrant ssh`.

### Update Deployment

Re-Deploying via Ansible to the Vagrant box is done with

```
vagrant reload --provision
```

A hard re-deployment on a fresh Vagrant box can be done with

```
vagrant destroy && vagrant up
```

### Connect to the Vagrant box

You can connect via ssh to the Vagrant box:

```
vagrant ssh
```

### Access the deployed applications

If you don't change the defaults in the Ansible and Vagrant properties, you can access the deployed applications at:

* webapp: http://kinsecta.devel
* api: http://api.kinsecta.devel
* MariaDB: `192.168.56.10:33306`

### Shut down the Vagrant box

To shut down the Vagrant box simply type:

```
vagrant halt
```


---

## Deploy

The Ansible playbook reads to deployable version number from the local `pom.xml` and then pulls the corresponding Docker image tags from the GitLab container registry of this project.

Therefore, if you deploy manually without CI, make sure you check out the correct git commit before running Ansible. If you want to deploy to production for example, you may want to check out the `master` branch first as it contains the semver stable releases.

Before running an Ansible deploy, make sure you have a working SSH key and SSH config setup as described in the section _SSH Setup_ and you configured all necessary credentials as described in the sections above:

* SSH Keys
* Ansible vault password saved in `.ansible/.vault-password`

### Deploy to all hosts

_NOTE: Deploying to all hosts simultaneously is **strongly discouraged**! Limit your playbook run to a specific host or group!_

```
ansible-playbook playbook.yml
```

### Deploy to a specific host or group only

```
ansible-playbook playbook.yml --limit GROUPNAME
```

**Available Groups**

* `default`
  * the local Vagrant box
* `ionos`
  * all servers at Ionos' datacenter for this project
* `staging`
  * the staging server(s)
* `production`
  * the production server(s)

You can print an up-to-date inventory list with `ansible-inventory --list`.


---

## Database Backups

With the playbook `play_mysql_backup.yml` you can perform a database backup from the specified server:

```
ansible-playbook mysql_backup.yml --limit GROUPNAME
```

The SQL dump will be saved on the server in `/tmp/db_backup/`. The SQL dump is gzipped and then encrypted with OpenSSL (`AES256`). The encrypted file is then uploaded to the KInsecta-Cloud Nextcloud via WebDAV. The username and password and also the WebDAV endpoint is saved in the Keepass vault in this project.

### Nightly backup

A scheduled GitLab CI job performs a nightly database backup for the `production` database.

### Restore

Currently, there isn't an automated restore option.

However, you can download the encrypted SQL backup, decrypt, unzip and import it via the `mysql` command manually:

```bash
openssl enc -aes-256-cbc -d -in kinsecta-staging-2022-03-08-15:12:49.sql.tar.gz.enc -out kinsecta-staging-2022-03-08-15:12:49.sql.tar.gz
tar -xvzf kinsecta-staging-2022-03-08-15:12:49.sql.tar.gz
mysql -u USERNAME -p DATABASE < kinsecta-staging-2022-03-08-15:12:49.sql
```
