package org.kinsecta.webapp.api.service;

import org.kinsecta.webapp.api.exception.unchecked.WrongArgumentException;
import org.kinsecta.webapp.api.model.entities.Sensor;
import org.kinsecta.webapp.api.model.entities.UserRole;
import org.kinsecta.webapp.api.model.mapping.SensorDtoMapper;
import org.kinsecta.webapp.api.security.ContextUserService;
import org.kinsecta.webapp.api.v1.model.SensorDto;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


@Service
@Transactional
public class SensorMappingService {

    private final SensorService sensorService;
    private final SensorDtoMapper sensorDtoMapper;
    private final ContextUserService contextUserService;


    public SensorMappingService(SensorService sensorService, SensorDtoMapper sensorDtoMapper, ContextUserService contextUserService) {
        this.sensorService = sensorService;
        this.sensorDtoMapper = sensorDtoMapper;
        this.contextUserService = contextUserService;
    }


    public Sensor createSensor(SensorDto sensorDto) {
        Sensor newSensor = sensorDtoMapper.toSensor(sensorDto);
        return sensorService.createSensor(newSensor);
    }

    public Sensor updateSensor(Long sensorId, SensorDto sensorDto) {
        Sensor sensor = sensorService.getSensor(sensorId);

        // #183 Non-Admin Users cannot change the operator of a Sensor
        if (!contextUserService.getUserRole().equals(UserRole.ADMIN) && !sensorDto.getOperator().getId().get().equals(sensor.getOperator().getId())) {
            throw new WrongArgumentException("Your user role does not allow to change the Operator of a Sensor!");
        }

        sensor = sensorDtoMapper.updateSensor(sensor, sensorDto);
        return sensorService.save(sensor);
    }

}
