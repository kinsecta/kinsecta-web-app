package org.kinsecta.webapp.api.model.repositories;

import org.kinsecta.webapp.api.model.entities.Classification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface ClassificationRepository extends JpaRepository<Classification, Long> {

    List<Classification> findAllByDatasetId(Long datasetId);

    List<Classification> findAllByDatasetImageId(Long datasetImageId);

    List<Classification> findAllByMeasurementWingbeatId(Long measurementWingbeatId);

}
