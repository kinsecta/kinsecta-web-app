package org.kinsecta.webapp.api.util;

import org.springframework.data.domain.Page;


public class PageableHelper {

    private PageableHelper() {
        throw new IllegalStateException("Utility class");
    }


    public static <T> Page<T> returnPageOrEmpty(Page<T> page) {
        if (page.hasContent()) {
            return page;
        } else {
            return Page.empty();
        }
    }

}
