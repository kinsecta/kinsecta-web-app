package org.kinsecta.webapp.api.model.repositories;

import org.kinsecta.webapp.api.model.entities.Sensor;
import org.kinsecta.webapp.api.model.repositories.projections.PublicSensorProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;


public interface SensorRepository extends JpaRepository<Sensor, Long> {

    List<Sensor> findAllBySensorLocationId(Long id);

    Optional<Sensor> findByIdAndShowPublic(Long sensorId, boolean showPublic);

    @Query(value = """
        SELECT s.id AS id,
            sl.name AS name,
            sl.postcode AS postcode,
            sl.city AS city,
            sl.latitude AS latitude,
            sl.longitude AS longitude
        FROM Sensor s
            JOIN s.sensorLocation sl
        WHERE s.showPublic = true
        """)
    List<PublicSensorProjection> findAllPublic();

}
