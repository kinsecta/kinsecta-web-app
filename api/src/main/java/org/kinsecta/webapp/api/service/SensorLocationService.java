package org.kinsecta.webapp.api.service;

import org.kinsecta.webapp.api.exception.unchecked.ForbiddenException;
import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.model.entities.SensorLocation;
import org.kinsecta.webapp.api.model.repositories.SensorLocationRepository;
import org.kinsecta.webapp.api.model.repositories.SensorRepository;
import org.kinsecta.webapp.api.util.PageableHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class SensorLocationService {

    private static final Logger logger = LoggerFactory.getLogger(SensorLocationService.class);

    private final SensorLocationRepository sensorLocationRepository;
    private final SensorRepository sensorRepository;


    public SensorLocationService(SensorLocationRepository sensorLocationRepository, SensorRepository sensorRepository) {
        this.sensorLocationRepository = sensorLocationRepository;
        this.sensorRepository = sensorRepository;
    }


    @Transactional
    public SensorLocation createSensorLocation(SensorLocation sensorLocation) {
        return save(sensorLocation);
    }

    @Transactional(readOnly = true)
    public SensorLocation getSensorLocation(Long sensorLocationId) {
        return sensorLocationRepository.findById(sensorLocationId).orElseThrow(() -> new NotFoundException(String.format("Could not find SensorLocation with id %d", sensorLocationId)));
    }

    @Transactional(readOnly = true)
    public Page<SensorLocation> getAllSensorLocations(Pageable pageable) {
        Page<SensorLocation> pagedResult = sensorLocationRepository.findAll(pageable);
        return PageableHelper.returnPageOrEmpty(pagedResult);
    }

    @Transactional(readOnly = true)
    public List<SensorLocation> getAllSensorLocations() {
        return sensorLocationRepository.findAll();
    }

    @Transactional
    public SensorLocation save(SensorLocation sensorLocation) {
        return sensorLocationRepository.save(sensorLocation);
    }

    @Transactional
    public void delete(Long sensorLocationId) {
        if (!sensorRepository.findAllBySensorLocationId(sensorLocationId).isEmpty()) {
            throw new ForbiddenException(String.format("Cannot delete SensorLocation with id %d because some Sensors are linked to it.", sensorLocationId));
        } else {
            sensorLocationRepository.deleteById(sensorLocationId);
            logger.info("Successfully deleted SensorLocation with id {}", sensorLocationId);
        }
    }

}
