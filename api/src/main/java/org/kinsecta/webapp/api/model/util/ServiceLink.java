package org.kinsecta.webapp.api.model.util;

import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.model.entities.InsectOrder;
import org.kinsecta.webapp.api.service.InsectOrderService;
import org.mapstruct.Named;
import org.springframework.stereotype.Service;


@Service
@Named("ServiceLink")
public class ServiceLink {

    private final InsectOrderService insectOrderService;

    public ServiceLink(InsectOrderService insectOrderService) {
        this.insectOrderService = insectOrderService;
    }


    @Named("insectOrderById")
    public InsectOrder getInsectOrderById(Long id) {
        if (id == null) {
            return null;
        }
        try {
            return insectOrderService.getInsectOrder(id);
        } catch (NotFoundException e) {
            return null;
        }
    }

}
