package org.kinsecta.webapp.api.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Optional;


/**
 * This Spring Security Filter adds JWT based authorization via cookies to the project.
 * <p>
 * It is used for checking the 'JWT' cookie and validating the included 'Bearer Token' (the JWT)
 * with every API request.
 * <p>
 * Note: Mind the difference between <i>authentication</i> and <i>authorization</i>!
 */
public class JwtCookieFilter extends OncePerRequestFilter {

    private static final Logger log = LoggerFactory.getLogger(JwtCookieFilter.class);

    private final JwtTokenService jwtTokenService;


    public JwtCookieFilter(JwtTokenService jwtTokenService) {
        this.jwtTokenService = jwtTokenService;
    }


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        // Skip Authorization for public endpoints
        if (request.getRequestURI().startsWith("/public/")) {
            log.debug("Skipped JwtAuthorizationFilter for public resource path '{}'", request.getRequestURI());
            chain.doFilter(request, response);
            return;
        }

        // Get resource mapping from request (https://stackoverflow.com/a/60528458/1128689)
        log.debug("API request to authorized resource path '{}'", request.getRequestURI());

        // Read cookies from request
        Cookie[] cookies = request.getCookies();

        // Skip this JwtCookieFilter if the request doesn't contain any cookie at all
        if (cookies == null || cookies.length < 1) {
            log.debug("Skipped JwtCookieFilter because the request didn't contain any cookies at all");
            chain.doFilter(request, response);
            return;
        }

        // Try to read the 'JWT' cookie
        Optional<Cookie> jwtCookie = Arrays.stream(cookies).filter(cookie -> cookie.getName().equals("JWT")).findFirst();

        // Skip this JwtCookieFilter if the request cookies doesn't contain a 'JWT' cookie
        if (jwtCookie.isEmpty()) {
            log.debug("Skipped JwtCookieFilter because the request didn't contain a 'JWT' cookie");
            chain.doFilter(request, response);
            return;
        }

        // Get UsernamePasswordAuthenticationToken object by reading and validating the JWT
        UsernamePasswordAuthenticationToken authentication = getAuthenticationFromCookie(jwtCookie.get());

        // Set the authentication in the Spring Security context - even if it is null
        // (which means that the next Spring Security Chain Filter tries to authorize the request)
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // Call the next filter in the Spring Security filter chain
        chain.doFilter(request, response);
    }

    protected UsernamePasswordAuthenticationToken getAuthenticationFromCookie(Cookie jwtCookie) {
        // Read encoded JWT from cookie bearer token
        String encodedJwt = URLDecoder.decode(jwtCookie.getValue(), StandardCharsets.UTF_8).replace(JwtTokenService.TOKEN_PREFIX, "");

        UsernamePasswordAuthenticationToken authentication = jwtTokenService.verifyJwtAndGetAuthentication(encodedJwt);
        log.debug("API request successfully authorized via JWT in Cookie");

        return authentication;
    }

}
