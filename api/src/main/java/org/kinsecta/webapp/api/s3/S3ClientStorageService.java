package org.kinsecta.webapp.api.s3;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Random;


@Service
public class S3ClientStorageService {

    private final S3Client s3Client;

    private final Random random = new Random();

    @Value("${app.s3storage.bucket-name.list}")
    private String[] availableS3BucketNames;


    public S3ClientStorageService(S3Client s3Client) {
        this.s3Client = s3Client;
    }


    public String store(String s3BucketName, String s3Key, InputStream inputStream, long contentSize) throws StorageException {
        validateS3BucketName(s3BucketName, true);
        validateS3Key(s3Key);

        try {
            s3Client.uploadObject(s3BucketName, s3Key, inputStream, contentSize);
            return s3Key;

        } catch (S3ClientException e) {
            throw new StorageException(String.format("Failed to store InputStream as S3 object: bucket='%s', s3Key='%s'", s3BucketName, s3Key), e);
        }
    }

    public String store(String s3BucketName, String s3Key, MultipartFile file) throws StorageException {
        try {
            return store(s3BucketName, s3Key, file.getInputStream(), file.getSize());
        } catch (IOException e) {
            throw new StorageException(String.format("Failed to store multipart file with original name '%s' as S3 object: bucket='%s', s3Key='%s'", file.getOriginalFilename(), s3BucketName, s3Key), e);
        }
    }

    public void load(String s3BucketName, String s3Key, OutputStream outputStream) throws StorageException {
        validateS3BucketName(s3BucketName);
        validateS3Key(s3Key);

        try {
            s3Client.getObject(s3BucketName, s3Key, outputStream);
        } catch (S3ClientException e) {
            throw new StorageException(String.format("Failed to load file '%s' in bucket '%s'.", s3Key, s3BucketName), e);
        }
    }

    public InputStream load(String s3BucketName, String s3Key) throws StorageException {
        validateS3BucketName(s3BucketName);
        validateS3Key(s3Key);

        try {
            return s3Client.getObject(s3BucketName, s3Key);
        } catch (S3ClientException e) {
            throw new StorageException(String.format("Failed to load file '%s' in bucket '%s'.", s3Key, s3BucketName), e);
        }
    }

    public void delete(String s3BucketName, String s3Key) throws StorageException {
        validateS3BucketName(s3BucketName);
        validateS3Key(s3Key);

        try {
            s3Client.deleteObject(s3BucketName, s3Key);
        } catch (S3ClientException e) {
            throw new StorageException(String.format("Failed to delete file '%s' in bucket '%s'.", s3Key, s3BucketName), e);
        }
    }

    public String getCurrentS3BucketName() {
        int randomBucket = random.nextInt(availableS3BucketNames.length);
        return availableS3BucketNames[randomBucket];
    }


    // Helper functions ------------------------------------------------------------------------------------------------

    private void validateS3BucketName(String s3BucketName) throws StorageException {
        validateS3BucketName(s3BucketName, false);
    }

    private void validateS3BucketName(String s3BucketName, boolean checkAgainstList) throws StorageException {
        if (s3BucketName == null || s3BucketName.isBlank()) {
            throw new StorageException("Invalid S3 bucket name (null or blank): " + s3BucketName);
        }

        if (checkAgainstList && !Arrays.asList(availableS3BucketNames).contains(s3BucketName)) {
            throw new StorageException(String.format("Invalid S3 bucket name '%s': Name is not in list of available S3 buckets!", s3BucketName));
        }
    }

    private void validateS3Key(String s3Key) throws StorageException {
        if (s3Key == null || s3Key.isBlank()) {
            throw new StorageException("Invalid S3 key (null or blank)");
        }
    }

}
