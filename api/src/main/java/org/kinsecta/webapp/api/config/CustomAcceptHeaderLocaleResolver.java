package org.kinsecta.webapp.api.config;

import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.Locale;


/**
 * Resolves the Locale requested by the <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Accept-Language">'Accept-Language' header</a>.
 * Values can be Strings like 'de', 'en-US' or multiple types, weighted with the quality value syntax 'de,en-US;q=0.7,en;q=0.3'
 * <p>
 * Compared to the resolveLocale() method of the extended AcceptHeaderLocaleResolver class, this implementation goes
 * one step further and also tries to find a best match in the list of supported locales if the requestLocale defines
 * only a language ('de') and no country ('de-DE'). AcceptHeaderLocaleResolver.resolveLocale() returns the defaultLocale
 * in this special case.
 */
public class CustomAcceptHeaderLocaleResolver extends AcceptHeaderLocaleResolver {

    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        List<Locale> headerLocales = Collections.list(request.getLocales());
        if (!headerLocales.isEmpty()) {
            for (Locale requestLocale : headerLocales) {
                // return matching locale from supportedLocales array
                if (this.getSupportedLocales().contains(requestLocale)) {
                    return requestLocale;
                }
                // requestLocale has no 'country part'? then try finding the best matching supportedLocale by 'language part'
                else if (requestLocale.getCountry() == null || requestLocale.getCountry().isEmpty()) {
                    for (Locale candidateLocale : this.getSupportedLocales()) {
                        if (candidateLocale.getLanguage().equals(requestLocale.getLanguage())) {
                            return candidateLocale;
                        }
                    }
                }
            }
        }
        // Fallback: return defaultLocale
        return this.getDefaultLocale();
    }

}
