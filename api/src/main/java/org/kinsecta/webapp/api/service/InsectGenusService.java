package org.kinsecta.webapp.api.service;

import org.kinsecta.webapp.api.exception.unchecked.ForbiddenException;
import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.model.entities.InsectGenus;
import org.kinsecta.webapp.api.model.repositories.InsectGenusRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;


@Service
public class InsectGenusService {

    private static final Logger logger = LoggerFactory.getLogger(InsectGenusService.class);

    private final InsectGenusRepository insectGenusRepository;


    public InsectGenusService(InsectGenusRepository insectGenusRepository) {
        this.insectGenusRepository = insectGenusRepository;
    }


    @Transactional
    public InsectGenus save(InsectGenus insectGenus) {
        return insectGenusRepository.save(insectGenus);
    }

    @Transactional(readOnly = true)
    public InsectGenus getInsectGenus(Long insectGenusId) {
        return insectGenusRepository.findById(insectGenusId).orElseThrow(() -> new NotFoundException(String.format("Could not find InsectGenus with id %d", insectGenusId)));
    }

    @Transactional(readOnly = true)
    public InsectGenus getInsectGenus(String insectGenusName) {
        return insectGenusRepository.findByName(insectGenusName).orElseThrow(() -> new NotFoundException(String.format("Could not find InsectGenus with name '%s' ", insectGenusName)));
    }

    @Transactional(readOnly = true)
    public InsectGenus getInsectGenusByGbifId(Long gbifId) {
        return insectGenusRepository.findByGbifId(gbifId).orElseThrow(() -> new NotFoundException(String.format("Could not find InsectGenus with GBIF id %d ", gbifId)));
    }

    @Transactional(readOnly = true)
    public List<InsectGenus> getAllInsectGenuses() {
        return insectGenusRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<InsectGenus> getAllInsectGenusesForSearchQuery(String query) {
        String queryDecoded;
        try {
            queryDecoded = URLDecoder.decode(query, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
        return insectGenusRepository.findAllByNameContains(queryDecoded.toLowerCase());
    }

    @Transactional
    public void delete(Long insectGenusId) {
        InsectGenus insectGenus = getInsectGenus(insectGenusId);
        if (!insectGenus.getInsectSpeciesList().isEmpty()) {
            throw new ForbiddenException(String.format("InsectGenus with id '%s' is linked to one or many InsectSpecies and therefore cannot be deleted", insectGenusId));
        } else {
            insectGenusRepository.deleteById(insectGenusId);
            logger.info("Successfully deleted InsectGenus with id {}", insectGenusId);
        }
    }

}
