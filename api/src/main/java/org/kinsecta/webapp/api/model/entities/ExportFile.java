package org.kinsecta.webapp.api.model.entities;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Table(name = "export_file")
public class ExportFile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "export_id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Export export;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "s3_object_id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private S3Object s3Object;

    @Column(name = "created", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW()")
    @Generated(GenerationTime.INSERT)
    @ReadOnlyProperty
    private LocalDateTime created;

    @Column(name = "modified", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW() ON UPDATE NOW()")
    @Generated(GenerationTime.ALWAYS)
    @ReadOnlyProperty
    private LocalDateTime modified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Export getExport() {
        return export;
    }

    public void setExport(Export export) {
        this.export = export;
    }

    public S3Object getS3Object() {
        return s3Object;
    }

    public void setS3Object(S3Object s3Object) {
        this.s3Object = s3Object;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }


    @Override
    public String toString() {
        return "ExportFile{" +
            "id=" + id +
            ", export=" + export.getId() +
            ", s3Object=" + s3Object.getId() +
            ", created=" + created +
            ", modified=" + modified +
            '}';
    }

}
