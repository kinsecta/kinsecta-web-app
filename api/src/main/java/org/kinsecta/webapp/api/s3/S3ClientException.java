package org.kinsecta.webapp.api.s3;

public class S3ClientException extends Exception {

    public S3ClientException(String message, Exception e) {
        super(message, e);
    }

    public S3ClientException(String message) {
        super(message);
    }

}
