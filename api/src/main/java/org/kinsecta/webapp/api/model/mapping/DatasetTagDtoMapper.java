package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.model.entities.DatasetTag;
import org.kinsecta.webapp.api.service.DatasetTagService;
import org.kinsecta.webapp.api.v1.model.DatasetTagDto;
import org.kinsecta.webapp.api.v1.model.IdObject;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@Mapper(
    componentModel = "spring",
    uses = {
        DtoMapperConfig.class
    },
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public abstract class DatasetTagDtoMapper {

    @Autowired
    private DatasetTagService datasetTagService;


    public abstract DatasetTagDto datasetTagToDatasetTagDto(DatasetTag datasetTag);

    public abstract DatasetTag datasetTagDtoToDatasetTag(DatasetTagDto datasetTagDto);

    public IdObject datasetTagToIdObject(DatasetTag datasetTag) {
        return new IdObject().id(datasetTag.getId());
    }

    public static IdObject datasetTagIdToIdObject(Long datasetTagId) {
        return new IdObject().id(datasetTagId);
    }

    public DatasetTag idObjectToDatasetTag(IdObject idObject) {
        if (idObject != null) {
            return datasetTagService.getDatasetTag(idObject.getId());
        }
        return null;
    }

    public static List<IdObject> datasetTagIdStringListToIdObjectList(String datasetTagIdsAsString) {
        if (datasetTagIdsAsString == null) {
            return new ArrayList<>();
        }
        List<String> datasetTagIds = List.of(datasetTagIdsAsString.split(";"));
        return datasetTagIds.stream().map(datasetTagId -> datasetTagIdToIdObject(Long.parseLong(datasetTagId))).toList();
    }

    public List<DatasetTagDto> datasetTagIdStringListToDatasetTagDto(String datasetTagIdsAsString) {
        if (datasetTagIdsAsString == null) {
            return new ArrayList<>();
        }
        List<IdObject> datasetTagIdsAsIdObject = datasetTagIdStringListToIdObjectList(datasetTagIdsAsString);
        return idObjectListToDatasetTagSet(datasetTagIdsAsIdObject).stream().map(this::datasetTagToDatasetTagDto).toList();
    }

    public abstract List<IdObject> datasetTagSetToIdObjectList(Set<DatasetTag> datasetTagSet);

    public abstract Set<DatasetTag> idObjectListToDatasetTagSet(List<IdObject> idObjectList);

}
