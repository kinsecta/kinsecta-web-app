package org.kinsecta.webapp.api.scheduling;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;


@EnableScheduling
@ConditionalOnProperty(name = "app.scheduler.enabled")
@Configuration
public class SchedulerConfig {
}
