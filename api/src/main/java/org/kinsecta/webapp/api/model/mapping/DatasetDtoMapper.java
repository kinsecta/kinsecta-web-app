package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.model.entities.Dataset;
import org.kinsecta.webapp.api.service.DatasetService;
import org.kinsecta.webapp.api.v1.model.DatasetDto;
import org.kinsecta.webapp.api.v1.model.DatasetTableViewDto;
import org.kinsecta.webapp.api.v1.model.IdObject;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;


@Mapper(
    componentModel = "spring",
    uses = {
        DtoMapperConfig.class,
        UserDtoMapper.class,
        DatasetImageDtoMapper.class,
        DatasetTagDtoMapper.class,
        UploadDtoMapper.class,
        S3ObjectDtoMapper.class,
        MeasurementWingbeatDtoMapper.class,
        SensorDtoMapper.class
    },
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public abstract class DatasetDtoMapper {

    @Autowired
    private DatasetService datasetService;

    @Autowired
    private DatasetTagDtoMapper datasetTagDtoMapper;


    public abstract DatasetDto datasetToDatasetDto(Dataset dataset);

    public abstract Dataset datasetDtoToDataset(DatasetDto datasetDto);

    public abstract Dataset datasetTableViewDtoToDataset(DatasetTableViewDto dataset);

    public abstract DatasetTableViewDto datasetToDatasetTableViewDto(Dataset dataset);

    public Dataset idObjectToDataset(IdObject idObject) {
        if (idObject != null && idObject.getId() != null) {
            return datasetService.getDataset(idObject.getId());
        }
        return null;
    }

    public IdObject datasetToIdObject(Dataset dataset) {
        if (dataset == null) {
            return null;
        }
        return new IdObject().id(dataset.getId());
    }

}
