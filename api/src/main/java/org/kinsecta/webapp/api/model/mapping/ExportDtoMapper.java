package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.model.entities.Export;
import org.kinsecta.webapp.api.v1.model.ExportDto;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;


@Mapper(
    componentModel = "spring",
    uses = {
        DtoMapperConfig.class,
        SensorDtoMapper.class,
        SensorLocationDtoMapper.class,
        TransferStatusMapper.class,
        UserDtoMapper.class
    },
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public abstract class ExportDtoMapper {

    public abstract Export exportDtoToExport(ExportDto exportDto);

    public abstract ExportDto exportToExportDto(Export export);

}
