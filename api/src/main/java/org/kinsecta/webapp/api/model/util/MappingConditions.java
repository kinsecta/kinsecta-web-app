package org.kinsecta.webapp.api.model.util;

import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.model.entities.UserRole;
import org.kinsecta.webapp.api.security.ContextUserService;
import org.mapstruct.Named;
import org.springframework.stereotype.Service;


@Service
@Named("MappingConditions")
public class MappingConditions {

    private final ContextUserService contextUserService;


    public MappingConditions(ContextUserService contextUserService) {
        this.contextUserService = contextUserService;
    }


    @Named("omitUsernameIfContextUserIsNotAdminAndUserIsNotSelf")
    public String omitUsernameIfContextUserIsNotAdminAndUserIsNotSelf(User inputUser) {
        if (inputUser.getId().equals(contextUserService.getUserId()) || contextUserService.getUserRole().equals(UserRole.ADMIN)) {
            return inputUser.getUsername();
        } else {
            return null;
        }
    }

    @Named("omitFullNameIfContextUserIsNotAdminAndUserIsNotSelf")
    public String omitFullNameIfContextUserIsNotAdminAndUserIsNotSelf(User inputUser) {
        if (inputUser.getId().equals(contextUserService.getUserId()) || contextUserService.getUserRole().equals(UserRole.ADMIN)) {
            return inputUser.getFullName();
        } else {
            return null;
        }
    }

}
