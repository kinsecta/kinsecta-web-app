package org.kinsecta.webapp.api.model.repositories;

import org.kinsecta.webapp.api.model.entities.Upload;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UploadRepository extends JpaRepository<Upload, Long> {

    Page<Upload> findAllByUserId(Long userId, Pageable pageable);

}
