package org.kinsecta.webapp.api.service;

import org.kinsecta.webapp.api.exception.unchecked.ForbiddenException;
import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.model.entities.Sensor;
import org.kinsecta.webapp.api.model.repositories.DatasetRepository;
import org.kinsecta.webapp.api.model.repositories.ExportRepository;
import org.kinsecta.webapp.api.model.repositories.SensorRepository;
import org.kinsecta.webapp.api.model.repositories.projections.PublicSensorProjection;
import org.kinsecta.webapp.api.util.PageableHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
public class SensorService {

    private static final Logger logger = LoggerFactory.getLogger(SensorService.class);

    private final SensorRepository sensorRepository;
    private final ExportRepository exportRepository;
    private final DatasetRepository datasetRepository;


    public SensorService(SensorRepository sensorRepository, ExportRepository exportRepository, DatasetRepository datasetRepository) {
        this.sensorRepository = sensorRepository;
        this.exportRepository = exportRepository;
        this.datasetRepository = datasetRepository;
    }


    @Transactional
    public Sensor createSensor(Sensor sensor) {
        return save(sensor);
    }

    @Transactional(readOnly = true)
    public Sensor getSensor(Long sensorId) {
        return getOptionalSensor(sensorId).orElseThrow(() -> new NotFoundException(String.format("Could not find Sensor with id %d", sensorId)));
    }

    @Transactional(readOnly = true)
    public Sensor getSensorPublic(Long sensorId) {
        return sensorRepository.findByIdAndShowPublic(sensorId, true).orElseThrow(() -> new NotFoundException(String.format("Could not find Sensor with id %d. It may not be publicly available.", sensorId)));
    }

    @Transactional(readOnly = true)
    public Optional<Sensor> getOptionalSensor(Long sensorId) {
        return sensorRepository.findById(sensorId);
    }

    @Transactional(readOnly = true)
    public Page<Sensor> getAllSensors(Pageable pageable) {
        Page<Sensor> pagedResult = sensorRepository.findAll(pageable);
        return PageableHelper.returnPageOrEmpty(pagedResult);
    }

    @Transactional(readOnly = true)
    public List<Sensor> getAllSensors() {
        return sensorRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<PublicSensorProjection> getAllSensorsPublic() {
        return sensorRepository.findAllPublic();
    }

    @Transactional
    public Sensor save(Sensor sensor) {
        return sensorRepository.save(sensor);
    }

    @Transactional
    public void delete(Long sensorId) {
        if (!datasetRepository.findAllBySensorId(sensorId).isEmpty()) {
            throw new ForbiddenException(String.format("Cannot delete Sensor with id %d because some Datasets are linked to it.", sensorId));
        } else {
            sensorRepository.deleteById(sensorId);
            logger.info("Successfully deleted Sensor with id {}", sensorId);
        }
    }

}
