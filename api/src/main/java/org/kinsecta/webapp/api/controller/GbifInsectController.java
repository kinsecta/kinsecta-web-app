package org.kinsecta.webapp.api.controller;

import org.kinsecta.webapp.api.model.dto.GbifResponseItem;
import org.kinsecta.webapp.api.model.mapping.GbifResponseItemMapper;
import org.kinsecta.webapp.api.service.GbifApiService;
import org.kinsecta.webapp.api.v1.GbifInsectsApi;
import org.kinsecta.webapp.api.v1.model.GbifInsectDto;
import org.kinsecta.webapp.api.v1.model.TaxonomyLevel;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;


@Controller
public class GbifInsectController extends BaseController implements GbifInsectsApi {

    private final GbifApiService gbifApiService;
    private final GbifResponseItemMapper gbifResponseItemMapper;


    public GbifInsectController(GbifApiService gbifApiService, GbifResponseItemMapper gbifResponseItemMapper) {
        this.gbifApiService = gbifApiService;
        this.gbifResponseItemMapper = gbifResponseItemMapper;
    }


    @Override
    public ResponseEntity<GbifInsectDto> getGbifInsect(Long gbifId) {
        GbifResponseItem gbifResponseItem = gbifApiService.getGbifResponseItemForGbifId(gbifId);
        GbifInsectDto gbifInsectDto = gbifResponseItemMapper.gbifResponseItemToGbifInsectDto(gbifResponseItem);
        return ResponseEntity.ok(gbifInsectDto);
    }

    @Override
    public ResponseEntity<GbifInsectDto> postGbifInsect(Long gbifId) {
        GbifResponseItem gbifResponseItem = gbifApiService.getGbifResponseItemForGbifId(gbifId);
        TaxonomyLevel taxonomyLevel = TaxonomyLevel.fromValue(gbifResponseItem.getRank());
        switch (taxonomyLevel) {
            case ORDER -> gbifApiService.getOrCreateInsectOrder(gbifResponseItem);
            case FAMILY -> gbifApiService.getOrCreateInsectFamily(gbifResponseItem);
            case GENUS -> gbifApiService.getOrCreateInsectGenus(gbifResponseItem);
            case SPECIES -> gbifApiService.getOrCreateInsectSpecies(gbifResponseItem);
        }
        GbifInsectDto gbifInsectDto = gbifResponseItemMapper.gbifResponseItemToGbifInsectDto(gbifResponseItem);
        return ResponseEntity.ok(gbifInsectDto);
    }

}
