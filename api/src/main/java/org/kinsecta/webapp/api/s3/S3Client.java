package org.kinsecta.webapp.api.s3;

import com.amazonaws.AmazonClientException;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.s3.transfer.Download;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;
import com.amazonaws.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


public class S3Client {

    private static final Logger logger = LoggerFactory.getLogger(S3Client.class);

    private String accessKey;
    private String secretKey;
    private String serviceEndpoint;


    /**
     * Setter for the S3 Access Key
     *
     * @param accessKey the access key
     */
    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    /**
     * Setter for the S3 Secret Key
     *
     * @param secretKey the secret key
     */
    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    /**
     * Setter for the S3 Endpoint URL
     *
     * @param serviceEndpoint the endpoint URL
     */
    public void setServiceEndpoint(String serviceEndpoint) {
        this.serviceEndpoint = serviceEndpoint;
    }

    /**
     * Checks whether there is an object in a given bucket starting with the given prefix
     *
     * @param bucketName the bucket to search in for the object
     * @param prefix     the prefix to search for
     *
     * @return true, if there is an object starting with the given prefix in the given bucket
     *
     * @throws S3ClientException
     */
    public boolean hasObjectsThatStartWith(String bucketName, String prefix) throws S3ClientException {
        AmazonS3 client = createAmazonS3Client();
        try {
            ListObjectsV2Result allObjects = client.listObjectsV2(bucketName, prefix);
            return !allObjects.getObjectSummaries().isEmpty();

        } catch (AmazonClientException e) {
            throw new S3ClientException(String.format("Error while looking for files with prefix %s", prefix), e);

        } finally {
            client.shutdown();
        }
    }

    /**
     * Uploads a local file to the configured S3 Server and stores it in the given bucket at the given filename
     *
     * @param bucketName     the name of the target S3 bucket
     * @param targetFilename the filename (S3 key) on the S3 Server
     * @param sourceFile     the local file
     *
     * @throws S3ClientException
     */
    public void uploadObject(String bucketName, String targetFilename, File sourceFile) throws S3ClientException {
        if (!sourceFile.isFile()) {
            throw new S3ClientException(String.format("File at '%s' doesn't exist or isn't a file", sourceFile.getAbsolutePath()));
        }

        TransferManager transferManager = createTransferManager();

        try (FileInputStream fis = new FileInputStream(sourceFile)) {
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentLength(sourceFile.length());
            Upload upload = transferManager.upload(bucketName, targetFilename, fis, objectMetadata);
            upload.waitForCompletion();
            logger.debug("Local File successfully uploaded to S3: sourceFile='{}', bucket='{}', key='{}'", sourceFile.getAbsolutePath(), bucketName, targetFilename);

        } catch (AmazonClientException | IOException e) {
            throw new S3ClientException(String.format("Upload to S3 failed for local File: sourceFile='%s', bucket='%s', key='%s'", sourceFile.getAbsolutePath(), bucketName, targetFilename), e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new S3ClientException(String.format("Upload to S3 failed for local File: sourceFile='%s', bucket='%s', key='%s'", sourceFile.getAbsolutePath(), bucketName, targetFilename), e);

        } finally {
            transferManager.shutdownNow();
        }
    }

    /**
     * Uploads a file from an InputStream to the configured S3 Server and stores it in the given bucket at the given targetFilename
     *
     * @param bucketName          the name of the target S3 bucket
     * @param targetFilename      the targetFilename (S3 key) on the S3 Server
     * @param sourceInputStream   the input stream to store in S3
     * @param sourceContentLength the file size
     *
     * @throws S3ClientException
     */
    public void uploadObject(String bucketName, String targetFilename, InputStream sourceInputStream, long sourceContentLength) throws S3ClientException {
        TransferManager transferManager = createTransferManager();
        try {
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(sourceContentLength);
            Upload upload = transferManager.upload(bucketName, targetFilename, sourceInputStream, metadata);
            upload.waitForCompletion();
            logger.debug("InputStream successfully uploaded to S3: bucket='{}', key='{}'", bucketName, targetFilename);

        } catch (AmazonClientException e) {
            throw new S3ClientException(String.format("Upload to S3 failed for InputStream: bucket='%s', key='%s'", bucketName, targetFilename), e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new S3ClientException(String.format("Upload to S3 failed for InputStream: bucket='%s', key='%s'", bucketName, targetFilename), e);
        } finally {
            transferManager.shutdownNow();
        }
    }

    /**
     * Retrieves a file stored in S3 at given filename and writes its content to the outputStream.
     * <p>
     * Don't forget to close the OutputStream after calling this method!
     *
     * @param bucketName   the name of the S3 bucket to retrieve the file from
     * @param filename     the filename to retrieve
     * @param outputStream the {@link OutputStream} to write to
     *
     * @throws S3ClientException
     */
    public void getObject(String bucketName, String filename, OutputStream outputStream) throws S3ClientException {
        AmazonS3 client = createAmazonS3Client();
        try {
            S3Object object = client.getObject(bucketName, filename);
            S3ObjectInputStream receiver = object.getObjectContent();
            IOUtils.copy(receiver, outputStream);
            receiver.close();
            logger.debug("Successfully received S3 object and copied it to the output stream: bucket='{}', key='{}'", bucketName, filename);

        } catch (AmazonClientException | IOException e) {
            throw new S3ClientException(String.format("Download from S3 failed: bucket='%s', key='%s'", bucketName, filename), e);

        } finally {
            client.shutdown();
        }
    }

    /**
     * Retrieves a file stored in S3 at given filename and returns an {@link InputStream} instance.
     * <p>
     * Don't forget to close the S3ObjectInputStream after calling this method!
     *
     * @param bucketName the name of the S3 bucket to retrieve the file from
     * @param filename   the filename to retrieve
     *
     * @return an {@link InputStream} instance
     *
     * @throws S3ClientException
     */
    public InputStream getObject(String bucketName, String filename) throws S3ClientException {
        AmazonS3 client = createAmazonS3Client();
        try {
            S3Object object = client.getObject(bucketName, filename);
            logger.debug("Started streaming an S3 object to an InputStream: bucket='{}', key='{}'", bucketName, filename);
            return object.getObjectContent().getDelegateStream();

        } catch (AmazonClientException e) {
            throw new S3ClientException(String.format("Download from S3 failed: bucket='%s', key='%s'", bucketName, filename), e);

        } finally {
            // Cannot close this connection because otherwise the S3ObjectInputStream cannot be read later on
            // and the Spring REST Controller fails with a 'Socket closed' exception.
            //client.shutdown();
        }
    }

    /**
     * Retrieves a file stored in S3 at given filename and stores it in the given targetDirectory.
     *
     * @param bucketName      the name of the S3 bucket to retrieve the file from
     * @param filename        the filename to retrieve
     * @param targetDirectory the target directory to store the retrieved file in
     *
     * @throws S3ClientException
     */
    public void getAndDeleteObject(String bucketName, String filename, Path targetDirectory) throws S3ClientException {
        TransferManager transferManager = createTransferManager();
        try {
            Path outputPath = Paths.get(targetDirectory.toString(), filename);
            Download download = transferManager.download(bucketName, filename, outputPath.toFile());
            download.waitForCompletion();
            transferManager.getAmazonS3Client().deleteObject(bucketName, filename);
            logger.debug("Successfully received and deleted S3 object and stored it locally: bucket='{}', key='{}', targetDirectory='{}', outputPath='{}'", bucketName, filename, targetDirectory, outputPath);

        } catch (AmazonClientException e) {
            throw new S3ClientException(String.format("Download from S3 failed: bucket='%s', key='%s', targetDirectory='%s'", bucketName, filename, targetDirectory.toAbsolutePath()), e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new S3ClientException(String.format("Download from S3 failed: bucket='%s', key='%s', targetDirectory='%s'", bucketName, filename, targetDirectory.toAbsolutePath()), e);

        } finally {
            transferManager.shutdownNow();
        }
    }

    /**
     * Deletes a file stored in S3 at given filename
     *
     * @param bucketName the name of the S3 bucket to delete the file from
     * @param filename   the filename to delete
     *
     * @throws S3ClientException
     */
    public void deleteObject(String bucketName, String filename) throws S3ClientException {
        AmazonS3 client = createAmazonS3Client();
        try {
            client.deleteObject(bucketName, filename);
            logger.info("Successfully deleted S3 object: bucket='{}', key='{}'", bucketName, filename);

        } catch (AmazonClientException e) {
            throw new S3ClientException(String.format("Deletion of S3 object failed: bucket='%s', key='%s", bucketName, filename), e);

        } finally {
            client.shutdown();
        }
    }

    /**
     * List all S3 objects in given bucket
     *
     * @param bucketName the name of the S3 bucket to query
     *
     * @return a {@link List} with filenames in the given bucket
     */
    public List<String> listAllObjects(String bucketName) {
        AmazonS3 client = createAmazonS3Client();
        try {
            ListObjectsV2Result allObjects = client.listObjectsV2(bucketName);
            List<String> result = new ArrayList<>();
            for (S3ObjectSummary s3ObjectSummary : allObjects.getObjectSummaries()) {
                result.add(s3ObjectSummary.getKey());
            }
            return result;

        } finally {
            client.shutdown();
        }
    }


    private TransferManager createTransferManager() {
        return TransferManagerBuilder.standard().withS3Client(createAmazonS3Client()).build();
    }

    private AmazonS3 createAmazonS3Client() {
        AwsClientBuilder.EndpointConfiguration endpointConfiguration = createEndpointConfiguration();
        ClientConfiguration clientConfiguration = new ClientConfiguration();
        clientConfiguration.setProtocol(Protocol.HTTP);
        return AmazonS3ClientBuilder
            .standard()
            .withCredentials(createCredentialProvider())
            .withEndpointConfiguration(endpointConfiguration)
            .enablePathStyleAccess()
            .withClientConfiguration(clientConfiguration)
            .build();
    }

    private AwsClientBuilder.EndpointConfiguration createEndpointConfiguration() {
        return new AwsClientBuilder.EndpointConfiguration(serviceEndpoint, null);
    }

    private AWSCredentialsProvider createCredentialProvider() {
        final AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        return new AWSCredentialsProvider() {
            @Override
            public AWSCredentials getCredentials() {
                return credentials;
            }

            @Override
            public void refresh() {
                // is never used
            }
        };
    }

}
