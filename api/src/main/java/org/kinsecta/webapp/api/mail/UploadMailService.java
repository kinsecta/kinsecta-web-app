package org.kinsecta.webapp.api.mail;

import org.kinsecta.webapp.api.model.entities.Upload;
import org.kinsecta.webapp.api.model.entities.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UploadMailService extends MailService {

    private static final Logger logger = LoggerFactory.getLogger(UploadMailService.class);


    public UploadMailService(MessageSource messageSource, JavaMailSenderFacade javaMailSenderFacade) {
        super(messageSource, javaMailSenderFacade);
    }


    // TODO: CC admin team
    public void sendUploadErrorMessage(Upload upload, List<String> errors) {
        User user = upload.getUser();
        String mailTo = user.getEmail();
        String mailSubject = translate("upload.error-message.mail.subject", upload.getId());
        String mailText = translate("upload.error-message.mail.body",
            user.getFullName(),
            upload.getId(),
            String.join("\n\n", errors),
            appName);

        logger.info("About to send 'upload error message' email for user '{}'", user.getUsername());
        javaMailSenderFacade.sendMailAsync(mailTo, mailSubject, mailText);
    }

}

