package org.kinsecta.webapp.api.controller;

import org.kinsecta.webapp.api.model.entities.InsectOrder;
import org.kinsecta.webapp.api.model.mapping.InsectOrderDtoMapper;
import org.kinsecta.webapp.api.service.InsectOrderService;
import org.kinsecta.webapp.api.v1.InsectOrdersApi;
import org.kinsecta.webapp.api.v1.model.InsectOrderDto;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.List;


@Controller
public class InsectOrderController extends BaseController implements InsectOrdersApi {

    private final InsectOrderDtoMapper insectOrderDtoMapper;
    private final InsectOrderService insectOrderService;


    public InsectOrderController(InsectOrderDtoMapper insectOrderDtoMapper, InsectOrderService insectOrderService) {
        this.insectOrderDtoMapper = insectOrderDtoMapper;
        this.insectOrderService = insectOrderService;
    }


    @Override
    public ResponseEntity<List<InsectOrderDto>> getAllInsectOrders() {
        List<InsectOrder> insectOrders = insectOrderService.getAllInsectOrders();
        List<InsectOrderDto> dtoList = insectOrders.stream().map(insectOrderDtoMapper::insectOrderToInsectOrderDto).toList();
        return ResponseEntity.ok(dtoList);
    }

    @Override
    public ResponseEntity<List<InsectOrderDto>> searchInsectOrders(String query) {
        if (query == null || query.isEmpty()) {
            return getAllInsectOrders();
        }
        List<InsectOrder> allInsectOrdersForSearchQuery = insectOrderService.getAllInsectOrdersForSearchQuery(query);
        List<InsectOrderDto> dtoList = allInsectOrdersForSearchQuery.stream().map(insectOrderDtoMapper::insectOrderToInsectOrderDto).toList();
        return ResponseEntity.ok(dtoList);
    }

    @Override
    public ResponseEntity<InsectOrderDto> getInsectOrder(Long insectOrderId) {
        InsectOrder insectOrder = insectOrderService.getInsectOrder(insectOrderId);
        InsectOrderDto insectOrderDto = insectOrderDtoMapper.insectOrderToInsectOrderDto(insectOrder);
        return ResponseEntity.ok(insectOrderDto);
    }

}
