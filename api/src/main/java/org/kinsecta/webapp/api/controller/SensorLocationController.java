package org.kinsecta.webapp.api.controller;

import org.kinsecta.webapp.api.model.entities.SensorLocation;
import org.kinsecta.webapp.api.model.mapping.SensorLocationDtoMapper;
import org.kinsecta.webapp.api.service.SensorLocationService;
import org.kinsecta.webapp.api.v1.SensorLocationsApi;
import org.kinsecta.webapp.api.v1.model.ApiViewDto;
import org.kinsecta.webapp.api.v1.model.SensorLocationDto;
import org.kinsecta.webapp.api.v1.model.SensorLocationSimpleViewDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.List;


@Controller
public class SensorLocationController extends BaseController implements SensorLocationsApi {

    private final SensorLocationDtoMapper sensorLocationDtoMapper;
    private final SensorLocationService sensorLocationService;


    public SensorLocationController(SensorLocationDtoMapper sensorLocationDtoMapper, SensorLocationService sensorLocationService) {
        this.sensorLocationDtoMapper = sensorLocationDtoMapper;
        this.sensorLocationService = sensorLocationService;
    }


    @Override
    public ResponseEntity<SensorLocationDto> createSensorLocation(SensorLocationDto sensorLocationDto) {
        SensorLocation sensorLocation = sensorLocationDtoMapper.sensorLocationDtoToSensorLocation(sensorLocationDto);
        SensorLocation createdSensorLocation = sensorLocationService.createSensorLocation(sensorLocation);
        SensorLocationDto createdSensorLocationDto = sensorLocationDtoMapper.sensorLocationToSensorLocationDto(createdSensorLocation);
        return new ResponseEntity<>(createdSensorLocationDto, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<SensorLocationDto>> getAllSensorLocations(Integer page, Integer size, String sort, Pageable pageable) {
        Page<SensorLocation> paginatedSensorLocations = sensorLocationService.getAllSensorLocations(pageable);
        List<SensorLocationDto> dtoList = paginatedSensorLocations.getContent().stream().map(sensorLocationDtoMapper::sensorLocationToSensorLocationDto).toList();
        return returnPaginatedResponse(paginatedSensorLocations, dtoList);
    }

    @Override
    public ResponseEntity<List<ApiViewDto>> getAllSensorLocationViews() {
        ApiViewDto simpleView = new ApiViewDto();
        simpleView.setView("simple");
        simpleView.setUri("/sensor_locations/_views/simple");
        simpleView.setDescription("Gets all SensorLocations in an array. This is a simplified version of the `/sensor_locations` GET endpoint for all Non-ADMIN roles. The result is a `SensorLocationSimpleViewDto` format, which is basically a stripped down `SensorLocationDto` without certain fields.");
        return ResponseEntity.ok(List.of(simpleView));
    }

    @Override
    public ResponseEntity<List<SensorLocationSimpleViewDto>> getAllSensorLocationsSimple() {
        List<SensorLocation> sensorLocations = sensorLocationService.getAllSensorLocations();
        List<SensorLocationSimpleViewDto> sensorLocationSimpleViewDtoList = sensorLocations.stream().map(sensorLocationDtoMapper::sensorLocationToSimpleViewDto).toList();
        return ResponseEntity.ok(sensorLocationSimpleViewDtoList);
    }

    @Override
    public ResponseEntity<SensorLocationDto> getSensorLocation(Long sensorLocationId) {
        SensorLocation sensorLocation = sensorLocationService.getSensorLocation(sensorLocationId);
        SensorLocationDto sensorLocationDto = sensorLocationDtoMapper.sensorLocationToSensorLocationDto(sensorLocation);
        return ResponseEntity.ok(sensorLocationDto);
    }

    @Override
    public ResponseEntity<SensorLocationDto> editSensorLocation(Long sensorLocationId, SensorLocationDto sensorLocationDto) {
        SensorLocation sensorLocation = sensorLocationService.getSensorLocation(sensorLocationId);
        sensorLocation = sensorLocationDtoMapper.updateSensorLocationWithSensorLocationDto(sensorLocationDto, sensorLocation);
        SensorLocation savedSensorLocation = sensorLocationService.save(sensorLocation);
        SensorLocationDto savedSensorLocationDto = sensorLocationDtoMapper.sensorLocationToSensorLocationDto(savedSensorLocation);
        return ResponseEntity.ok(savedSensorLocationDto);
    }

    @Override
    public ResponseEntity<Void> deleteSensorLocation(Long sensorLocationId) {
        sensorLocationService.delete(sensorLocationId);
        return ResponseEntity.ok().build();
    }

}
