package org.kinsecta.webapp.api.service;

import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.model.entities.InsectSpecies;
import org.kinsecta.webapp.api.model.repositories.InsectSpeciesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;


@Service
public class InsectSpeciesService {

    private static final Logger logger = LoggerFactory.getLogger(InsectSpeciesService.class);

    private final InsectSpeciesRepository insectSpeciesRepository;


    public InsectSpeciesService(InsectSpeciesRepository insectSpeciesRepository) {
        this.insectSpeciesRepository = insectSpeciesRepository;
    }


    @Transactional
    public InsectSpecies save(InsectSpecies insectSpecies) {
        return insectSpeciesRepository.save(insectSpecies);
    }

    @Transactional(readOnly = true)
    public InsectSpecies getInsectSpecies(Long insectSpeciesId) {
        return insectSpeciesRepository.findById(insectSpeciesId).orElseThrow(() -> new NotFoundException(String.format("Could not find InsectSpecies with id %d", insectSpeciesId)));
    }

    @Transactional(readOnly = true)
    public InsectSpecies getInsectSpecies(String insectSpeciesName) {
        return insectSpeciesRepository.findByName(insectSpeciesName).orElseThrow(() -> new NotFoundException(String.format("Could not find InsectSpecies with name '%s' ", insectSpeciesName)));
    }

    @Transactional(readOnly = true)
    public InsectSpecies getInsectSpeciesByGbifId(Long gbifId) {
        return insectSpeciesRepository.findByGbifId(gbifId).orElseThrow(() -> new NotFoundException(String.format("Could not find InsectSpecies with GBIF id %d ", gbifId)));
    }

    @Transactional(readOnly = true)
    public List<InsectSpecies> getAllInsectSpecies() {
        return insectSpeciesRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<InsectSpecies> getAllInsectSpeciesForSearchQuery(String query) {
        String queryDecoded;
        try {
            queryDecoded = URLDecoder.decode(query, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
        return insectSpeciesRepository.findAllByNameContains(queryDecoded.toLowerCase());
    }

    @Transactional
    public void delete(Long insectSpeciesId) {
        insectSpeciesRepository.deleteById(insectSpeciesId);
        logger.info("Successfully deleted InsectSpecies with id {}", insectSpeciesId);
    }

}
