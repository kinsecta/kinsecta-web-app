package org.kinsecta.webapp.api.model.mapping;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(value = "classifications")
public abstract class KinsectaMeasurementWingbeatMixin {

}
