package org.kinsecta.webapp.api.security;

import org.kinsecta.webapp.api.model.entities.LoginAttempt;
import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.model.entities.UserRole;
import org.kinsecta.webapp.api.model.entities.UserStatus;
import org.kinsecta.webapp.api.service.LoginAttemptService;
import org.kinsecta.webapp.api.service.UserService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;


/**
 * This implementation of the {@link UserDetails} allows setting a project {@link User} entity
 * so that we can access certain properties like the loginAttempts etc. in an easy and convenient
 * way and are able to use these in the {@link UserDetails} interface methods.
 */
@Component
public class UserDetailsImpl implements UserDetails {

    private final LoginAttemptService loginAttemptService;
    private User user;


    public UserDetailsImpl(LoginAttemptService loginAttemptService) {
        this.loginAttemptService = loginAttemptService;
    }


    /**
     * A setter for the application's {@link User} object
     *
     * @param user a {@link User} object
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * A getter for the application's {@link User} object
     */
    protected User getUser() {
        return this.user;
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    /**
     * A list of {@link GrantedAuthority}'s for this user.
     * <p>
     * In our implementation this is currently only the {@link UserRole} enum value.
     *
     * @return a Collection of {@link GrantedAuthority}'s
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // Add UserRole as GrantedAuthority
        SimpleGrantedAuthority userRoleAuthority = new SimpleGrantedAuthority(user.getRole().toString());
        return List.of(userRoleAuthority);
    }

    /**
     * Returns <code>true</code> if account is not expired.
     * <p>
     * In our implementation this is always <code>true</code> because we don't support this feature at the moment.
     *
     * @return boolean value
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * Returns <code>true</code> if account is not locked, <code>false</code> otherwise.
     * <p>
     * In our implementation we use the {@link LoginAttemptService} to retrieve the lock status for this account.
     *
     * @return boolean value
     */
    @Override
    public boolean isAccountNonLocked() {
        LoginAttempt loginAttempt = user.getLoginAttempt();
        if (loginAttempt == null) {
            // account is not locked if the @OneToOne JPA relation did not load any LoginAttempt object for the user (a.k.a. is null)
            return true;
        }
        return !loginAttemptService.isLocked(loginAttempt);
    }

    /**
     * Returns <code>true</code> if credentials are not expired.
     * <p>
     * In our implementation this is always <code>true</code> because we don't support this feature at the moment.
     *
     * @return boolean value
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * Returns <code>true</code> if user is enabled.
     * <p>
     * In our implementation we use the {@link UserService#isActiveUser(User)} static method (which leverages the
     * {@link UserStatus} enum and the <code>deleted</code> date field) to check the enabled status.
     *
     * @return boolean value
     */
    @Override
    public boolean isEnabled() {
        return UserService.isActiveUser(user);
    }


    @Override
    public String toString() {
        return "UserDetailsImpl{" +
            "user=" + user +
            '}';
    }

}
