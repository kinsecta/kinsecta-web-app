package org.kinsecta.webapp.api.scheduling;

import org.kinsecta.webapp.api.mail.ExportMailService;
import org.kinsecta.webapp.api.model.entities.Export;
import org.kinsecta.webapp.api.service.DeletionService;
import org.kinsecta.webapp.api.service.ExportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.TimeUnit;


@Service
public class DeleteExportJob {

    private static final Logger logger = LoggerFactory.getLogger(DeleteExportJob.class);

    private final ExportService exportService;
    private final DeletionService deletionService;
    private final ExportMailService exportMailService;


    public DeleteExportJob(ExportService exportService, DeletionService deletionService, ExportMailService exportMailService) {
        this.exportService = exportService;
        this.deletionService = deletionService;
        this.exportMailService = exportMailService;
    }


    @Scheduled(fixedDelayString = "${app.export.delete.interval}", timeUnit = TimeUnit.MINUTES)
    public void deleteExpiredExports() {
        List<Export> expiredExports = exportService.getAllExpiredExports();
        for (Export export : expiredExports) {
            try {
                deletionService.deleteExport(export.getId());
            } catch (Exception e) {
                logger.error("Error while trying to delete an expired Export via Spring Scheduler", e);
                exportMailService.sendExportDeletionFailedMessageToAdmin(export, e.getMessage());
            }
        }
    }

}
