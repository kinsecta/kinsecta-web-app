package org.kinsecta.webapp.api.model.entities;

public enum DatasetAutoTag {
    WINGBEAT("Wingbeat"),
    VIDEO("Video"),
    IMAGES("Fotos"),
    MISSING_MAIN_CLASSIFICATION("Haupt-Klassifikation fehlt");

    private final String tagName;

    DatasetAutoTag(String datasetTagName) {
        this.tagName = datasetTagName;
    }

    public String getTagName() {
        return tagName;
    }
}
