package org.kinsecta.webapp.api.controller;

import org.apache.commons.lang3.StringUtils;
import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.exception.unchecked.StorageRuntimeException;
import org.kinsecta.webapp.api.exception.unchecked.WrongArgumentException;
import org.kinsecta.webapp.api.model.entities.*;
import org.kinsecta.webapp.api.model.mapping.*;
import org.kinsecta.webapp.api.s3.StorageException;
import org.kinsecta.webapp.api.service.ClassificationService;
import org.kinsecta.webapp.api.service.DatasetService;
import org.kinsecta.webapp.api.service.DeletionService;
import org.kinsecta.webapp.api.util.ValidationTools;
import org.kinsecta.webapp.api.v1.DatasetsApi;
import org.kinsecta.webapp.api.v1.model.*;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Optional;
import java.util.Set;


@Controller
public class DatasetController extends BaseController implements DatasetsApi {

    private final DatasetService datasetService;
    private final DeletionService deletionService;
    private final DatasetDtoMapper datasetDtoMapper;
    private final S3ObjectDtoMapper s3ObjectDtoMapper;
    private final DatasetImageDtoMapper datasetImageDtoMapper;
    private final DatasetTagDtoMapper datasetTagDtoMapper;
    private final MeasurementWingbeatDtoMapper measurementWingbeatDtoMapper;
    private final ClassificationService classificationService;
    private final ClassificationDtoMapper classificationDtoMapper;


    public DatasetController(DatasetService datasetService, DeletionService deletionService, DatasetDtoMapper datasetDtoMapper, S3ObjectDtoMapper s3ObjectDtoMapper, DatasetImageDtoMapper datasetImageDtoMapper, DatasetTagDtoMapper datasetTagDtoMapper, MeasurementWingbeatDtoMapper measurementWingbeatDtoMapper, ClassificationService classificationService, ClassificationDtoMapper classificationDtoMapper) {
        this.datasetService = datasetService;
        this.deletionService = deletionService;
        this.datasetDtoMapper = datasetDtoMapper;
        this.s3ObjectDtoMapper = s3ObjectDtoMapper;
        this.datasetImageDtoMapper = datasetImageDtoMapper;
        this.datasetTagDtoMapper = datasetTagDtoMapper;
        this.measurementWingbeatDtoMapper = measurementWingbeatDtoMapper;
        this.classificationService = classificationService;
        this.classificationDtoMapper = classificationDtoMapper;
    }


    // --- Datasets ----------------------------------------------------------------------------------------------------

    @Override
    public ResponseEntity<List<DatasetDto>> getAllDatasets(Integer page, Integer size, String sort, DatasetFilterDto filterDto, Pageable pageable) {
        Page<Dataset> paginatedDatasetList = datasetService.getDatasetsByFilterDto(filterDto, pageable);
        List<DatasetDto> dtoList = paginatedDatasetList.stream().map(datasetDtoMapper::datasetToDatasetDto).toList();
        return ResponseEntity.ok(dtoList);
    }

    // --- Dataset Views -----------------------------------------------------------------------------------------------

    @Override
    public ResponseEntity<List<ApiViewDto>> getAllDatasetViews() {
        ApiViewDto tableView = new ApiViewDto();
        tableView.setView("table");
        tableView.setUri("/datasets/_views/table");
        tableView.setDescription("Gets all Datasets in an array, paginated by the `page` and `size` query parameters, optionally filtered by `TODO` and `TODO` query parameters. This is a simplified version of the `/datasets` endpoint for use in a data table. The result is a `DatasetTableDto` format, which is basically a stripped down `DatasetDto` without most of the measurement fields.");
        ApiViewDto idOnlyView = new ApiViewDto();
        idOnlyView.setView("id_only");
        idOnlyView.setUri("/datasets/_views/id_only");
        idOnlyView.setDescription("Gets an array of Dataset Ids for Datasets matching the provided query param filters.");
        return ResponseEntity.ok(List.of(tableView, idOnlyView));
    }

    @Override
    public ResponseEntity<List<DatasetTableViewDto>> getAllDatasetsTableView(Integer page, Integer size, String sort, DatasetFilterDto filterDto, Pageable pageable) {
        validateDatasetFilterDto(filterDto);
        Page<Dataset> paginatedDatasets = datasetService.getDatasetsByFilterDto(filterDto, pageable);
        List<DatasetTableViewDto> dtoList = paginatedDatasets.getContent().stream().map(datasetDtoMapper::datasetToDatasetTableViewDto).toList();
        return returnPaginatedResponse(paginatedDatasets, dtoList);
    }

    @Override
    public ResponseEntity<IdListDto> getAllDatasetsIdOnlyView(DatasetFilterDto filterDto) {
        validateDatasetFilterDto(filterDto);
        List<Long> datasetIds = datasetService.getAllDatasetIdsByFilters(filterDto);
        return ResponseEntity.status(HttpStatus.OK).body(new IdListDto().ids(datasetIds));
    }

    @Override
    public ResponseEntity<IdListDto> getAllDatasetsIdOnlyViewPaginated(Integer page, Integer size, String sort, DatasetFilterDto filterDto, Pageable pageable) {
        validateDatasetFilterDto(filterDto);
        List<Long> datasetIds = datasetService.getAllDatasetIdsByFiltersPageable(filterDto, pageable);
        return ResponseEntity.status(HttpStatus.OK).body(new IdListDto().ids(datasetIds));
    }


    // --- Single Dataset ----------------------------------------------------------------------------------------------

    @Override
    public ResponseEntity<DatasetDto> getDataset(Long datasetId) {
        Dataset dataset = datasetService.getDataset(datasetId);
        DatasetDto datasetDto = datasetDtoMapper.datasetToDatasetDto(dataset);
        return ResponseEntity.ok(datasetDto);
    }

    @Override
    public ResponseEntity<Void> deleteDataset(Long datasetId) {
        try {
            deletionService.deleteDataset(datasetId);
            return ResponseEntity.ok().build();
        } catch (StorageException e) {
            throw new StorageRuntimeException(e);
        }
    }


    // --- DatasetTags subresource of Dataset --------------------------------------------------------------------------

    @Override
    public ResponseEntity<DatasetTagDto> createDatasetTagForDataset(Long datasetId, DatasetTagDto datasetTagDto) {
        DatasetTag datasetTag = datasetTagDtoMapper.datasetTagDtoToDatasetTag(datasetTagDto);
        DatasetTag savedDatasetTag = datasetService.createDatasetTagForDataset(datasetId, datasetTag);
        DatasetTagDto savedDatasetTagDto = datasetTagDtoMapper.datasetTagToDatasetTagDto(savedDatasetTag);
        return new ResponseEntity<>(savedDatasetTagDto, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<DatasetTagDto>> getAllDatasetTagsForDataset(Long datasetId, Optional<Boolean> autoTag) {
        List<DatasetTag> datasetTags = datasetService.getAllDatasetTagsForDataset(datasetId, autoTag);
        List<DatasetTagDto> datasetTagDtoList = datasetTags.stream().map(datasetTagDtoMapper::datasetTagToDatasetTagDto).toList();
        return ResponseEntity.ok(datasetTagDtoList);
    }

    @Override
    public ResponseEntity<DatasetTagDto> getDatasetTagForDataset(Long datasetId, Long datasetTagId) {
        DatasetTag datasetTag = datasetService.getDatasetTagForDataset(datasetId, datasetTagId);
        DatasetTagDto datasetTagDto = datasetTagDtoMapper.datasetTagToDatasetTagDto(datasetTag);
        return ResponseEntity.ok(datasetTagDto);
    }

    @Override
    public ResponseEntity<DatasetTagDto> linkDatasetTagToDataset(Long datasetId, Long datasetTagId) {
        DatasetTag datasetTag = datasetService.linkDatasetTagToDataset(datasetId, datasetTagId);
        DatasetTagDto datasetTagDto = datasetTagDtoMapper.datasetTagToDatasetTagDto(datasetTag);
        return ResponseEntity.ok(datasetTagDto);
    }

    @Override
    public ResponseEntity<Void> unlinkDatasetTagFromDataset(Long datasetId, Long datasetTagId) {
        datasetService.unlinkDatasetTagFromDataset(datasetId, datasetTagId);
        return ResponseEntity.ok().build();
    }


    // --- Classification subresource of Dataset -----------------------------------------------------------------------

    @Override
    public ResponseEntity<ClassificationDto> createClassificationForDataset(Long datasetId, ClassificationDto classificationDto) {
        Classification classification = classificationDtoMapper.classificationDtoToClassification(classificationDto);
        Classification createdClassification = datasetService.createClassificationForDataset(classification, datasetId);
        ClassificationDto createdClassificationDto = classificationDtoMapper.classificationToClassificationDto(createdClassification);
        return new ResponseEntity<>(createdClassificationDto, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<ClassificationDto>> getAllClassificationsForDataset(Long datasetId) {
        Set<Classification> classifications = datasetService.getAllClassificationsForDataset(datasetId);
        List<ClassificationDto> classificationDtos = classifications.stream().map(classificationDtoMapper::classificationToClassificationDto).toList();
        return ResponseEntity.ok(classificationDtos);
    }

    @Override
    public ResponseEntity<ClassificationDto> getClassificationForDataset(Long datasetId, Long classificationId) {
        Dataset dataset = datasetService.getDataset(datasetId);
        Classification classification = classificationService.getClassification(classificationId);
        if (!dataset.getId().equals(classification.getDataset().getId())) {
            throw new NotFoundException(String.format("Could not find Classification with id %d for Dataset with id %d", classificationId, datasetId));
        }
        ClassificationDto classificationDto = classificationDtoMapper.classificationToClassificationDto(classification);
        return ResponseEntity.ok(classificationDto);
    }

    @Override
    public ResponseEntity<ClassificationDto> linkClassificationToDataset(Long datasetId, Long classificationId, ClassificationDto classificationDto) {
        Dataset dataset = datasetService.getDataset(datasetId);
        Classification classification = classificationService.getClassification(classificationId);
        if ((classification.getDataset() != null && !classification.getDataset().getId().equals(datasetId)) || classification.getDatasetImage() != null || classification.getMeasurementWingbeat() != null) {
            throw new WrongArgumentException("A Classification which already belongs to another Dataset, DatasetImage or MeasurementWingbeat cannot be linked with this Dataset.");
        }
        if (classificationDto != null) {
            classification = classificationDtoMapper.updateClassificationWithClassificationDto(classificationDto, classification);
        }
        classification.setDataset(dataset);
        Classification updatedClassification = classificationService.save(classification);
        ClassificationDto updatedClassificationDto = classificationDtoMapper.classificationToClassificationDto(updatedClassification);
        return ResponseEntity.ok(updatedClassificationDto);
    }

    @Override
    public ResponseEntity<Void> unlinkClassificationFromDataset(Long datasetId, Long classificationId) {
        datasetService.unlinkClassificationFromDataset(datasetId, classificationId);
        return ResponseEntity.ok().build();
    }


    // --- SizeInsect subresource of Dataset ---------------------------------------------------------------------------

    @Override
    public ResponseEntity<Void> editSizeInsectOfDataset(Long datasetId, InsectSizeDto insectSizeDto) {
        Dataset dataset = datasetService.getDataset(datasetId);
        dataset.setMeasurementSizeInsectWidth(insectSizeDto.getWidth());
        dataset.setMeasurementSizeInsectLength(insectSizeDto.getLength());
        datasetService.save(dataset);
        return ResponseEntity.ok().build();
    }


    // --- VideoS3Object subresource of Dataset ------------------------------------------------------------------------

    @Override
    public ResponseEntity<S3ObjectDto> getVideoS3ObjectForDataset(Long datasetId) {
        S3Object videoS3Object = datasetService.getVideoS3ObjectForDataset(datasetId);
        S3ObjectDto videoS3ObjectDto = s3ObjectDtoMapper.s3ObjectToS3ObjectDto(videoS3Object);
        return ResponseEntity.ok(videoS3ObjectDto);
    }

    @Override
    public ResponseEntity<Void> deleteVideoS3ObjectFromDataset(Long datasetId) {
        // TODO implement this method
        return DatasetsApi.super.deleteVideoS3ObjectFromDataset(datasetId);
    }

    @Override
    public ResponseEntity<Resource> downloadVideoS3ObjectForDataset(Long datasetId) {
        return datasetService.downloadVideoS3ObjectForDataset(datasetId);
    }


    // --- DatasetImages subresource of Dataset ------------------------------------------------------------------------

    @Override
    public ResponseEntity<List<DatasetImageDto>> getAllDatasetImagesForDataset(Long datasetId) {
        List<DatasetImage> datasetImageList = datasetService.getAllDatasetImagesForDataset(datasetId);
        List<DatasetImageDto> datasetImageDtoList = datasetImageList.stream().map(datasetImageDtoMapper::datasetImageToDatasetImageDto).toList();
        return ResponseEntity.ok(datasetImageDtoList);
    }

    @Override
    public ResponseEntity<S3ObjectDto> getDatasetImageForDataset(Long datasetId, Long datasetImageId) {
        S3Object imageS3Object = datasetService.getDatasetImageForDataset(datasetImageId);
        S3ObjectDto imageS3ObjectDto = s3ObjectDtoMapper.s3ObjectToS3ObjectDto(imageS3Object);
        return ResponseEntity.ok(imageS3ObjectDto);
    }

    @Override
    public ResponseEntity<DatasetImageDto> linkDatasetImageToDataset(Long datasetId, Long datasetImageId) {
        // TODO implement this method
        return DatasetsApi.super.linkDatasetImageToDataset(datasetId, datasetImageId);
    }

    @Override
    public ResponseEntity<Void> deleteDatasetImageFromDataset(Long datasetId, Long datasetImageId) {
        // TODO implement this method
        return DatasetsApi.super.deleteDatasetImageFromDataset(datasetId, datasetImageId);
    }

    @Override
    public ResponseEntity<Resource> downloadDatasetImageForDataset(Long datasetId, Long datasetImageId) {
        return datasetService.downloadDatasetImageForDataset(datasetImageId);
    }


    // --- MeasurementWingbeat subresource of Dataset ------------------------------------------------------------------

    @Override
    public ResponseEntity<MeasurementWingbeatDto> getMeasurementWingbeatForDataset(Long datasetId) {
        Dataset dataset = datasetService.getDataset(datasetId);
        MeasurementWingbeat measurementWingbeat = dataset.getMeasurementWingbeat();
        if (measurementWingbeat == null) {
            throw new NotFoundException(String.format("No Wingbeat Measurement found for Dataset with id %d", datasetId));
        }
        MeasurementWingbeatDto wingbeatDto = measurementWingbeatDtoMapper.measurementWingbeatToMeasurementWingbeatDto(measurementWingbeat);
        return ResponseEntity.ok(wingbeatDto);
    }

    @Override
    public ResponseEntity<Void> deleteMeasurementWingbeatFromDataset(Long datasetId) {
        // TODO implement this method
        return DatasetsApi.super.deleteMeasurementWingbeatFromDataset(datasetId);
    }

    @Override
    public ResponseEntity<S3ObjectDto> getMeasurementWingbeatWavForDataset(Long datasetId) {
        S3Object wavS3Object = datasetService.getMeasurementWingbeatWavForDataset(datasetId);
        S3ObjectDto wavS3ObjectDto = s3ObjectDtoMapper.s3ObjectToS3ObjectDto(wavS3Object);
        return ResponseEntity.ok(wavS3ObjectDto);
    }

    @Override
    public ResponseEntity<Resource> downloadMeasurementWingbeatWavForDataset(Long datasetId) {
        return datasetService.downloadMeasurementWingbeatWavForDataset(datasetId);
    }

    @Override
    public ResponseEntity<S3ObjectDto> getMeasurementWingbeatImageForDataset(Long datasetId) {
        S3Object imageS3Object = datasetService.getMeasurementWingbeatImageForDataset(datasetId);
        S3ObjectDto imageS3ObjectDto = s3ObjectDtoMapper.s3ObjectToS3ObjectDto(imageS3Object);
        return ResponseEntity.ok(imageS3ObjectDto);
    }

    @Override
    public ResponseEntity<Resource> downloadMeasurementWingbeatImageForDataset(Long datasetId) {
        return datasetService.downloadMeasurementWingbeatImageForDataset(datasetId);
    }

    protected void validateDatasetFilterDto(DatasetFilterDto datasetFilterDTO) {
        if (datasetFilterDTO.getTaxonomyLevel() == null && StringUtils.isNotEmpty(datasetFilterDTO.getTaxonomyFilters())) {
            throw new WrongArgumentException("There are taxonomy filters present, but no taxonomy level has been chosen.");
        }
        if (StringUtils.isNotBlank(datasetFilterDTO.getSensorLocationFilters()) && !ValidationTools.isValidIdListString(datasetFilterDTO.getSensorLocationFilters())) {
            throw new WrongArgumentException("Sensor Location IDs are not passed as a semicolon separated list of positive integers.");
        }
        if (StringUtils.isNotBlank(datasetFilterDTO.getSensorFilters()) && !ValidationTools.isValidIdListString(datasetFilterDTO.getSensorFilters())) {
            throw new WrongArgumentException("Sensor IDs are not passed as a semicolon separated list of positive integers.");
        }
        if (datasetFilterDTO.getTagFilterList() != null && !datasetFilterDTO.getTagFilterList().isEmpty()) {
            for (String tagFilterString : datasetFilterDTO.getTagFilterList()) {
                if (StringUtils.isBlank(tagFilterString) || !ValidationTools.isValidNegativeIdListString(tagFilterString)) {
                    throw new WrongArgumentException("DatasetTag IDs are not passed as semicolon separated lists of positive or negative integers " +
                        "or there is an empty list among them");
                }
            }
        }
    }

}
