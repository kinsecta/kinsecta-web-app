package org.kinsecta.webapp.api.util;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


/**
 * This implementation of the Spring 'Converter' interface converts dates in String format to LocalDate
 * by specifying an explicit conversion strategy via a specific DateTimeFormatter.
 * Inspired by https://medium.com/swlh/spring-boot-and-the-localdate-request-parameter-2c9cdbb085bb
 */
@Component
public class LocalDateConverter implements Converter<String, LocalDate> {

    private static final Map<String, DateTimeFormatter> DATE_TIME_FORMATTERS = new HashMap<>();


    // Add all LocalDate patterns you wish to support in this application.
    // In the best case, the pattern should already exist as a predefined static DateTimeFormatter.
    static {
        DATE_TIME_FORMATTERS.put("yyyy-MM-dd", DateTimeFormatter.ISO_LOCAL_DATE);
    }


    @Override
    public LocalDate convert(String s) {
        for (Map.Entry<String, DateTimeFormatter> formatter : Collections.unmodifiableMap(DATE_TIME_FORMATTERS).entrySet()) {
            try {
                return LocalDate.parse(s, formatter.getValue());
            } catch (DateTimeParseException ex) {
                // deliberate empty block so that all parsers run
            }
        }

        throw new DateTimeException(String.format("unable to parse '%s' as LocalDate, supported formats are '%s'",
            s, String.join("', '", DATE_TIME_FORMATTERS.keySet())));
    }

}
