package org.kinsecta.webapp.api.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.kinsecta.webapp.api.model.entities.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;


/**
 * A Service to create and verify JWT tokens with the help of the auth0 JWT library.
 * <p>
 * The JWT is created with multiple Claims which allow for additional checks beside the expiration date,
 * e.g. to avoid that a user who got deactivated can continue working in his current session.
 * <p>
 * JWT's from this Service also render invalid when a user changes his security details, such as
 * password, username, email. So in these cases, the API application must make sure that endpoints
 * where a user can change those details update the 'security_details_modified' timestamp in the DB
 * and return a new JWT token. The Frontend application must handle updated JWT tokens for those
 * actions and use them further on in the session.
 *
 * @see <a href="https://github.com/auth0/java-jwt">https://github.com/auth0/java-jwt</a>
 */
@Service
public class JwtTokenService {

    private static final Logger log = LoggerFactory.getLogger(JwtTokenService.class);

    private final UserDetailsServiceImpl userDetailsService;

    private Algorithm algorithm;

    // Mind the space at the end! It's important!
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String CLAIM_ROLE = "role";
    public static final String CLAIM_SECURITY_DETAILS_MODIFIED = "security_details_modified";

    @Value("${app.authentication.jwt-token.secret}")
    private String jwtSecret;

    @Value("${app.authentication.jwt-token.expiration}")
    private long jwtExpirationMs;


    public JwtTokenService(UserDetailsServiceImpl userDetailsService) {
        this.userDetailsService = userDetailsService;
    }


    @PostConstruct
    private void init() {
        algorithm = Algorithm.HMAC512(jwtSecret);
    }

    /**
     * Generate JWT from a {@link UserDetailsImpl} object.
     * <p>
     * See {@link #createJwtToken(User)} for full method docs and security details notes.
     *
     * @param userDetails the {@link UserDetailsImpl} object used for creating the JWT
     *
     * @return a JWT token as String
     *
     * @throws JWTCreationException for invalid Signing configuration or if Claims couldn't be converted.
     */
    public String createJwtToken(UserDetailsImpl userDetails) throws JWTCreationException {
        User user = userDetails.getUser();
        return createJwtToken(user);
    }

    /**
     * Generate JWT from a {@link User} object with the following props:
     * <ul>
     * <li>the username
     * <li>a claim with the UserRole enum value
     * <li>a claim with a timestamp when the user's security details last changed
     * </ul>
     * The claims must be validated when verifying the JWT upon an API request!
     * <p>
     * The 'security details modified' timestamp is added so that any existing JWT is automatically
     * considered as invalid when user security details (such as password, username, email) are changed.
     * We could have also used an hash over those fields as the security details claim, but an extra field
     * in the DB offers the possibility to revoke all active, non-expired JWT tokens of a user
     * by just updating the 'security_details_modified' timestamp.
     *
     * @param user the {@link User} object used for creating the JWT
     *
     * @return a JWT token as String
     *
     * @throws JWTCreationException for invalid Signing configuration or if Claims couldn't be converted.
     */
    public String createJwtToken(User user) throws JWTCreationException {
        String token = JWT.create()
            .withSubject(user.getUsername())
            .withClaim(CLAIM_ROLE, user.getRole().toString())
            .withClaim(CLAIM_SECURITY_DETAILS_MODIFIED, user.getSecurityDetailsModified().toString())
            .withExpiresAt(new Date(System.currentTimeMillis() + jwtExpirationMs))
            .sign(algorithm);
        log.debug("JWT token created for username '{}'", user.getUsername());
        return token;
    }

    /**
     * Generate JWT from a {@link UserDetailsImpl} object and immediately add it to the
     * given {@link HttpServletResponse}.
     * <p>
     * See {@link #createJwtToken(User)} for full docs and security details notes about JWT creation.
     *
     * @param userDetails the {@link UserDetailsImpl} object used for creating the JWT
     *
     * @return a JWT token as String
     *
     * @throws JWTCreationException for invalid Signing configuration or if Claims couldn't be converted.
     */
    public String createJwtTokenAndAddToResponse(UserDetailsImpl userDetails, HttpServletResponse response) throws JWTCreationException {
        User user = userDetails.getUser();
        return createJwtTokenAndAddToResponse(user, response);
    }

    /**
     * Generate JWT from a {@link User} object and immediately add it to the
     * given {@link HttpServletResponse}.
     * <p>
     * See {@link #createJwtToken(User)} for full docs and security details notes about JWT creation.
     *
     * @param user the {@link User} object used for creating the JWT
     *
     * @return a JWT token as String
     *
     * @throws JWTCreationException for invalid Signing configuration or if Claims couldn't be converted.
     */
    public String createJwtTokenAndAddToResponse(User user, HttpServletResponse response) throws JWTCreationException {
        String token = createJwtToken(user);
        addJwtToResponseHeaders(token, response);
        return token;
    }

    /**
     * Creates a new HttpServletResponse Header named 'Authorization' and adds the given
     * JWT token prefixed with the 'Bearer ' prefix.
     *
     * @param jwtToken the JWT token to add
     * @param response the {@link HttpServletResponse} to add the header to
     */
    public void addJwtToResponseHeaders(String jwtToken, HttpServletResponse response) {
        log.trace("JWT token added to HttpServletResponse");
        response.addHeader(HttpHeaders.AUTHORIZATION, TOKEN_PREFIX + jwtToken);
    }


    /**
     * Generate JWT from a {@link User} object and return it as a proper 'Authorization' header object.
     * <p>
     * See {@link #createJwtToken(User)} for full docs and security details notes about JWT creation.
     *
     * @param user the {@link User} object used for creating the JWT
     *
     * @return an {@link HttpHeaders} object containing the 'Authorization' header with the generated JWT
     *
     * @throws JWTCreationException for invalid Signing configuration or if Claims couldn't be converted.
     */
    public HttpHeaders createJwtTokenAndReturnInAuthorizationHeader(User user) throws JWTCreationException {
        String token = createJwtToken(user);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, TOKEN_PREFIX + token);
        log.trace("JWT token added to HttpHeaders");
        return headers;
    }

    /**
     * Decode and verify an encoded JWT - without verifying the claims!
     * <p>
     * This method may be used if UserDetails aren't present yet.
     *
     * @param encodedJwt the encoded JWT as String
     *
     * @return a {@link DecodedJWT}
     */
    public DecodedJWT verifyJwt(String encodedJwt) {
        DecodedJWT decodedJWT = JWT.require(algorithm)
            .build()
            .verify(encodedJwt);
        log.trace("JWT token verified without claims and decoded successfully");
        return decodedJWT;
    }

    /**
     * Verifies an encoded JWT - with claims!
     * <p>
     * This method may be used if UserDetails are present yet
     *
     * @param encodedJwt  the encoded JWT as String
     * @param userDetails the {@link UserDetailsImpl} object to check the claims against
     *
     * @return a {@link DecodedJWT}
     */
    public DecodedJWT verifyJwtWithClaims(String encodedJwt, UserDetailsImpl userDetails) {
        return getVerifierWithClaims(userDetails).verify(encodedJwt);
    }

    /**
     * Verifies a decoded JWT - with claims!
     * <p>
     * This method may be used if UserDetails are present yet and the JWT has already been
     * decoded in a previous step.
     *
     * @param decodedJwt  the encoded JWT as String
     * @param userDetails the {@link UserDetailsImpl} object to check the claims against
     *
     * @return a {@link DecodedJWT}
     */
    public DecodedJWT verifyDecodedJwtWithClaims(DecodedJWT decodedJwt, UserDetailsImpl userDetails) {
        DecodedJWT decodedJWT = getVerifierWithClaims(userDetails).verify(decodedJwt);
        log.trace("JWT token verified with claims and decoded successfully");
        return decodedJWT;

    }

    /**
     * A helper method to construct a JWTVerifier with all claims checks as defined in
     * the {@link #createJwtToken(User)} method when creating a JWT.
     *
     * @param userDetails the {@link UserDetailsImpl} object to check the claims against
     *
     * @return a reusable {@link JWTVerifier} instance
     */
    private JWTVerifier getVerifierWithClaims(UserDetailsImpl userDetails) {
        User user = userDetails.getUser();
        return JWT.require(algorithm)
            .withSubject(userDetails.getUsername())
            .withClaim(CLAIM_ROLE, user.getRole().toString())
            .withClaim(CLAIM_SECURITY_DETAILS_MODIFIED, user.getSecurityDetailsModified().toString())
            .build();
    }

    /**
     * Verifies an encoded JWT and returns an 'authentication' object.
     *
     * @param encodedJwt the encoded JWT to verify
     *
     * @return a {@link UsernamePasswordAuthenticationToken} object
     */
    public UsernamePasswordAuthenticationToken verifyJwtAndGetAuthentication(String encodedJwt) {
        // Verify JWT - without verifying the claims as no UserDetails are present yet.
        // All exceptions are caught by our ExceptionHandlerFilter class.
        DecodedJWT decodedJwt = verifyJwt(encodedJwt);

        // Extract username from JWT and return null if no username is present
        String username = decodedJwt.getSubject();
        log.debug("Username decoded from JWT: {}", username);

        // Get User by username via UserDetailsServiceImpl (includes a null check on 'username')
        UserDetailsImpl userDetails = userDetailsService.loadUserByUsername(username);
        log.debug("UserDetails loaded after decrypting the JWT: {}", userDetails);

        // Check if User is inactive or locked, then return null.
        // This check handles two edge cases where a user issues an API request with a valid non-expired JWT
        // 1. but was set "inactive" by an Admin user in the meantime.
        if (!userDetails.isEnabled()) {
            SecurityContextHolder.clearContext();
            log.info("Request with valid JWT was blocked because the UserStatus of the decoded user '{}' is no longer 'ACTIVE'.", username);
            throw new DisabledException(JwtAuthenticationFailureHandler.MESSAGE_USER_DISABLED);
        }
        // 2. but tried to sign-in too many times in another session so their account was locked.
        if (!userDetails.isAccountNonLocked()) {
            SecurityContextHolder.clearContext();
            log.info("Request with valid JWT was blocked because the decoded user '{}' was found to be blocked for too many failed login attempts.", username);
            throw new LockedException(JwtAuthenticationFailureHandler.MESSAGE_USER_LOCKED);
        }

        // Verify the JWT a second time, this time also verifying all claims (UserRole, modified date, etc.).
        // All exceptions are caught by our ExceptionHandlerFilter class.
        verifyDecodedJwtWithClaims(decodedJwt, userDetails);

        // Create and return a new UsernamePasswordAuthenticationToken object which basically
        // sets the 'authenticated' flag in Spring Security context to 'true' so that this API request is authorized.
        return new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
    }

}
