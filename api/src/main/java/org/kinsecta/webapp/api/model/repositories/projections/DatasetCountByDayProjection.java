package org.kinsecta.webapp.api.model.repositories.projections;

import java.util.Date;


public interface DatasetCountByDayProjection {

    Date getDate();

    int getCount();

}
