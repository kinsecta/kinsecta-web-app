package org.kinsecta.webapp.api.cache;

import org.kinsecta.webapp.api.model.entities.TransferStatus;

import java.util.HashMap;
import java.util.Map;


public class UploadStatusCache {

    private Map<Long, TransferStatus> statusMap;


    public UploadStatusCache() {
        init();
    }

    private void init() {
        this.statusMap = new HashMap<>();
    }


    public TransferStatus getUploadStatus(Long uploadId) {
        return statusMap.get(uploadId);
    }

    public void addUploadStatus(Long uploadId, TransferStatus transferStatus) {
        if (statusMap == null) {
            init();
        }
        this.statusMap.put(uploadId, transferStatus);
    }

    public void removeUploadStatus(Long uploadId) {
        if (statusMap != null) {
            this.statusMap.remove(uploadId);
        }
    }

}
