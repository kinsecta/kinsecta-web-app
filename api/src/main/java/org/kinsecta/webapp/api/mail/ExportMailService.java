package org.kinsecta.webapp.api.mail;

import org.kinsecta.webapp.api.model.entities.Export;
import org.kinsecta.webapp.api.model.entities.ExportFile;
import org.kinsecta.webapp.api.model.entities.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


@Service
public class ExportMailService extends MailService {

    private static final Logger logger = LoggerFactory.getLogger(ExportMailService.class);
    private static final String DOWNLOAD_LINK_FORMAT = "%s/exports/%s/export_files/%s/download?token=%s";
    private static final DateTimeFormatter humanReadableDateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");

    public ExportMailService(MessageSource messageSource, JavaMailSenderFacade javaMailSenderFacade) {
        super(messageSource, javaMailSenderFacade);
    }


    public void sendExportFinishedMessage(Export export, boolean multipleLinks, int maxDatasets) {
        User user = export.getUser();
        String mailTo = user.getEmail();
        String mailSubject = translate("export.export-finished.subject", export.getId());
        List<String> downloadLinks = new ArrayList<>();
        for (ExportFile exportFile : export.getExportFiles()) {
            downloadLinks.add(String.format(DOWNLOAD_LINK_FORMAT, apiBaseUrl, export.getId(), exportFile.getId(), export.getDownloadToken()));
        }

        String mailText = translate(
            multipleLinks ? "export.export-finished-multiple.body" : "export.export-finished.body",
            user.getFullName(),
            export.getCreated().format(humanReadableDateTimeFormatter),
            export.getDatasetIds().split(";").length,
            String.join("\n", downloadLinks),
            maxDatasets);


        logger.info("About to send 'export finished' email for user '{}'", user.getUsername());
        javaMailSenderFacade.sendMailAsync(mailTo, mailSubject, mailText);
    }

    public void sendExportFailedMessage(Export export, List<String> exceptionMessages) {
        User user = export.getUser();
        String mailTo = user.getEmail();
        String mailSubject = translate("export.export-failed.subject", export.getId());
        String mailText = translate("export.export-failed.body",
            user.getFullName(),
            export.getCreated().format(humanReadableDateTimeFormatter),
            export.getDatasetIds().split(";").length,
            String.join("\n\n", exceptionMessages)
        );

        logger.info("About to send 'export failed' email for user '{}'", user.getUsername());
        javaMailSenderFacade.sendMailAsync(mailTo, mailSubject, mailText);
    }

    public void sendExportDeletionFailedMessageToAdmin(Export export, String exceptionMessage) {
        String mailTo = sysadminEmailAddress;
        String mailSubject = translate("export.export-deletion-failed.subject");
        String mailText = translate("export.export-deletion-failed.body",
            export.getId(),
            exceptionMessage);

        logger.info("About to send 'export deletion failed' email to sysadmin contact");
        javaMailSenderFacade.sendMailAsync(mailTo, mailSubject, mailText);
    }

}

