package org.kinsecta.webapp.api.exception.checked;

/**
 * This custom exception indicates, that a duplicate resource is detected and should not be uploaded
 */
public class DuplicateHashException extends Exception {

    private final Long id;
    private final String hash;


    public DuplicateHashException(Long id, String hash) {
        super();
        this.id = id;
        this.hash = hash;
    }


    public Long getId() {
        return id;
    }

    public String getHash() {
        return hash;
    }

}
