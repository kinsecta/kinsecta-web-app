package org.kinsecta.webapp.api.model.entities;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "export")
public class Export {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private User user;

    @Column(name = "dataset_ids", columnDefinition = "TEXT")
    private String datasetIds;

    @Column(name = "status", nullable = false, length = 15)
    @Enumerated(EnumType.STRING)
    private TransferStatus status;

    @Column(name = "finished", columnDefinition = "DATETIME(3)")
    private LocalDateTime finished;

    @Column(name = "download_token")
    private String downloadToken;

    @Column(name = "expires", columnDefinition = "DATETIME(3)")
    private LocalDateTime expires;

    // #79 use HashSet with FetchType.EAGER otherwise there's a chance Hibernate returns duplicates
    @OneToMany(mappedBy = "export", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<ExportFile> exportFiles = new HashSet<>();

    @Column(name = "created", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW()")
    @Generated(GenerationTime.INSERT)
    @ReadOnlyProperty
    private LocalDateTime created;

    @Column(name = "modified", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW() ON UPDATE NOW()")
    @Generated(GenerationTime.ALWAYS)
    @ReadOnlyProperty
    private LocalDateTime modified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDatasetIds() {
        return datasetIds;
    }

    public void setDatasetIds(String datasetIds) {
        this.datasetIds = datasetIds;
    }

    public TransferStatus getStatus() {
        return status;
    }

    public void setStatus(TransferStatus status) {
        this.status = status;
    }

    public LocalDateTime getFinished() {
        return finished;
    }

    public void setFinished(LocalDateTime finished) {
        this.finished = finished;
    }

    public String getDownloadToken() {
        return downloadToken;
    }

    public void setDownloadToken(String downloadToken) {
        this.downloadToken = downloadToken;
    }

    public LocalDateTime getExpires() {
        return expires;
    }

    public void setExpires(LocalDateTime expires) {
        this.expires = expires;
    }

    public Set<ExportFile> getExportFiles() {
        return exportFiles;
    }

    public void setExportFiles(Set<ExportFile> exportFiles) {
        this.exportFiles = exportFiles;
    }

    public void addExportFile(ExportFile exportFile) {
        if (this.exportFiles == null) {
            this.exportFiles = new HashSet<>();
        }
        this.exportFiles.add(exportFile);
        exportFile.setExport(this);
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }


    @Override
    public String toString() {
        return "Export{" +
            "id=" + id +
            ", user=" + user.getId() +
            ", status=" + status +
            ", finished=" + (finished != null ? finished : "null") +
            ", downloadToken='" + (downloadToken != null ? downloadToken : "null") + '\'' +
            ", expires=" + (expires != null ? expires : "null") +
            ", created=" + created +
            ", modified=" + modified +
            '}';
    }

}
