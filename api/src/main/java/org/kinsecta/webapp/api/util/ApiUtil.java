package org.kinsecta.webapp.api.util;

import net.lingala.zip4j.ZipFile;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class ApiUtil {

    private ApiUtil() {
        throw new IllegalStateException("Utility class");
    }


    public static void saveResourceStreamToFile(Resource inputResource, File destinationFile) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(destinationFile)) {
            IOUtils.copy(inputResource.getInputStream(), fos);
        }
    }

    public static void unzipFileIntoDirectory(File zipFile, File directory) throws IOException {
        try (ZipFile zip = new ZipFile(zipFile)) {
            zip.extractAll(directory.toString());
        }
    }

}
