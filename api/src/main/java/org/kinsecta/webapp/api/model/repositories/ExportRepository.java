package org.kinsecta.webapp.api.model.repositories;

import org.kinsecta.webapp.api.model.entities.Export;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;


public interface ExportRepository extends JpaRepository<Export, Long> {

    List<Export> findAllByExpiresIsBefore(LocalDateTime localDateTime);

}
