package org.kinsecta.webapp.api.model.entities;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;

    @Column(name = "username", unique = true, nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "email", unique = true, nullable = false)
    private String email;

    @Column(name = "full_name", nullable = false)
    private String fullName;

    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private UserRole role;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private UserStatus status;

    @Column(name = "deleted", columnDefinition = "DATETIME(3)")
    @ReadOnlyProperty
    private LocalDateTime deleted;

    @Column(name = "security_details_modified", nullable = false, columnDefinition = "DATETIME(3) DEFAULT NOW()")
    @ReadOnlyProperty
    private LocalDateTime securityDetailsModified;

    @Column(name = "created", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW()")
    @Generated(GenerationTime.INSERT)
    @ReadOnlyProperty
    private LocalDateTime created;

    @Column(name = "modified", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW() ON UPDATE NOW()")
    @Generated(GenerationTime.ALWAYS)
    @ReadOnlyProperty
    private LocalDateTime modified;

    // Reverse Mappings

    @OneToOne(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private LoginAttempt loginAttempt;

    @OneToOne(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private ResetToken resetToken;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    @LazyCollection(LazyCollectionOption.EXTRA)
    private List<Upload> uploads = new ArrayList<>();

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Dataset> datasets = new ArrayList<>();

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Export> exports = new ArrayList<>();

    @OneToMany(mappedBy = "operator", fetch = FetchType.LAZY)
    private List<Sensor> sensors = new ArrayList<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public LocalDateTime getDeleted() {
        return deleted;
    }

    public void setDeleted(LocalDateTime deleted) {
        this.deleted = deleted;
    }

    public LocalDateTime getSecurityDetailsModified() {
        return securityDetailsModified;
    }

    public void setSecurityDetailsModified(LocalDateTime securityDetailsModified) {
        // Change LocalDateTime precision to 3 instead of 6 so that it matches datetime(3) in the schema
        // but the real reason is, that it is comparable with the value stored in the JWT.
        this.securityDetailsModified = securityDetailsModified.truncatedTo(ChronoUnit.MILLIS);
    }

    public void updateSecurityDetailsModified() {
        setSecurityDetailsModified(LocalDateTime.now());
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    // Getters, Setters, Adders for reverse Mappings

    public LoginAttempt getLoginAttempt() {
        return loginAttempt;
    }

    public void setLoginAttempt(LoginAttempt loginAttempt) {
        this.loginAttempt = loginAttempt;
    }

    public ResetToken getResetToken() {
        return resetToken;
    }

    public void setResetToken(ResetToken resetToken) {
        this.resetToken = resetToken;
    }

    public List<Upload> getUploads() {
        return uploads;
    }

    public void setUploads(List<Upload> uploads) {
        this.uploads = uploads;
    }

    public void addUpload(Upload upload) {
        if (this.uploads == null) {
            this.uploads = new ArrayList<>();
        }
        this.uploads.add(upload);
        upload.setUser(this);
    }

    public void removeUpload(Upload upload) {
        if (this.uploads != null) {
            this.uploads.remove(upload);
            upload.setUser(null);
        }
    }

    public List<Dataset> getDatasets() {
        return datasets;
    }

    public void setDatasets(List<Dataset> datasets) {
        this.datasets = datasets;
    }

    public void addDataset(Dataset dataset) {
        if (this.datasets == null) {
            this.datasets = new ArrayList<>();
        }
        this.datasets.add(dataset);
        dataset.setUser(this);
    }

    public void removeDataset(Dataset dataset) {
        if (this.datasets != null) {
            this.datasets.remove(dataset);
            dataset.setUser(null);
        }
    }

    public List<Export> getExports() {
        return exports;
    }

    public void setExports(List<Export> export) {
        this.exports = export;
    }

    public void addExport(Export export) {
        if (this.exports == null) {
            this.exports = new ArrayList<>();
        }
        this.exports.add(export);
        export.setUser(this);
    }

    public void removeExport(Export export) {
        if (this.exports != null) {
            this.exports.remove(export);
            export.setUser(null);
        }
    }

    public List<Sensor> getSensors() {
        return sensors;
    }

    public void setSensors(List<Sensor> sensors) {
        this.sensors = sensors;
    }

    public void addSensor(Sensor sensor) {
        if (this.sensors == null) {
            this.sensors = new ArrayList<>();
        }
        this.sensors.add(sensor);
        sensor.setOperator(this);
    }

    public void removeSensor(Sensor sensor) {
        if (this.sensors != null) {
            this.sensors.remove(sensor);
            sensor.setOperator(null);
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return Objects.equals(id, user.id) &&
            Objects.equals(username, user.username) &&
            Objects.equals(email, user.email) &&
            role == user.role &&
            Objects.equals(fullName, user.fullName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, email, role, fullName, status);
    }

    @Override
    public String toString() {
        return "User{" +
            "id=" + id +
            ", username='" + username + '\'' +
            ", password='[PROTECTED]'" +
            ", email='" + email + '\'' +
            ", fullName='" + fullName + '\'' +
            ", role=" + role +
            ", status=" + status +
            ", deleted=" + (deleted != null ? deleted : "null") +
            ", created=" + created +
            ", modified=" + modified +
            '}';
    }

}

