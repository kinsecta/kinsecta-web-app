package org.kinsecta.webapp.api.model.repositories;

import org.kinsecta.webapp.api.model.entities.LoginAttempt;
import org.kinsecta.webapp.api.model.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface LoginAttemptRepository extends JpaRepository<LoginAttempt, Long> {

    Optional<LoginAttempt> findByUser(User user);

}
