package org.kinsecta.webapp.api.security;

import com.auth0.jwt.exceptions.InvalidClaimException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.kinsecta.webapp.api.exception.GlobalExceptionHandler;
import org.kinsecta.webapp.api.v1.model.JsonError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * This filter, which needs to be placed very early in the <a href="https://docs.spring.io/spring-security/reference/servlet/architecture.html#servlet-security-filters">Spring Security Filter Chain</a>,
 * catches specific (Runtime)Exceptions thrown by any of the following filters in the chain.
 * <p>
 * IMPORTANT NOTE: This handler must not catch a generic {@link RuntimeException} but only specific
 * RuntimeException instances because otherwise our global {@link GlobalExceptionHandler} cannot
 * catch RuntimeException's from Controller methods anymore!
 * <p>
 * Tha handled exceptions build the error response as a {@link JsonError} object which provides
 * the following attributes:
 * <ul>
 * <li>timestamp - The time that the errors were extracted</li>
 * <li>status - The status code; may be overridden for specific Exceptions</li>
 * <li>error - The error reason</li>
 * <li>exception - The class name of the root exception</li>
 * <li>message - The exception message; may be overridden for specific Exceptions</li>
 * <li>path - The URL path when the exception was raised</li>
 * </ul>
 * Optionally provides the following attributes when the message is overridden:
 * <ul>
 * <li>original_message - The original exception message</li>
 * </ul>
 * Optionally provides the following nested attributes when an Exception "cause" is present:
 * <ul>
 * <li>cause.exception - The class name of the root exception</li>
 * <li>cause.message - The exception message; may be overridden for specific Exceptions</li>
 * </ul>
 * <p>
 * A few words by the author why this class is implemented as a {@link OncePerRequestFilter}:
 * <p>
 * A {@link org.springframework.web.bind.annotation.ControllerAdvice} annotated class
 * with dedicated {@link org.springframework.web.bind.annotation.ExceptionHandler}
 * won't work here as the Spring Security Filter Chain is too low-level:
 * "As specified by the java servlet specification Filters execute always before
 * a Servlet is invoked [and] a @ControllerAdvice is only useful for controller
 * which are executed inside the DispatcherServlet."
 * <p>
 * A class extending {@link org.springframework.boot.web.servlet.error.DefaultErrorAttributes}
 * and overriding {@link org.springframework.boot.web.servlet.error.DefaultErrorAttributes#getErrorAttributes(WebRequest, ErrorAttributeOptions)} Attributes(HttpServletRequest, ErrorAttributeOptions)}
 * won't work here either as we can only override the response error attributes, but cannot
 * change the response status code.
 * <p>
 * Therefore, we implement this ExceptionHandlerFilter as a {@link OncePerRequestFilter}
 * and register it very early in the <a href="https://docs.spring.io/spring-security/reference/servlet/architecture.html#servlet-security-filters">Spring Security Filter Chain</a>,
 * at least before <code>UsernamePasswordAuthenticationFilter.class</code> or other auth filters.
 * <p>
 * Based on an idea by <a href="https://stackoverflow.com/a/34633687/1128689">https://stackoverflow.com/a/34633687/1128689</a>.
 *
 * @author Tobias Fischer
 */
@Component
public class ExceptionHandlerFilter extends OncePerRequestFilter {

    private static final Logger log = LoggerFactory.getLogger(ExceptionHandlerFilter.class);

    public static final String RESPONSE_ENCODING = "UTF-8";
    public static final String RESPONSE_CONTENT_TYPE = "application/json";

    private final ObjectMapper objectMapper;


    public ExceptionHandlerFilter(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            // Just call the next filter(s) inside a try to be able to catch all RuntimeExceptions thrown in those filters
            filterChain.doFilter(request, response);

        } catch (SignatureVerificationException e) {
            handleSignatureVerificationException(e, request, response);

        } catch (TokenExpiredException e) {
            handleTokenExpiredException(e, request, response);

        } catch (InvalidClaimException e) {
            handleInvalidClaimException(e, request, response);

        } catch (DisabledException e) {
            handleDisabledException(e, request, response);

        } catch (LockedException e) {
            handleLockedException(e, request, response);
        }
    }

    protected void setServletResponse(HttpServletResponse response, JsonError jsonError, Exception exception) throws IOException {
        // Log the exception
        log.error("[return HTTP status {}] {}", jsonError.getStatus(), jsonError.getMessage(), exception);

        // Update HttpServletResponse to return the overridden status and our custom error attributes as JSON response.
        response.setStatus(jsonError.getStatus());
        response.setCharacterEncoding(RESPONSE_ENCODING);
        response.setContentType(RESPONSE_CONTENT_TYPE);
        response.getWriter().write(convertObjectToJson(jsonError));
        response.getWriter().flush();
        // Don't close the Writer stream because we didn't open it in the first place and don't know what Spring
        // is doing with the response object after this filter.
    }

    protected String convertObjectToJson(Object object) throws JsonProcessingException {
        if (object == null) {
            return null;
        }
        return objectMapper.writeValueAsString(object);
    }


    /**
     * Set HTTP Status to 401 Unauthorized for a JWT {@link SignatureVerificationException}.
     *
     * @param exception a {@link SignatureVerificationException} instance
     * @param request   the current {@link HttpServletRequest}
     */
    protected void handleSignatureVerificationException(SignatureVerificationException exception, HttpServletRequest request, HttpServletResponse response) throws IOException {
        JsonError jsonError = GlobalExceptionHandler.buildJsonError(exception, "JWT secret token has changed. Re-login required!", HttpStatus.UNAUTHORIZED, request.getServletPath());
        setServletResponse(response, jsonError, exception);
    }

    /**
     * Set HTTP Status to 401 Unauthorized for a JWT {@link TokenExpiredException}.
     *
     * @param exception a {@link TokenExpiredException} instance
     * @param request   the current {@link HttpServletRequest}
     */
    protected void handleTokenExpiredException(TokenExpiredException exception, HttpServletRequest request, HttpServletResponse response) throws IOException {
        JsonError jsonError = GlobalExceptionHandler.buildJsonError(exception, exception.getMessage(), HttpStatus.UNAUTHORIZED, request.getServletPath());
        setServletResponse(response, jsonError, exception);
    }

    /**
     * Set HTTP Status to 401 Unauthorized for a JWT {@link InvalidClaimException}.
     *
     * @param exception an {@link InvalidClaimException} instance
     * @param request   the current {@link HttpServletRequest}
     */
    protected void handleInvalidClaimException(InvalidClaimException exception, HttpServletRequest request, HttpServletResponse response) throws IOException {
        JsonError jsonError = GlobalExceptionHandler.buildJsonError(exception, exception.getMessage(), HttpStatus.UNAUTHORIZED, request.getServletPath());
        setServletResponse(response, jsonError, exception);
    }

    /**
     * Set HTTP Status to 401 Unauthorized for a {@link DisabledException} thrown during JWT authorization.
     *
     * @param exception a {@link DisabledException} instance
     * @param request   the current {@link HttpServletRequest}
     */
    protected void handleDisabledException(DisabledException exception, HttpServletRequest request, HttpServletResponse response) throws IOException {
        JsonError jsonError = GlobalExceptionHandler.buildJsonError(exception, exception.getMessage(), HttpStatus.UNAUTHORIZED, request.getServletPath());
        setServletResponse(response, jsonError, exception);
    }

    /**
     * Set HTTP Status to 401 Unauthorized for a {@link LockedException} thrown during JWT authorization.
     *
     * @param exception a {@link LockedException} instance
     * @param request   the current {@link HttpServletRequest}
     */
    protected void handleLockedException(LockedException exception, HttpServletRequest request, HttpServletResponse response) throws IOException {
        JsonError jsonError = GlobalExceptionHandler.buildJsonError(exception, exception.getMessage(), HttpStatus.UNAUTHORIZED, request.getServletPath());
        setServletResponse(response, jsonError, exception);
    }

}
