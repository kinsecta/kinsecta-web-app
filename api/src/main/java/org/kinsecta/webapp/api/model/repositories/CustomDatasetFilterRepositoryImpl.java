package org.kinsecta.webapp.api.model.repositories;

import org.apache.commons.lang3.StringUtils;
import org.kinsecta.webapp.api.model.entities.Classification;
import org.kinsecta.webapp.api.model.entities.Dataset;
import org.kinsecta.webapp.api.v1.model.DatasetFilterDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.query.QueryUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CustomDatasetFilterRepositoryImpl implements CustomDatasetFilterRepository {

    @PersistenceContext
    private EntityManager em;

    public List<Long> findAllDatasetIdsByFilters(DatasetFilterDto filterDto) {
        @SuppressWarnings("DuplicatedCode")
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);

        Root<Dataset> datasetRoot = cq.from(Dataset.class);
        cq.where(createPredicate(cb, cq, datasetRoot, filterDto));
        cq.select(datasetRoot.get("id"));

        return em.createQuery(cq).getResultList();
    }

    public List<Long> findAllDatasetIdsByFilters(DatasetFilterDto filterDto, Pageable pageable) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);

        Root<Dataset> datasetRoot = cq.from(Dataset.class);
        cq.where(createPredicate(cb, cq, datasetRoot, filterDto));
        cq.select(datasetRoot.get("id"));

        if (pageable != null && pageable.getSort().isSorted()) {
            cq.orderBy(QueryUtils.toOrders(pageable.getSort(), datasetRoot, cb));
        }

        return em.createQuery(cq).setFirstResult((int) pageable.getOffset()).setMaxResults(pageable.getPageSize()).getResultList();
    }

    public Long countAllResults(DatasetFilterDto filterDto) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Dataset> datasetRoot = cq.from(Dataset.class);
        cq.where(createPredicate(cb, cq, datasetRoot, filterDto));
        cq.select(cb.count(datasetRoot));

        return em.createQuery(cq).getSingleResult();
    }

    public Predicate createPredicate(CriteriaBuilder cb, CriteriaQuery<?> cq, Root<Dataset> datasetRoot, DatasetFilterDto filterDto) {
        List<Predicate> predicates = new ArrayList<>();

        if (StringUtils.isNotBlank(filterDto.getSensorFilters())) {
            Path<Long> sensorId = datasetRoot
                .join("sensor")
                .get("id");
            List<Long> ids = Arrays.stream(filterDto.getSensorFilters()
                    .split(";"))
                .map(Long::parseLong)
                .toList();
            predicates.add(sensorId.in(ids));
            //use else, because sensorLocationFilters are unnecessary when sensor filters are set
        } else if (StringUtils.isNotBlank(filterDto.getSensorLocationFilters())) {
            Path<Long> sensorLocationId = datasetRoot
                .join("sensor")
                .join("sensorLocation")
                .get("id");
            List<Long> ids = Arrays.stream(filterDto.getSensorLocationFilters()
                    .split(";"))
                .map(Long::parseLong)
                .toList();
            predicates.add(sensorLocationId.in(ids));
        }

        if (filterDto.getSensorIsPublic() != null) {
            Path<Boolean> sensorIsPublic = datasetRoot
                .join("sensor")
                .get("showPublic");
            predicates.add(cb.equal(sensorIsPublic, filterDto.getSensorIsPublic()));
        }

        if (filterDto.getMeasurementStartDate() != null) {
            predicates.add(cb.greaterThanOrEqualTo(datasetRoot.get("measurementDatetime"), filterDto.getMeasurementStartDate().atTime(LocalTime.MIN)));
        }

        if (filterDto.getMeasurementEndDate() != null) {
            predicates.add(cb.lessThanOrEqualTo(datasetRoot.get("measurementDatetime"), filterDto.getMeasurementEndDate().atTime(LocalTime.MAX)));
        }

        if (filterDto.getModifiedStartDate() != null) {
            predicates.add(cb.greaterThanOrEqualTo(datasetRoot.get("computedModified"), filterDto.getModifiedStartDate().atTime(LocalTime.MIN)));
        }

        if (filterDto.getModifiedEndDate() != null) {
            predicates.add(cb.lessThanOrEqualTo(datasetRoot.get("computedModified"), filterDto.getModifiedEndDate().atTime(LocalTime.MAX)));
        }

        if (StringUtils.isNotBlank(filterDto.getTaxonomyFilters())) {
            Join<Dataset, Classification> classificationJoin = datasetRoot.join("classifications");
            String insectField = switch (filterDto.getTaxonomyLevel()) {
                case ORDER -> "insectOrder";
                case FAMILY -> "insectFamily";
                case GENUS -> "insectGenus";
                case SPECIES -> "insectSpecies";
            };
            Path<?> insectId = classificationJoin.get(insectField).get("id");
            List<Long> ids = Arrays.stream(filterDto.getTaxonomyFilters()
                    .split(";"))
                .map(Long::parseLong)
                .toList();
            predicates.add(insectId.in(ids));
        }

        if (filterDto.getTagFilterList() != null && !filterDto.getTagFilterList().isEmpty()) {
            for (String tagIdString : filterDto.getTagFilterList()) {
                List<Predicate> orPredicates = new ArrayList<>();
                List<Long> idsToHave = new ArrayList<>();
                List<Long> idsNotToHave = new ArrayList<>();
                Arrays.stream(tagIdString.split(";"))
                    .forEach(idString -> {
                        if (idString.startsWith("-")) {
                            idsNotToHave.add(
                                Long.parseLong(idString.substring(1)));
                        } else {
                            idsToHave.add(Long.parseLong(idString));
                        }
                    });
                if (!idsNotToHave.isEmpty()) {
                    Subquery<Long> datasetIdsNotToHaveQuery = cq.subquery(Long.class);
                    Root<Dataset> subqueryDatasetRoot = datasetIdsNotToHaveQuery.from(Dataset.class);
                    datasetIdsNotToHaveQuery.select(subqueryDatasetRoot.get("id"));
                    datasetIdsNotToHaveQuery.groupBy(subqueryDatasetRoot.get("id"));
                    List<Predicate> havingPredicates = new ArrayList<>();
                    Path<Long> datasetTagIdPath = subqueryDatasetRoot.join("datasetTags")
                        .get("id");
                    for (Long idNotToHave : idsNotToHave) {
                        havingPredicates.add(
                            cb.equal(
                                cb.sum(
                                    cb.<Long>selectCase()
                                        .when(cb.equal(datasetTagIdPath, idNotToHave), 1L)
                                        .otherwise(0L)), 1L));
                    }
                    datasetIdsNotToHaveQuery.having(havingPredicates.toArray(new Predicate[0]));
                    Predicate notContainsPredicate =
                        cb.not(datasetRoot.get("id").in(datasetIdsNotToHaveQuery));
                    orPredicates.add(notContainsPredicate);
                }

                if (!idsToHave.isEmpty()) {
                    // this needs to be a left join, otherwise the other predicate (not contains tag)
                    // will not select datasets that have no tags at all, which it should
                    Predicate containsPredicate =
                        datasetRoot.join("datasetTags", JoinType.LEFT)
                            .get("id")
                            .in(idsToHave);
                    orPredicates.add(containsPredicate);
                }
                predicates.add(cb.or(orPredicates.toArray(new Predicate[0])));
            }
        }

        if (filterDto.getUserId() != null) {
            predicates.add(cb.equal(datasetRoot.get("user"), filterDto.getUserId()));
        }

        cq.distinct(true);

        return cb.and(predicates.toArray(new Predicate[0]));
    }

}
