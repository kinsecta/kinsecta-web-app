package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.model.entities.MeasurementWingbeat;
import org.kinsecta.webapp.api.model.repositories.MeasurementWingbeatRepository;
import org.kinsecta.webapp.api.v1.model.IdObject;
import org.kinsecta.webapp.api.v1.model.MeasurementWingbeatDto;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;


@Mapper(
    componentModel = "spring",
    uses = {
        DtoMapperConfig.class
    },
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public abstract class MeasurementWingbeatDtoMapper {

    @Autowired
    private MeasurementWingbeatRepository measurementWingbeatRepository;


    public abstract MeasurementWingbeat measurementWingbeatDtoToMeasurementWingbeat(MeasurementWingbeatDto measurementWingbeatDto);

    public abstract MeasurementWingbeatDto measurementWingbeatToMeasurementWingbeatDto(MeasurementWingbeat measurementWingbeat);

    public MeasurementWingbeat idObjectToMeasurementWingbeat(IdObject idObject) {
        if (idObject != null) {
            return measurementWingbeatRepository.getById(idObject.getId());
        }
        return null;
    }

    public IdObject measurementWingbeatToIdObject(MeasurementWingbeat measurementWingbeat) {
        if (measurementWingbeat == null) {
            return null;
        }
        return new IdObject().id(measurementWingbeat.getId());
    }

}
