package org.kinsecta.webapp.api.service;

import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.exception.unchecked.WrongArgumentException;
import org.kinsecta.webapp.api.model.entities.*;
import org.kinsecta.webapp.api.model.repositories.DatasetRepository;
import org.kinsecta.webapp.api.model.repositories.projections.DatasetCountByDayProjection;
import org.kinsecta.webapp.api.model.repositories.projections.DatasetCountByMonthProjection;
import org.kinsecta.webapp.api.model.repositories.projections.DatasetCountByWeekProjection;
import org.kinsecta.webapp.api.model.repositories.projections.DatasetCountByYearProjection;
import org.kinsecta.webapp.api.util.PageableHelper;
import org.kinsecta.webapp.api.v1.model.DatasetFilterDto;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;


@Service
public class DatasetService {

    private final DatasetRepository datasetRepository;
    private final DatasetTagService datasetTagService;
    private final S3ObjectService s3ObjectService;
    private final DatasetImageService datasetImageService;
    private final ClassificationService classificationService;


    public DatasetService(DatasetRepository datasetRepository, DatasetTagService datasetTagService, S3ObjectService s3ObjectService, DatasetImageService datasetImageService, ClassificationService classificationService) {
        this.datasetRepository = datasetRepository;
        this.datasetTagService = datasetTagService;
        this.s3ObjectService = s3ObjectService;
        this.datasetImageService = datasetImageService;
        this.classificationService = classificationService;
    }


    // --- Dataset & Datasets ------------------------------------------------------------------------------------------

    public Dataset save(Dataset dataset) {
        datasetTagService.updateAutoTags(dataset);
        // update this, because it would not happen automatically if only dataset tags were added
        dataset.updateComputedModified();
        return datasetRepository.save(dataset);
    }

    public Dataset getDataset(Long id) {
        return datasetRepository.findById(id).orElseThrow(() -> new NotFoundException(String.format("Could not find Dataset with id %d", id)));
    }

    public Optional<Dataset> getOptionalDataset(Long id) {
        return datasetRepository.findById(id);
    }

    public List<Dataset> getAllDatasets(Pageable pageable) {
        Page<Dataset> pagedResult = datasetRepository.findAll(pageable);
        if (pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<>();
        }
    }

    public List<Dataset> getAllDatasets() {
        return datasetRepository.findAll();
    }

    public List<Dataset> getDatasetsForUpload(Upload upload) {
        return datasetRepository.findDatasetsByUpload(upload);
    }

    public Page<Dataset> getDatasetsByFilterDto(DatasetFilterDto filterDto, Pageable pageable) {
        List<Long> datasetIds = datasetRepository.findAllDatasetIdsByFilters(filterDto, pageable);
        Long count = datasetRepository.countAllResults(filterDto);
        List<Dataset> datasets = datasetRepository.findDatasetsByIdIn(datasetIds, pageable.getSort());
        return new PageImpl<>(datasets, pageable, count);
    }

    public List<Long> getAllDatasetIdsByFilters(DatasetFilterDto filterDto) {
        return datasetRepository.findAllDatasetIdsByFilters(filterDto);
    }

    public List<Long> getAllDatasetIdsByFiltersPageable(DatasetFilterDto filterDto, Pageable pageable) {
        return datasetRepository.findAllDatasetIdsByFilters(filterDto, pageable);
    }


    // --- Dataset Statistics ------------------------------------------------------------------------------------------

    public Long countDatasetsByYear(Integer year) {
        return datasetRepository.countDatasetsByYear(year);
    }

    public List<DatasetCountByYearProjection> countDatasetsByYear() {
        return datasetRepository.getCountsForYear();
    }

    public List<DatasetCountByMonthProjection> countDatasetsByMonth() {
        return datasetRepository.getCountsForMonth();
    }

    public List<DatasetCountByWeekProjection> countDatasetsByWeek() {
        return datasetRepository.getCountsForWeek();
    }

    public List<DatasetCountByDayProjection> countDatasetsByDay() {
        return datasetRepository.getCountsForDay();
    }


    // --- Dataset Views -----------------------------------------------------------------------------------------------

    public Page<Dataset> getAllDatasetsForTableView(Pageable pageable) {
        Page<Dataset> pagedDatasets = datasetRepository.findAll(pageable);
        return PageableHelper.returnPageOrEmpty(pagedDatasets);
    }

    public Optional<Long> findDatasetIdByMd5Hash(String md5Hash) {
        return datasetRepository.findDatasetIdByMd5Hash(md5Hash);
    }

    // --- DatasetTags subresource of Dataset --------------------------------------------------------------------------

    @Transactional
    public DatasetTag createDatasetTagForDataset(Long datasetId, DatasetTag datasetTag) {
        DatasetTag createdDatasetTag = datasetTagService.createDatasetTag(datasetTag);
        Dataset dataset = getDataset(datasetId);
        dataset.addDatasetTag(createdDatasetTag);
        save(dataset);
        return createdDatasetTag;
    }

    @Transactional(readOnly = true)
    public List<DatasetTag> getAllDatasetTagsForDataset(Long datasetId, Optional<Boolean> autoTag) {
        Dataset dataset = getDataset(datasetId);
        List<DatasetTag> datasetTags = new ArrayList<>(dataset.getDatasetTags());
        if (autoTag.isPresent()) {
            Boolean autoTagFilter = autoTag.get();
            datasetTags = datasetTags.stream().filter(datasetTag -> datasetTag.getAutoTag().equals(autoTagFilter)).toList();
        }
        return datasetTags;
    }

    @Transactional(readOnly = true)
    public DatasetTag getDatasetTagForDataset(Long datasetId, Long datasetTagId) {
        Dataset dataset = getDataset(datasetId);
        Optional<DatasetTag> datasetTagOpt = dataset.getDatasetTags().stream().filter(datasetTag -> datasetTag.getId().equals(datasetTagId)).findFirst();
        if (datasetTagOpt.isEmpty()) {
            throw new NotFoundException(String.format("Could not find DatasetTag with id %d for Dataset with id %d", datasetTagId, datasetId));
        }
        return datasetTagOpt.get();
    }

    @Transactional
    public DatasetTag linkDatasetTagToDataset(Long datasetId, Long datasetTagId) {
        Dataset dataset = getDataset(datasetId);
        DatasetTag datasetTag = datasetTagService.getDatasetTag(datasetTagId);
        dataset.addDatasetTag(datasetTag);
        save(dataset);
        return datasetTag;
    }

    @Transactional
    public void unlinkDatasetTagFromDataset(Long datasetId, Long datasetTagId) {
        Dataset dataset = getDataset(datasetId);
        DatasetTag datasetTag = datasetTagService.getDatasetTag(datasetTagId);
        dataset.removeDatasetTag(datasetTag);
        save(dataset);
    }


    // --- DatasetImages subresource of Dataset ------------------------------------------------------------------------

    public List<DatasetImage> getAllDatasetImagesForDataset(Long datasetId) {
        Dataset dataset = getDataset(datasetId);
        return datasetImageService.getDatasetImagesForDataset(dataset);
    }

    public S3Object getDatasetImageForDataset(Long datasetImageId) {
        DatasetImage datasetImage = datasetImageService.getDatasetImage(datasetImageId);
        return datasetImage.getS3Object();
    }

    public ResponseEntity<Resource> downloadDatasetImageForDataset(Long datasetImageId) {
        S3Object imageS3Object = getDatasetImageForDataset(datasetImageId);
        return s3ObjectService.downloadInline(imageS3Object);
    }

    public ResponseEntity<Resource> downloadDatasetImageForDatasetPublic(Long datasetId, Long datasetImageId) {
        DatasetImage datasetImage = datasetImageService.getDatasetImage(datasetImageId);
        if (!datasetImage.getDataset().getId().equals(datasetId)) {
            throw new WrongArgumentException(String.format("DatasetImage with id %d does not belong to dataset with id %d", datasetImageId, datasetId));
        }
        S3Object imageS3Object = getDatasetImageForDataset(datasetImageId);
        return s3ObjectService.downloadInline(imageS3Object);
    }


    public S3Object getVideoS3ObjectForDataset(Long datasetId) {
        Dataset dataset = getDataset(datasetId);
        return dataset.getVideoS3Object();
    }


    // --- VideoS3Object subresource of Dataset ------------------------------------------------------------------------

    public ResponseEntity<Resource> downloadVideoS3ObjectForDataset(Long datasetId) {
        S3Object videoS3Object = getVideoS3ObjectForDataset(datasetId);
        return s3ObjectService.downloadInline(videoS3Object);
    }


    // --- MeasurementWingbeat subresource of Dataset ------------------------------------------------------------------

    public MeasurementWingbeat getMeasurementWingbeatForDataset(Long datasetId) {
        Dataset dataset = getDataset(datasetId);
        return dataset.getMeasurementWingbeat();
    }

    public S3Object getMeasurementWingbeatWavForDataset(Long datasetId) {
        MeasurementWingbeat measurementWingbeat = getMeasurementWingbeatForDataset(datasetId);
        S3Object wavS3Object = measurementWingbeat.getWavS3Object();
        if (wavS3Object == null) {
            throw new NotFoundException(String.format("Could not find MeasurementWingbeat wave S3Object for Dataset with id %d", datasetId));
        }
        return wavS3Object;
    }

    public ResponseEntity<Resource> downloadMeasurementWingbeatWavForDataset(Long uploadId) {
        S3Object s3Object = getMeasurementWingbeatWavForDataset(uploadId);
        return s3ObjectService.downloadInline(s3Object);
    }

    public S3Object getMeasurementWingbeatImageForDataset(Long datasetId) {
        MeasurementWingbeat measurementWingbeat = getMeasurementWingbeatForDataset(datasetId);
        S3Object imageS3Object = measurementWingbeat.getImageS3Object();
        if (imageS3Object == null) {
            throw new NotFoundException(String.format("Could not find MeasurementWingbeat image S3Object for Dataset with id %d", datasetId));
        }
        return imageS3Object;
    }

    public ResponseEntity<Resource> downloadMeasurementWingbeatImageForDataset(Long uploadId) {
        S3Object s3Object = getMeasurementWingbeatImageForDataset(uploadId);
        return s3ObjectService.downloadInline(s3Object);
    }


    // --- Classification subresource of Dataset -----------------------------------------------------------------------

    @Transactional(readOnly = true)
    public Set<Classification> getAllClassificationsForDataset(Long datasetId) {
        Dataset dataset = getDataset(datasetId);
        return dataset.getClassifications();
    }

    @Transactional
    public void unlinkClassificationFromDataset(Long datasetId, Long classificationId) {
        Dataset dataset = getDataset(datasetId);
        Classification classification = classificationService.getClassification(classificationId);
        dataset.removeClassification(classification);
        save(dataset);
    }

    @Transactional
    public Classification createClassificationForDataset(Classification classification, Long datasetId) {
        Dataset dataset = getDataset(datasetId);
        classification.setDataset(dataset);
        Classification savedClassification = classificationService.save(classification);

        dataset.addClassification(savedClassification);
        datasetTagService.updateAutoTags(dataset);
        save(dataset);

        return savedClassification;
    }


    // --- Helper Function ---------------------------------------------------------------------------------------------

    public Boolean isUserSameAsDatasetUser(Long userId, Long datasetId) {
        return datasetRepository.existsByIdAndUserId(datasetId, userId);
    }

}
