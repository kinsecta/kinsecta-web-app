package org.kinsecta.webapp.api.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * This Spring Security Filter adds JWT based authorization to the project.
 * <p>
 * It is used for checking the 'Authorization' header and validating the 'Bearer Token' (the JWT)
 * with every API request.
 * <p>
 * Note: Mind the difference between <i>authentication</i> and <i>authorization</i>!
 */
public class JwtAuthorizationFilter extends OncePerRequestFilter {

    private static final Logger log = LoggerFactory.getLogger(JwtAuthorizationFilter.class);

    private final JwtTokenService jwtTokenService;


    public JwtAuthorizationFilter(JwtTokenService jwtTokenService) {
        this.jwtTokenService = jwtTokenService;
    }


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        // Skip Authorization for public endpoints
        if (request.getRequestURI().startsWith("/public/")) {
            log.debug("Skipped JwtAuthorizationFilter for public resource path '{}'", request.getRequestURI());
            chain.doFilter(request, response);
            return;
        }

        // Get resource mapping from request (https://stackoverflow.com/a/60528458/1128689)
        log.debug("API request to authorized resource path '{}'", request.getRequestURI());

        // Skip this JwtAuthorizationFilter if the request headers doesn't contain
        // an 'Authorization' header which starts with 'Bearer '.
        String authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (authorizationHeader == null || !authorizationHeader.startsWith(JwtTokenService.TOKEN_PREFIX)) {
            log.debug("Skipped JwtAuthorizationFilter because '{}' header is not present in request or it is present but doesn't start with '{}'", HttpHeaders.AUTHORIZATION, JwtTokenService.TOKEN_PREFIX);
            chain.doFilter(request, response);
            return;
        }

        // Get UsernamePasswordAuthenticationToken object by reading and validating the JWT
        UsernamePasswordAuthenticationToken authentication = getAuthenticationFromBearerToken(authorizationHeader);

        // Set the authentication in the Spring Security context - even if it is null
        // (which means that the next Spring Security Chain Filter tries to authorize the request)
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // Call the next filter in the Spring Security filter chain
        chain.doFilter(request, response);
    }

    protected UsernamePasswordAuthenticationToken getAuthenticationFromBearerToken(String authorizationHeader) {
        // Read encoded JWT from bearer token
        String encodedJwt = authorizationHeader.replace(JwtTokenService.TOKEN_PREFIX, "");

        UsernamePasswordAuthenticationToken authentication = jwtTokenService.verifyJwtAndGetAuthentication(encodedJwt);
        log.debug("API request successfully authorized via JWT in 'Authorization' header");

        return authentication;
    }

}
