package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.exception.checked.MappingException;
import org.kinsecta.webapp.api.model.entities.*;
import org.kinsecta.webapp.api.v1.model.*;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


/**
 * A Mapstruct Mapper class to configure all mappers to map from entity classes to <code>KinsectaMeasurement</code>.
 */
@Mapper(
    componentModel = "spring",
    uses = {
        DtoMapperConfig.class
    },
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public abstract class KinsectaMeasurementExportMapper {

    @Mapping(target = "userId", source = "user.id")
    @Mapping(target = "sensorId", source = "sensor.id")
    @Mapping(target = "dateTime", source = "measurementDatetime")
    @Mapping(target = "sizeInsect.length", source = "measurementSizeInsectLength")
    @Mapping(target = "sizeInsect.width", source = "measurementSizeInsectWidth")
    @Mapping(target = "temperature.value", source = "measurementTemperature")
    @Mapping(target = "airPressure.value", source = "measurementAirPressure")
    @Mapping(target = "humidity.value", source = "measurementHumidity")
    @Mapping(target = "luminosity.value", source = "measurementLuminosity")
    @Mapping(target = "spectrum.channel415nm", source = "measurementSpectrumChannel415nm")
    @Mapping(target = "spectrum.channel445nm", source = "measurementSpectrumChannel445nm")
    @Mapping(target = "spectrum.channel480nm", source = "measurementSpectrumChannel480nm")
    @Mapping(target = "spectrum.channel515nm", source = "measurementSpectrumChannel515nm")
    @Mapping(target = "spectrum.channel555nm", source = "measurementSpectrumChannel555nm")
    @Mapping(target = "spectrum.channel590nm", source = "measurementSpectrumChannel590nm")
    @Mapping(target = "spectrum.channel630nm", source = "measurementSpectrumChannel630nm")
    @Mapping(target = "spectrum.channel680nm", source = "measurementSpectrumChannel680nm")
    @Mapping(target = "spectrum.nearIr", source = "measurementSpectrumNearIr")
    @Mapping(target = "spectrum.clearLight", source = "measurementSpectrumClearLight")
    @Mapping(target = "particulateMatter.particles03um", source = "measurementParticulateMatterParticles03um")
    @Mapping(target = "particulateMatter.particles05um", source = "measurementParticulateMatterParticles05um")
    @Mapping(target = "particulateMatter.particles10um", source = "measurementParticulateMatterParticles10um")
    @Mapping(target = "particulateMatter.particles25um", source = "measurementParticulateMatterParticles25um")
    @Mapping(target = "particulateMatter.particles50um", source = "measurementParticulateMatterParticles50um")
    @Mapping(target = "particulateMatter.particles100um", source = "measurementParticulateMatterParticles100um")
    @Mapping(target = "particulateMatter.pm1", source = "measurementParticulateMatterPm1")
    @Mapping(target = "particulateMatter.pm2p5", source = "measurementParticulateMatterPm2p5")
    @Mapping(target = "particulateMatter.pm10", source = "measurementParticulateMatterPm10")
    @Mapping(target = "rainfall.quantity", source = "measurementRainfall")
    @Mapping(target = "windSensor.speed", source = "measurementWindSensorSpeed")
    @Mapping(target = "windSensor.direction", source = "measurementWindSensorDirection")
    @Mapping(target = "wingbeat", source = "measurementWingbeat")
    @Mapping(target = "mainClassifications", source = "classifications")
    @Mapping(target = "sensorLocation", source = "sensor.sensorLocation")
    @Mapping(target = "video", source = "videoS3Object")
    @Mapping(target = "images", source = "datasetImages")
    @Mapping(target = "tags", source = "datasetTags")
    public abstract KinsectaMeasurement datasetToKinsectaMeasurement(Dataset dataset) throws MappingException;

    @Mapping(target = "order", source = "insectOrder")
    @Mapping(target = "family", source = "insectFamily")
    @Mapping(target = "genus", source = "insectGenus")
    @Mapping(target = "species", source = "insectSpecies")
    public abstract KinsectaMeasurementClassification classificationToKinsectaMeasurementClassification(Classification classification) throws MappingException;

    @Mapping(target = "filename", source = "wavS3Object.filename")
    @Mapping(target = "pngFilename", source = "imageS3Object.filename")
    public abstract KinsectaMeasurementWingbeat measurementWingbeatToKinsectaMeasurementWingbeat(MeasurementWingbeat measurementWingbeat);

    public abstract KinsectaMeasurementClassificationInsectOrder insectOrderToKinsectaMeasurementClassificationInsectOrder(InsectOrder insectOrder) throws MappingException;

    public abstract KinsectaMeasurementClassificationInsect insectFamilyToKinsectaMeasurementClassificationInsect(InsectFamily insectFamily) throws MappingException;

    public abstract KinsectaMeasurementClassificationInsect insectGenusToKinsectaMeasurementClassificationInsect(InsectGenus insectGenus) throws MappingException;

    public abstract KinsectaMeasurementClassificationInsect insectSpeciesToKinsectaMeasurementClassificationInsect(InsectSpecies insectSpecies) throws MappingException;

    public abstract KinsectaMeasurementSensorLocation sensorLocationToKinsectaMeasurementSensorLocation(SensorLocation sensorLocation);

    public abstract KinsectaMeasurementVideo s3ObjectToKinsectaMeasurementVideo(S3Object s3Object);

    @Mapping(target = "filename", source = "s3Object.filename")
    public abstract KinsectaMeasurementImage datasetImageToKinsectaMeasurementImage(DatasetImage datasetImage);

}
