package org.kinsecta.webapp.api;

import org.apache.commons.lang3.LocaleUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.test.context.ActiveProfiles;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


@SpringBootApplication
@ActiveProfiles("ext")
public class ApiApplication implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(ApiApplication.class);

    @Value("${app.timezone.default}")
    private String defaultTimezone;

    @Value("${app.locales.default}")
    private String defaultLocale;


    public static void main(String[] args) {
        SpringApplication.run(ApiApplication.class, "");
    }


    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        // this can be empty, because spring boot handles the web server startup
    }


    @PostConstruct
    public void init() {
        // set default Locale
        Locale.setDefault(LocaleUtils.toLocale(defaultLocale));
        logger.info("Read app default locale '{}' from properties and configured the system default locale as '{}'", defaultLocale, Locale.getDefault());

        // set default TimeZone
        TimeZone.setDefault(TimeZone.getTimeZone(defaultTimezone));
        logger.info("Read app default timezone '{}' from properties and configured the system default timezone as '{}, {}'", defaultTimezone, TimeZone.getDefault().getID(), TimeZone.getDefault().getDisplayName());
        logger.info("Current application time is: {}", new Date());
    }

}
