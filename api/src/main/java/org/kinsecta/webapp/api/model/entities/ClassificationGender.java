package org.kinsecta.webapp.api.model.entities;

public enum ClassificationGender {
    FEMALE,
    MALE,
    UNSPECIFIED
}
