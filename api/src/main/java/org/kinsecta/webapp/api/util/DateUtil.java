package org.kinsecta.webapp.api.util;

import java.time.*;
import java.util.Date;


public class DateUtil {

    private DateUtil() {
        throw new IllegalStateException("Utility class");
    }


    public static LocalDateTime parseDateOrDateTimeAsLocalDateTime(String dateOrDateTimeString, boolean atStartOfDay) {
        // trying to parse input as LocalDateTime first
        try {
            return new LocalDateTimeConverter().convert(dateOrDateTimeString);
        }
        // if that fails, trying to parse input as LocalDate and
        catch (DateTimeException e1) {
            try {
                LocalDate date = new LocalDateConverter().convert(dateOrDateTimeString);
                if (atStartOfDay) {
                    return date.atStartOfDay();
                } else {
                    return date.atTime(LocalTime.MAX);
                }
            } catch (DateTimeException e2) {
                throw new DateTimeException(String.format("unable to parse '%s' as either LocalDateTime or LocalDate", dateOrDateTimeString));
            }
        }
    }

    /**
     * https://www.baeldung.com/java-date-to-localdate-and-localdatetime
     *
     * @param date date to convert
     *
     * @return LocalDate of Date object
     */
    public static LocalDate convertToLocalDate(Date date) {
        return date.toInstant()
            .atZone(ZoneId.systemDefault())
            .toLocalDate();
    }

    /**
     * https://www.baeldung.com/java-date-to-localdate-and-localdatetime
     *
     * @param localDate LocalDate to convert
     *
     * @return Date of LocalDate object
     */
    public static Date convertToDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay()
            .atZone(ZoneId.systemDefault())
            .toInstant());
    }

}
