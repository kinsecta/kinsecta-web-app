package org.kinsecta.webapp.api.security;

public final class CustomHttpHeaders {

    public static final String X_XSRF_TOKEN = "x-xsrf-token";
    public static final String X_AUTH_TOKEN = "x-auth-token";
    public static final String X_REQUESTED_WITH = "x-requested-with";
    public static final String X_TOTAL_COUNT = "x-total-count";


    private CustomHttpHeaders() {}

}
