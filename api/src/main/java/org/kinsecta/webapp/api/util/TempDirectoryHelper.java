package org.kinsecta.webapp.api.util;


import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.UUID;


@Component
public class TempDirectoryHelper {

    private static final Logger logger = LoggerFactory.getLogger(TempDirectoryHelper.class);

    @Value("${app.temp-directory}")
    private String tempBaseDirString;

    private File tempBaseDir;


    @PostConstruct
    private void init() {
        tempBaseDir = Paths.get(tempBaseDirString).toFile();
    }


    public File getTempBaseDir() {
        if (tempBaseDir == null) {
            init();
        }
        return tempBaseDir;
    }

    public void createTempBaseDirectory() {
        if (!tempBaseDir.exists() && !tempBaseDir.mkdir()) {
            throw new IllegalStateException(String.format("Temp directory '%s' does not exist at '%s' and cannot be created!", tempBaseDirString, tempBaseDir.getAbsolutePath()));
        }
    }

    public File createRandomTempSubDirectory() {
        UUID uuid = UUID.randomUUID();
        File tempSubDirectory = new File(tempBaseDir, uuid.toString());
        if (!tempSubDirectory.mkdir()) {
            throw new IllegalStateException(String.format("Cannot create temp directory at '%s'", tempSubDirectory.getAbsolutePath()));
        }
        return tempSubDirectory;
    }

    /**
     * Deletes a temp sub directory and ensures that the deleted dir is inside the temp base dir.
     *
     * @param deleteFolder the folder to be deleted
     */
    public void deleteTempSubDirectory(File deleteFolder) {
        if (!deleteFolder.getAbsolutePath().startsWith(tempBaseDir.getAbsolutePath())) {
            throw new IllegalStateException(String.format("Deleting a temp directory which is outside the base temp directory is not allowed! deleteFolder='%s'; tempBaseDir='%s'", deleteFolder.getAbsolutePath(), tempBaseDir.getAbsolutePath()));
        }
        try {
            FileUtils.deleteDirectory(deleteFolder);
        } catch (IOException e) {
            logger.error("Cannot delete upload temp directory", e);
        }
    }

    /**
     * Cleans the whole temp base directory
     */
    public void cleanTempBaseDirectory() {
        if (tempBaseDir.exists()) {
            try {
                FileUtils.cleanDirectory(tempBaseDir);
            } catch (IOException e) {
                throw new IllegalStateException(String.format("Cleaning the temp base directory failed: %s", e.getMessage()));
            }
        }
    }

}
