package org.kinsecta.webapp.api.model.repositories;

import org.kinsecta.webapp.api.model.entities.SensorLocation;
import org.springframework.data.jpa.repository.JpaRepository;


public interface SensorLocationRepository extends JpaRepository<SensorLocation, Long> {

}
