package org.kinsecta.webapp.api.util;

import org.kinsecta.webapp.api.exception.unchecked.WrongArgumentException;
import org.kinsecta.webapp.api.v1.model.PasswordDto;

import java.util.Set;


public class ValidationTools {

    /**
     * Taken from the W3C <input type="email"> specification at
     * https://html.spec.whatwg.org/multipage/input.html#email-state-(type=email)
     * but changed in the domain part to enforce a TLD where W3C also allows for '@localhost'
     * <p>
     * 13.11.2023: added '(?:\.[a-zA-Z0-9-](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9]))*' to ensure that email
     * addresses with the pattern like 'name@subdomain.maindomain.de' are accepted as well
     * <p>
     * Must match the 'isValidEmail()' pattern in Vue util 'StringValidationUtils.js'
     */
    private static final String EMAIL_REGEX = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9](?:\\.[a-zA-Z0-9-](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9]))*)$";

    /**
     * See https://stackoverflow.com/a/3802238 for reference
     * ^                            # start-of-string
     * (?=.*[0-9])                  # a digit must occur at least once
     * (?=.*[a-zäöüß])              # a lower case letter must occur at least once
     * (?=.*[A-ZÄÖÜẞ])              # an upper case letter must occur at least once
     * (?=.*[@#$%^&+=§*; ... ])     # a special character must occur at least once
     * .{8,200}                     # anything, at least 8 chars and max 200
     * $                            # end-of-string
     * <p>
     * Must match the 'isValidPassword()' pattern in Vue util 'StringValidationUtils.js'
     */
    private static final String PASSWORD_REGEX = "^(?=.*[0-9])(?=.*[a-zäöüß])(?=.*[A-ZÄÖÜẞ])(?=.*[@#$%^&+=§*;:!.,|<>\\-_'~°?/()\\[\\]{}\"`´€\\\\]).{8,200}$";

    /**
     * A pattern for a semicolon separated list of integers
     */
    private static final String ID_LIST_REGEX = "^\\d+(;\\d+)*$";

    /**
     * A pattern for a semicolon separated list of integers, allowing negative values
     */
    private static final String NEGATIVE_ID_LIST_REGEX = "^(-)?\\d+(;(-)?\\d+)*$";

    private ValidationTools() {
    }


    /**
     * Checks whether a given email address matches the regex rules for a valid email address
     * and if the length is below 256 chars
     *
     * @param email the email address to check
     *
     * @return true if email address is valid, false otherwise
     */
    public static boolean isValidEmail(String email) {
        return email.matches(EMAIL_REGEX) && email.length() < 256;
    }

    /**
     * Checks if email is valid and of specific domain
     *
     * @param email                the email address to check
     * @param emailDomainWhitelist a list of whitelisted domains.
     *                             Emails matching these TLD are considered valid.
     *                             If an empty set is passed, all TLD are allowed.
     *
     * @return true if email address is valid, false otherwise
     */
    public static boolean isValidEmail(String email, Set<String> emailDomainWhitelist) {
        if (isValidEmail(email)) {
            if (emailDomainWhitelist.isEmpty()) {
                return true;
            } else {
                for (String allowedDomain : emailDomainWhitelist) {
                    if (email.endsWith("@" + allowedDomain)) {
                        return true;
                    }
                }
                return false;
            }
        } else {
            return false;
        }
    }

    public static void validateNewPassword(PasswordDto passwordDto) {
        // check if both new passwords are the same
        if (!passwordDto.getNewPassword().equals(passwordDto.getConfNewPassword())) {
            throw new WrongArgumentException("New passwords do not match");
        }

        isValidPassword(passwordDto.getNewPassword());
    }

    public static void isValidPassword(String password) {
        if (!password.matches(PASSWORD_REGEX)) {
            throw new WrongArgumentException("Password must contain at least one digit, one uppercase character, one lowercase character and one special char and must be 8 to 200 characters long.");
        }
    }

    public static boolean isValidIdListString(String idListString) {
        return idListString.matches(ID_LIST_REGEX);
    }

    public static boolean isValidNegativeIdListString(String negativeIdListString) {
        return negativeIdListString.matches(NEGATIVE_ID_LIST_REGEX);
    }

}
