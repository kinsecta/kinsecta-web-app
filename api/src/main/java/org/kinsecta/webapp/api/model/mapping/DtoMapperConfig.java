package org.kinsecta.webapp.api.model.mapping;

import org.mapstruct.Condition;
import org.mapstruct.Mapper;
import org.openapitools.jackson.nullable.JsonNullable;

import java.util.Optional;


@Mapper(componentModel = "spring")
public abstract class DtoMapperConfig {

    <T> T unwrap(JsonNullable<T> jsonNullable) {
        return jsonNullable.orElse(null);
    }

    <T> JsonNullable<T> wrap(T object) {
        return JsonNullable.of(object);
    }

    @Condition
    <T> boolean isNotEmpty(JsonNullable<T> field) {
        return field != null && field.isPresent();
    }


    <T> T unwrapOptional(Optional<T> optional) {
        return optional.orElse(null);
    }

    <T> Optional<T> wrapOptional(T object) {
        return Optional.of(object);
    }

    @Condition
    <T> boolean isNotEmpty(Optional<T> field) {
        return field.isPresent();
    }

}
