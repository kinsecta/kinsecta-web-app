package org.kinsecta.webapp.api.model.entities;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "insect_order")
public class InsectOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "gbif_id", columnDefinition = "BIGINT UNSIGNED")
    private Long gbifId;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private List<InsectFamily> insectFamilyList = new ArrayList<>();

    @Column(name = "created", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW()")
    @Generated(GenerationTime.INSERT)
    @ReadOnlyProperty
    private LocalDateTime created;

    @Column(name = "modified", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW() ON UPDATE NOW()")
    @Generated(GenerationTime.ALWAYS)
    @ReadOnlyProperty
    private LocalDateTime modified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getGbifId() {
        return gbifId;
    }

    public void setGbifId(Long gbifId) {
        this.gbifId = gbifId;
    }

    public List<InsectFamily> getInsectFamilyList() {
        return insectFamilyList;
    }

    public void setInsectFamilyList(List<InsectFamily> insectFamilyList) {
        this.insectFamilyList = insectFamilyList;
    }

    public void addInsectFamily(InsectFamily insectFamily) {
        if (this.insectFamilyList == null) {
            this.insectFamilyList = new ArrayList<>();
        }
        this.insectFamilyList.add(insectFamily);
        insectFamily.setOrder(this);
    }

    public void removeInsectFamily(InsectFamily insectFamily) {
        if (this.insectFamilyList != null) {
            this.insectFamilyList.remove(insectFamily);
            insectFamily.setOrder(null);
        }
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }


    @Override
    public String toString() {
        return "InsectOrder{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", gbifId=" + gbifId +
            ", insectFamilyList=" + insectFamilyList +
            ", created=" + created +
            ", modified=" + modified +
            '}';
    }

}
