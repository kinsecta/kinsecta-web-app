package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.model.entities.Sensor;
import org.kinsecta.webapp.api.model.entities.SensorLocation;
import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.service.SensorLocationService;
import org.kinsecta.webapp.api.service.UserService;
import org.kinsecta.webapp.api.v1.model.SensorDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Objects;


public abstract class SensorDtoMapperDecorator implements SensorDtoMapper {

    @Autowired
    @Qualifier("delegate")
    private SensorDtoMapper delegate;


    @Autowired
    private UserService userService;

    @Autowired
    private SensorLocationService sensorLocationService;

    @Override
    public Sensor toSensor(SensorDto sensorDto) {
        Sensor sensor = delegate.toSensor(sensorDto);

        setOperator(sensor, sensorDto);
        setSensorLocation(sensor, sensorDto);

        return sensor;
    }

    @Override
    public Sensor updateSensor(Sensor sensor, SensorDto sensorDto) {
        Sensor updatedSensor = delegate.updateSensor(sensor, sensorDto);

        setOperator(updatedSensor, sensorDto);
        setSensorLocation(updatedSensor, sensorDto);

        return updatedSensor;
    }

    private void setSensorLocation(Sensor sensor, SensorDto sensorDto) {

        // if no sensorLocation was set before
        if (sensor.getSensorLocation() == null && sensorDto.getSensorLocation().getId().isPresent() && sensorDto.getSensorLocation().getId().get() != null) {
            SensorLocation newSensorLocation = sensorLocationService.getSensorLocation(sensorDto.getSensorLocation().getId().get());
            newSensorLocation.addSensor(sensor);
        }
        // update relations if sensorLocation was changed
        else if (sensor.getSensorLocation() != null && sensorDto.getSensorLocation().getId().isPresent() && sensorDto.getSensorLocation().getId().get() != null && !Objects.equals(sensor.getSensorLocation().getId(), sensorDto.getSensorLocation().getId().get())) {
            SensorLocation oldLocation = sensor.getSensorLocation();
            oldLocation.removeSensor(sensor);

            SensorLocation newSensorLocation = sensorLocationService.getSensorLocation(sensorDto.getSensorLocation().getId().get());
            newSensorLocation.addSensor(sensor);
        }
    }

    private void setOperator(Sensor sensor, SensorDto sensorDto) {
        // if no operator was set before
        if (sensor.getOperator() == null && sensorDto.getOperator().getId().isPresent() && sensorDto.getOperator().getId().get() != null) {
            User newOperator = userService.getUser(sensorDto.getOperator().getId().get());
            newOperator.addSensor(sensor);
        }
        // update relations if operator was changed
        else if (sensor.getOperator() != null && sensorDto.getOperator().getId().isPresent() && sensorDto.getOperator().getId().get() != null && !Objects.equals(sensor.getOperator().getId(), sensorDto.getOperator().getId().get())) {
            User oldOperator = sensor.getOperator();
            oldOperator.removeSensor(sensor);

            User newOperator = userService.getUser(sensorDto.getOperator().getId().get());
            newOperator.addSensor(sensor);
        }
    }

}
