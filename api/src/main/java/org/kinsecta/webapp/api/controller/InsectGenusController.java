package org.kinsecta.webapp.api.controller;

import org.kinsecta.webapp.api.model.entities.InsectGenus;
import org.kinsecta.webapp.api.model.mapping.InsectGenusDtoMapper;
import org.kinsecta.webapp.api.service.InsectGenusService;
import org.kinsecta.webapp.api.v1.InsectGenusesApi;
import org.kinsecta.webapp.api.v1.model.InsectGenusDto;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.List;


@Controller
public class InsectGenusController extends BaseController implements InsectGenusesApi {

    private final InsectGenusDtoMapper insectGenusDtoMapper;
    private final InsectGenusService insectGenusService;


    public InsectGenusController(InsectGenusDtoMapper insectGenusDtoMapper, InsectGenusService insectGenusService) {
        this.insectGenusDtoMapper = insectGenusDtoMapper;
        this.insectGenusService = insectGenusService;
    }


    @Override
    public ResponseEntity<List<InsectGenusDto>> getAllInsectGenuses() {
        List<InsectGenus> insectGenuses = insectGenusService.getAllInsectGenuses();
        List<InsectGenusDto> dtoList = insectGenuses.stream().map(insectGenusDtoMapper::insectGenusToInsectGenusDto).toList();
        return ResponseEntity.ok(dtoList);
    }

    @Override
    public ResponseEntity<List<InsectGenusDto>> searchInsectGenuses(String query) {
        if (query == null || query.isEmpty()) {
            return getAllInsectGenuses();
        }
        List<InsectGenus> allInsectGenusesForSearchQuery = insectGenusService.getAllInsectGenusesForSearchQuery(query);
        List<InsectGenusDto> dtoList = allInsectGenusesForSearchQuery.stream().map(insectGenusDtoMapper::insectGenusToInsectGenusDto).toList();
        return ResponseEntity.ok(dtoList);
    }

    @Override
    public ResponseEntity<InsectGenusDto> getInsectGenus(Long insectGenusId) {
        InsectGenus insectGenus = insectGenusService.getInsectGenus(insectGenusId);
        InsectGenusDto insectGenusDto = insectGenusDtoMapper.insectGenusToInsectGenusDto(insectGenus);
        return ResponseEntity.ok(insectGenusDto);
    }

}
