package org.kinsecta.webapp.api.security;

import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.model.entities.UserRole;
import org.kinsecta.webapp.api.model.entities.UserStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import java.time.LocalDateTime;


@Service
@RequestScope
public class ContextUserService {

    private final UserDetailsServiceImpl userDetailsService;
    private final User user;


    public ContextUserService(UserDetailsServiceImpl userDetailsService) {
        this.userDetailsService = userDetailsService;
        this.user = getUserFromContext();
    }


    public User getUserFromContext() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object authPrincipal = auth.getPrincipal();

        // principal is an instance of our 'UserDetailsImpl' when using the live application
        if (authPrincipal instanceof UserDetailsImpl userDetails) {
            return userDetails.getUser();
        }

        // principal is an instance of 'org.springframework.security.core.userdetails.User' when running integration tests annotated with @WithMockUser
        if (authPrincipal instanceof org.springframework.security.core.userdetails.User springUser) {
            return userDetailsService.loadUserByUsername(springUser.getUsername()).getUser();
        }

        throw new IllegalStateException("No context user found - the Spring Boot application cannot be run without a context user!");
    }


    // Proxied User details --------------------------------------------------------------------------------------------

    public User getUser() {
        return this.user;
    }

    public Long getUserId() {
        return this.user.getId();
    }

    public UserRole getUserRole() {
        return this.user.getRole();
    }

    public String getUsername() {
        return this.user.getUsername();
    }

    public String getUserPassword() {
        return this.user.getPassword();
    }

    public UserStatus getUserStatus() {
        return this.user.getStatus();
    }

    public boolean getUserDeleted() {
        return this.user.getDeleted() != null;
    }

    public LocalDateTime getSecurityDetailsModified() {
        return this.user.getSecurityDetailsModified();
    }

}
