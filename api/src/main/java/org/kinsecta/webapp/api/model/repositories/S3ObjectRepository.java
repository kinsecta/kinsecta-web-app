package org.kinsecta.webapp.api.model.repositories;

import org.kinsecta.webapp.api.model.entities.S3Object;
import org.springframework.data.jpa.repository.JpaRepository;


public interface S3ObjectRepository extends JpaRepository<S3Object, Long> {

}
