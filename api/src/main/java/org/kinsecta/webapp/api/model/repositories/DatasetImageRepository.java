package org.kinsecta.webapp.api.model.repositories;

import org.kinsecta.webapp.api.model.entities.Dataset;
import org.kinsecta.webapp.api.model.entities.DatasetImage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface DatasetImageRepository extends JpaRepository<DatasetImage, Long> {

    List<DatasetImage> findDatasetImagesByDataset(Dataset dataset);

}
