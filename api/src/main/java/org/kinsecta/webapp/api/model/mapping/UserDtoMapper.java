package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.model.entities.UserRole;
import org.kinsecta.webapp.api.model.entities.UserStatus;
import org.kinsecta.webapp.api.model.repositories.UserRepository;
import org.kinsecta.webapp.api.model.util.MappingConditions;
import org.kinsecta.webapp.api.v1.model.*;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


@Mapper(
    componentModel = "spring",
    uses = {
        DtoMapperConfig.class,
        MappingConditions.class
    },
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public abstract class UserDtoMapper {

    @Autowired
    private UserRepository userRepository;


    @Mapping(target = "password", ignore = true)
    public abstract User userDtoToUser(UserDto userDto);

    public abstract UserDto userToUserDto(User user);

    public abstract List<User> userDtoListToUserList(List<UserDto> userDtos);

    public abstract List<UserDto> userListToUserDtoList(List<User> users);

    public abstract UserStatusDto userStatusToUserStatusDto(UserStatus userStatus);

    public abstract UserStatus userStatusDtoToUserStatus(UserStatusDto userStatusDto);

    public abstract UserRoleDto userRoleToUserRoleDto(UserRole userRole);

    public abstract UserRole userRoleDtoToUserRole(UserRoleDto userRoleDto);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "password", ignore = true)
    public abstract User updateUserWithUserDto(UserDto update, @MappingTarget User destination);

    public User idObjectToUser(IdObject idObject) {
        if (idObject != null) {
            return userRepository.getById(idObject.getId());
        }
        return null;
    }

    public IdObject userToIdObject(User user) {
        if (user == null) {
            return null;
        }
        return new IdObject().id(user.getId());
    }

    // UserSimpleViewDto -----------------------------------------------------------------------------------------------

    public User userSimpleViewDtoToUser(UserSimpleViewDto userSimpleViewDto) {
        User user = new User();
        if (userSimpleViewDto.getId().isPresent()) {
            user = userRepository.findById(userSimpleViewDto.getId().get()).orElse(null);
        }
        return user;
    }

    /**
     * When using this method in a test and outside a Spring Request Scope, make sure to mock the contextUser (like in
     * UploadDtoMapperImplIT.mockContextUserServiceWithUser().
     */
    @Mapping(target = "fullName", source = "user", qualifiedByName = {"MappingConditions", "omitFullNameIfContextUserIsNotAdminAndUserIsNotSelf"})
    @Mapping(target = "username", source = "user", qualifiedByName = {"MappingConditions", "omitUsernameIfContextUserIsNotAdminAndUserIsNotSelf"})
    public abstract UserSimpleViewDto userToUserSimpleViewDto(User user);

    public abstract List<UserSimpleViewDto> userListToUserSimpleViewDtoList(List<User> users);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "password", ignore = true)
    public abstract User updateUserWithUserSimpleViewDto(UserSimpleViewDto update, @MappingTarget User destination);

}
