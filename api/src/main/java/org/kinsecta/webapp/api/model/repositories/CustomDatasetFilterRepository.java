package org.kinsecta.webapp.api.model.repositories;

import org.kinsecta.webapp.api.model.entities.Dataset;
import org.kinsecta.webapp.api.v1.model.DatasetFilterDto;
import org.springframework.data.domain.Pageable;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;


public interface CustomDatasetFilterRepository {

    List<Long> findAllDatasetIdsByFilters(DatasetFilterDto filterDto);

    List<Long> findAllDatasetIdsByFilters(DatasetFilterDto filterDto, Pageable pageable);

    Predicate createPredicate(CriteriaBuilder cb, CriteriaQuery<?> cq, Root<Dataset> datasetRoot, DatasetFilterDto filterDto);

    Long countAllResults(DatasetFilterDto filterDto);

}
