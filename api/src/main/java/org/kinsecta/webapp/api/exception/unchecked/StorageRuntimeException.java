package org.kinsecta.webapp.api.exception.unchecked;

import org.kinsecta.webapp.api.s3.StorageException;


public class StorageRuntimeException extends RuntimeException {

    public StorageRuntimeException(StorageException storageException) {
        super(storageException);
    }

}
