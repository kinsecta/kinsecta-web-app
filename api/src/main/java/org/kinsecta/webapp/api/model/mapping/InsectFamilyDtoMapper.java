package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.model.entities.InsectFamily;
import org.kinsecta.webapp.api.v1.model.IdObject;
import org.kinsecta.webapp.api.v1.model.InsectFamilyDto;
import org.mapstruct.*;


@Mapper(
    componentModel = "spring",
    uses = {
        DtoMapperConfig.class,
        InsectOrderDtoMapper.class
    },
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public abstract class InsectFamilyDtoMapper {

    public abstract InsectFamilyDto insectFamilyToInsectFamilyDto(InsectFamily insectFamily);

    public abstract InsectFamily insectFamilyDtoToInsectFamily(InsectFamilyDto insectFamilyDto);

    public IdObject insectFamilyToIdObject(InsectFamily insectFamily) {
        if (insectFamily == null) {
            return null;
        }
        return new IdObject().id(insectFamily.getId());
    }

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "id", ignore = true)
    public abstract InsectFamily updateInsectFamilyWithInsectFamilyDto(InsectFamilyDto insectFamilyDto, @MappingTarget InsectFamily insectFamily);

    /**
     * Don't ignore id when there is none on the Entity
     *
     * @param insectFamilyDto
     * @param insectFamily
     */
    @AfterMapping
    public void updateInsectFamilyWithInsectFamilyDtoAfterMapping(InsectFamilyDto insectFamilyDto, @MappingTarget InsectFamily insectFamily) {
        if (insectFamily.getId() == null && insectFamilyDto.getId().isPresent() && insectFamilyDto.getId().get() != null) {
            insectFamily.setId(insectFamilyDto.getId().get());
        }
    }

}
