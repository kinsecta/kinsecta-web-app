package org.kinsecta.webapp.api.exception.checked;

/**
 * This custom exception indicates that certain S3 transactions need to be rolled back.
 * <p>
 * This Exception MUST be handled where it is thrown as it is not automatically
 * handled by the {@linkplain org.kinsecta.webapp.api.exception.GlobalExceptionHandler GlobalExceptionHandler}.
 */
public class RollbackS3Exception extends Exception {

    public RollbackS3Exception() {
        // No message needed as this is only a helper exception to trigger a DB and S3 rollback
    }

}
