package org.kinsecta.webapp.api.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.PropertyBindingException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.kinsecta.webapp.api.cache.UploadStatusCache;
import org.kinsecta.webapp.api.context.KinsectaMeasurementUploadContext;
import org.kinsecta.webapp.api.exception.checked.*;
import org.kinsecta.webapp.api.exception.unchecked.UploadCancelledException;
import org.kinsecta.webapp.api.model.entities.*;
import org.kinsecta.webapp.api.model.mapping.KinsectaMeasurementImportMapper;
import org.kinsecta.webapp.api.s3.StorageException;
import org.kinsecta.webapp.api.util.FileNameComparator;
import org.kinsecta.webapp.api.util.LocalDateTimeConverter;
import org.kinsecta.webapp.api.util.TempDirectoryHelper;
import org.kinsecta.webapp.api.v1.model.KinsectaMeasurement;
import org.kinsecta.webapp.api.v1.model.KinsectaMeasurementImage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;


/**
 * This Service handles the all the mapping from a raw KInsecta Measurement to entities via
 * our Mapstruct mappers configured in {@link KinsectaMeasurementImportMapper}.
 */
@Service
public class KinsectaMeasurementImportService {

    private static final Logger logger = LoggerFactory.getLogger(KinsectaMeasurementImportService.class);

    private final ObjectMapper objectMapper;
    private final Validator validator;
    private final S3ObjectService s3ObjectService;
    private final SensorService sensorService;
    private final DatasetService datasetService;
    private final MeasurementWingbeatService measurementWingbeatService;
    private final KinsectaMeasurementImportMapper kinsectaMeasurementImportMapper;
    private final LocalDateTimeConverter localDateTimeConverter;
    private final TempDirectoryHelper tempDirectoryHelper;
    private final UploadStatusCache uploadStatusCache;
    protected static final String DATA_FOLDER_NAME_REGEX = "\\d+_\\d{4}-\\d{2}-\\d{2}_\\d{2}-\\d{2}-\\d{2}";
    private static final String JSON_FILENAME = "data.json";


    public KinsectaMeasurementImportService(ObjectMapper objectMapper, Validator validator, S3ObjectService s3ObjectService, SensorService sensorService, DatasetService datasetService, MeasurementWingbeatService measurementWingbeatService, KinsectaMeasurementImportMapper kinsectaMeasurementImportMapper, LocalDateTimeConverter localDateTimeConverter, TempDirectoryHelper tempDirectoryHelper, UploadStatusCache uploadStatusCache) {
        this.objectMapper = objectMapper;
        this.validator = validator;
        this.s3ObjectService = s3ObjectService;
        this.sensorService = sensorService;
        this.datasetService = datasetService;
        this.measurementWingbeatService = measurementWingbeatService;
        this.kinsectaMeasurementImportMapper = kinsectaMeasurementImportMapper;
        this.localDateTimeConverter = localDateTimeConverter;
        this.tempDirectoryHelper = tempDirectoryHelper;
        this.uploadStatusCache = uploadStatusCache;
    }


    /**
     * Read the Kinsecta Measurement JSON structure and map to a KinsectaMeasurement object.
     * Then validate the class with the constraints defined in the OpenAPI definition for the KinsectaMeasurement schema.
     *
     * @param jsonFile the JSON file containing the raw Kinsecta Measurement JSON structure
     *
     * @return A {@link KinsectaMeasurement} object if the structure was mapped and validated successfully
     *
     * @throws IOException                  if JSON file is inaccessible
     * @throws ValidationViolationException if Kinsecta Measurement JSON structure fails validation constraints
     * @throws JsonDeserializationException if Kinsecta Measurement JSON structure is not well-formed
     */
    protected KinsectaMeasurement mapJsonFileToKinsectaMeasurementAndValidate(File jsonFile) throws IOException, ValidationViolationException, JsonDeserializationException {
        try {
            // TODO: Bounding boxes and wingbeat classifications are set to be ignored via mixins in the object mapper config
            KinsectaMeasurement kinsectaMeasurement = objectMapper.readValue(jsonFile, KinsectaMeasurement.class);
            Set<ConstraintViolation<KinsectaMeasurement>> violations = validator.validate(kinsectaMeasurement);
            if (violations.isEmpty()) {
                return kinsectaMeasurement;
            } else {
                throw new ValidationViolationException(violations, "Validation failed for KinsectaMeasurement");
            }
        } catch (PropertyBindingException e) {
            // wrap in checked exception
            throw new JsonDeserializationException(e);
        }
    }

    /**
     * Parses the KinsectaMeasurement temp directory which contains the unzipped upload sources
     * and calls {@linkplain #processKinsectaMeasurementDirectory(File, KinsectaMeasurementUploadContext, Sensor, LocalDateTime)}  handleKinsectaMeasurementDirectory()}
     * for further processing of each measurement.
     *
     * @param context The {@link KinsectaMeasurementUploadContext} in which this is processed
     *
     * @throws RollbackS3Exception when creating or storing a child entity or S3 object fails and a rollback of all DB and S3 transactions is necessary
     */
    @Transactional(rollbackFor = RollbackS3Exception.class, propagation = Propagation.REQUIRES_NEW)
    public void processMeasurementSubfoldersInUnzippedUploadDir(KinsectaMeasurementUploadContext context) throws RollbackS3Exception, InvalidUploadZipStructureException {
        Set<File> measurementDirectories = listMeasurementDirectoriesInDir(context.getTempDir());

        if (measurementDirectories.isEmpty()) {
            String errorMessage = "No subfolders found in ZIP file which match the required directory naming scheme for a single KInsecta Measurement";
            context.addErrorMessage(errorMessage);
            throw new InvalidUploadZipStructureException(errorMessage);
        }

        // Iterate over all files and folders
        for (File dir : measurementDirectories) {
            logger.debug("Processing upload ID '{}', subdirectory '{}'", context.getUpload().getId(), dir.getAbsolutePath());
            // check whether Upload has been cancelled in the meantime by the user via the frontend
            if (uploadStatusCache.getUploadStatus(context.getUpload().getId()).equals(TransferStatus.CANCELLED)) {
                context.addErrorMessage(UploadCancelledException.MESSAGE);
                throw new RollbackS3Exception();
            }

            // Split directory name in Sensor ID and timestamp
            String[] dirNameParts = dir.getName().split("_", 2);
            try {
                // Construct timestamp and Sensor object from directory name parts
                Long folderSensorId = Long.parseLong(dirNameParts[0]);
                LocalDateTime folderTimestamp = localDateTimeConverter.convert(dirNameParts[1]);

                // Process a single measurement directory
                processKinsectaMeasurementDirectory(dir, context, folderSensorId, folderTimestamp);

            } catch (InvalidUploadZipStructureException e) {
                String errorMessage = String.format("[%s] %s", dir.getName(), e.getMessage());
                context.addErrorMessage(errorMessage);

            } catch (DateTimeException e) {
                String errorMessage = String.format("[%s] An error occurred while trying to format the datetime from a given folder: %s", dir.getName(), e.getMessage());
                context.addErrorMessage(errorMessage);

            } catch (DuplicateHashException e) {
                String errorMessage = String.format("[%s] Hashing the current folder/dataset produced the duplicate hash '%s'. This folder has already been imported as Dataset ID %s",
                    dir.getName(), e.getHash(), e.getId());
                context.addErrorMessage(errorMessage);

            } catch (ValidationViolationException e) {
                Set<ConstraintViolation<KinsectaMeasurement>> violations = e.getViolations(KinsectaMeasurement.class);
                violations.forEach(violation -> context.addErrorMessage(String.format("[%s] JSON validation error for the '%s' metadata file at property path %s: %s", dir.getName(), JSON_FILENAME, violation.getPropertyPath(), violation.getMessage())));

            } catch (MappingException e) {
                String errorMessage = String.format("[%s] An error occurred while mapping a JSON structure: %s", dir.getName(), e.getMessage());
                context.addErrorMessage(errorMessage);

            } catch (IOException e) {
                String errorMessage = String.format("[%s] An error occurred while trying to access a file: %s", dir.getName(), e.getMessage());
                context.addErrorMessage(errorMessage);

            } catch (JsonDeserializationException e) {
                String errorMessage = String.format("[%s] An error occurred while trying to deserialize a JSON file: %s", dir.getName(), e.getMessage());
                context.addErrorMessage(errorMessage);

            } catch (StorageException e) {
                // unexpected S3 error, don't keep going
                context.addErrorMessage(String.format("[%s] Unexpected S3 error, upload processing aborted! %s", dir.getName(), e.getMessage()));
                throw new RollbackS3Exception();
            }
        }

        // Processing is finished, so upload temp directory can safely be deleted
        tempDirectoryHelper.deleteTempSubDirectory(context.getTempDir());

        // Roll back transactions and S3 uploads in case of errors
        if (context.hasErrorMessages()) {
            throw new RollbackS3Exception();
        }
    }

    private Set<File> listMeasurementDirectoriesInDir(File directory) {
        // List all measurement directories in extracted upload zip folder
        File[] measurementDirectories = directory.listFiles(file -> {
            // Check if file is a directory and if its name matches the naming scheme for measurement subfolders (${SENSOR_ID}_YYYY-MM-DD_HH-MM-SS)
            // as defined in https://gitlab.com/kinsecta/kinsecta-web-app/-/wikis/Phase-1/Ergebnisse/Spezifikation-Datenformat#zip-datenstruktur
            if (file.isDirectory() && file.getName().matches(DATA_FOLDER_NAME_REGEX)) {
                return true;
            } else {
                // Log unprocessed files (just log, don't report to user) but ignore certain files from logging
                List<String> ignoreList = List.of("upload.zip", ".DS_Store");
                if (!ignoreList.contains(file.getName())) {
                    logger.warn("Encountered file or directory inside the extracted upload zip which does not match the schema for measurement subfolders: '{}'", file.getName());
                }
                return false;
            }
        });

        // Sort files and directories by name to avoid glitches when running this app on different OS'es
        if (measurementDirectories != null && measurementDirectories.length > 0) {
            Arrays.sort(measurementDirectories, new FileNameComparator());
            return Arrays.stream(measurementDirectories).collect(Collectors.toSet());
        } else {
            return new HashSet<>();
        }

    }

    private void processKinsectaMeasurementDirectory(File directory, KinsectaMeasurementUploadContext context, Long folderSensorId, LocalDateTime folderTimestamp) throws IOException, StorageException, ValidationViolationException, JsonDeserializationException, MappingException, InvalidUploadZipStructureException, DuplicateHashException {
        // Ensure the JSON file is present
        File jsonFile = Paths.get(directory.toString(), JSON_FILENAME).toFile();
        if (!jsonFile.exists()) {
            throw new InvalidUploadZipStructureException(String.format("KInsecta Measurement metadata file '%s' not found", JSON_FILENAME));
        }

        // Parse JSON metadata file and create a KinsectaMeasurement instance.
        // Process and validate the JSON in any case, even if errors occurred in previous measurement subfolders.
        // This is to ensure that the user gets a validation report for ALL uploaded measurements.
        KinsectaMeasurement kinsectaMeasurement = mapJsonFileToKinsectaMeasurementAndValidate(jsonFile);

        // #108 Generate MD5 hash for files and KinsectaMeasurement properties to detect duplicates on upload
        String md5Hash = generateHashForDirectory(directory, kinsectaMeasurement);

        // #108 search for possible duplicates in database
        Optional<Long> datasetIdWithSameMd5Hash = datasetService.findDatasetIdByMd5Hash(md5Hash);
        if (datasetIdWithSameMd5Hash.isPresent()) {
            throw new DuplicateHashException(datasetIdWithSameMd5Hash.get(), md5Hash);
        }

        // #109 Compare sensorId folder metadata with json data
        Optional<Sensor> sensor = Optional.empty();
        if (!folderSensorId.equals(kinsectaMeasurement.getSensorId())) {
            String errorMessage = String.format("[%s] Sensor ID '%d' in data.json does not match Sensor ID part in measurement subfolder name '%d'", directory.getName(), kinsectaMeasurement.getSensorId(), folderSensorId);
            context.addErrorMessage(errorMessage);
        } else {
            sensor = sensorService.getOptionalSensor(folderSensorId);
            if (sensor.isEmpty()) {
                String errorMessage = String.format("[%s] Could not find Sensor with ID '%d'", directory.getName(), folderSensorId);
                context.addErrorMessage(errorMessage);
            }
        }

        // Compare timestamp folder metadata with json data
        if (!folderTimestamp.equals(kinsectaMeasurement.getDateTime())) {
            String errorMessage = String.format("[%s] Timestamp '%s' in data.json does not match timestamp part in measurement subfolder name '%s'", directory.getName(), kinsectaMeasurement.getDateTime(), folderTimestamp);
            context.addErrorMessage(errorMessage);
        }

        // Check if datasetTags are present in data.json upon upload - abort upload to avoid duplicates
        if (kinsectaMeasurement.getTags() != null && !kinsectaMeasurement.getTags().isEmpty()) {
            String errorMessage = String.format("[%s] The key \'tags\' is specified in the data.json, which indicates that the dataset has been exported and already exists in the system.", directory.getName());
            context.addErrorMessage(errorMessage);
        }

        // Only continue with saving the entities in the DB and binary data in S3 if no errors occurred so far!
        // Otherwise, return and exit the function which means, that the errors will be reported to the user and the measurement isn't saved.
        if (context.hasNoErrorMessages()) {
            Upload upload = context.getUpload();
            // Create the Dataset entity from the KinsectaMeasurement object via a Mapstruct mapper
            Dataset dataset = kinsectaMeasurementImportMapper.kinsectaMeasurementToDataset(kinsectaMeasurement);
            // Set the Dataset properties which can't be set by the mapper
            dataset.setUpload(upload);
            dataset.setSensor(sensor.get());
            dataset.setMd5Hash(md5Hash);

            // #146 Set Dataset.User to the Upload.User
            dataset.setUser(upload.getUser());

            // Add Video to the Dataset if present
            addVideoS3Object(kinsectaMeasurement, dataset, directory, context);
            // Add Wingbeat Measurement to the Dataset if present
            addMeasurementWingbeat(kinsectaMeasurement, dataset, directory, context);
            // Add Images to the Dataset if present
            addDatasetImages(kinsectaMeasurement, dataset, directory, context);

            // Save Dataset
            datasetService.save(dataset);
        }
    }

    protected String generateHashForDirectory(File directory, KinsectaMeasurement kinsectaMeasurement) throws IOException {
        MessageDigest md5 = DigestUtils.getMd5Digest();
        // generate a MD5 hash from the KinsectaMeasurement object via its toString() method
        DigestUtils.updateDigest(md5, kinsectaMeasurement.toString());

        // generate a list of files for the current measurement
        File[] files = directory.listFiles();
        if (files != null && files.length > 0) {
            // sort files to avoid issues between Linux/Windows/macOS
            List<File> filesSorted = Arrays.stream(files).sorted().toList();
            // iterate the files and hash the file contents and update the parent MD5 hash
            for (File file : filesSorted) {
                if (!file.getName().equals(JSON_FILENAME)) {
                    DigestUtils.updateDigest(md5, file);
                }
            }
        }
        return Hex.encodeHexString(md5.digest());
    }

    private void addVideoS3Object(KinsectaMeasurement kinsectaMeasurement, Dataset dataset, File directory, KinsectaMeasurementUploadContext context) throws IOException, StorageException {
        if (kinsectaMeasurement.getVideo() != null && StringUtils.isNotBlank(kinsectaMeasurement.getVideo().getFilename())) {
            File videoFile = Paths.get(directory.toString(), kinsectaMeasurement.getVideo().getFilename()).toFile();
            if (videoFile.exists()) {
                S3Object videoS3Object = s3ObjectService.storeFile(videoFile);
                dataset.setVideoS3Object(videoS3Object);
                context.addUploadedObject(videoS3Object);
            }
        }
    }

    private void addDatasetImages(KinsectaMeasurement kinsectaMeasurement, Dataset dataset, File directory, KinsectaMeasurementUploadContext context) throws IOException, StorageException {
        if (kinsectaMeasurement.getImages() != null) {
            for (KinsectaMeasurementImage image : kinsectaMeasurement.getImages()) {
                if (StringUtils.isNotBlank(image.getFilename())) {
                    File imageFile = Paths.get(directory.toString(), image.getFilename()).toFile();
                    if (imageFile.exists()) {
                        S3Object imageS3Object = s3ObjectService.storeFile(imageFile);
                        DatasetImage datasetImage = new DatasetImage();
                        datasetImage.setS3Object(imageS3Object);
                        dataset.addDatasetImage(datasetImage);
                        context.addUploadedObject(imageS3Object);
                    }
                }
            }
        }
    }

    private void addMeasurementWingbeat(KinsectaMeasurement kinsectaMeasurement, Dataset dataset, File directory, KinsectaMeasurementUploadContext context) throws IOException, StorageException {
        if (kinsectaMeasurement.getWingbeat() != null && StringUtils.isNotBlank(kinsectaMeasurement.getWingbeat().getFilename())) {
            File wavFile = Paths.get(directory.toString(), kinsectaMeasurement.getWingbeat().getFilename()).toFile();
            if (wavFile.exists()) {
                MeasurementWingbeat measurementWingbeat = dataset.getMeasurementWingbeat();
                S3Object wavS3Object = s3ObjectService.storeFile(wavFile);
                measurementWingbeat.setWavS3Object(wavS3Object);
                context.addUploadedObject(wavS3Object);
                if (StringUtils.isNotBlank(kinsectaMeasurement.getWingbeat().getPngFilename())) {
                    File imageFile = Paths.get(directory.toString(), kinsectaMeasurement.getWingbeat().getPngFilename()).toFile();
                    if (imageFile.exists()) {
                        S3Object imageS3Object = s3ObjectService.storeFile(imageFile);
                        measurementWingbeat.setImageS3Object(imageS3Object);
                        context.addUploadedObject(imageS3Object);
                    }
                }
                measurementWingbeat = measurementWingbeatService.saveMeasurementWingbeat(measurementWingbeat);
                dataset.setMeasurementWingbeat(measurementWingbeat);
                return;
            }
        }
        // remove wingbeat entity if no wingbeat files were found
        dataset.setMeasurementWingbeat(null);
    }

}
