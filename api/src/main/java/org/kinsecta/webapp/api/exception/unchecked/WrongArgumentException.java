package org.kinsecta.webapp.api.exception.unchecked;

/**
 * This custom exception indicates arbitrary wrong arguments in any request.
 * <p>
 * The exception and especially the response status is handled in our
 * {@linkplain org.kinsecta.webapp.api.exception.GlobalExceptionHandler GlobalExceptionHandler}.
 */
public class WrongArgumentException extends RuntimeException {

    public WrongArgumentException(String message) {
        super(message);
    }

}
