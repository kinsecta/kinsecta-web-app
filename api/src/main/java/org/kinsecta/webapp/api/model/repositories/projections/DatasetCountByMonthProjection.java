package org.kinsecta.webapp.api.model.repositories.projections;

public interface DatasetCountByMonthProjection {

    int getYear();

    int getMonth();

    int getCount();

}
