package org.kinsecta.webapp.api.exception.checked;

/**
 * This custom exception is thrown when trying to parse a not well-formed JSON
 * object with Jackson.
 * <p>
 * This Exception MUST be handled where it is thrown as it is not automatically
 * handled by the {@linkplain org.kinsecta.webapp.api.exception.GlobalExceptionHandler GlobalExceptionHandler}.
 */
public class JsonDeserializationException extends Exception {

    public JsonDeserializationException(Throwable e) {
        super(e);
    }

}
