package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.model.entities.TransferStatus;
import org.kinsecta.webapp.api.v1.model.TransferStatusDto;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;


@Mapper(
    componentModel = "spring",
    uses = {
        DtoMapperConfig.class
    },
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public interface TransferStatusMapper {

    TransferStatusDto transferStatusToTransferStatusDto(TransferStatus transferStatus);

    TransferStatus transferStatusDtoToTransferStatus(TransferStatusDto transferStatusDto);

}
