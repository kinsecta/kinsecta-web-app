package org.kinsecta.webapp.api.mail;

import org.kinsecta.webapp.api.model.entities.ResetToken;
import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;


@Service
public class PasswordResetMailService extends MailService {

    private static final Logger logger = LoggerFactory.getLogger(PasswordResetMailService.class);


    public PasswordResetMailService(MessageSource messageSource, JavaMailSenderFacade javaMailSenderFacade) {
        super(messageSource, javaMailSenderFacade);
    }


    public void sendPwResetMessage(User user) {
        if (UserService.isActiveUser(user)) {
            String mailTo = user.getEmail();
            String mailSubject = translate("account.password-reset.mail.reset-successful.subject");
            String mailText = translate("account.password-reset.mail.reset-successful.body",
                user.getFullName(),
                frontendBaseUrl + loginPath);

            logger.info("About to send 'password reset request' email for active user '{}'", user.getUsername());
            javaMailSenderFacade.sendMail(mailTo, mailSubject, mailText);
        } else {
            logger.info("No 'password reset request' email sent for inactive user '{}'", user.getUsername());
        }
    }

    public void sendPwResetLink(User user, ResetToken resetToken) {
        if (UserService.isActiveUser(user)) {
            String mailTo = user.getEmail();
            String mailSubject = translate("account.password-reset.mail.send-reset-link.subject");
            String resetLink = frontendBaseUrl + passwordResetPath + "?token=" + resetToken.getToken();
            String mailText = translate("account.password-reset.mail.send-reset-link.body",
                user.getFullName(),
                user.getUsername(),
                resetLink,
                appName);

            logger.info("About to send 'password reset link' email for active user '{}'", user.getUsername());
            javaMailSenderFacade.sendMail(mailTo, mailSubject, mailText);
        } else {
            logger.info("No 'password reset link' email sent for inactive user '{}'", user.getUsername());
        }
    }

    public void sendPwResetLinkForNewUser(User user, ResetToken resetToken) {
        String mailTo = user.getEmail();
        String mailSubject = translate("account.password-reset.mail.send-reset-link-new-user.subject", appName);
        String resetLink = frontendBaseUrl + passwordResetPath + "?token=" + resetToken.getToken();
        String mailText = translate("account.password-reset.mail.send-reset-link-new-user.body",
            user.getFullName(),
            appName,
            user.getUsername(),
            resetLink,
            frontendBaseUrl + loginPath);

        logger.info("About to send 'password reset link' email for new user '{}'", user.getUsername());
        javaMailSenderFacade.sendMail(mailTo, mailSubject, mailText);

    }

}

