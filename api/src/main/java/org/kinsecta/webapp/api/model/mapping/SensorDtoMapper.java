package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.model.entities.Sensor;
import org.kinsecta.webapp.api.v1.model.SensorDto;
import org.kinsecta.webapp.api.v1.model.SensorSimpleViewDto;
import org.mapstruct.*;


@Mapper(
    componentModel = MappingConstants.ComponentModel.SPRING,
    uses = {
        DtoMapperConfig.class,
        UserDtoMapper.class
    },
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
@DecoratedWith(SensorDtoMapperDecorator.class)
public interface SensorDtoMapper {

    @Mapping(target = "operator", ignore = true)
    @Mapping(target = "sensorLocation", ignore = true)
    Sensor toSensor(SensorDto sensorDto);

    SensorDto sensorToSensorDto(Sensor sensor);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "operator", ignore = true)
    @Mapping(target = "sensorLocation", ignore = true)
    Sensor updateSensor(@MappingTarget Sensor target, SensorDto update);


    // SensorSimpleViewDto -----------------------------------------------------------------------------------------------

    SensorSimpleViewDto sensorToSensorSimpleViewDto(Sensor sensor);

}
