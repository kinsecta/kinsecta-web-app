package org.kinsecta.webapp.api.exception.unchecked;


/**
 * This custom exception indicates that an upload process has been cancelled in the meantime.
 * <p>
 * The exception and especially the response status is handled in our
 * {@linkplain org.kinsecta.webapp.api.exception.GlobalExceptionHandler GlobalExceptionHandler}.
 */
public class UploadCancelledException extends RuntimeException {

    public static final String MESSAGE = "The Upload processing was cancelled by the user!";

    public UploadCancelledException() {
        super(MESSAGE);
    }

}
