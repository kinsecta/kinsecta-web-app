package org.kinsecta.webapp.api.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.kinsecta.webapp.api.model.entities.LoginAttempt;
import org.kinsecta.webapp.api.service.LoginAttemptService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


/**
 * An implementation of the {@link AuthenticationFailureHandler} interface.
 * <p>
 * This handler method is called last by the {@link org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter#unsuccessfulAuthentication(HttpServletRequest, HttpServletResponse, AuthenticationException)}
 * and it could have also been written as <code>@Override unsuccessfulAuthentication()</code> in the {@link JwtAuthenticationFilter}
 * overriding the above-mentioned method from the <code>UsernamePasswordAuthenticationFilter</code> class,
 * but then the overridden method functionality is lost – hence this specific handler implementation.
 */
@Component
public class JwtAuthenticationFailureHandler implements AuthenticationFailureHandler {

    private static final Logger log = LoggerFactory.getLogger(JwtAuthenticationFailureHandler.class);

    public static final String MESSAGE_USER_DISABLED = "authentication.user-disabled";
    public static final String MESSAGE_BAD_CREDENTIALS = "authentication.bad-credentials";
    public static final String MESSAGE_BAD_CREDENTIALS_NOW_LOCKED = "authentication.bad-credentials-now-locked";
    public static final String MESSAGE_USER_LOCKED = "authentication.user-locked";
    public static final String MESSAGE_AUTH_FAILED = "authentication.failed";
    public static final String ATTRIBUTE_AUTH_USERNAME = "auth_username";

    private final LoginAttemptService loginAttemptService;
    private final ObjectMapper objectMapper;


    public JwtAuthenticationFailureHandler(LoginAttemptService loginAttemptService, ObjectMapper objectMapper) {
        this.loginAttemptService = loginAttemptService;
        this.objectMapper = objectMapper;
    }


    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException {
        log.trace("User authentication failed, now processing JwtAuthenticationFailureHandler.onAuthenticationFailure()");

        Map<String, Object> responseBodyData = new HashMap<>();

        // Retrieve username from session to avoid "Stream closed" exceptions at this point
        // when trying the access the request body to re-parse the JSON request
        String username = (String) request.getSession().getAttribute(ATTRIBUTE_AUTH_USERNAME);

        // Construct error message - depending on other possible exceptions or the number of remaining login attempts
        String message;
        if (exception.getClass().isAssignableFrom(DisabledException.class)) {
            message = MESSAGE_USER_DISABLED;

        } else if (exception.getClass().isAssignableFrom(BadCredentialsException.class)) {
            message = MESSAGE_BAD_CREDENTIALS;

            // Count failed login attempts for user
            Optional<LoginAttempt> loginAttemptOptional = loginAttemptService.loginFailed(username);
            if (loginAttemptOptional.isPresent()) {
                // Get remaining login attempts for user
                int remainingAttempts = loginAttemptService.getRemainingAttempts(loginAttemptOptional.get());
                if (remainingAttempts == 0) {
                    message = MESSAGE_BAD_CREDENTIALS_NOW_LOCKED;
                } else if (remainingAttempts > 0) {
                    responseBodyData.put("remainingLoginAttempts", remainingAttempts);
                }
            }

        } else if (exception.getClass().isAssignableFrom(LockedException.class)) {
            message = MESSAGE_USER_LOCKED;

        } else {
            message = MESSAGE_AUTH_FAILED;
            log.warn("Authentication failed with unhandled AuthenticationException of type '{}' with message: {}", exception.getClass().getSimpleName(), exception.getMessage());
        }
        responseBodyData.put("message", message);

        // Set HttpServletResponse to 403 FORBIDDEN and add the message and possible remainingLoginAttempts in the response body
        response.getWriter().write(convertObjectToJson(responseBodyData));
        response.setStatus(HttpStatus.FORBIDDEN.value());

        log.info("Login failed for username '{}'; cause: {}", username, message);
    }

    public String convertObjectToJson(Object object) throws JsonProcessingException {
        if (object == null) {
            return null;
        }
        return objectMapper.writeValueAsString(object);
    }

}
