package org.kinsecta.webapp.api.exception.unchecked;

/**
 * This custom exception indicates that there was a problem with the data returned by the GBIF API.
 * <p>
 * The exception and especially the response status is handled in our
 * {@linkplain org.kinsecta.webapp.api.exception.GlobalExceptionHandler GlobalExceptionHandler}.
 */
public class GbifApiResponseDataException extends RuntimeException {

    public GbifApiResponseDataException(String message) {
        super(message);
    }

}
