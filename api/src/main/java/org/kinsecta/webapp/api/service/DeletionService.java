package org.kinsecta.webapp.api.service;

import org.kinsecta.webapp.api.model.entities.*;
import org.kinsecta.webapp.api.model.repositories.DatasetRepository;
import org.kinsecta.webapp.api.model.repositories.ExportRepository;
import org.kinsecta.webapp.api.model.repositories.UploadRepository;
import org.kinsecta.webapp.api.s3.S3ClientStorageService;
import org.kinsecta.webapp.api.s3.StorageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class DeletionService {

    private static final Logger logger = LoggerFactory.getLogger(DeletionService.class);

    private final DatasetService datasetService;
    private final UploadService uploadService;
    private final ExportService exportService;
    private final S3ClientStorageService s3ClientStorageService;
    private final DatasetRepository datasetRepository;
    private final UploadRepository uploadRepository;
    private final ExportRepository exportRepository;


    public DeletionService(DatasetService datasetService, UploadService uploadService, ExportService exportService, S3ClientStorageService s3ClientStorageService, DatasetRepository datasetRepository, UploadRepository uploadRepository, ExportRepository exportRepository) {
        this.datasetService = datasetService;
        this.uploadService = uploadService;
        this.exportService = exportService;
        this.s3ClientStorageService = s3ClientStorageService;
        this.datasetRepository = datasetRepository;
        this.uploadRepository = uploadRepository;
        this.exportRepository = exportRepository;
    }


    @Transactional(rollbackFor = StorageException.class)
    public void deleteUpload(Long uploadId) throws StorageException {
        Upload upload = uploadService.getUpload(uploadId);
        // delete upload
        List<S3Object> s3ObjectsToDelete = collectS3ObjectsFromUpload(upload);
        uploadRepository.delete(upload);

        // delete attached s3 files
        for (S3Object s3Object : s3ObjectsToDelete) {
            s3ClientStorageService.delete(s3Object.getBucket(), s3Object.getKey());
        }
        logger.info("Successfully deleted Upload with id {} and associated S3Objects", uploadId);
    }

    @Transactional(rollbackFor = StorageException.class)
    public void deleteDataset(Long datasetId) throws StorageException {
        Dataset dataset = datasetService.getDataset(datasetId);
        Upload upload = dataset.getUpload();
        // delete dataset
        List<S3Object> s3ObjectsToDelete = collectS3ObjectsFromDataset(dataset);
        upload.removeDataset(dataset);
        datasetRepository.delete(dataset);

        // delete upload if it has no more datasets
        if (upload.getDatasets().isEmpty()) {
            List<S3Object> s3ObjectsToDeleteFromUpload = collectS3ObjectsFromUpload(upload);
            s3ObjectsToDelete.addAll(s3ObjectsToDeleteFromUpload);
            uploadRepository.delete(upload);
        }

        // delete attached s3 files
        for (S3Object s3Object : s3ObjectsToDelete) {
            s3ClientStorageService.delete(s3Object.getBucket(), s3Object.getKey());
        }
        logger.info("Successfully deleted Dataset with id {} and associated S3Objects", dataset.getId());
    }

    @Transactional(rollbackFor = StorageException.class)
    public void deleteExport(Long exportId) throws StorageException {
        Export export = exportService.getExport(exportId);
        List<S3Object> s3ObjectsToDelete = collectS3ObjectsFromExport(export);
        // delete export
        exportRepository.delete(export);

        // delete attached s3 files
        for (S3Object s3Object : s3ObjectsToDelete) {
            s3ClientStorageService.delete(s3Object.getBucket(), s3Object.getKey());
        }
        logger.info("Successfully deleted Export with id {} and associated S3Objects", export.getId());
    }

    /**
     * collect all s3 objects that belong to this upload
     *
     * @param upload
     *
     * @return a list of s3 Objects that belonged to the upload and should be deleted at the end of the transaction
     */
    private List<S3Object> collectS3ObjectsFromUpload(Upload upload) {
        List<S3Object> s3ObjectsToDelete = new ArrayList<>();
        if (upload.getS3Object() != null) {
            s3ObjectsToDelete.add(upload.getS3Object());
        }
        for (Dataset dataset : upload.getDatasets()) {
            List<S3Object> s3ObjectsToDeleteFromDataset = collectS3ObjectsFromDataset(dataset);
            s3ObjectsToDelete.addAll(s3ObjectsToDeleteFromDataset);
        }
        return s3ObjectsToDelete;
    }

    /**
     * collect all s3 objects that belong to this dataset
     *
     * @param dataset
     *
     * @return a list of s3 Objects that belonged to the dataset and should be deleted at the end of the transaction
     */
    private List<S3Object> collectS3ObjectsFromDataset(Dataset dataset) {
        List<S3Object> s3ObjectsToDelete = new ArrayList<>();
        if (dataset.getVideoS3Object() != null) {
            s3ObjectsToDelete.add(dataset.getVideoS3Object());
        }
        for (DatasetImage datasetImage : dataset.getDatasetImages()) {
            s3ObjectsToDelete.add(datasetImage.getS3Object());
        }
        if (dataset.getMeasurementWingbeat() != null) {
            MeasurementWingbeat measurementWingbeat = dataset.getMeasurementWingbeat();
            s3ObjectsToDelete.add(measurementWingbeat.getWavS3Object());
            if (measurementWingbeat.getImageS3Object() != null) {
                s3ObjectsToDelete.add(measurementWingbeat.getImageS3Object());
            }
        }
        return s3ObjectsToDelete;
    }

    /**
     * collect all s3 objects that belong to this export
     *
     * @param export
     *
     * @return a list of s3 Objects that belonged to the export and should be deleted at the end of the transaction
     */
    private List<S3Object> collectS3ObjectsFromExport(Export export) {
        List<S3Object> s3ObjectsToDelete = new ArrayList<>();
        for (ExportFile exportFile : export.getExportFiles()) {
            s3ObjectsToDelete.add(exportFile.getS3Object());
        }
        return s3ObjectsToDelete;
    }

}
