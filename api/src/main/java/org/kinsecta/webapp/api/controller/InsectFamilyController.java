package org.kinsecta.webapp.api.controller;

import org.kinsecta.webapp.api.model.entities.InsectFamily;
import org.kinsecta.webapp.api.model.mapping.InsectFamilyDtoMapper;
import org.kinsecta.webapp.api.service.InsectFamilyService;
import org.kinsecta.webapp.api.v1.InsectFamiliesApi;
import org.kinsecta.webapp.api.v1.model.InsectFamilyDto;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.List;


@Controller
public class InsectFamilyController extends BaseController implements InsectFamiliesApi {

    private final InsectFamilyDtoMapper insectFamilyDtoMapper;
    private final InsectFamilyService insectFamilyService;


    public InsectFamilyController(InsectFamilyDtoMapper insectFamilyDtoMapper, InsectFamilyService insectFamilyService) {
        this.insectFamilyDtoMapper = insectFamilyDtoMapper;
        this.insectFamilyService = insectFamilyService;
    }


    @Override
    public ResponseEntity<List<InsectFamilyDto>> getAllInsectFamilies() {
        List<InsectFamily> insectFamilies = insectFamilyService.getAllInsectFamilies();
        List<InsectFamilyDto> dtoList = insectFamilies.stream().map(insectFamilyDtoMapper::insectFamilyToInsectFamilyDto).toList();
        return ResponseEntity.ok(dtoList);
    }

    @Override
    public ResponseEntity<List<InsectFamilyDto>> searchInsectFamilies(String query) {
        if (query == null || query.isEmpty()) {
            return getAllInsectFamilies();
        }
        List<InsectFamily> allInsectFamiliesForSearchQuery = insectFamilyService.getAllInsectFamiliesForSearchQuery(query);
        List<InsectFamilyDto> dtoList = allInsectFamiliesForSearchQuery.stream().map(insectFamilyDtoMapper::insectFamilyToInsectFamilyDto).toList();
        return ResponseEntity.ok(dtoList);
    }

    @Override
    public ResponseEntity<InsectFamilyDto> getInsectFamily(Long insectFamilyId) {
        InsectFamily insectFamily = insectFamilyService.getInsectFamily(insectFamilyId);
        InsectFamilyDto insectFamilyDto = insectFamilyDtoMapper.insectFamilyToInsectFamilyDto(insectFamily);
        return ResponseEntity.ok(insectFamilyDto);
    }

}
