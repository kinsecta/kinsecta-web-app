package org.kinsecta.webapp.api.model.repositories;

import org.kinsecta.webapp.api.model.entities.DatasetTag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface DatasetTagRepository extends JpaRepository<DatasetTag, Long> {

    List<DatasetTag> findAllByAutoTag(Boolean autoTag);

    long countByAutoTag(Boolean autoTag);

    void deleteDatasetTagByIdAndAutoTagIsFalse(Long id);

    Optional<DatasetTag> findByName(String datasetTagName);

}
