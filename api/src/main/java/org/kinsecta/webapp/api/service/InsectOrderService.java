package org.kinsecta.webapp.api.service;

import org.kinsecta.webapp.api.exception.unchecked.ForbiddenException;
import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.model.entities.InsectOrder;
import org.kinsecta.webapp.api.model.repositories.InsectOrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;


@Service
public class InsectOrderService {

    private static final Logger logger = LoggerFactory.getLogger(InsectOrderService.class);

    private final InsectOrderRepository insectOrderRepository;


    public InsectOrderService(InsectOrderRepository insectOrderRepository) {
        this.insectOrderRepository = insectOrderRepository;
    }


    @Transactional
    public InsectOrder save(InsectOrder insectOrder) {
        return insectOrderRepository.save(insectOrder);
    }

    @Transactional(readOnly = true)
    public InsectOrder getInsectOrder(Long insectOrderId) {
        return insectOrderRepository.findById(insectOrderId).orElseThrow(() -> new NotFoundException(String.format("Could not find InsectOrder with id %d", insectOrderId)));
    }

    @Transactional(readOnly = true)
    public InsectOrder getInsectOrder(String insectOrderName) {
        return insectOrderRepository.findByName(insectOrderName).orElseThrow(() -> new NotFoundException(String.format("Could not find InsectOrder with name '%s' ", insectOrderName)));
    }

    @Transactional(readOnly = true)
    public InsectOrder getInsectOrderByGbifId(Long gbifId) {
        return insectOrderRepository.findByGbifId(gbifId).orElseThrow(() -> new NotFoundException(String.format("Could not find InsectOrder with GBIF id %d ", gbifId)));
    }

    @Transactional(readOnly = true)
    public List<InsectOrder> getAllInsectOrders() {
        return insectOrderRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<InsectOrder> getAllInsectOrdersForSearchQuery(String query) {
        String queryDecoded;
        try {
            queryDecoded = URLDecoder.decode(query, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
        return insectOrderRepository.findAllByNameContains(queryDecoded.toLowerCase());
    }

    @Transactional
    public void delete(Long insectOrderId) {
        InsectOrder insectOrder = getInsectOrder(insectOrderId);
        if (!insectOrder.getInsectFamilyList().isEmpty()) {
            throw new ForbiddenException(String.format("InsectOrder with id '%s' is linked to one or many InsectFamilies and therefore cannot be deleted", insectOrderId));
        } else {
            insectOrderRepository.delete(insectOrder);
            logger.info("Successfully deleted InsectOrder with id {}", insectOrderId);
        }
    }

}
