package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.exception.checked.MappingException;
import org.kinsecta.webapp.api.model.entities.*;
import org.kinsecta.webapp.api.model.repositories.InsectFamilyRepository;
import org.kinsecta.webapp.api.model.repositories.InsectGenusRepository;
import org.kinsecta.webapp.api.model.repositories.InsectOrderRepository;
import org.kinsecta.webapp.api.model.repositories.InsectSpeciesRepository;
import org.kinsecta.webapp.api.v1.model.*;
import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;


/**
 * A Mapstruct Mapper class to configure all mappers to map from raw <code>KinsectaMeasurement</code> to entity classes.
 */
@Mapper(
    componentModel = "spring",
    uses = {
        DtoMapperConfig.class
    },
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED
)
public abstract class KinsectaMeasurementImportMapper {

    @Autowired
    private InsectOrderRepository insectOrderRepository;

    @Autowired
    private InsectFamilyRepository insectFamilyRepository;

    @Autowired
    private InsectGenusRepository insectGenusRepository;

    @Autowired
    private InsectSpeciesRepository insectSpeciesRepository;


    @Mapping(target = "measurementDatetime", source = "dateTime")
    @Mapping(target = "measurementSizeInsectLength", source = "sizeInsect.length")
    @Mapping(target = "measurementSizeInsectWidth", source = "sizeInsect.width")
    @Mapping(target = "measurementTemperature", source = "temperature.value")
    @Mapping(target = "measurementAirPressure", source = "airPressure.value")
    @Mapping(target = "measurementHumidity", source = "humidity.value")
    @Mapping(target = "measurementLuminosity", source = "luminosity.value")
    @Mapping(target = "measurementSpectrumChannel415nm", source = "spectrum.channel415nm")
    @Mapping(target = "measurementSpectrumChannel445nm", source = "spectrum.channel445nm")
    @Mapping(target = "measurementSpectrumChannel480nm", source = "spectrum.channel480nm")
    @Mapping(target = "measurementSpectrumChannel515nm", source = "spectrum.channel515nm")
    @Mapping(target = "measurementSpectrumChannel555nm", source = "spectrum.channel555nm")
    @Mapping(target = "measurementSpectrumChannel590nm", source = "spectrum.channel590nm")
    @Mapping(target = "measurementSpectrumChannel630nm", source = "spectrum.channel630nm")
    @Mapping(target = "measurementSpectrumChannel680nm", source = "spectrum.channel680nm")
    @Mapping(target = "measurementSpectrumNearIr", source = "spectrum.nearIr")
    @Mapping(target = "measurementSpectrumClearLight", source = "spectrum.clearLight")
    @Mapping(target = "measurementParticulateMatterParticles03um", source = "particulateMatter.particles03um")
    @Mapping(target = "measurementParticulateMatterParticles05um", source = "particulateMatter.particles05um")
    @Mapping(target = "measurementParticulateMatterParticles10um", source = "particulateMatter.particles10um")
    @Mapping(target = "measurementParticulateMatterParticles25um", source = "particulateMatter.particles25um")
    @Mapping(target = "measurementParticulateMatterParticles50um", source = "particulateMatter.particles50um")
    @Mapping(target = "measurementParticulateMatterParticles100um", source = "particulateMatter.particles100um")
    @Mapping(target = "measurementParticulateMatterPm1", source = "particulateMatter.pm1")
    @Mapping(target = "measurementParticulateMatterPm2p5", source = "particulateMatter.pm2p5")
    @Mapping(target = "measurementParticulateMatterPm10", source = "particulateMatter.pm10")
    @Mapping(target = "measurementRainfall", source = "rainfall.quantity")
    @Mapping(target = "measurementWindSensorSpeed", source = "windSensor.speed")
    @Mapping(target = "measurementWindSensorDirection", source = "windSensor.direction")
    @Mapping(target = "measurementWingbeat", source = "wingbeat")
    @Mapping(target = "classifications", source = "mainClassifications")
    @Mapping(target = "user", source = "userId", ignore = true)
    public abstract Dataset kinsectaMeasurementToDataset(KinsectaMeasurement kinsectaMeasurement) throws MappingException;

    @Mapping(target = "insectOrder", source = "order")
    @Mapping(target = "insectFamily", source = "family")
    @Mapping(target = "insectGenus", source = "genus")
    @Mapping(target = "insectSpecies", source = "species")
    public abstract Classification kinsectaMeasurementClassificationToClassification(KinsectaMeasurementClassification kinsectaMeasurementClassification) throws MappingException;

    public abstract MeasurementWingbeat kinsectaMeasurementWingbeatToMeasurementWingbeat(KinsectaMeasurementWingbeat kinsectaMeasurementWingbeat);

    public InsectOrder kinsectaMeasurementClassificationInsectOrderToInsectOrder(KinsectaMeasurementClassificationInsectOrder kinsectaMeasurementClassificationInsectOrder) throws MappingException {
        if (kinsectaMeasurementClassificationInsectOrder.getGbifId().isPresent()) {
            Optional<InsectOrder> insectOrderOpt = insectOrderRepository.findByGbifId(kinsectaMeasurementClassificationInsectOrder.getGbifId().get());
            if (insectOrderOpt.isPresent()) {
                return insectOrderOpt.get();
            }
        }
        // insect order is necessary
        return insectOrderRepository.findByName(kinsectaMeasurementClassificationInsectOrder.getName()).orElseThrow(
            () -> new MappingException(String.format("Could not find InsectOrder for Gbif Id '%s' or name '%s'",
                kinsectaMeasurementClassificationInsectOrder.getGbifId().isPresent() ? kinsectaMeasurementClassificationInsectOrder.getGbifId() : null,
                kinsectaMeasurementClassificationInsectOrder.getName())));
    }

    public InsectFamily kinsectaMeasurementClassificationInsectToInsectFamily(KinsectaMeasurementClassificationInsect kinsectaMeasurementClassificationInsect) {
        if (kinsectaMeasurementClassificationInsect == null) {
            return null;
        }

        if (kinsectaMeasurementClassificationInsect.getGbifId().isPresent()) {
            Optional<InsectFamily> insectFamilyOpt = insectFamilyRepository.findByGbifId(kinsectaMeasurementClassificationInsect.getGbifId().get());
            if (insectFamilyOpt.isPresent()) {
                return insectFamilyOpt.get();
            }
        }
        JsonNullable<String> insectFamilyName = kinsectaMeasurementClassificationInsect.getName();
        if (insectFamilyName.isPresent()) {
            return insectFamilyRepository.findByName(insectFamilyName.get()).orElse(null);
        } else {
            return null;
        }
    }

    public InsectGenus kinsectaMeasurementClassificationInsectToInsectGenus(KinsectaMeasurementClassificationInsect kinsectaMeasurementClassificationInsect) {
        if (kinsectaMeasurementClassificationInsect == null) {
            return null;
        }

        if (kinsectaMeasurementClassificationInsect.getGbifId().isPresent()) {
            Optional<InsectGenus> insectGenusOpt = insectGenusRepository.findByGbifId(kinsectaMeasurementClassificationInsect.getGbifId().get());
            if (insectGenusOpt.isPresent()) {
                return insectGenusOpt.get();
            }
        }
        JsonNullable<String> insectGenusName = kinsectaMeasurementClassificationInsect.getName();
        if (insectGenusName.isPresent()) {
            return insectGenusRepository.findByName(insectGenusName.get()).orElse(null);
        } else {
            return null;
        }
    }

    public InsectSpecies kinsectaMeasurementClassificationInsectToInsectSpecies(KinsectaMeasurementClassificationInsect kinsectaMeasurementClassificationInsect) {
        if (kinsectaMeasurementClassificationInsect == null) {
            return null;
        }

        if (kinsectaMeasurementClassificationInsect.getGbifId().isPresent()) {
            Optional<InsectSpecies> insectSpeciesOpt = insectSpeciesRepository.findByGbifId(kinsectaMeasurementClassificationInsect.getGbifId().get());
            if (insectSpeciesOpt.isPresent()) {
                return insectSpeciesOpt.get();
            }
        }

        JsonNullable<String> insectSpeciesName = kinsectaMeasurementClassificationInsect.getName();
        if (insectSpeciesName.isPresent()) {
            return insectSpeciesRepository.findByName(insectSpeciesName.get()).orElse(null);
        } else {
            return null;
        }
    }

}
