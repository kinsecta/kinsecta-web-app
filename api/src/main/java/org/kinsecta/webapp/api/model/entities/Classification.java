package org.kinsecta.webapp.api.model.entities;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;


@Entity
@Table(name = "classification")
public class Classification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "dataset_id", columnDefinition = "BIGINT UNSIGNED")
    private Dataset dataset;

    @ManyToOne
    @JoinColumn(name = "dataset_image_id", columnDefinition = "BIGINT UNSIGNED")
    private DatasetImage datasetImage;

    @ManyToOne
    @JoinColumn(name = "measurement_wingbeat_id", columnDefinition = "BIGINT UNSIGNED")
    private MeasurementWingbeat measurementWingbeat;

    @ManyToOne(optional = false)
    @JoinColumn(name = "insect_order_id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private InsectOrder insectOrder;

    @ManyToOne
    @JoinColumn(name = "insect_family_id", columnDefinition = "BIGINT UNSIGNED")
    private InsectFamily insectFamily;

    @ManyToOne
    @JoinColumn(name = "insect_genus_id", columnDefinition = "BIGINT UNSIGNED")
    private InsectGenus insectGenus;

    @ManyToOne
    @JoinColumn(name = "insect_species_id", columnDefinition = "BIGINT UNSIGNED")
    private InsectSpecies insectSpecies;

    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private ClassificationType type;

    @Column(name = "gender", nullable = false)
    @Enumerated(EnumType.STRING)
    private ClassificationGender gender = ClassificationGender.UNSPECIFIED;

    @Column(name = "probability", nullable = false, precision = 6, scale = 5)
    private BigDecimal probability;

    @Column(name = "created", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW()")
    @Generated(GenerationTime.INSERT)
    @ReadOnlyProperty
    private LocalDateTime created;

    @Column(name = "modified", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW() ON UPDATE NOW()")
    @Generated(GenerationTime.ALWAYS)
    @ReadOnlyProperty
    private LocalDateTime modified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Dataset getDataset() {
        return dataset;
    }

    public void setDataset(Dataset dataset) {
        this.dataset = dataset;
    }

    public DatasetImage getDatasetImage() {
        return datasetImage;
    }

    public void setDatasetImage(DatasetImage datasetImage) {
        this.datasetImage = datasetImage;
    }

    public MeasurementWingbeat getMeasurementWingbeat() {
        return measurementWingbeat;
    }

    public void setMeasurementWingbeat(MeasurementWingbeat measurementWingbeat) {
        this.measurementWingbeat = measurementWingbeat;
    }

    public InsectOrder getInsectOrder() {
        return insectOrder;
    }

    public void setInsectOrder(InsectOrder insectOrder) {
        this.insectOrder = insectOrder;
    }

    public InsectFamily getInsectFamily() {
        return insectFamily;
    }

    public void setInsectFamily(InsectFamily insectFamily) {
        this.insectFamily = insectFamily;
    }

    public InsectGenus getInsectGenus() {
        return insectGenus;
    }

    public void setInsectGenus(InsectGenus insectGenus) {
        this.insectGenus = insectGenus;
    }

    public InsectSpecies getInsectSpecies() {
        return insectSpecies;
    }

    public void setInsectSpecies(InsectSpecies insectSpecies) {
        this.insectSpecies = insectSpecies;
    }

    public ClassificationGender getGender() {
        return gender;
    }

    public void setGender(ClassificationGender gender) {
        this.gender = gender;
    }

    public ClassificationType getType() {
        return type;
    }

    public void setType(ClassificationType type) {
        this.type = type;
    }

    public BigDecimal getProbability() {
        return probability;
    }

    public void setProbability(BigDecimal probability) {
        this.probability = probability;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }


    @Override
    public String toString() {
        return "Classification{" +
            "id=" + id +
            ", dataset=" + (dataset != null ? dataset.getId() : "null") +
            ", datasetImage=" + (datasetImage != null ? datasetImage.getId() : "null") +
            ", measurementWingbeat=" + (measurementWingbeat != null ? measurementWingbeat.getId() : "null") +
            ", insectOrder=" + insectOrder.getId() +
            ", insectFamily=" + (insectFamily != null ? insectFamily.getId() : "null") +
            ", insectGenus=" + (insectGenus != null ? insectGenus.getId() : "null") +
            ", insectSpecies=" + (insectSpecies != null ? insectSpecies.getId() : "null") +
            ", type=" + type +
            ", probability=" + probability +
            ", created=" + created +
            ", modified=" + modified +
            '}';
    }

}
