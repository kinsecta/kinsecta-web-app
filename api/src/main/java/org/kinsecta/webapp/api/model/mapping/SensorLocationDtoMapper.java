package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.model.entities.SensorLocation;
import org.kinsecta.webapp.api.service.SensorLocationService;
import org.kinsecta.webapp.api.v1.model.IdObject;
import org.kinsecta.webapp.api.v1.model.SensorLocationDto;
import org.kinsecta.webapp.api.v1.model.SensorLocationSimpleViewDto;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


@Mapper(
    componentModel = "spring",
    uses = {
        DtoMapperConfig.class
    },
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public abstract class SensorLocationDtoMapper {

    @Autowired
    private SensorLocationService sensorLocationService;


    public SensorLocation idObjectToSensorLocation(IdObject idObject) {
        if (idObject != null && idObject.getId() != null) {
            return sensorLocationService.getSensorLocation(idObject.getId());
        }
        return null;
    }

    public IdObject sensorLocationToIdObject(SensorLocation sensorLocation) {
        if (sensorLocation == null) {
            return null;
        }
        return new IdObject().id(sensorLocation.getId());
    }

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "id", ignore = true)
    public abstract SensorLocation updateSensorLocationWithSensorLocationDto(SensorLocationDto update, @MappingTarget SensorLocation destination);

    public abstract SensorLocation sensorLocationDtoToSensorLocation(SensorLocationDto sensorLocationDto);

    public abstract SensorLocationDto sensorLocationToSensorLocationDto(SensorLocation sensorLocation);


    // SensorLocationSimpleViewDto -----------------------------------------------------------------------------------------------

    public SensorLocation sensorLocationSimpleViewDtoToSensorLocation(SensorLocationSimpleViewDto sensorLocationSimpleViewDto) {
        SensorLocation sensorLocation = new SensorLocation();
        if (sensorLocationSimpleViewDto.getId().isPresent()) {
            sensorLocation = sensorLocationService.getSensorLocation(sensorLocationSimpleViewDto.getId().get());
        }
        return updateSensorLocationWithSensorLocationSimpleViewDto(sensorLocationSimpleViewDto, sensorLocation);
    }

    public abstract SensorLocationSimpleViewDto sensorLocationToSimpleViewDto(SensorLocation sensorLocation);

    public abstract List<SensorLocationSimpleViewDto> sensorLocationListToSensorLocationSimpleViewDtoList(List<SensorLocation> sensorLocations);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "id", ignore = true)
    public abstract SensorLocation updateSensorLocationWithSensorLocationSimpleViewDto(SensorLocationSimpleViewDto update, @MappingTarget SensorLocation destination);

}
