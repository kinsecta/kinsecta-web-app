package org.kinsecta.webapp.api.service;

import net.lingala.zip4j.ZipFile;
import org.kinsecta.webapp.api.exception.checked.MappingException;
import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.mail.ExportMailService;
import org.kinsecta.webapp.api.model.entities.*;
import org.kinsecta.webapp.api.model.repositories.ExportRepository;
import org.kinsecta.webapp.api.s3.StorageException;
import org.kinsecta.webapp.api.util.TempDirectoryHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;


@Service
public class ExportService {

    private static final Logger logger = LoggerFactory.getLogger(ExportService.class);

    private final ExportRepository exportRepository;
    private final KinsectaMeasurementExportService kinsectaMeasurementExportService;
    private final TempDirectoryHelper tempDirectoryHelper;
    private final S3ObjectService s3ObjectService;
    private final ExportMailService exportMailService;
    private final DatasetService datasetService;

    @Value("${app.export.download.expiration}")
    private Integer expireHours;

    @Value("${app.export.max-datasets}")
    private Integer maxDatasetsPerExportLink;


    public ExportService(ExportRepository exportRepository, KinsectaMeasurementExportService kinsectaMeasurementExportService, TempDirectoryHelper tempDirectoryHelper, S3ObjectService s3ObjectService, ExportMailService exportMailService, DatasetService datasetService) {
        this.exportRepository = exportRepository;
        this.kinsectaMeasurementExportService = kinsectaMeasurementExportService;
        this.tempDirectoryHelper = tempDirectoryHelper;
        this.s3ObjectService = s3ObjectService;
        this.exportMailService = exportMailService;
        this.datasetService = datasetService;
    }


    public Export getExport(Long id) {
        return exportRepository.findById(id).orElseThrow(() -> new NotFoundException(String.format("Could not find export with id %d", id)));
    }

    public Optional<Export> getOptionalExport(Long id) {
        return exportRepository.findById(id);
    }

    public List<Export> getAllExpiredExports() {
        LocalDateTime now = LocalDateTime.now();
        return exportRepository.findAllByExpiresIsBefore(now);
    }

    public Export saveExport(Export export) {
        return exportRepository.save(export);
    }

    @Async
    @Transactional
    public void startAsyncCreationOfExportFilesForExport(Export export) {
        // Get all requested Datasets
        List<Dataset> datasets = Arrays.stream(export.getDatasetIds().split(";"))
            .map(Long::parseLong)
            .map(datasetService::getDataset)
            .toList();

        int numberOfExportIterations = (int) Math.ceil(datasets.size() / (double) maxDatasetsPerExportLink);
        boolean errorOnCreationOfExport = false;
        List<Exception> exceptionList = new ArrayList<>();
        List<File> exportTempDirectories = new ArrayList<>();
        for (int i = 0; i < numberOfExportIterations; i++) {
            int startIndex = i * maxDatasetsPerExportLink;
            int endIndex = Math.min(i * maxDatasetsPerExportLink + maxDatasetsPerExportLink, datasets.size());
            List<Dataset> subListToExport = datasets.subList(startIndex, endIndex);

            File exportTempDir = tempDirectoryHelper.createRandomTempSubDirectory();
            exportTempDirectories.add(exportTempDir);
            try {
                // Generate a local temp folder for every Dataset and generate data.json and download S3 objects to it
                kinsectaMeasurementExportService.generateDirectoriesForDatasets(exportTempDir, subListToExport);

                // Zip all dataset of the current iteration in a single zip file and store in S3 for future download
                // When the export limit is exceeded we add a numbering of the zip files
                if (numberOfExportIterations != 1) {
                    zipFilesInDirectoryAndAddToExport(exportTempDir, export, i + 1);
                } else {
                    zipFilesInDirectoryAndAddToExport(exportTempDir, export, null);
                }

            } catch (IOException | StorageException | MappingException e) {
                logger.error("An error occurred while exporting multiple Datasets:", e);
                exceptionList.add(e);
                errorOnCreationOfExport = true;

            } finally {
                if (i == numberOfExportIterations - 1) {
                    if (!errorOnCreationOfExport) {
                        export.setFinished(LocalDateTime.now());
                        export.setStatus(TransferStatus.SUCCESSFUL);
                        export.setDownloadToken(UUID.randomUUID().toString());
                        export.setExpires(export.getFinished().plusHours(expireHours));
                        Export savedExport = saveExport(export);

                        // Send email success message
                        exportMailService.sendExportFinishedMessage(savedExport, savedExport.getDatasetIds().length() > maxDatasetsPerExportLink, maxDatasetsPerExportLink);

                    } else {
                        export.setFinished(LocalDateTime.now());
                        export.setStatus(TransferStatus.ERRONEOUS);
                        Export savedExport = saveExport(export);

                        // Send email failure message
                        List<String> errorMessages = exceptionList.stream().map(Exception::getMessage).toList();
                        exportMailService.sendExportFailedMessage(savedExport, errorMessages);
                    }

                    for (File tempDir : exportTempDirectories) {
                        tempDirectoryHelper.deleteTempSubDirectory(tempDir);
                    }
                }
            }
        }
    }

    private void zipFilesInDirectoryAndAddToExport(File directory, Export export, Integer iteration) throws IOException, StorageException {
        File[] directoriesToZip = directory.listFiles();
        if (directoriesToZip == null) {
            throw new IllegalStateException("No data found to save in ExportFile!");
        }

        // Create a single Export ZIP file
        // TODO: after prototype phase this needs to ensure that the ZIP file does not exceed 2GB otherwise we must create multiple ExportFiles!
        String zipFilename = iteration != null ? String.format("export-%d-%d.zip", export.getId(), iteration) : String.format("export-%d.zip", export.getId());
        File zipFile = new File(directory.toString(), zipFilename);
        try (ZipFile zip = new ZipFile(zipFile)) {
            for (File directoryToZip : directoriesToZip) {
                zip.addFolder(directoryToZip);
            }
        }

        // Save final ZIP for download in S3
        S3Object zipObject = s3ObjectService.storeFile(zipFile);

        // Create an ExportFile to store in the current Export
        ExportFile exportFile = new ExportFile();
        exportFile.setS3Object(zipObject);
        export.addExportFile(exportFile);
    }

}
