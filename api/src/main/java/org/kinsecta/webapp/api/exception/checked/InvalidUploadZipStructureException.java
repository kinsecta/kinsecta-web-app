package org.kinsecta.webapp.api.exception.checked;

/**
 * This custom exception indicates an invalid structure in an upload zip file.
 * <p>
 * This Exception MUST be handled where it is thrown as it is not automatically
 * handled by the {@linkplain org.kinsecta.webapp.api.exception.GlobalExceptionHandler GlobalExceptionHandler}.
 */
public class InvalidUploadZipStructureException extends Exception {

    public InvalidUploadZipStructureException(String message) {
        super(message);
    }

}
