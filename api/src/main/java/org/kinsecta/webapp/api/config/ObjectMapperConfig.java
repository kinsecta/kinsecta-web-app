package org.kinsecta.webapp.api.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.kinsecta.webapp.api.model.mapping.KinsectaMeasurementImageMixin;
import org.kinsecta.webapp.api.model.mapping.KinsectaMeasurementWingbeatMixin;
import org.kinsecta.webapp.api.v1.model.KinsectaMeasurementImage;
import org.kinsecta.webapp.api.v1.model.KinsectaMeasurementWingbeat;
import org.openapitools.jackson.nullable.JsonNullableModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class ObjectMapperConfig {

    @Bean
    public ObjectMapper objectMapper() {
        return JsonMapper.builder()
            .addModule(new JavaTimeModule())
            .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)

            // Config for JsonNullable
            // https://github.com/OpenAPITools/jackson-databind-nullable/blob/master/README.md
            .serializationInclusion(JsonInclude.Include.NON_NULL)
            .addModule(new JsonNullableModule())

            // Accept Enum values case-insensitive when mapping JSON to a DTO
            .enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS)

            // Bounding boxes and wingbeat classifications will be ignored with these mixins
            // TODO: Remove this once they are implemented
            .addMixIn(KinsectaMeasurementImage.class, KinsectaMeasurementImageMixin.class)
            .addMixIn(KinsectaMeasurementWingbeat.class, KinsectaMeasurementWingbeatMixin.class)

            .build();
    }

}
