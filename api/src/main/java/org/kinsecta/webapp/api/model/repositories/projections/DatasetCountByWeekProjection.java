package org.kinsecta.webapp.api.model.repositories.projections;

public interface DatasetCountByWeekProjection {

    int getYear();

    int getWeek();

    int getCount();

}
