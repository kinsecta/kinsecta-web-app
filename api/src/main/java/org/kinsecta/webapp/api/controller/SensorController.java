package org.kinsecta.webapp.api.controller;

import org.kinsecta.webapp.api.exception.unchecked.WrongArgumentException;
import org.kinsecta.webapp.api.model.entities.Sensor;
import org.kinsecta.webapp.api.model.entities.UserRole;
import org.kinsecta.webapp.api.model.mapping.SensorDtoMapper;
import org.kinsecta.webapp.api.service.SensorMappingService;
import org.kinsecta.webapp.api.service.SensorService;
import org.kinsecta.webapp.api.v1.SensorsApi;
import org.kinsecta.webapp.api.v1.model.ApiViewDto;
import org.kinsecta.webapp.api.v1.model.SensorDto;
import org.kinsecta.webapp.api.v1.model.SensorSimpleViewDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.List;


@Controller
public class SensorController extends BaseController implements SensorsApi {

    private final SensorDtoMapper sensorDtoMapper;
    private final SensorService sensorService;
    private final SensorMappingService sensorMappingService;


    public SensorController(SensorDtoMapper sensorDtoMapper, SensorService sensorService, SensorMappingService sensorMappingService) {
        this.sensorDtoMapper = sensorDtoMapper;
        this.sensorService = sensorService;
        this.sensorMappingService = sensorMappingService;
    }


    @Override
    public ResponseEntity<SensorDto> createSensor(SensorDto sensorDto) {
        // #183 Non-Admin Users can only create Sensors for themselves
        if (!contextUserService.getUserRole().equals(UserRole.ADMIN) && !sensorDto.getOperator().getId().get().equals(contextUserService.getUserId())) {
            throw new WrongArgumentException("Your user role does not allow the creation of Sensors for other Users!");
        }
        Sensor createdSensor = sensorMappingService.createSensor(sensorDto);
        SensorDto createdSensorDto = sensorDtoMapper.sensorToSensorDto(createdSensor);
        return new ResponseEntity<>(createdSensorDto, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<SensorDto>> getAllSensors(Integer page, Integer size, String sort, Pageable pageable) {
        Page<Sensor> paginatedSensors = sensorService.getAllSensors(pageable);
        List<SensorDto> dtoList = paginatedSensors.getContent().stream().map(sensorDtoMapper::sensorToSensorDto).toList();
        return returnPaginatedResponse(paginatedSensors, dtoList);
    }

    @Override
    public ResponseEntity<List<ApiViewDto>> getAllSensorViews() {
        ApiViewDto simpleView = new ApiViewDto();
        simpleView.setView("simple");
        simpleView.setUri("/sensors/_views/simple");
        simpleView.setDescription("Gets all Sensors in an array. This is a simplified version of the `/sensors` GET endpoint for all Non-ADMIN roles. The result is a `SensorSimpleViewDto` format, which is basically a stripped down `SensorDto` without certain fields.");
        return ResponseEntity.ok(List.of(simpleView));
    }

    @Override
    public ResponseEntity<List<SensorSimpleViewDto>> getAllSensorsSimple() {
        List<Sensor> sensorList = sensorService.getAllSensors();
        List<SensorSimpleViewDto> sensorSimpleViewDtoList = sensorList.stream().map(sensorDtoMapper::sensorToSensorSimpleViewDto).toList();
        return ResponseEntity.ok(sensorSimpleViewDtoList);
    }

    @Override
    public ResponseEntity<SensorDto> getSensor(Long sensorId) {
        Sensor sensor = sensorService.getSensor(sensorId);
        SensorDto sensorDto = sensorDtoMapper.sensorToSensorDto(sensor);
        return ResponseEntity.ok(sensorDto);
    }

    @Override
    public ResponseEntity<SensorDto> editSensor(Long sensorId, SensorDto sensorDto) {
        // #183 Non-Admin Users cannot change the operator of a Sensor
        if (!contextUserService.getUserRole().equals(UserRole.ADMIN) && !sensorDto.getOperator().getId().get().equals(contextUserService.getUserId())) {
            throw new WrongArgumentException("Your user role does not allow editing a Sensor for another User!");
        }
        Sensor savedSensor = sensorMappingService.updateSensor(sensorId, sensorDto);
        SensorDto savedSensorDto = sensorDtoMapper.sensorToSensorDto(savedSensor);
        return ResponseEntity.ok(savedSensorDto);
    }

    @Override
    public ResponseEntity<Void> deleteSensor(Long sensorId) {
        Sensor sensor = sensorService.getSensor(sensorId);
        // #183 Non-Admin Users can only create Sensors for themselves
        if (!contextUserService.getUserRole().equals(UserRole.ADMIN) && !sensor.getOperator().getId().equals(contextUserService.getUserId())) {
            throw new WrongArgumentException("You cannot delete a Sensor of another User!");
        }
        sensorService.delete(sensorId);
        return ResponseEntity.ok().build();
    }

}
