package org.kinsecta.webapp.api.model.repositories;

import org.kinsecta.webapp.api.model.entities.InsectOrder;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface InsectOrderRepository extends JpaRepository<InsectOrder, Long> {

    Optional<InsectOrder> findByGbifId(Long gbifId);

    Optional<InsectOrder> findByName(String name);

    List<InsectOrder> findAllByNameContains(String query);

}
