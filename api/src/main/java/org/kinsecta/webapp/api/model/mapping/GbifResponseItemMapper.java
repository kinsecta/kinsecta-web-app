package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.model.dto.GbifResponseItem;
import org.kinsecta.webapp.api.model.entities.InsectFamily;
import org.kinsecta.webapp.api.model.entities.InsectGenus;
import org.kinsecta.webapp.api.model.entities.InsectOrder;
import org.kinsecta.webapp.api.model.entities.InsectSpecies;
import org.kinsecta.webapp.api.v1.model.GbifInsectDto;
import org.mapstruct.BeanMapping;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


@Mapper(
    componentModel = "spring",
    uses = {
        DtoMapperConfig.class
    },
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public abstract class GbifResponseItemMapper {

    @Mapping(source = "key", target = "gbifId")
    @Mapping(source = "rank", target = "taxonomyLevel")
    @Mapping(source = "canonicalName", target = "name")
    public abstract GbifInsectDto gbifResponseItemToGbifInsectDto(GbifResponseItem gbifResponseItem);

    @BeanMapping(ignoreByDefault = true)
    @Mapping(source = "order", target = "name")
    @Mapping(source = "orderKey", target = "gbifId")
    public abstract InsectOrder gbifResponseItemToInsectOrder(GbifResponseItem gbifResponseItem);

    @BeanMapping(ignoreByDefault = true)
    @Mapping(source = "family", target = "name")
    @Mapping(source = "familyKey", target = "gbifId")
    public abstract InsectFamily gbifResponseItemToInsectFamily(GbifResponseItem gbifResponseItem);

    @BeanMapping(ignoreByDefault = true)
    @Mapping(source = "genus", target = "name")
    @Mapping(source = "genusKey", target = "gbifId")
    public abstract InsectGenus gbifResponseItemToInsectGenus(GbifResponseItem gbifResponseItem);

    @BeanMapping(ignoreByDefault = true)
    @Mapping(source = "species", target = "name")
    @Mapping(source = "speciesKey", target = "gbifId")
    public abstract InsectSpecies gbifResponseItemToInsectSpecies(GbifResponseItem gbifResponseItem);

}
