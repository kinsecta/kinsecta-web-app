package org.kinsecta.webapp.api.service;

import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.model.entities.Export;
import org.kinsecta.webapp.api.model.entities.ExportFile;
import org.kinsecta.webapp.api.model.repositories.ExportFileRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class ExportFileService {

    private final ExportFileRepository exportFileRepository;


    public ExportFileService(ExportFileRepository exportFileRepository) {
        this.exportFileRepository = exportFileRepository;
    }


    public ExportFile getExportFile(Long id) {
        return exportFileRepository.findById(id).orElseThrow(() -> new NotFoundException(String.format("Could not find ExportFile with id %d", id)));
    }

    public Optional<ExportFile> getOptionalExportFile(Long id) {
        return exportFileRepository.findById(id);
    }

    public ExportFile saveExportFile(ExportFile exportFile) {
        return exportFileRepository.save(exportFile);
    }

    public List<ExportFile> getExportFilesForExport(Export export) {
        return exportFileRepository.findExportFilesByExport(export);
    }

}
