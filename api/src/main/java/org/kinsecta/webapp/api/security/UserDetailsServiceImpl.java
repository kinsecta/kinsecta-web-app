package org.kinsecta.webapp.api.security;

import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.model.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Optional;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private static final Logger log = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    private final UserRepository userRepository;
    private final UserDetailsImpl userDetails;


    public UserDetailsServiceImpl(UserRepository userRepository, UserDetailsImpl userDetails) {
        this.userRepository = userRepository;
        this.userDetails = userDetails;
    }


    /**
     * Load a User by its username
     *
     * @param username the username as String
     *
     * @return a {@link UserDetailsImpl} object which is our own implementation of the {@link UserDetails} interface
     *
     * @throws UsernameNotFoundException if username was not found in DB
     */
    @Override
    public UserDetailsImpl loadUserByUsername(String username) throws UsernameNotFoundException {
        Assert.notNull(username, "Username must not be null!");
        log.debug("loadUserByUsername('{}')", username);
        Optional<User> userOpt = userRepository.findByUsername(username);
        if (userOpt.isEmpty()) {
            throw new UsernameNotFoundException(username);
        }

        // Store the user in the UserDetails object and return
        userDetails.setUser(userOpt.get());
        log.debug("Loaded user by username: '{}'", userDetails.getUsername());
        return userDetails;
    }

}
