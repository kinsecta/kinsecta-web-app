package org.kinsecta.webapp.api.model.entities;


public enum ClassificationType {
    HUMAN,
    MACHINE,
    GROUNDTRUTH
}
