package org.kinsecta.webapp.api.controller;

import org.kinsecta.webapp.api.model.entities.InsectSpecies;
import org.kinsecta.webapp.api.model.mapping.InsectSpeciesDtoMapper;
import org.kinsecta.webapp.api.service.InsectSpeciesService;
import org.kinsecta.webapp.api.v1.InsectSpeciesApi;
import org.kinsecta.webapp.api.v1.model.InsectSpeciesDto;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.List;


@Controller
public class InsectSpeciesController extends BaseController implements InsectSpeciesApi {

    private final InsectSpeciesDtoMapper insectSpeciesDtoMapper;
    private final InsectSpeciesService insectSpeciesService;


    public InsectSpeciesController(InsectSpeciesDtoMapper insectSpeciesDtoMapper, InsectSpeciesService insectSpeciesService) {
        this.insectSpeciesDtoMapper = insectSpeciesDtoMapper;
        this.insectSpeciesService = insectSpeciesService;
    }


    @Override
    public ResponseEntity<List<InsectSpeciesDto>> getAllInsectSpecies() {
        List<InsectSpecies> insectSpecies = insectSpeciesService.getAllInsectSpecies();
        List<InsectSpeciesDto> dtoList = insectSpecies.stream().map(insectSpeciesDtoMapper::insectSpeciesToInsectSpeciesDto).toList();
        return ResponseEntity.ok(dtoList);
    }

    @Override
    public ResponseEntity<List<InsectSpeciesDto>> searchInsectSpecies(String query) {
        if (query == null || query.isEmpty()) {
            return getAllInsectSpecies();
        }
        List<InsectSpecies> allInsectSpeciesForSearchQuery = insectSpeciesService.getAllInsectSpeciesForSearchQuery(query);
        List<InsectSpeciesDto> dtoList = allInsectSpeciesForSearchQuery.stream().map(insectSpeciesDtoMapper::insectSpeciesToInsectSpeciesDto).toList();
        return ResponseEntity.ok(dtoList);
    }

    @Override
    public ResponseEntity<InsectSpeciesDto> getInsectSpecies(Long insectSpeciesId) {
        InsectSpecies insectSpecies = insectSpeciesService.getInsectSpecies(insectSpeciesId);
        InsectSpeciesDto insectSpeciesDto = insectSpeciesDtoMapper.insectSpeciesToInsectSpeciesDto(insectSpecies);
        return ResponseEntity.ok(insectSpeciesDto);
    }

}
