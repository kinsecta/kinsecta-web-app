package org.kinsecta.webapp.api.model.repositories;

import org.kinsecta.webapp.api.model.entities.InsectGenus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface InsectGenusRepository extends JpaRepository<InsectGenus, Long> {

    Optional<InsectGenus> findByGbifId(Long gbifId);

    Optional<InsectGenus> findByName(String name);

    List<InsectGenus> findAllByNameContains(String query);

}
