package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.model.entities.InsectSpecies;
import org.kinsecta.webapp.api.v1.model.IdObject;
import org.kinsecta.webapp.api.v1.model.InsectSpeciesDto;
import org.mapstruct.*;


@Mapper(
    componentModel = "spring",
    uses = {
        DtoMapperConfig.class,
        InsectGenusDtoMapper.class
    },
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public abstract class InsectSpeciesDtoMapper {

    public abstract InsectSpeciesDto insectSpeciesToInsectSpeciesDto(InsectSpecies insectSpecies);

    public abstract InsectSpecies insectSpeciesDtoToInsectSpecies(InsectSpeciesDto insectSpeciesDto);

    public IdObject insectSpeciesToIdObject(InsectSpecies insectSpecies) {
        if (insectSpecies == null) {
            return null;
        }
        return new IdObject().id(insectSpecies.getId());
    }

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "id", ignore = true)
    public abstract InsectSpecies updateInsectSpeciesWithInsectSpeciesDto(InsectSpeciesDto insectSpeciesDto, @MappingTarget InsectSpecies insectSpecies);

    /**
     * Don't ignore id when there is none on the Entity
     *
     * @param insectSpeciesDto
     * @param insectSpecies
     */
    @AfterMapping
    public void updateInsectSpeciesWithInsectSpeciesDtoAfterMapping(InsectSpeciesDto insectSpeciesDto, @MappingTarget InsectSpecies insectSpecies) {
        if (insectSpecies.getId() == null && insectSpeciesDto.getId().isPresent() && insectSpeciesDto.getId().get() != null) {
            insectSpecies.setId(insectSpeciesDto.getId().get());
        }
    }

}
