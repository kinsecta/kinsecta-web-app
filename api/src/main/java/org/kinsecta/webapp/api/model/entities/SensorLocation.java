package org.kinsecta.webapp.api.model.entities;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "sensor_location")
public class SensorLocation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "postcode", nullable = false, columnDefinition = "MEDIUMINT UNSIGNED")
    private Integer postcode;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "latitude", nullable = false, precision = 9, scale = 6)
    private BigDecimal latitude;

    @Column(name = "longitude", nullable = false, precision = 9, scale = 6)
    private BigDecimal longitude;

    @Column(name = "gps_masked", nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
    private Boolean gpsMasked = false;

    @OneToMany(mappedBy = "sensorLocation", cascade = CascadeType.ALL)
    private List<Sensor> sensors = new ArrayList<>();

    @Column(name = "created", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW()")
    @Generated(GenerationTime.INSERT)
    @ReadOnlyProperty
    private LocalDateTime created;

    @Column(name = "modified", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW() ON UPDATE NOW()")
    @Generated(GenerationTime.ALWAYS)
    @ReadOnlyProperty
    private LocalDateTime modified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPostcode() {
        return postcode;
    }

    public void setPostcode(Integer postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public Boolean getGpsMasked() {
        return gpsMasked;
    }

    public void setGpsMasked(Boolean gpsMasked) {
        this.gpsMasked = gpsMasked;
    }

    public List<Sensor> getSensors() {
        return sensors;
    }

    public void setSensors(List<Sensor> sensors) {
        this.sensors = sensors;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }


    @Override
    public String toString() {
        return "SensorLocation{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", postcode=" + postcode +
            ", city='" + city + '\'' +
            ", latitude=" + latitude +
            ", longitude=" + longitude +
            ", gpsMasked=" + gpsMasked +
            ", created=" + created +
            ", modified=" + modified +
            '}';
    }

    public void addSensor(Sensor sensor) {
        if (this.sensors == null) {
            this.sensors = new ArrayList<>();
        }
        if (!this.sensors.contains(sensor)) {
            this.sensors.add(sensor);
            sensor.setSensorLocation(this);
        }
    }

    public void removeSensor(Sensor sensor) {
        if (this.sensors != null) {
            this.sensors.remove(sensor);
            sensor.setSensorLocation(null);
        }
    }

}
