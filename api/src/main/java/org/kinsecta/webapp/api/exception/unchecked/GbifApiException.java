package org.kinsecta.webapp.api.exception.unchecked;

/**
 * This custom exception indicates that there was a problem while communicating with the GBIF API.
 * <p>
 * The exception and especially the response status is handled in our
 * {@linkplain org.kinsecta.webapp.api.exception.GlobalExceptionHandler GlobalExceptionHandler}.
 */
public class GbifApiException extends RuntimeException {

    public GbifApiException(String message) {
        super(message);
    }

}
