package org.kinsecta.webapp.api.model.repositories.projections;

public interface DatasetCountByYearProjection {

    int getYear();

    int getCount();

}
