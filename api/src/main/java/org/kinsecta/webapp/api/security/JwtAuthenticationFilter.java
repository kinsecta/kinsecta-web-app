package org.kinsecta.webapp.api.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.kinsecta.webapp.api.config.ObjectMapperConfig;
import org.kinsecta.webapp.api.exception.unchecked.WrongArgumentException;
import org.kinsecta.webapp.api.model.dto.LoginRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.kinsecta.webapp.api.security.JwtAuthenticationFailureHandler.ATTRIBUTE_AUTH_USERNAME;


/**
 * This Spring Security Filter adds JWT based authentication to the project.
 * <p>
 * It is used for signing the user in, checking credentials, creating the JWT and handling
 * login success and failure cases.
 * <p>
 * This class extends the Spring Security default {@link UsernamePasswordAuthenticationFilter} which
 * handles sign-in on the <code>/login</code> endpoint by default if nothing else is configured via the
 * {@link #setRequiresAuthenticationRequestMatcher(RequestMatcher)} method. The login endpoint in this setup
 * can be easily changed by specifying a new path in the <code>app.authentication.endpoints.login</code>
 * application property.
 * <p>
 * Note: Mind the difference between <i>authentication</i> and <i>authorization</i>!
 * <p>
 * Note: Autowiring the {@link AuthenticationManager} in the constructor would cause a circular bean
 * reference error because the {@link AuthenticationManager} bean is created where this class is finally used:
 * in the {@link SecurityConfig}. Therefore, this filter is created as a POJO.
 */

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private static final Logger log = LoggerFactory.getLogger(JwtAuthenticationFilter.class);


    public JwtAuthenticationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }


    /**
     * Overrides the {@link org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter#attemptAuthentication(HttpServletRequest, HttpServletResponse)}
     * method to user our own JSON structure for user sign-in
     *
     * @param request  the {@link HttpServletRequest}
     * @param response the {@link HttpServletResponse}
     *
     * @return an {@link Authentication} object
     *
     * @throws AuthenticationException or an extension class which gets caught and processed by our {@link JwtAuthenticationFailureHandler}
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        LoginRequest loginRequest = mapRequestBodyToLoginRequest(request);
        String username = loginRequest.getUsername();
        String password = loginRequest.getPassword();

        // Save username in session so that our JwtAuthenticationFailureHandler can access it later on
        request.getSession().setAttribute(ATTRIBUTE_AUTH_USERNAME, username);

        // Create authentication attempt
        UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username, password);
        log.debug("Authentication attempt for username '{}'", username);
        return getAuthenticationManager().authenticate(authRequest);
    }

    /**
     * Map our custom JSON data structure used for signing a user in to our own {@link LoginRequest} class.
     *
     * @param request the {@link HttpServletRequest}
     *
     * @return the {@link LoginRequest}
     */
    public static LoginRequest mapRequestBodyToLoginRequest(HttpServletRequest request) {
        try {
            ObjectMapper objectMapper = new ObjectMapperConfig().objectMapper();
            return objectMapper.readValue(request.getInputStream(), LoginRequest.class);
        } catch (IOException e) {
            throw new WrongArgumentException(e.getMessage());
        }
    }

}
