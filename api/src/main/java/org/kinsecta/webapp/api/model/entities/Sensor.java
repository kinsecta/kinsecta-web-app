package org.kinsecta.webapp.api.model.entities;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Table(name = "sensor")
public class Sensor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "operator", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private User operator;

    @ManyToOne(optional = false)
    @JoinColumn(name = "sensor_location_id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private SensorLocation sensorLocation;

    @Column(name = "show_public", nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
    private boolean showPublic = false;

    @Column(name = "hardware_platform", nullable = false)
    private String hardwarePlatform;

    @Column(name = "os", nullable = false)
    private String os;

    @Column(name = "camera_type", nullable = false)
    private String cameraType;

    @Column(name = "wingbeat_sensor_type", nullable = false)
    private String wingbeatSensorType;

    @Column(name = "bokeh_version", nullable = false)
    private String bokehVersion;

    @Column(name = "created", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW()")
    @Generated(GenerationTime.INSERT)
    @ReadOnlyProperty
    private LocalDateTime created;

    @Column(name = "modified", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW() ON UPDATE NOW()")
    @Generated(GenerationTime.ALWAYS)
    @ReadOnlyProperty
    private LocalDateTime modified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getOperator() {
        return operator;
    }

    public void setOperator(User operator) {
        this.operator = operator;
    }

    public SensorLocation getSensorLocation() {
        return sensorLocation;
    }

    public void setSensorLocation(SensorLocation sensorLocation) {
        this.sensorLocation = sensorLocation;
    }

    public boolean isShowPublic() {
        return showPublic;
    }

    public void setShowPublic(boolean showPublic) {
        this.showPublic = showPublic;
    }

    public String getHardwarePlatform() {
        return hardwarePlatform;
    }

    public void setHardwarePlatform(String hardwarePlatform) {
        this.hardwarePlatform = hardwarePlatform;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getCameraType() {
        return cameraType;
    }

    public void setCameraType(String cameraType) {
        this.cameraType = cameraType;
    }

    public String getWingbeatSensorType() {
        return wingbeatSensorType;
    }

    public void setWingbeatSensorType(String wingbeatSensorType) {
        this.wingbeatSensorType = wingbeatSensorType;
    }

    public String getBokehVersion() {
        return bokehVersion;
    }

    public void setBokehVersion(String bokehVersion) {
        this.bokehVersion = bokehVersion;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }


    @Override
    public String toString() {
        return "Sensor{" +
            "id=" + id +
            ", operator=" + operator.getId() +
            ", sensorLocation=" + sensorLocation.getId() +
            ", showPublic='" + showPublic + '\'' +
            ", hardwarePlatform='" + hardwarePlatform + '\'' +
            ", os='" + os + '\'' +
            ", cameraType='" + cameraType + '\'' +
            ", wingbeatSensorType='" + wingbeatSensorType + '\'' +
            ", bokehVersion='" + bokehVersion + '\'' +
            ", created=" + created +
            ", modified=" + modified +
            '}';
    }

}
