package org.kinsecta.webapp.api.config;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * A ControllerAdvice to set the HTTP header 'Content-Language' in every response to the resolved requested language
 * source: https://github.com/spring-projects/spring-framework/issues/19368#issuecomment-453446031
 */
@ControllerAdvice
public class ResponseContentTypeAdvice {

    // HACK: using ModelAttribute is a hacky but ensures this gets called for every request...not sure how else to do this
    @ModelAttribute
    public void setContentLanguageOnResponse(HttpServletRequest request, HttpServletResponse response) {
        response.setHeader(HttpHeaders.CONTENT_LANGUAGE, LocaleContextHolder.getLocale().toLanguageTag());
    }

}
