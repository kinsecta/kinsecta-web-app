package org.kinsecta.webapp.api.service;

import org.kinsecta.webapp.api.exception.unchecked.GbifApiException;
import org.kinsecta.webapp.api.exception.unchecked.GbifApiResponseDataException;
import org.kinsecta.webapp.api.model.dto.GbifResponseItem;
import org.kinsecta.webapp.api.model.entities.InsectFamily;
import org.kinsecta.webapp.api.model.entities.InsectGenus;
import org.kinsecta.webapp.api.model.entities.InsectOrder;
import org.kinsecta.webapp.api.model.entities.InsectSpecies;
import org.kinsecta.webapp.api.model.mapping.GbifResponseItemMapper;
import org.kinsecta.webapp.api.model.repositories.InsectFamilyRepository;
import org.kinsecta.webapp.api.model.repositories.InsectGenusRepository;
import org.kinsecta.webapp.api.model.repositories.InsectOrderRepository;
import org.kinsecta.webapp.api.model.repositories.InsectSpeciesRepository;
import org.kinsecta.webapp.api.v1.model.TaxonomyLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.RequestBodySpec;
import org.springframework.web.reactive.function.client.WebClient.ResponseSpec;
import org.springframework.web.reactive.function.client.WebClient.UriSpec;
import org.springframework.web.reactive.function.client.WebClientException;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.util.Optional;


@Service
public class GbifApiService {

    private static final Logger logger = LoggerFactory.getLogger(GbifApiService.class);

    private final WebClient gbifClient;
    private final InsectOrderRepository insectOrderRepository;
    private final InsectFamilyRepository insectFamilyRepository;
    private final InsectGenusRepository insectGenusRepository;
    private final InsectSpeciesRepository insectSpeciesRepository;
    private final GbifResponseItemMapper gbifResponseItemMapper;

    @Value("${app.gbif.base-url}")
    private String gbifApiBaseUrl;

    @Value("${app.gbif.request-uri}")
    private String gbifApiRequestPath;

    @Value("${app.gbif.placeholder}")
    private String gbifApiRequestPathIdPlaceholder;


    public GbifApiService(WebClient gbifClient, InsectOrderRepository insectOrderRepository, InsectFamilyRepository insectFamilyRepository, InsectGenusRepository insectGenusRepository, InsectSpeciesRepository insectSpeciesRepository, GbifResponseItemMapper gbifResponseItemMapper) {
        this.gbifClient = gbifClient;
        this.insectOrderRepository = insectOrderRepository;
        this.insectFamilyRepository = insectFamilyRepository;
        this.insectGenusRepository = insectGenusRepository;
        this.insectSpeciesRepository = insectSpeciesRepository;
        this.gbifResponseItemMapper = gbifResponseItemMapper;
    }


    public GbifResponseItem getGbifResponseItemForGbifId(Long gbifId) {
        logger.info("[GBIF ID '{}'] Requested GBIF dataset via public GBIF API at {}", gbifId, gbifApiBaseUrl);

        // check that our gbifId is a valid positive integer
        if (gbifId < 0 || gbifId > Integer.MAX_VALUE) {
            throw new GbifApiException(String.format("'%d' is not a valid GBIF ID. Must be a positive integer between 0 and %d.", gbifId, Integer.MAX_VALUE));
        }

        // make the API call
        UriSpec<RequestBodySpec> uriSpec = gbifClient.method(HttpMethod.GET);
        String uri = gbifApiRequestPath.replace(gbifApiRequestPathIdPlaceholder, Long.toString(gbifId));
        RequestBodySpec bodySpec = uriSpec.uri(uri);
        ResponseSpec responseSpec = bodySpec
            .accept(MediaType.APPLICATION_JSON)
            .retrieve();

        GbifResponseItem response;
        try {
            ResponseEntity<GbifResponseItem> responseEntity = responseSpec.toEntity(GbifResponseItem.class).block();

            // assert that the responseEntity is not null
            if (responseEntity == null) {
                throw new GbifApiException("Exception while trying to retrieve data from the public GBIF API. Error message: responseEntity is null. Please try again!");
            }

            // get the response body and map it to our GbifResponseItem class
            response = responseEntity.getBody();
            logger.info("[GBIF ID '{}'] Successfully retrieved GBIF dataset", gbifId);

        } catch (WebClientException e) {
            if (e instanceof WebClientResponseException wcre && wcre.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                throw new GbifApiException(String.format("The GBIF API did not return any results for the GBIF ID '%s'", gbifId));
            }
            throw new GbifApiException(String.format("Exception while trying to retrieve data from the public GBIF API. Error message: %s. Please try again!", e.getMessage()));
        }

        return performChecksAndResolveIds(response, gbifId);
    }


    protected GbifResponseItem performChecksAndResolveIds(GbifResponseItem gbifResponseItem, Long requestedGbifId) {
        logger.info("[GBIF ID '{}'] Performing data integrity checks...", requestedGbifId);

        // assert that the response is not null
        if (gbifResponseItem == null) {
            throw new GbifApiException("Exception while trying to retrieve data from the public GBIF API. Error message: response is null. Please try again!");
        }

        // assert that the GbifResponseItem key equals the requested ID
        if (!gbifResponseItem.getKey().equals(requestedGbifId)) {
            throw new GbifApiResponseDataException(String.format("Requested GBIF ID '%s' does not match the 'key' ID in the API response: '%s'", requestedGbifId, gbifResponseItem.getKey()));
        }

        // assert that the rank in the response is a supported rank
        TaxonomyLevel taxonomyLevel;
        try {
            taxonomyLevel = TaxonomyLevel.fromValue(gbifResponseItem.getRank());
        } catch (IllegalArgumentException e) {
            throw new GbifApiResponseDataException(String.format("GBIF ID '%s' belongs to an unsupported rank: '%s'", requestedGbifId, gbifResponseItem.getRank()));
        }

        // if response item is not a backbone version (gbifId/key != nubKey) load the backbone version
        Long nubKey = gbifResponseItem.getNubKey();
        if (!nubKey.equals(gbifResponseItem.getKey())) {
            // resolve backbone version by trying to query the nubKey ID
            logger.info("[GBIF ID '{}'] Found nubKey with value '{}' which does not match the currently processed. Issuing a new API request with GBIF ID '{}'", requestedGbifId, nubKey, nubKey);
            return getGbifResponseItemForGbifId(nubKey);
        }

        // if response item is a synonym for another entry, load this entry instead
        if (Boolean.TRUE.equals(gbifResponseItem.getSynonym())) {
            logger.info("[GBIF ID '{}'] Found synonym = true. Checking taxonomy level.", requestedGbifId);
            Long actualId = switch (taxonomyLevel) {
                case ORDER -> gbifResponseItem.getOrderKey();
                case FAMILY -> gbifResponseItem.getFamilyKey();
                case GENUS -> gbifResponseItem.getGenusKey();
                case SPECIES -> gbifResponseItem.getSpeciesKey();
            };

            // resolve synonym by trying to query the resolved ID
            if (!gbifResponseItem.getKey().equals(actualId)) {
                logger.info("[GBIF ID '{}'] Current dataset is a synonym for GBIF ID '{}'. Issuing a new API request with GBIF ID '{}'", requestedGbifId, actualId, actualId);
                return getGbifResponseItemForGbifId(actualId);
            }
            // otherwise it is marked as a synonym but with matching ID's, so we just return the originally requested gbifResponseItem
            logger.warn("[GBIF ID '{}'] Current dataset is marked as a synonym for another record but no matching ID was found. Returning the originally requested GBIF dataset.", requestedGbifId);
        }

        // assert that the response is a child of the 'insect' 'CLASS' rank (GBIF ID 216)
        if (!gbifResponseItem.getClassKey().equals(216L)) {
            throw new GbifApiResponseDataException(String.format("GBIF ID '%s' does not belong to the 'Insect' class", requestedGbifId));
        }

        // return the responseItem as requested
        logger.info("[GBIF ID '{}'] Data integrity checks finished.", requestedGbifId);
        return gbifResponseItem;
    }

    @Transactional
    public InsectOrder getOrCreateInsectOrder(GbifResponseItem gbifResponseItem) {
        Long orderKey = gbifResponseItem.getOrderKey();
        Optional<InsectOrder> insectOrderOpt = insectOrderRepository.findByGbifId(orderKey);
        logger.info("[GBIF ID '{}'] Checking whether InsectOrder already exists: {}", orderKey, insectOrderOpt.isPresent());
        return insectOrderOpt.orElseGet(() -> {
                InsectOrder entity = gbifResponseItemMapper.gbifResponseItemToInsectOrder(gbifResponseItem);
                InsectOrder saved = insectOrderRepository.save(entity);
                logger.info("[GBIF ID '{}'] Created new InsectOrder with internal ID '{}'", orderKey, saved.getId());
                return saved;
            }
        );
    }

    @Transactional
    public InsectFamily getOrCreateInsectFamily(GbifResponseItem gbifResponseItem) {
        Long familyKey = gbifResponseItem.getFamilyKey();
        Optional<InsectFamily> insectFamilyOpt = insectFamilyRepository.findByGbifId(familyKey);
        logger.info("[GBIF ID '{}'] Checking whether InsectFamily already exists: {}", familyKey, insectFamilyOpt.isPresent());
        return insectFamilyOpt.orElseGet(() -> {
            InsectFamily insectFamily = gbifResponseItemMapper.gbifResponseItemToInsectFamily(gbifResponseItem);
            InsectOrder insectOrder = getOrCreateInsectOrder(gbifResponseItem);
            insectOrder.addInsectFamily(insectFamily);
            InsectFamily saved = insectFamilyRepository.save(insectFamily);
            logger.info("[GBIF ID '{}'] Created new InsectFamily with internal ID '{}'", familyKey, saved.getId());
            return saved;
        });
    }

    @Transactional
    public InsectGenus getOrCreateInsectGenus(GbifResponseItem gbifResponseItem) {
        Long genusKey = gbifResponseItem.getGenusKey();
        Optional<InsectGenus> insectGenusOpt = insectGenusRepository.findByGbifId(genusKey);
        logger.info("[GBIF ID '{}'] Checking whether InsectGenus already exists: {}", genusKey, insectGenusOpt.isPresent());
        return insectGenusOpt.orElseGet(() -> {
            InsectGenus insectGenus = gbifResponseItemMapper.gbifResponseItemToInsectGenus(gbifResponseItem);
            InsectFamily insectFamily = getOrCreateInsectFamily(gbifResponseItem);
            insectFamily.addInsectGenus(insectGenus);
            InsectGenus saved = insectGenusRepository.save(insectGenus);
            logger.info("[GBIF ID '{}'] Created new InsectGenus with internal ID '{}'", genusKey, saved.getId());
            return saved;
        });
    }

    @Transactional
    public InsectSpecies getOrCreateInsectSpecies(GbifResponseItem gbifResponseItem) {
        Long speciesKey = gbifResponseItem.getSpeciesKey();
        Optional<InsectSpecies> insectSpeciesOpt = insectSpeciesRepository.findByGbifId(speciesKey);
        logger.info("[GBIF ID '{}'] Checking whether InsectSpecies already exists: {}", speciesKey, insectSpeciesOpt.isPresent());
        return insectSpeciesOpt.orElseGet(() -> {
            InsectSpecies insectSpecies = gbifResponseItemMapper.gbifResponseItemToInsectSpecies(gbifResponseItem);
            InsectGenus insectGenus = getOrCreateInsectGenus(gbifResponseItem);
            insectGenus.addInsectSpecies(insectSpecies);
            InsectSpecies saved = insectSpeciesRepository.save(insectSpecies);
            logger.info("[GBIF ID '{}'] Created new InsectSpecies with internal ID '{}'", speciesKey, saved.getId());
            return saved;
        });
    }

}
