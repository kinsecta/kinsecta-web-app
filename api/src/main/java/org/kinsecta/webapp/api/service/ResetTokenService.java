package org.kinsecta.webapp.api.service;

import org.kinsecta.webapp.api.exception.unchecked.WrongArgumentException;
import org.kinsecta.webapp.api.model.entities.ResetToken;
import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.model.repositories.ResetTokenRepository;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;


@Service
public class ResetTokenService {

    private final ResetTokenRepository resetTokenRepository;


    public ResetTokenService(ResetTokenRepository resetTokenRepository) {
        this.resetTokenRepository = resetTokenRepository;
    }


    public Optional<ResetToken> getResetToken(String token) {
        return resetTokenRepository.findByToken(token);
    }

    public Optional<ResetToken> getResetTokenForUser(User user) {
        return resetTokenRepository.findByUser(user);
    }

    public ResetToken createResetToken(User user) {
        ResetToken resetToken = new ResetToken();
        resetToken.setToken(createNewToken());
        resetToken.setExpires(LocalDateTime.now().plusHours(24));
        resetToken.setUser(user);
        return resetTokenRepository.save(resetToken);
    }

    public ResetToken save(ResetToken resetToken) {
        return resetTokenRepository.save(resetToken);
    }

    public void delete(ResetToken resetToken) {
        resetTokenRepository.delete(resetToken);
    }

    public void deleteExistingResetTokenForUser(User user) {
        resetTokenRepository.findByUser(user).ifPresent(resetTokenRepository::delete);
    }


    public static String createNewToken() {
        String token;
        try {
            MessageDigest salt = MessageDigest.getInstance("SHA-256");
            salt.update(UUID.randomUUID().toString().getBytes(StandardCharsets.UTF_8));
            token = new String(Hex.encode(salt.digest()));
        } catch (NoSuchAlgorithmException e) {
            token = UUID.randomUUID().toString();
        }
        return token;
    }

    public static void validateResetToken(Optional<ResetToken> token) {
        if (token.isEmpty()) {
            throw new WrongArgumentException("Token does not exist");
        }
        if (token.get().getExpires().isBefore(LocalDateTime.now())) {
            throw new WrongArgumentException("Token has expired");
        }
    }

}
