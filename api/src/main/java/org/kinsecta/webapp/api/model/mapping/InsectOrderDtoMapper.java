package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.model.entities.InsectOrder;
import org.kinsecta.webapp.api.v1.model.IdObject;
import org.kinsecta.webapp.api.v1.model.InsectOrderDto;
import org.mapstruct.*;


@Mapper(
    componentModel = "spring",
    uses = {
        DtoMapperConfig.class
    },
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public abstract class InsectOrderDtoMapper {

    public abstract InsectOrderDto insectOrderToInsectOrderDto(InsectOrder insectOrder);

    public abstract InsectOrder insectOrderDtoToInsectOrder(InsectOrderDto insectOrderDto);

    public IdObject insectOrderToIdObject(InsectOrder insectOrder) {
        if (insectOrder == null) {
            return null;
        }
        return new IdObject().id(insectOrder.getId());
    }

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "id", ignore = true)
    public abstract InsectOrder updateInsectOrderWithInsectOrderDto(InsectOrderDto insectOrderDto, @MappingTarget InsectOrder insectOrder);

}
