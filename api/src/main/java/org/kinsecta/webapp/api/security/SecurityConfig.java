package org.kinsecta.webapp.api.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.springframework.security.config.Customizer.withDefaults;


/**
 * The Spring Security Config for this API project.
 * <p>
 * Set the <code>logging.level.org.kinsecta.webapp.api.security</code> property
 * to DEBUG or TRACE (default INFO) if you want to follow along the authentication
 * and authorization flow through the Spring Security Filter Chain.
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(SecurityConfig.class);

    private final UserDetailsServiceImpl userDetailsService;
    private final ExceptionHandlerFilter exceptionHandlerFilter;
    private final JwtTokenService jwtTokenService;
    private final JwtAuthenticationSuccessHandler jwtAuthenticationSuccessHandler;
    private final JwtAuthenticationFailureHandler jwtAuthenticationFailureHandler;

    @Value("${app.security.csrf-enabled}")
    private boolean csrfEnabled;

    @Value("${app.frontend.base-url}")
    private String frontendUrl;

    @Value("${app.security.cors.allowed-origins}")
    private Set<String> additionalCorsAllowedOrigins;

    @Value("${app.authentication.endpoints.login}")
    private String loginEndpoint;

    @Value("${app.authentication.endpoints.logout}")
    private String logoutEndpoint;


    @Autowired
    public SecurityConfig(UserDetailsServiceImpl userDetailsService, ExceptionHandlerFilter exceptionHandlerFilter, JwtTokenService jwtTokenService, JwtAuthenticationSuccessHandler jwtAuthenticationSuccessHandler, JwtAuthenticationFailureHandler jwtAuthenticationFailureHandler) {
        this.userDetailsService = userDetailsService;
        this.exceptionHandlerFilter = exceptionHandlerFilter;
        this.jwtTokenService = jwtTokenService;
        this.jwtAuthenticationSuccessHandler = jwtAuthenticationSuccessHandler;
        this.jwtAuthenticationFailureHandler = jwtAuthenticationFailureHandler;
    }


    /**
     * Override and configure {@link org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter#configure(HttpSecurity)}
     * to implement our own Spring Security context.
     * <p>
     * This implementation uses a JWT based authentication and authorization approach with the following filter classes:
     * <ul>
     * <li>{@link JwtAuthenticationFilter} for signing the user in, checking credentials, creating the JWT and handling login success and failure cases
     * <li>{@link JwtAuthorizationFilter} for checking the 'Authorization' header and validating the 'Bearer Token' / JWT with every API request
     * </ul>
     * <p>
     * Note: mind the difference between <i>authentication</i> and <i>authorization</i>!
     *
     * @param httpSecurity the HttpSecurity object
     *
     * @throws Exception in case of misconfiguration
     */
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        // CSRF setup
        if (!csrfEnabled) {
            logger.info("Disabled CSRF");
            httpSecurity.csrf().disable();
        } else {
            logger.info("Enabled CSRF");
            // @formatter:off
            httpSecurity.csrf()
                    .ignoringAntMatchers("/security/csrf")
                    .csrfTokenRepository(csrfTokenRepository())
                    .and()
                        .authorizeHttpRequests(authorize -> authorize
                            .mvcMatchers("/security/csrf").permitAll()
                        );
            // @formatter:on
        }

        // Configure CORS -> by default uses a Bean by the name of corsConfigurationSource which we configure further below , Session Management and public routes
        httpSecurity.cors(withDefaults());

        // Configure Session Management -> Stateless
        httpSecurity.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        // Add custom ExceptionHandlerFilter to catch very early errors in Spring Security context, during authentication and authorization phase
        httpSecurity.addFilterBefore(exceptionHandlerFilter, CorsFilter.class);

        // Add custom JwtAuthenticationFilter which handles user login with credentials check, JWT creation and login success and failure cases
        // at the position of the default UsernamePasswordAuthenticationFilter in the Spring Security filter chain.
        httpSecurity.addFilter(jwtAuthenticationFilter());

        // Add custom JwtAuthorizationFilter which checks the 'Authorization' header and validates the JWT with every API request
        // before the default BasicAuthenticationFilter in the Spring Security filter chain.
        httpSecurity.addFilterBefore(jwtAuthorizationFilter(), BasicAuthenticationFilter.class);

        // Add custom JwtCookieFilter which checks the 'JWT' cookie and validates the JWT with every API request
        // after the JwtAuthorizationFilter in the Spring Security filter chain.
        httpSecurity.addFilterAfter(jwtCookieFilter(), JwtAuthorizationFilter.class);

        // Configure authorization for endpoints
        httpSecurity.authorizeHttpRequests(authorize -> authorize
            .mvcMatchers("/error").permitAll()
            .mvcMatchers(loginEndpoint).permitAll()
            .mvcMatchers("/account/password/begin_reset").permitAll()
            .mvcMatchers("/account/password/reset").permitAll()
            .mvcMatchers("/exports/*/export_files/*/download").permitAll()
            .mvcMatchers("/public/**").permitAll()
            .anyRequest().authenticated()
        );

        // Configure the logout process
        httpSecurity.logout(logout -> logout
            .deleteCookies("JSESSIONID")
            .logoutUrl(logoutEndpoint)
        );
    }

    /**
     * Configures our custom JwtAuthenticationFilter which handles user login with credentials check,
     * JWT creation and login success and failure cases.
     *
     * @return a {@link JwtAuthenticationFilter} object
     *
     * @throws Exception possible exceptions thrown while instantiating the AuthenticationManager
     */
    @Bean
    public JwtAuthenticationFilter jwtAuthenticationFilter() throws Exception {
        JwtAuthenticationFilter jwtAuthenticationFilter = new JwtAuthenticationFilter(authenticationManager());
        jwtAuthenticationFilter.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(loginEndpoint, "POST"));
        jwtAuthenticationFilter.setAuthenticationSuccessHandler(jwtAuthenticationSuccessHandler);
        jwtAuthenticationFilter.setAuthenticationFailureHandler(jwtAuthenticationFailureHandler);
        return jwtAuthenticationFilter;
    }


    /**
     * Configures our custom JwtAuthorizationFilter which checks the 'Authorization' header and
     * validates the JWT with every API request.
     *
     * @return a {@link JwtAuthorizationFilter} object
     */
    @Bean
    public JwtAuthorizationFilter jwtAuthorizationFilter() {
        return new JwtAuthorizationFilter(jwtTokenService);
    }


    /**
     * Configures our custom JwtCookieFilter which checks the 'Authorization' header and
     * validates the JWT with every API request.
     *
     * @return a {@link JwtCookieFilter} object
     */
    @Bean
    public JwtCookieFilter jwtCookieFilter() {
        return new JwtCookieFilter(jwtTokenService);
    }


    /**
     * Override and configure the {@link org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter#configure(AuthenticationManagerBuilder)}
     * to use our BCryptPasswordEncoder configured at {@link #bCryptPasswordEncoder()}.
     *
     * @param auth the {@link AuthenticationManagerBuilder} to use
     *
     * @throws Exception any exception
     */
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }

    /**
     * A simple {@link BCryptPasswordEncoder} bean, using the {@link BCryptPasswordEncoder}
     *
     * @return the {@link BCryptPasswordEncoder}
     */
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * Simple Cross-Origin Resource Sharing (CORS) filter registration bean.
     *
     * @return the {@link CorsConfigurationSource}
     *
     * @see <a href="https://docs.spring.io/spring-security/site/docs/5.5.4/reference/html5/#cors">https://docs.spring.io/spring-security/site/docs/5.5.4/reference/html5/#cors</a>
     */
    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration config = new CorsConfiguration();

        // When using ajax/axios option 'withCredentials: true' we require exact origin match
        config.setAllowCredentials(true);

        List<String> corsAllowedOrigins = new ArrayList<>();
        corsAllowedOrigins.add(frontendUrl);
        corsAllowedOrigins.addAll(additionalCorsAllowedOrigins);
        config.setAllowedOrigins(corsAllowedOrigins);
        logger.info("Configured CORS to allow requests from the following origins: {}", config.getAllowedOrigins());

        // Set allowed HTTP methods
        config.setAllowedMethods(List.of("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH"));

        // Set HTTP Headers allowed to be used in an API request
        config.setAllowedHeaders(List.of(HttpHeaders.AUTHORIZATION, HttpHeaders.CONTENT_TYPE, CustomHttpHeaders.X_XSRF_TOKEN));

        // Set HTTP Headers which are included in the API response
        config.setExposedHeaders(List.of(HttpHeaders.AUTHORIZATION, HttpHeaders.CONTENT_LANGUAGE, HttpHeaders.CONTENT_TYPE, HttpHeaders.CONTENT_DISPOSITION, CustomHttpHeaders.X_XSRF_TOKEN, CustomHttpHeaders.X_AUTH_TOKEN, CustomHttpHeaders.X_REQUESTED_WITH, CustomHttpHeaders.X_TOTAL_COUNT));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);
        return source;
    }

    /**
     * A simple cookie-based CSRF Token Repository Bean configured for the project's frontend URL
     *
     * @return the configured {@link CsrfTokenRepository}
     *
     * @see <a href="https://docs.spring.io/spring-security/site/docs/5.5.4/reference/html5/#webflux-csrf-configure-custom-repository">https://docs.spring.io/spring-security/site/docs/5.5.4/reference/html5/#webflux-csrf-configure-custom-repository</a>
     */
    @Bean
    public CsrfTokenRepository csrfTokenRepository() {
        CookieCsrfTokenRepository csrfRepo = new CookieCsrfTokenRepository();
        // Explicitly configure for our frontend domain
        csrfRepo.setCookieDomain(frontendUrl.replaceAll("^https?://", ""));
        // Explicitly set cookieHttpOnly=false. This is necessary to allow JavaScript (i.e. AngularJS) to read it.
        csrfRepo.setCookieHttpOnly(false);
        return csrfRepo;
    }

}
