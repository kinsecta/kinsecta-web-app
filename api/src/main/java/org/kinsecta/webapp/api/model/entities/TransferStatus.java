package org.kinsecta.webapp.api.model.entities;


public enum TransferStatus {
    IN_PROGRESS,
    SUCCESSFUL,
    ERRONEOUS,
    CANCELLED
}
