package org.kinsecta.webapp.api.model.entities;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "measurement_wingbeat")
public class MeasurementWingbeat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;

    @OneToOne(mappedBy = "measurementWingbeat")
    private Dataset dataset;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "wav_s3_object_id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private S3Object wavS3Object;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "image_s3_object_id", columnDefinition = "BIGINT UNSIGNED")
    private S3Object imageS3Object;

    @Column(name = "sample_rate", nullable = false, columnDefinition = "MEDIUMINT")
    private Integer sampleRate;

    @OneToMany(mappedBy = "measurementWingbeat", cascade = CascadeType.ALL)
    private List<Classification> classifications = new ArrayList<>();

    @Column(name = "created", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW()")
    @Generated(GenerationTime.INSERT)
    @ReadOnlyProperty
    private LocalDateTime created;

    @Column(name = "modified", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW() ON UPDATE NOW()")
    @Generated(GenerationTime.ALWAYS)
    @ReadOnlyProperty
    private LocalDateTime modified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Dataset getDataset() {
        return dataset;
    }

    public void setDataset(Dataset dataset) {
        this.dataset = dataset;
    }

    public S3Object getWavS3Object() {
        return wavS3Object;
    }

    public void setWavS3Object(S3Object wavS3Object) {
        this.wavS3Object = wavS3Object;
    }

    public S3Object getImageS3Object() {
        return imageS3Object;
    }

    public void setImageS3Object(S3Object imageS3Object) {
        this.imageS3Object = imageS3Object;
    }

    public Integer getSampleRate() {
        return sampleRate;
    }

    public void setSampleRate(Integer sampleRate) {
        this.sampleRate = sampleRate;
    }

    public List<Classification> getClassifications() {
        return classifications;
    }

    public void setClassifications(List<Classification> classifications) {
        this.classifications = classifications;
    }

    public void addClassification(Classification classification) {
        if (this.classifications == null) {
            this.classifications = new ArrayList<>();
        }
        this.classifications.add(classification);
        classification.setMeasurementWingbeat(this);
    }

    public void removeClassification(Classification classification) {
        if (this.classifications != null) {
            this.classifications.remove(classification);
            classification.setMeasurementWingbeat(null);
        }
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }


    @Override
    public String toString() {
        return "MeasurementWingbeat{" +
            "id=" + id +
            ", wavS3Object=" + wavS3Object.getId() +
            ", imageS3Object=" + (imageS3Object != null ? imageS3Object.getId() : "null") +
            ", sampleRate=" + sampleRate +
            ", created=" + created +
            ", modified=" + modified +
            '}';
    }

}
