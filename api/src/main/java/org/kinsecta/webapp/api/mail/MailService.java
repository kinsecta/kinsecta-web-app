package org.kinsecta.webapp.api.mail;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;


@Component
public class MailService {

    final MessageSource messageSource;
    final JavaMailSenderFacade javaMailSenderFacade;

    @Value("${app.frontend.base-url}")
    String frontendBaseUrl;

    @Value("${app.base-url}")
    String apiBaseUrl;

    @Value("${app.frontend.password-reset.path}")
    String passwordResetPath;

    @Value("${app.frontend.login.path}")
    String loginPath;

    @Value("${app.contact.sysadmin.email}")
    String sysadminEmailAddress;

    @Value("${app.name}")
    String appName;


    public MailService(MessageSource messageSource, JavaMailSenderFacade javaMailSenderFacade) {
        this.messageSource = messageSource;
        this.javaMailSenderFacade = javaMailSenderFacade;
    }


    String translate(String messageKey) {
        return messageSource.getMessage(messageKey, null, LocaleContextHolder.getLocale());
    }

    String translate(String messageKey, Object... args) {
        return messageSource.getMessage(messageKey, args, LocaleContextHolder.getLocale());
    }

}

