package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.model.entities.InsectGenus;
import org.kinsecta.webapp.api.v1.model.IdObject;
import org.kinsecta.webapp.api.v1.model.InsectGenusDto;
import org.mapstruct.*;


@Mapper(
    componentModel = "spring",
    uses = {
        DtoMapperConfig.class,
        InsectFamilyDtoMapper.class
    },
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public abstract class InsectGenusDtoMapper {

    public abstract InsectGenusDto insectGenusToInsectGenusDto(InsectGenus insectGenus);

    public abstract InsectGenus insectGenusDtoToInsectGenus(InsectGenusDto insectGenusDto);

    public IdObject insectGenusToIdObject(InsectGenus insectGenus) {
        if (insectGenus == null) {
            return null;
        }
        return new IdObject().id(insectGenus.getId());
    }

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "id", ignore = true)
    public abstract InsectGenus updateInsectGenusWithInsectGenusDto(InsectGenusDto insectGenusDto, @MappingTarget InsectGenus insectGenus);

    /**
     * Don't ignore id when there is none on the Entity
     *
     * @param insectGenusDto
     * @param insectGenus
     */
    @AfterMapping
    public void updateInsectGenusWithInsectGenusDtoAfterMapping(InsectGenusDto insectGenusDto, @MappingTarget InsectGenus insectGenus) {
        if (insectGenus.getId() == null && insectGenusDto.getId().isPresent() && insectGenusDto.getId().get() != null) {
            insectGenus.setId(insectGenusDto.getId().get());
        }
    }

}
