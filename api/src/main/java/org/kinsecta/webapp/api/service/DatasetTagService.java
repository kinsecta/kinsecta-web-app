package org.kinsecta.webapp.api.service;

import org.kinsecta.webapp.api.exception.unchecked.ForbiddenException;
import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.model.entities.Dataset;
import org.kinsecta.webapp.api.model.entities.DatasetAutoTag;
import org.kinsecta.webapp.api.model.entities.DatasetTag;
import org.kinsecta.webapp.api.model.repositories.DatasetTagRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
public class DatasetTagService {

    private static final Logger logger = LoggerFactory.getLogger(DatasetTagService.class);

    private final DatasetTagRepository datasetTagRepository;


    public DatasetTagService(DatasetTagRepository datasetTagRepository) {
        this.datasetTagRepository = datasetTagRepository;
    }


    @Transactional
    public DatasetTag createDatasetTag(DatasetTag datasetTag) {
        datasetTag.setId(null);
        datasetTag.setAutoTag(false);
        return save(datasetTag);
    }

    @Transactional
    public DatasetTag save(DatasetTag datasetTag) {
        return datasetTagRepository.save(datasetTag);
    }

    @Transactional(readOnly = true)
    public DatasetTag getDatasetTag(Long datasetTagId) {
        return datasetTagRepository.findById(datasetTagId).orElseThrow(() -> new NotFoundException(String.format("Could not find DatasetTag with id %d", datasetTagId)));
    }

    @Transactional(readOnly = true)
    public DatasetTag getDatasetTag(String datasetTagName) {
        return datasetTagRepository.findByName(datasetTagName).orElseThrow(() -> new NotFoundException(String.format("Could not find DatasetTag with name '%s' ", datasetTagName)));
    }

    @Transactional
    public void deleteDatasetTagById(Long datasetTagId) {
        DatasetTag datasetTag = getDatasetTag(datasetTagId);
        if (Boolean.TRUE.equals(datasetTag.getAutoTag())) {
            throw new ForbiddenException(String.format("DatasetTag with id '%s' is an auto-generated tag and therefore cannot be deleted", datasetTag.getId()));
        } else {
            datasetTagRepository.deleteDatasetTagByIdAndAutoTagIsFalse(datasetTagId);
            logger.info("Successfully deleted DatasetTag with id {}", datasetTag);
        }
    }

    @Transactional(readOnly = true)
    public List<DatasetTag> getAllDatasetTags(Optional<Boolean> autoTag) {
        return (autoTag.isPresent()) ? datasetTagRepository.findAllByAutoTag(autoTag.get()) : datasetTagRepository.findAll();
    }

    @Transactional
    public void updateAutoTags(Dataset dataset) {
        if (dataset.getMeasurementWingbeat() != null) {
            dataset.addDatasetTag(getDatasetTag(DatasetAutoTag.WINGBEAT.getTagName()));
        }
        if (dataset.getVideoS3Object() != null) {
            dataset.addDatasetTag(getDatasetTag(DatasetAutoTag.VIDEO.getTagName()));
        }
        if (dataset.getDatasetImages() != null && !dataset.getDatasetImages().isEmpty()) {
            dataset.addDatasetTag(getDatasetTag(DatasetAutoTag.IMAGES.getTagName()));
        }

        // #111 "missing main classification" auto-tag
        // This is an updatable auto-tag, so check for all conditions and remove if a classification got added
        DatasetTag missingClassificationTag = getDatasetTag(DatasetAutoTag.MISSING_MAIN_CLASSIFICATION.getTagName());
        if (dataset.getClassifications() == null || dataset.getClassifications().isEmpty()) {
            dataset.addDatasetTag(missingClassificationTag);
        } else if (dataset.getDatasetTags().contains(missingClassificationTag) && dataset.getClassifications() != null && !dataset.getClassifications().isEmpty()) {
            dataset.removeDatasetTag(missingClassificationTag);
        }
    }

}
