package org.kinsecta.webapp.api.controller;

import org.kinsecta.webapp.api.exception.unchecked.WrongArgumentException;
import org.kinsecta.webapp.api.mail.PasswordResetMailService;
import org.kinsecta.webapp.api.model.entities.LoginAttempt;
import org.kinsecta.webapp.api.model.entities.ResetToken;
import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.model.entities.UserStatus;
import org.kinsecta.webapp.api.model.mapping.UserDtoMapper;
import org.kinsecta.webapp.api.model.repositories.LoginAttemptRepository;
import org.kinsecta.webapp.api.security.JwtTokenService;
import org.kinsecta.webapp.api.service.ResetTokenService;
import org.kinsecta.webapp.api.util.ValidationTools;
import org.kinsecta.webapp.api.v1.AccountApi;
import org.kinsecta.webapp.api.v1.model.PasswordChangeDto;
import org.kinsecta.webapp.api.v1.model.PasswordResetDto;
import org.kinsecta.webapp.api.v1.model.PasswordResetRequestDto;
import org.kinsecta.webapp.api.v1.model.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;

import java.util.Optional;
import java.util.Set;


/**
 * Manages interaction regarding the user profile and account settings
 */
@Controller
public class AccountController extends BaseController implements AccountApi {

    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    private final PasswordResetMailService passwordResetMailService;
    private final UserDtoMapper userDtoMapper;
    private final BCryptPasswordEncoder passwordEncoder;
    private final LoginAttemptRepository loginAttemptRepository;
    private final ResetTokenService resetTokenService;
    private final JwtTokenService jwtTokenService;

    @Value("${app.account.email-domain-whitelist}")
    private Set<String> emailDomainWhitelist;


    public AccountController(PasswordResetMailService passwordResetMailService, UserDtoMapper userDtoMapper, BCryptPasswordEncoder passwordEncoder, LoginAttemptRepository loginAttemptRepository, ResetTokenService resetTokenService, JwtTokenService jwtTokenService) {
        this.passwordResetMailService = passwordResetMailService;
        this.userDtoMapper = userDtoMapper;
        this.passwordEncoder = passwordEncoder;
        this.loginAttemptRepository = loginAttemptRepository;
        this.resetTokenService = resetTokenService;
        this.jwtTokenService = jwtTokenService;
    }


    @Override
    public ResponseEntity<UserDto> saveAccountProfile(UserDto userDto) {
        HttpHeaders headers = new HttpHeaders();
        boolean regenerateJwt = false;
        User currentUser = contextUserService.getUser();

        // fail if context user tries to update other user's data
        failIfCurrentUserTriesToQueryArbitraryUserWithId(userDto.getId().get());

        // update fullName
        currentUser.setFullName(userDto.getFullName());

        // update email
        if (!userDto.getEmail().equals(currentUser.getEmail())) {
            if (!ValidationTools.isValidEmail(userDto.getEmail(), this.emailDomainWhitelist)) {
                throw new WrongArgumentException(String.format("Cannot save account profile: The new email address '%s' is not valid with regard to the allowed email domain whitelist: %s", userDto.getEmail(), this.emailDomainWhitelist));
            }
            currentUser.setEmail(userDto.getEmail());
            // update the security details modified timestamp so that all JWT tokens of the specified user render invalid immediately
            currentUser.updateSecurityDetailsModified();
            regenerateJwt = true;
        }

        // save updated user info
        User savedUser = userService.save(currentUser);

        // regenerate JWT if necessary
        if (regenerateJwt) {
            headers = jwtTokenService.createJwtTokenAndReturnInAuthorizationHeader(savedUser);
        }

        // convert saved User to UserDto
        UserDto updatedUser = userDtoMapper.userToUserDto(savedUser);

        // return 200 OK with updated User details als UserDto and maybe with a new JWT if security details changed
        return ResponseEntity.status(HttpStatus.OK).headers(headers).body(updatedUser);
    }

    @Override
    public ResponseEntity<Void> changePassword(PasswordChangeDto passwordChangeDto) {
        User currentUser = contextUserService.getUser();

        if (currentUser.getStatus().equals(UserStatus.DELETED)) {
            throw new WrongArgumentException("You cannot change the password of a User that is marked as 'deleted'.");
        }

        // check current password
        userService.checkCurrentUserPasswordMatchesPassword(passwordChangeDto.getCurrentPassword(), currentUser);

        // validate new password
        ValidationTools.validateNewPassword(passwordChangeDto);

        // set the password for the current User entity
        currentUser.setPassword(passwordEncoder.encode(passwordChangeDto.getNewPassword()));

        // update the security details modified timestamp so that all JWT tokens of the specified user render invalid immediately
        currentUser.updateSecurityDetailsModified();

        // save the current user with new password
        User savedUser = userService.save(currentUser);
        logger.info("Password change successful for user '{}'", currentUser);

        // regenerate JWT
        HttpHeaders headers = jwtTokenService.createJwtTokenAndReturnInAuthorizationHeader(savedUser);

        // return 200 OK and with a new JWT in Authorization header
        return ResponseEntity.status(HttpStatus.OK).headers(headers).build();
    }

    @Override
    public ResponseEntity<UserDto> getCurrentAccount() {
        return ResponseEntity.ok(userDtoMapper.userToUserDto(contextUserService.getUser()));
    }

    @Override
    public ResponseEntity<Void> startPasswordResetFlow(PasswordResetRequestDto passwordResetRequestDto) {
        String userEmail = passwordResetRequestDto.getUserEmail();
        if (!ValidationTools.isValidEmail(userEmail)) {
            throw new WrongArgumentException(String.format("The email address '%s' is not valid!", userEmail));
        }

        User user = userService.getUserByEmail(userEmail);
        if (user.getStatus().equals(UserStatus.DELETED)) {
            throw new WrongArgumentException("You cannot reset the password of a User that is marked as 'deleted'.");
        }
        resetTokenService.deleteExistingResetTokenForUser(user);
        ResetToken resetToken = resetTokenService.createResetToken(user);
        passwordResetMailService.sendPwResetLink(user, resetToken);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> resetPassword(PasswordResetDto passwordResetDto) {
        String token = passwordResetDto.getToken();
        String newPassword = passwordResetDto.getNewPassword();
        Optional<ResetToken> resetTokenOpt = resetTokenService.getResetToken(token);
        ResetTokenService.validateResetToken(resetTokenOpt);
        ResetToken resetToken = resetTokenOpt.get();
        User userToReset = resetToken.getUser();
        ValidationTools.validateNewPassword(passwordResetDto);

        userToReset.setPassword(passwordEncoder.encode(newPassword));
        // update the security details modified timestamp so that all JWT tokens of the specified user render invalid immediately
        userToReset.updateSecurityDetailsModified();
        userService.save(userToReset);

        logger.info("The ResetToken {} with token {} was redeemed by {}",
            resetToken.getId(),
            resetToken.getToken(),
            resetToken.getUser().getUsername());
        resetTokenService.delete(resetToken);

        // delete/reset possible loginAttempts
        Optional<LoginAttempt> optionalLoginAttempt = loginAttemptRepository.findByUser(userToReset);
        optionalLoginAttempt.ifPresent(loginAttemptRepository::delete);

        passwordResetMailService.sendPwResetMessage(userToReset);
        logger.info("Password was reset by {}", userToReset.getUsername());
        return ResponseEntity.ok().build();
    }

}
