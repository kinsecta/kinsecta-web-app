package org.kinsecta.webapp.api.model.entities;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "insect_genus")
public class InsectGenus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "family_id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private InsectFamily family;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "gbif_id", columnDefinition = "BIGINT UNSIGNED")
    private Long gbifId;

    @OneToMany(mappedBy = "genus", cascade = CascadeType.ALL)
    private List<InsectSpecies> insectSpeciesList = new ArrayList<>();

    @Column(name = "created", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW()")
    @Generated(GenerationTime.INSERT)
    @ReadOnlyProperty
    private LocalDateTime created;

    @Column(name = "modified", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW() ON UPDATE NOW()")
    @Generated(GenerationTime.ALWAYS)
    @ReadOnlyProperty
    private LocalDateTime modified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public InsectFamily getFamily() {
        return family;
    }

    public void setFamily(InsectFamily family) {
        this.family = family;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getGbifId() {
        return gbifId;
    }

    public void setGbifId(Long gbifId) {
        this.gbifId = gbifId;
    }

    public List<InsectSpecies> getInsectSpeciesList() {
        return insectSpeciesList;
    }

    public void setInsectSpeciesList(List<InsectSpecies> insectSpeciesList) {
        this.insectSpeciesList = insectSpeciesList;
    }

    public void addInsectSpecies(InsectSpecies insectSpecies) {
        if (this.insectSpeciesList == null) {
            this.insectSpeciesList = new ArrayList<>();
        }
        this.insectSpeciesList.add(insectSpecies);
        insectSpecies.setGenus(this);
    }

    public void removeInsectSpecies(InsectSpecies insectSpecies) {
        if (this.insectSpeciesList != null) {
            this.insectSpeciesList.remove(insectSpecies);
            insectSpecies.setGenus(null);
        }
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }


    @Override
    public String toString() {
        return "InsectGenus{" +
            "id=" + id +
            ", family=" + family +
            ", name='" + name + '\'' +
            ", gbifId=" + gbifId +
            ", insectSpeciesList=" + insectSpeciesList +
            ", created=" + created +
            ", modified=" + modified +
            '}';
    }

}
