package org.kinsecta.webapp.api.exception.unchecked;

/**
 * This custom exception indicates arbitrary errors during upload of a ZIP file.
 * <p>
 * The exception and especially the response status is handled in our
 * {@linkplain org.kinsecta.webapp.api.exception.GlobalExceptionHandler GlobalExceptionHandler}.
 */
public class ZipUploadException extends RuntimeException {

    public ZipUploadException(String message) {
        super(message);
    }

}
