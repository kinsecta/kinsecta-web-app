package org.kinsecta.webapp.api.model.repositories;

import org.kinsecta.webapp.api.model.entities.MeasurementWingbeat;
import org.springframework.data.jpa.repository.JpaRepository;


public interface MeasurementWingbeatRepository extends JpaRepository<MeasurementWingbeat, Long> {

}
