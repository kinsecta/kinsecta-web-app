package org.kinsecta.webapp.api.model.entities;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "dataset_tag")
public class DatasetTag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;

    @ManyToMany(mappedBy = "datasetTags")
    private Set<Dataset> datasets = new HashSet<>();

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "auto_tag", nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
    private Boolean autoTag = false;

    @Column(name = "created", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW()")
    @Generated(GenerationTime.INSERT)
    @ReadOnlyProperty
    private LocalDateTime created;

    @Column(name = "modified", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW() ON UPDATE NOW()")
    @Generated(GenerationTime.ALWAYS)
    @ReadOnlyProperty
    private LocalDateTime modified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Dataset> getDatasets() {
        return datasets;
    }

    public void setDatasets(Set<Dataset> datasets) {
        this.datasets = datasets;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getAutoTag() {
        return autoTag;
    }

    public void setAutoTag(Boolean autoTag) {
        this.autoTag = autoTag;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }


    @Override
    public String toString() {
        return "DatasetTag{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", autoTag=" + autoTag +
            ", created=" + created +
            ", modified=" + modified +
            '}';
    }

}
