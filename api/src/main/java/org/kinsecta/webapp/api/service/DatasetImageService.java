package org.kinsecta.webapp.api.service;

import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.model.entities.Dataset;
import org.kinsecta.webapp.api.model.entities.DatasetImage;
import org.kinsecta.webapp.api.model.repositories.DatasetImageRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class DatasetImageService {

    private final DatasetImageRepository datasetImageRepository;


    public DatasetImageService(DatasetImageRepository datasetImageRepository) {
        this.datasetImageRepository = datasetImageRepository;
    }

    public DatasetImage getDatasetImage(Long id) {
        return datasetImageRepository.findById(id).orElseThrow(() -> new NotFoundException(String.format("Could not find DatasetImage with id %d", id)));
    }

    public Optional<DatasetImage> getOptionalDatasetImage(Long id) {
        return datasetImageRepository.findById(id);
    }

    public List<DatasetImage> getDatasetImagesForDataset(Dataset dataset) {
        return datasetImageRepository.findDatasetImagesByDataset(dataset);
    }

}
