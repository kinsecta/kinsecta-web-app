package org.kinsecta.webapp.api.config;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import org.apache.commons.lang3.LocaleUtils;
import org.kinsecta.webapp.api.cache.UploadStatusCache;
import org.kinsecta.webapp.api.s3.S3Client;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.servlet.LocaleResolver;
import reactor.netty.http.client.HttpClient;

import javax.validation.Validator;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


@Configuration
public class AppConfig {

    @Value("${app.locales.default}")
    private String defaultLocaleConfig;

    @Value("${app.locales.supported}")
    private String supportedLocalesConfig;

    @Value("${app.s3storage.access-key}")
    private String s3AccessKey;

    @Value("${app.s3storage.secret-key}")
    private String s3SecretKey;

    @Value("${app.s3storage.service-endpoint}")
    private String s3ServiceEndpoint;

    @Value("${app.gbif.base-url}")
    private String gbifApiBaseUrl;

    @Value("${app.gbif.timeout-in-ms}")
    private Integer gbifApiRequestTimeout;


    @Bean
    @Primary
    public LocaleResolver localeResolver() {
        CustomAcceptHeaderLocaleResolver resolver = new CustomAcceptHeaderLocaleResolver();

        Locale defaultLocale = LocaleUtils.toLocale(defaultLocaleConfig);
        resolver.setDefaultLocale(defaultLocale);

        List<Locale> supportedLocales = Arrays.stream(supportedLocalesConfig.split(",")).map(LocaleUtils::toLocale).toList();
        resolver.setSupportedLocales(supportedLocales);

        return resolver;
    }

    @Bean
    public Validator localValidatorFactoryBean() {
        return new LocalValidatorFactoryBean();
    }

    @Bean
    public S3Client s3Client() {
        S3Client s3Client = new S3Client();
        s3Client.setAccessKey(s3AccessKey);
        s3Client.setSecretKey(s3SecretKey);
        s3Client.setServiceEndpoint(s3ServiceEndpoint);
        return s3Client;
    }

    @Bean
    @Scope("singleton")
    public UploadStatusCache uploadStatusCacheSingleton() {
        return new UploadStatusCache();
    }


    @Bean
    public WebClient gbifWebclient() {
        HttpClient httpClient = HttpClient.create()
            .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, gbifApiRequestTimeout)
            .responseTimeout(Duration.ofMillis(gbifApiRequestTimeout))
            .doOnConnected(conn ->
                conn.addHandlerLast(new ReadTimeoutHandler(gbifApiRequestTimeout, TimeUnit.MILLISECONDS))
                    .addHandlerLast(new WriteTimeoutHandler(gbifApiRequestTimeout, TimeUnit.MILLISECONDS)));


        return WebClient.builder()
            .baseUrl(gbifApiBaseUrl)
            .clientConnector(new ReactorClientHttpConnector(httpClient))
            .build();
    }

}
