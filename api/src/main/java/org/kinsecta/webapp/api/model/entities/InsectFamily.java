package org.kinsecta.webapp.api.model.entities;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "insect_family")
public class InsectFamily {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "order_id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private InsectOrder order;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "gbif_id", columnDefinition = "BIGINT UNSIGNED")
    private Long gbifId;

    @OneToMany(mappedBy = "family", cascade = CascadeType.ALL)
    private List<InsectGenus> insectGenusList = new ArrayList<>();

    @Column(name = "created", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW()")
    @Generated(GenerationTime.INSERT)
    @ReadOnlyProperty
    private LocalDateTime created;

    @Column(name = "modified", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW() ON UPDATE NOW()")
    @Generated(GenerationTime.ALWAYS)
    @ReadOnlyProperty
    private LocalDateTime modified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public InsectOrder getOrder() {
        return order;
    }

    public void setOrder(InsectOrder order) {
        this.order = order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getGbifId() {
        return gbifId;
    }

    public void setGbifId(Long gbifId) {
        this.gbifId = gbifId;
    }

    public List<InsectGenus> getInsectGenusList() {
        return insectGenusList;
    }

    public void setInsectGenusList(List<InsectGenus> insectGenusList) {
        this.insectGenusList = insectGenusList;
    }

    public void addInsectGenus(InsectGenus insectGenus) {
        if (this.insectGenusList == null) {
            this.insectGenusList = new ArrayList<>();
        }
        this.insectGenusList.add(insectGenus);
        insectGenus.setFamily(this);
    }

    public void removeInsectGenus(InsectGenus insectGenus) {
        if (this.insectGenusList != null) {
            this.insectGenusList.remove(insectGenus);
            insectGenus.setFamily(null);
        }
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }


    @Override
    public String toString() {
        return "InsectFamily{" +
            "id=" + id +
            ", order=" + order +
            ", name='" + name + '\'' +
            ", gbifId=" + gbifId +
            ", insectGenusList=" + insectGenusList +
            ", created=" + created +
            ", modified=" + modified +
            '}';
    }

}
