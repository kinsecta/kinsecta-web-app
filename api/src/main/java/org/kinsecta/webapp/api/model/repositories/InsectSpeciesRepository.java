package org.kinsecta.webapp.api.model.repositories;

import org.kinsecta.webapp.api.model.entities.InsectSpecies;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface InsectSpeciesRepository extends JpaRepository<InsectSpecies, Long> {

    Optional<InsectSpecies> findByGbifId(Long gbifId);

    Optional<InsectSpecies> findByName(String name);

    List<InsectSpecies> findAllByNameContains(String query);

}
