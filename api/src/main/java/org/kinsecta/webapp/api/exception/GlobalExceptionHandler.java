package org.kinsecta.webapp.api.exception;

import org.kinsecta.webapp.api.exception.unchecked.*;
import org.kinsecta.webapp.api.v1.model.JsonError;
import org.kinsecta.webapp.api.v1.model.JsonErrorCause;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;


/**
 * A global Exception Handler for the application.
 *
 * @see <a href="https://reflectoring.io/spring-boot-exception-handling/">https://reflectoring.io/spring-boot-exception-handling/</a>
 */
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);


    /**
     * Set HTTP Status to '400 Bad Request' for a {@link ConstraintViolationException} which can happen
     * when request parameters, the request body oder query parameters fail validation (e.g. against the OpenAPI spec).
     *
     * @param exception a {@link ConstraintViolationException} instance
     * @param request   the current {@link HttpServletRequest}
     *
     * @return a {@link ResponseEntity} object with overridden HTTP Status and a {@link JsonError} object as body
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException exception, HttpServletRequest request) {
        return buildJsonErrorResponse(exception, "Request param or query param validation failed", HttpStatus.BAD_REQUEST, request);
    }

    /**
     * Set HTTP Status to '403 Forbidden' for a {@link ForbiddenException} which may be thrown
     * when access to a specific resource is forbidden.
     *
     * @param exception a {@link ForbiddenException} instance
     * @param request   the current {@link HttpServletRequest}
     *
     * @return a {@link ResponseEntity} object with overridden HTTP Status and a {@link JsonError} object as body
     */
    @ExceptionHandler(ForbiddenException.class)
    public ResponseEntity<Object> handleForbiddenException(ForbiddenException exception, HttpServletRequest request) {
        return buildJsonErrorResponse(exception, HttpStatus.FORBIDDEN, request);
    }

    /**
     * Set HTTP Status to '404 Not Found' for a {@link NotFoundException} which may be thrown
     * when an object or entity is not found.
     *
     * @param exception a {@link NotFoundException} instance
     * @param request   the current {@link HttpServletRequest}
     *
     * @return a {@link ResponseEntity} object with overridden HTTP Status and a {@link JsonError} object as body
     */
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Object> handleNotFoundException(NotFoundException exception, HttpServletRequest request) {
        return buildJsonErrorResponse(exception, HttpStatus.NOT_FOUND, request);
    }

    /**
     * Set HTTP Status to '400 Bad Request' for a {@link WrongArgumentException} which may be thrown
     * for arbitrary wrong arguments in any request.
     *
     * @param exception a {@link WrongArgumentException} instance
     * @param request   the current {@link HttpServletRequest}
     *
     * @return a {@link ResponseEntity} object with overridden HTTP Status and a {@link JsonError} object as body
     */
    @ExceptionHandler(WrongArgumentException.class)
    public ResponseEntity<Object> handleWrongArgumentException(WrongArgumentException exception, HttpServletRequest request) {
        return buildJsonErrorResponse(exception, HttpStatus.BAD_REQUEST, request);
    }

    /**
     * Set HTTP Status to '400 Bad Request' for a {@link ZipUploadException} which may be thrown
     * for arbitrary errors during upload of a ZIP file.
     *
     * @param exception a {@link ZipUploadException} instance
     * @param request   the current {@link HttpServletRequest}
     *
     * @return a {@link ResponseEntity} object with overridden HTTP Status and a {@link JsonError} object as body
     */
    @ExceptionHandler(ZipUploadException.class)
    public ResponseEntity<Object> handleZipUploadException(ZipUploadException exception, HttpServletRequest request) {
        return buildJsonErrorResponse(exception, HttpStatus.BAD_REQUEST, request);
    }

    /**
     * Set HTTP Status to '400 Bad Request' for a {@link UnsupportedQueryFilterException} which may be thrown
     * when a Query Parameter filter is not supported.
     *
     * @param exception a {@link UnsupportedQueryFilterException} instance
     * @param request   the current {@link HttpServletRequest}
     *
     * @return a {@link ResponseEntity} object with overridden HTTP Status and a {@link JsonError} object as body
     */
    @ExceptionHandler(UnsupportedQueryFilterException.class)
    public ResponseEntity<Object> handleUnsupportedQueryFilterException(UnsupportedQueryFilterException exception, HttpServletRequest request) {
        return buildJsonErrorResponse(exception, HttpStatus.BAD_REQUEST, request);
    }

    /**
     * Set HTTP Status to '410 Gone' for a {@link UploadCancelledException} which may be thrown
     * when an Upload has been cancelled in the meantime.
     *
     * @param exception a {@link UploadCancelledException} instance
     * @param request   the current {@link HttpServletRequest}
     *
     * @return a {@link ResponseEntity} object with overridden HTTP Status and a {@link JsonError} object as body
     */
    @ExceptionHandler(UploadCancelledException.class)
    public ResponseEntity<Object> handleUploadCancelledException(UploadCancelledException exception, HttpServletRequest request) {
        return buildJsonErrorResponse(exception, HttpStatus.GONE, request);
    }

    /**
     * Set HTTP Status to '403 Forbidden' for a {@link InvalidDownloadTokenException} which may be thrown
     * when the download token request parameter is invalid.
     *
     * @param exception a {@link InvalidDownloadTokenException} instance
     * @param request   the current {@link HttpServletRequest}
     *
     * @return a {@link ResponseEntity} object with overridden HTTP Status and a {@link JsonError} object as body
     */
    @ExceptionHandler(InvalidDownloadTokenException.class)
    public ResponseEntity<Object> handleInvalidDownloadTokenException(InvalidDownloadTokenException exception, HttpServletRequest request) {
        return buildJsonErrorResponse(exception, HttpStatus.FORBIDDEN, request);
    }

    /**
     * Set HTTP Status to '500 Internal Server Error' for a {@link StorageRuntimeException} which may be thrown
     * when there is a problem interacting with the s3 storage.
     *
     * @param exception a {@link StorageRuntimeException} instance
     * @param request   the current {@link HttpServletRequest}
     *
     * @return q {@link ResponseEntity} object with the overridden HTTP Status and a {@link  JsonError} object as body
     */
    @ExceptionHandler(StorageRuntimeException.class)
    public ResponseEntity<Object> handleStorageRuntimeException(StorageRuntimeException exception, HttpServletRequest request) {
        return buildJsonErrorResponse(exception, HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    /**
     * Set HTTP Status to '502 Bad Gateway' for a {@link GbifApiException} which may be thrown
     * when the communication to the GBIF API fails.
     *
     * @param exception a {@link GbifApiException} instance
     * @param request   the current {@link HttpServletRequest}
     *
     * @return a {@link ResponseEntity} object with overridden HTTP Status and a {@link JsonError} object as body
     */
    @ExceptionHandler(GbifApiException.class)
    public ResponseEntity<Object> handleGbifApiException(GbifApiException exception, HttpServletRequest request) {
        return buildJsonErrorResponse(exception, HttpStatus.BAD_GATEWAY, request);
    }

    /**
     * Set HTTP Status to '400 Bad Request' for a {@link GbifApiResponseDataException} which may be thrown
     * when there was a problem with the data returned by the GBIF API.
     *
     * @param exception a {@link GbifApiResponseDataException} instance
     * @param request   the current {@link HttpServletRequest}
     *
     * @return a {@link ResponseEntity} object with overridden HTTP Status and a {@link JsonError} object as body
     */
    @ExceptionHandler(GbifApiResponseDataException.class)
    public ResponseEntity<Object> handleGbifApiDataException(GbifApiResponseDataException exception, HttpServletRequest request) {
        return buildJsonErrorResponse(exception, HttpStatus.BAD_REQUEST, request);
    }


    @Override
    public ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        if (request instanceof ServletWebRequest servletWebRequest) {
            HttpServletRequest httpServletRequest = servletWebRequest.getRequest();
            return buildJsonErrorResponse(ex, status, httpServletRequest);
        } else {
            return buildJsonErrorResponse(ex, status);
        }
    }

    private ResponseEntity<Object> buildJsonErrorResponse(Exception exception, HttpStatus httpStatus) {
        return buildJsonErrorResponse(exception, exception.getMessage(), httpStatus, "UNKNOWN");
    }

    private ResponseEntity<Object> buildJsonErrorResponse(Exception exception, HttpStatus httpStatus, HttpServletRequest request) {
        return buildJsonErrorResponse(exception, exception.getMessage(), httpStatus, request.getServletPath());
    }

    private ResponseEntity<Object> buildJsonErrorResponse(Exception exception, String message, HttpStatus httpStatus, HttpServletRequest request) {
        return buildJsonErrorResponse(exception, message, httpStatus, request.getServletPath());
    }

    private ResponseEntity<Object> buildJsonErrorResponse(Exception exception, String message, HttpStatus httpStatus, String requestPath) {
        JsonError jsonError = buildJsonError(exception, message, httpStatus, requestPath);
        log.error("[Return HTTP Status {}] {}", httpStatus.value(), message, exception);
        return ResponseEntity.status(httpStatus).body(jsonError);
    }

    public static JsonError buildJsonError(Exception exception, String message, HttpStatus httpStatus, String path) {
        JsonError errorResponse = new JsonError();
        errorResponse.setTimestamp(LocalDateTime.now());
        errorResponse.setStatus(httpStatus.value());
        errorResponse.setError(httpStatus.getReasonPhrase());
        errorResponse.setException(exception.getClass().getName());
        errorResponse.setMessage(message);
        errorResponse.setPath(path);

        if (!message.equals(exception.getMessage())) {
            errorResponse.setOriginalMessage(exception.getMessage());
        }

        Throwable cause = exception.getCause();
        if (cause != null) {
            JsonErrorCause responseCause = new JsonErrorCause();
            responseCause.setException(cause.getClass().getName());
            responseCause.setMessage(cause.getMessage());
            errorResponse.setCause(responseCause);
        }

        return errorResponse;
    }

}
