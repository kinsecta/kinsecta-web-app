package org.kinsecta.webapp.api.util;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


/**
 * This implementation of the Spring 'Converter' interface converts dateTime strings to LocalDateTime
 * by specifying an explicit conversion strategy via a specific DateTimeFormatter.
 * Inspired by https://medium.com/swlh/spring-boot-and-the-localdate-request-parameter-2c9cdbb085bb
 */
@Component
public class LocalDateTimeConverter implements Converter<String, LocalDateTime> {

    private static final Map<String, DateTimeFormatter> DATE_TIME_FORMATTERS = new HashMap<>();

    public static final String DATE_TIME_FORMAT_YYYY_MM_DD_SPACE_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_TIME_FORMAT_YYYY_MM_DD_UNDERSCORE_HH_MM_SS = "yyyy-MM-dd_HH-mm-ss";

    // Add all LocalDate patterns you wish to support in this application.
    // In the best case, the pattern should already exist as a predefined static DateTimeFormatter.
    static {
        DATE_TIME_FORMATTERS.put(DATE_TIME_FORMAT_YYYY_MM_DD_SPACE_HH_MM_SS, DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_YYYY_MM_DD_SPACE_HH_MM_SS));
        DATE_TIME_FORMATTERS.put("yyyy-MM-ddTHH:mm:ss", DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        DATE_TIME_FORMATTERS.put(DATE_TIME_FORMAT_YYYY_MM_DD_UNDERSCORE_HH_MM_SS, DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_YYYY_MM_DD_UNDERSCORE_HH_MM_SS));
    }


    public static DateTimeFormatter getFormatterForPattern(String pattern) {
        return DATE_TIME_FORMATTERS.get(pattern);
    }

    @Override
    public LocalDateTime convert(String s) {
        for (Map.Entry<String, DateTimeFormatter> formatter : Collections.unmodifiableMap(DATE_TIME_FORMATTERS).entrySet()) {
            try {
                return LocalDateTime.parse(s, formatter.getValue());
            } catch (DateTimeParseException ex) {
                // deliberate empty block so that all parsers run
            }
        }

        throw new DateTimeException(String.format("unable to parse '%s' as LocalDateTime, supported formats are '%s'",
            s, String.join("', '", DATE_TIME_FORMATTERS.keySet())));
    }

}
