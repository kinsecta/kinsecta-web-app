package org.kinsecta.webapp.api.model.entities;

import org.hibernate.annotations.*;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "dataset")
public class Dataset {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "upload_id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Upload upload;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private User user;

    @ManyToOne(optional = false)
    @JoinColumn(name = "sensor_id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Sensor sensor;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "video_s3_object_id", columnDefinition = "BIGINT UNSIGNED")
    private S3Object videoS3Object;

    // #79 use HashSet with FetchType.EAGER otherwise there's a chance Hibernate returns duplicates
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "datasets_dataset_tags",
        joinColumns = @JoinColumn(name = "dataset_id", referencedColumnName = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED"),
        inverseJoinColumns = @JoinColumn(name = "dataset_tag_id", referencedColumnName = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    )
    private Set<DatasetTag> datasetTags = new HashSet<>();

    // #79 use HashSet with FetchType.EAGER otherwise there's a chance Hibernate returns duplicates
    @OneToMany(mappedBy = "dataset", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Classification> classifications = new HashSet<>();

    // #79 use HashSet with FetchType.EAGER otherwise there's a chance Hibernate returns duplicates
    @OneToMany(mappedBy = "dataset", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private Set<DatasetImage> datasetImages = new HashSet<>();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "measurement_wingbeat_id", columnDefinition = "BIGINT UNSIGNED")
    private MeasurementWingbeat measurementWingbeat;

    @Column(name = "measurement_datetime", nullable = false, columnDefinition = "DATETIME(3)")
    private LocalDateTime measurementDatetime;

    @Column(name = "measurement_size_insect_length", precision = 3, scale = 1)
    private BigDecimal measurementSizeInsectLength;

    @Column(name = "measurement_size_insect_width", precision = 3, scale = 1)
    private BigDecimal measurementSizeInsectWidth;

    @Column(name = "measurement_temperature", precision = 3, scale = 1)
    private BigDecimal measurementTemperature;

    @Column(name = "measurement_air_pressure", precision = 4, scale = 3)
    private BigDecimal measurementAirPressure;

    @Column(name = "measurement_humidity", precision = 4, scale = 1)
    private BigDecimal measurementHumidity;

    @Column(name = "measurement_luminosity", precision = 5, scale = 1)
    private BigDecimal measurementLuminosity;

    @Column(name = "measurement_spectrum_channel_415nm", columnDefinition = "SMALLINT")
    private Integer measurementSpectrumChannel415nm;

    @Column(name = "measurement_spectrum_channel_445nm", columnDefinition = "SMALLINT")
    private Integer measurementSpectrumChannel445nm;

    @Column(name = "measurement_spectrum_channel_480nm", columnDefinition = "SMALLINT")
    private Integer measurementSpectrumChannel480nm;

    @Column(name = "measurement_spectrum_channel_515nm", columnDefinition = "SMALLINT")
    private Integer measurementSpectrumChannel515nm;

    @Column(name = "measurement_spectrum_channel_555nm", columnDefinition = "SMALLINT")
    private Integer measurementSpectrumChannel555nm;

    @Column(name = "measurement_spectrum_channel_590nm", columnDefinition = "SMALLINT")
    private Integer measurementSpectrumChannel590nm;

    @Column(name = "measurement_spectrum_channel_630nm", columnDefinition = "SMALLINT")
    private Integer measurementSpectrumChannel630nm;

    @Column(name = "measurement_spectrum_channel_680nm", columnDefinition = "SMALLINT")
    private Integer measurementSpectrumChannel680nm;

    @Column(name = "measurement_spectrum_near_ir", columnDefinition = "SMALLINT")
    private Integer measurementSpectrumNearIr;

    @Column(name = "measurement_spectrum_clear_light", columnDefinition = "SMALLINT")
    private Integer measurementSpectrumClearLight;

    @Column(name = "measurement_particulate_matter_particles_03um", columnDefinition = "SMALLINT")
    private Integer measurementParticulateMatterParticles03um;

    @Column(name = "measurement_particulate_matter_particles_05um", columnDefinition = "SMALLINT")
    private Integer measurementParticulateMatterParticles05um;

    @Column(name = "measurement_particulate_matter_particles_10um", columnDefinition = "SMALLINT")
    private Integer measurementParticulateMatterParticles10um;

    @Column(name = "measurement_particulate_matter_particles_25um", columnDefinition = "SMALLINT")
    private Integer measurementParticulateMatterParticles25um;

    @Column(name = "measurement_particulate_matter_particles_50um", columnDefinition = "SMALLINT")
    private Integer measurementParticulateMatterParticles50um;

    @Column(name = "measurement_particulate_matter_particles_100um", columnDefinition = "SMALLINT")
    private Integer measurementParticulateMatterParticles100um;

    @Column(name = "measurement_particulate_matter_pm1", columnDefinition = "SMALLINT")
    private Integer measurementParticulateMatterPm1;

    @Column(name = "measurement_particulate_matter_pm2p5", columnDefinition = "SMALLINT")
    private Integer measurementParticulateMatterPm2p5;

    @Column(name = "measurement_particulate_matter_pm10", columnDefinition = "SMALLINT")
    private Integer measurementParticulateMatterPm10;

    @Column(name = "measurement_rainfall", precision = 5, scale = 2)
    private BigDecimal measurementRainfall;

    @Column(name = "measurement_wind_sensor_speed", precision = 5, scale = 2)
    private BigDecimal measurementWindSensorSpeed;

    @Column(name = "measurement_wind_sensor_direction", length = 10)
    private String measurementWindSensorDirection;

    @Column(name = "md5_hash", length = 32, unique = true)
    private String md5Hash;

    @Column(name = "computed_modified", nullable = false, columnDefinition = "DATETIME(3) DEFAULT NOW() ON UPDATE NOW()")
    @UpdateTimestamp
    private LocalDateTime computedModified;

    @Column(name = "created", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW()")
    @Generated(GenerationTime.INSERT)
    @ReadOnlyProperty
    private LocalDateTime created;

    @Column(name = "modified", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW() ON UPDATE NOW()")
    @Generated(GenerationTime.ALWAYS)
    @ReadOnlyProperty
    private LocalDateTime modified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Upload getUpload() {
        return upload;
    }

    public void setUpload(Upload upload) {
        this.upload = upload;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Sensor getSensor() {
        return sensor;
    }

    public void setSensor(Sensor sensor) {
        this.sensor = sensor;
    }

    public S3Object getVideoS3Object() {
        return videoS3Object;
    }

    public void setVideoS3Object(S3Object videoS3Object) {
        this.videoS3Object = videoS3Object;
    }

    public Set<DatasetTag> getDatasetTags() {
        return datasetTags;
    }

    public void setDatasetTags(Set<DatasetTag> datasetTags) {
        this.datasetTags = datasetTags;
    }

    public void addDatasetTag(DatasetTag datasetTag) {
        if (this.datasetTags == null) {
            this.datasetTags = new HashSet<>();
        }
        this.datasetTags.add(datasetTag);
    }

    public void removeDatasetTag(DatasetTag datasetTag) {
        if (this.datasetTags != null) {
            this.datasetTags.remove(datasetTag);
        }
    }

    public Set<Classification> getClassifications() {
        return classifications;
    }

    public void setClassifications(Set<Classification> classifications) {
        this.classifications = classifications;
    }

    public void addClassification(Classification classification) {
        if (this.classifications == null) {
            this.classifications = new HashSet<>();
        }
        this.classifications.add(classification);
        classification.setDataset(this);
    }

    public void removeClassification(Classification classification) {
        if (this.classifications != null) {
            this.classifications.remove(classification);
            classification.setDataset(null);
        }
    }

    public Set<DatasetImage> getDatasetImages() {
        return datasetImages;
    }

    public void setDatasetImages(Set<DatasetImage> classifications) {
        this.datasetImages = classifications;
    }

    public void addDatasetImage(DatasetImage datasetImage) {
        if (this.datasetImages == null) {
            this.datasetImages = new HashSet<>();
        }
        this.datasetImages.add(datasetImage);
        datasetImage.setDataset(this);
    }

    public void removeDatasetImage(DatasetImage datasetImage) {
        if (this.datasetImages != null) {
            this.datasetImages.remove(datasetImage);
            datasetImage.setDataset(null);
        }
    }

    public MeasurementWingbeat getMeasurementWingbeat() {
        return measurementWingbeat;
    }

    public void setMeasurementWingbeat(MeasurementWingbeat measurementWingbeat) {
        this.measurementWingbeat = measurementWingbeat;
    }

    public LocalDateTime getMeasurementDatetime() {
        return measurementDatetime;
    }

    public void setMeasurementDatetime(LocalDateTime measurementDatetime) {
        this.measurementDatetime = measurementDatetime;
    }

    public BigDecimal getMeasurementSizeInsectLength() {
        return measurementSizeInsectLength;
    }

    public void setMeasurementSizeInsectLength(BigDecimal measurementSizeInsectLength) {
        this.measurementSizeInsectLength = measurementSizeInsectLength;
    }

    public BigDecimal getMeasurementSizeInsectWidth() {
        return measurementSizeInsectWidth;
    }

    public void setMeasurementSizeInsectWidth(BigDecimal measurementSizeInsectWidth) {
        this.measurementSizeInsectWidth = measurementSizeInsectWidth;
    }

    public BigDecimal getMeasurementTemperature() {
        return measurementTemperature;
    }

    public void setMeasurementTemperature(BigDecimal measurementTemperature) {
        this.measurementTemperature = measurementTemperature;
    }

    public BigDecimal getMeasurementAirPressure() {
        return measurementAirPressure;
    }

    public void setMeasurementAirPressure(BigDecimal measurementAirPressure) {
        this.measurementAirPressure = measurementAirPressure;
    }

    public BigDecimal getMeasurementHumidity() {
        return measurementHumidity;
    }

    public void setMeasurementHumidity(BigDecimal measurementHumidity) {
        this.measurementHumidity = measurementHumidity;
    }

    public BigDecimal getMeasurementLuminosity() {
        return measurementLuminosity;
    }

    public void setMeasurementLuminosity(BigDecimal measurementLuminosity) {
        this.measurementLuminosity = measurementLuminosity;
    }

    public Integer getMeasurementSpectrumChannel415nm() {
        return measurementSpectrumChannel415nm;
    }

    public void setMeasurementSpectrumChannel415nm(Integer measurementSpectrumChannel415nm) {
        this.measurementSpectrumChannel415nm = measurementSpectrumChannel415nm;
    }

    public Integer getMeasurementSpectrumChannel445nm() {
        return measurementSpectrumChannel445nm;
    }

    public void setMeasurementSpectrumChannel445nm(Integer measurementSpectrumChannel445nm) {
        this.measurementSpectrumChannel445nm = measurementSpectrumChannel445nm;
    }

    public Integer getMeasurementSpectrumChannel480nm() {
        return measurementSpectrumChannel480nm;
    }

    public void setMeasurementSpectrumChannel480nm(Integer measurementSpectrumChannel480nm) {
        this.measurementSpectrumChannel480nm = measurementSpectrumChannel480nm;
    }

    public Integer getMeasurementSpectrumChannel515nm() {
        return measurementSpectrumChannel515nm;
    }

    public void setMeasurementSpectrumChannel515nm(Integer measurementSpectrumChannel515nm) {
        this.measurementSpectrumChannel515nm = measurementSpectrumChannel515nm;
    }

    public Integer getMeasurementSpectrumChannel555nm() {
        return measurementSpectrumChannel555nm;
    }

    public void setMeasurementSpectrumChannel555nm(Integer measurementSpectrumChannel555nm) {
        this.measurementSpectrumChannel555nm = measurementSpectrumChannel555nm;
    }

    public Integer getMeasurementSpectrumChannel590nm() {
        return measurementSpectrumChannel590nm;
    }

    public void setMeasurementSpectrumChannel590nm(Integer measurementSpectrumChannel590nm) {
        this.measurementSpectrumChannel590nm = measurementSpectrumChannel590nm;
    }

    public Integer getMeasurementSpectrumChannel630nm() {
        return measurementSpectrumChannel630nm;
    }

    public void setMeasurementSpectrumChannel630nm(Integer measurementSpectrumChannel630nm) {
        this.measurementSpectrumChannel630nm = measurementSpectrumChannel630nm;
    }

    public Integer getMeasurementSpectrumChannel680nm() {
        return measurementSpectrumChannel680nm;
    }

    public void setMeasurementSpectrumChannel680nm(Integer measurementSpectrumChannel680nm) {
        this.measurementSpectrumChannel680nm = measurementSpectrumChannel680nm;
    }

    public Integer getMeasurementSpectrumNearIr() {
        return measurementSpectrumNearIr;
    }

    public void setMeasurementSpectrumNearIr(Integer measurementSpectrumNearIr) {
        this.measurementSpectrumNearIr = measurementSpectrumNearIr;
    }

    public Integer getMeasurementParticulateMatterPm1() {
        return measurementParticulateMatterPm1;
    }

    public void setMeasurementParticulateMatterPm1(Integer measurementParticulateMatterPm1) {
        this.measurementParticulateMatterPm1 = measurementParticulateMatterPm1;
    }

    public Integer getMeasurementParticulateMatterPm2p5() {
        return measurementParticulateMatterPm2p5;
    }

    public void setMeasurementParticulateMatterPm2p5(Integer measurementParticulateMatterPm2p5) {
        this.measurementParticulateMatterPm2p5 = measurementParticulateMatterPm2p5;
    }

    public Integer getMeasurementParticulateMatterPm10() {
        return measurementParticulateMatterPm10;
    }

    public void setMeasurementParticulateMatterPm10(Integer measurementParticulateMatterPm10) {
        this.measurementParticulateMatterPm10 = measurementParticulateMatterPm10;
    }

    public Integer getMeasurementSpectrumClearLight() {
        return measurementSpectrumClearLight;
    }

    public void setMeasurementSpectrumClearLight(Integer measurementSpectrumClearLight) {
        this.measurementSpectrumClearLight = measurementSpectrumClearLight;
    }

    public Integer getMeasurementParticulateMatterParticles03um() {
        return measurementParticulateMatterParticles03um;
    }

    public void setMeasurementParticulateMatterParticles03um(Integer measurementParticulateMatterParticles03um) {
        this.measurementParticulateMatterParticles03um = measurementParticulateMatterParticles03um;
    }

    public Integer getMeasurementParticulateMatterParticles05um() {
        return measurementParticulateMatterParticles05um;
    }

    public void setMeasurementParticulateMatterParticles05um(Integer measurementParticulateMatterParticles05um) {
        this.measurementParticulateMatterParticles05um = measurementParticulateMatterParticles05um;
    }

    public Integer getMeasurementParticulateMatterParticles10um() {
        return measurementParticulateMatterParticles10um;
    }

    public void setMeasurementParticulateMatterParticles10um(Integer measurementParticulateMatterParticles10um) {
        this.measurementParticulateMatterParticles10um = measurementParticulateMatterParticles10um;
    }

    public Integer getMeasurementParticulateMatterParticles25um() {
        return measurementParticulateMatterParticles25um;
    }

    public void setMeasurementParticulateMatterParticles25um(Integer measurementParticulateMatterParticles25um) {
        this.measurementParticulateMatterParticles25um = measurementParticulateMatterParticles25um;
    }

    public Integer getMeasurementParticulateMatterParticles50um() {
        return measurementParticulateMatterParticles50um;
    }

    public void setMeasurementParticulateMatterParticles50um(Integer measurementParticulateMatterParticles50um) {
        this.measurementParticulateMatterParticles50um = measurementParticulateMatterParticles50um;
    }

    public Integer getMeasurementParticulateMatterParticles100um() {
        return measurementParticulateMatterParticles100um;
    }

    public void setMeasurementParticulateMatterParticles100um(Integer measurementParticulateMatterParticles100um) {
        this.measurementParticulateMatterParticles100um = measurementParticulateMatterParticles100um;
    }

    public BigDecimal getMeasurementRainfall() {
        return measurementRainfall;
    }

    public void setMeasurementRainfall(BigDecimal measurementRainfall) {
        this.measurementRainfall = measurementRainfall;
    }

    public BigDecimal getMeasurementWindSensorSpeed() {
        return measurementWindSensorSpeed;
    }

    public void setMeasurementWindSensorSpeed(BigDecimal measurementWindSensorSpeed) {
        this.measurementWindSensorSpeed = measurementWindSensorSpeed;
    }

    public String getMeasurementWindSensorDirection() {
        return measurementWindSensorDirection;
    }

    public void setMeasurementWindSensorDirection(String measurementWindSensorDirection) {
        this.measurementWindSensorDirection = measurementWindSensorDirection;
    }

    public String getMd5Hash() {
        return md5Hash;
    }

    public void setMd5Hash(String md5Hash) {
        this.md5Hash = md5Hash;
    }

    public LocalDateTime getComputedModified() {
        return computedModified;
    }

    public void updateComputedModified() {
        this.computedModified = LocalDateTime.now();
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    @Override
    public String toString() {
        return "Dataset{" +
            "id=" + id +
            ", upload=" + upload +
            ", user=" + user +
            ", sensor=" + sensor +
            ", videoS3Object=" + videoS3Object +
            ", datasetTags=" + datasetTags +
            ", classifications=" + classifications +
            ", datasetImages=" + datasetImages +
            ", measurementWingbeat=" + measurementWingbeat +
            ", measurementDatetime=" + measurementDatetime +
            ", measurementSizeInsectLength=" + measurementSizeInsectLength +
            ", measurementSizeInsectWidth=" + measurementSizeInsectWidth +
            ", measurementTemperature=" + measurementTemperature +
            ", measurementAirPressure=" + measurementAirPressure +
            ", measurementHumidity=" + measurementHumidity +
            ", measurementLuminosity=" + measurementLuminosity +
            ", measurementSpectrumChannel415nm=" + measurementSpectrumChannel415nm +
            ", measurementSpectrumChannel445nm=" + measurementSpectrumChannel445nm +
            ", measurementSpectrumChannel480nm=" + measurementSpectrumChannel480nm +
            ", measurementSpectrumChannel515nm=" + measurementSpectrumChannel515nm +
            ", measurementSpectrumChannel555nm=" + measurementSpectrumChannel555nm +
            ", measurementSpectrumChannel590nm=" + measurementSpectrumChannel590nm +
            ", measurementSpectrumChannel630nm=" + measurementSpectrumChannel630nm +
            ", measurementSpectrumChannel680nm=" + measurementSpectrumChannel680nm +
            ", measurementSpectrumNearIr=" + measurementSpectrumNearIr +
            ", measurementSpectrumClearLight=" + measurementSpectrumClearLight +
            ", measurementParticulateMatterParticles03um=" + measurementParticulateMatterParticles03um +
            ", measurementParticulateMatterParticles05um=" + measurementParticulateMatterParticles05um +
            ", measurementParticulateMatterParticles10um=" + measurementParticulateMatterParticles10um +
            ", measurementParticulateMatterParticles25um=" + measurementParticulateMatterParticles25um +
            ", measurementParticulateMatterParticles50um=" + measurementParticulateMatterParticles50um +
            ", measurementParticulateMatterParticles100um=" + measurementParticulateMatterParticles100um +
            ", measurementParticulateMatterPm1=" + measurementParticulateMatterPm1 +
            ", measurementParticulateMatterPm2p5=" + measurementParticulateMatterPm2p5 +
            ", measurementParticulateMatterPm10=" + measurementParticulateMatterPm10 +
            ", measurementRainfall=" + measurementRainfall +
            ", measurementWindSensorSpeed=" + measurementWindSensorSpeed +
            ", measurementWindSensorDirection='" + measurementWindSensorDirection + '\'' +
            ", md5Hash='" + md5Hash + '\'' +
            ", computedModified=" + computedModified +
            ", created=" + created +
            ", modified=" + modified +
            '}';
    }

}
