package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.model.entities.DatasetImage;
import org.kinsecta.webapp.api.service.DatasetImageService;
import org.kinsecta.webapp.api.v1.model.DatasetImageDto;
import org.kinsecta.webapp.api.v1.model.IdObject;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@Mapper(
    componentModel = "spring",
    uses = {
        DtoMapperConfig.class,
        S3ObjectDtoMapper.class
    },
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public abstract class DatasetImageDtoMapper {

    @Autowired
    private DatasetImageService datasetImageService;


    public abstract DatasetImageDto datasetImageToDatasetImageDto(DatasetImage datasetImage);

    public abstract DatasetImage datasetImageDtoToDatasetImage(DatasetImageDto datasetImageDto);

    public DatasetImage idObjectToDatasetImage(IdObject idObject) {
        if (idObject != null && idObject.getId() != null) {
            return datasetImageService.getDatasetImage(idObject.getId());
        }
        return null;
    }

    public IdObject datasetImageToIdObject(DatasetImage datasetImage) {
        if (datasetImage == null) {
            return null;
        }
        return new IdObject().id(datasetImage.getId());
    }

    public static IdObject datasetImageIdToIdObject(Long datasetImageId) {
        return new IdObject().id(datasetImageId);
    }

    public static List<IdObject> datasetImageIdStringListToIdObjectList(String datasetImageIdsAsString) {
        if (datasetImageIdsAsString == null) {
            return new ArrayList<>();
        }
        List<String> datasetImageIds = List.of(datasetImageIdsAsString.split(";"));
        return datasetImageIds.stream().map(datasetImageId -> datasetImageIdToIdObject(Long.parseLong(datasetImageId))).toList();
    }

    public List<DatasetImageDto> datasetImageIdStringListToDatasetImageDto(String datasetImageIdsAsString) {
        if (datasetImageIdsAsString == null) {
            return new ArrayList<>();
        }
        List<IdObject> datasetImageIdsAsIdObject = datasetImageIdStringListToIdObjectList(datasetImageIdsAsString);
        return idObjectListToDatasetImageSet(datasetImageIdsAsIdObject).stream().map(this::datasetImageToDatasetImageDto).toList();
    }

    public abstract List<IdObject> datasetImageSetToIdObjectList(Set<DatasetImage> datasetImageSet);

    public abstract Set<DatasetImage> idObjectListToDatasetImageSet(List<IdObject> idObjectList);

}
