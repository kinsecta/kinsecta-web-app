package org.kinsecta.webapp.api.exception.unchecked;


/**
 * This custom exception indicates that access to a specific resource is forbidden.
 * <p>
 * The exception and especially the response status is handled in our
 * {@linkplain org.kinsecta.webapp.api.exception.GlobalExceptionHandler GlobalExceptionHandler}.
 */
public class ForbiddenException extends RuntimeException {

    public ForbiddenException(String message) {
        super(message);
    }

}
