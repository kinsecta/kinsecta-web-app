package org.kinsecta.webapp.api.service;

import org.hibernate.Hibernate;
import org.kinsecta.webapp.api.cache.UploadStatusCache;
import org.kinsecta.webapp.api.context.KinsectaMeasurementUploadContext;
import org.kinsecta.webapp.api.exception.checked.InvalidUploadZipStructureException;
import org.kinsecta.webapp.api.exception.checked.RollbackS3Exception;
import org.kinsecta.webapp.api.exception.unchecked.ForbiddenException;
import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.exception.unchecked.UploadCancelledException;
import org.kinsecta.webapp.api.exception.unchecked.ZipUploadException;
import org.kinsecta.webapp.api.mail.UploadMailService;
import org.kinsecta.webapp.api.model.entities.*;
import org.kinsecta.webapp.api.model.mapping.S3ObjectDtoMapper;
import org.kinsecta.webapp.api.model.repositories.UploadRepository;
import org.kinsecta.webapp.api.s3.StorageException;
import org.kinsecta.webapp.api.util.ApiUtil;
import org.kinsecta.webapp.api.util.PageableHelper;
import org.kinsecta.webapp.api.util.TempDirectoryHelper;
import org.kinsecta.webapp.api.v1.model.S3ObjectDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;


@Service
public class UploadService {

    private static final Logger logger = LoggerFactory.getLogger(UploadService.class);

    private final UploadRepository uploadRepository;
    private final TempDirectoryHelper tempDirectoryHelper;
    private final KinsectaMeasurementImportService kinsectaMeasurementImportService;
    private final S3ObjectDtoMapper s3ObjectDtoMapper;
    private final S3ObjectService s3ObjectService;
    private final UploadMailService uploadMailService;
    private final UploadStatusCache uploadStatusCache;
    private final EntityManager entityManager;


    public UploadService(UploadRepository uploadRepository, TempDirectoryHelper tempDirectoryHelper, KinsectaMeasurementImportService kinsectaMeasurementImportService, S3ObjectDtoMapper s3ObjectDtoMapper, S3ObjectService s3ObjectService, UploadMailService uploadMailService, UploadStatusCache uploadStatusCache, EntityManager entityManager) {
        this.uploadRepository = uploadRepository;
        this.tempDirectoryHelper = tempDirectoryHelper;
        this.kinsectaMeasurementImportService = kinsectaMeasurementImportService;
        this.s3ObjectDtoMapper = s3ObjectDtoMapper;
        this.s3ObjectService = s3ObjectService;
        this.uploadMailService = uploadMailService;
        this.uploadStatusCache = uploadStatusCache;
        this.entityManager = entityManager;
    }


    @Transactional
    public Upload createUploadForUser(User user) {
        Upload upload = new Upload();
        upload.setUser(user);
        upload.setStatus(TransferStatus.IN_PROGRESS);
        Upload savedUpload = uploadRepository.save(upload);
        uploadStatusCache.addUploadStatus(savedUpload.getId(), savedUpload.getStatus());
        return savedUpload;
    }

    @Transactional(readOnly = true)
    public Page<Upload> getAllUploadsForUserIdAndUserRole(Long userId, UserRole userRole, Pageable pageable) {
        // When current user has UserRole=DATA_COLLECTOR, only return the Uploads of the User, otherwise all
        Page<Upload> pagedResult = (userRole.equals(UserRole.DATA_COLLECTOR)) ? uploadRepository.findAllByUserId(userId, pageable) : uploadRepository.findAll(pageable);
        return PageableHelper.returnPageOrEmpty(pagedResult);
    }

    @Transactional(readOnly = true)
    public Upload getUpload(Long id) {
        return uploadRepository.findById(id).orElseThrow(() -> new NotFoundException(String.format("Could not find Upload with id %d", id)));
    }

    @Transactional(readOnly = true)
    public Optional<Upload> getOptionalUpload(Long id) {
        return uploadRepository.findById(id);
    }

    @Transactional
    public Upload getUploadWithDatasets(Long id) {
        Upload upload = getUpload(id);
        Hibernate.initialize(upload.getDatasets());
        return upload;
    }

    public Upload saveUpload(Upload upload) {
        return uploadRepository.save(upload);
    }

    public Upload finishUpload(Upload upload, TransferStatus status) {
        upload.setFinished(LocalDateTime.now());
        upload.setStatus(status);
        uploadStatusCache.removeUploadStatus(upload.getId());
        return saveUpload(upload);
    }

    public TransferStatus updateTransferStatusOfUpload(Long uploadId, TransferStatus newTransferStatus, Long userId, UserRole userRole) {
        Upload upload = getUpload(uploadId);

        // Assert that only the user who created the upload can change the status
        if (userRole.equals(UserRole.DATA_COLLECTOR) && !upload.getUser().getId().equals(userId)) {
            throw new ForbiddenException("You are not allowed to update the TransferStatus of an Upload of another user!");
        }

        // Assert that TransferStatus can only be updated from IN_PROGRESS to CANCELLED
        if (!(upload.getStatus().equals(TransferStatus.IN_PROGRESS) && newTransferStatus.equals(TransferStatus.CANCELLED))) {
            throw new ForbiddenException("You can only update the TransferStatus of an Upload with status IN_PROGRESS and change it to CANCELLED!");
        }

        // Assert that an Admin or TeamMember can only cancel uploads of another user if the Upload is older than 2 hours
        if (!userRole.equals(UserRole.DATA_COLLECTOR)
            && !upload.getUser().getId().equals(userId)
            && LocalDateTime.now().minus(2, ChronoUnit.HOURS).isBefore(upload.getCreated())
        ) {
            throw new ForbiddenException("Cancelling an Upload is only possible for Uploads older than 2 hours!");
        }

        upload.setStatus(newTransferStatus);
        logger.debug(UploadCancelledException.MESSAGE);
        TransferStatus status = saveUpload(upload).getStatus();
        uploadStatusCache.addUploadStatus(upload.getId(), status);
        return status;
    }

    public S3ObjectDto handleS3ObjectUpload(Upload upload, Resource body) {
        // Create a new KinsectaMeasurementUploadContext to save errors during processing of all measurements
        KinsectaMeasurementUploadContext context = new KinsectaMeasurementUploadContext();
        context.setUpload(upload);

        // Create a random upload temp directory in our app temp dir
        File uploadTempDir = tempDirectoryHelper.createRandomTempSubDirectory();
        context.setTempDir(uploadTempDir);

        // Handle zip upload and create the S3Object for the ZIP -> returns Optional.empty() if errors occurred
        Optional<Upload> savedUploadWithS3ObjectOpt = handleZipUpload(context, body);
        if (savedUploadWithS3ObjectOpt.isEmpty()) {
            // ZIP failures, abort the upload process
            abortUploadProcessDueToErrors(context);
        } else {
            upload = savedUploadWithS3ObjectOpt.get();
        }

        try {
            // Handles the creation of all necessary entities for all raw KinsectaMeasurement datasets in the upload zip file.
            // This needs to be called from the controller instead of from within the service, or the @Transactional will not work.
            // See https://stackoverflow.com/questions/3423972/spring-transaction-method-call-by-the-method-within-the-same-class-does-not-wo
            kinsectaMeasurementImportService.processMeasurementSubfoldersInUnzippedUploadDir(context);

        } catch (RollbackS3Exception exception) {
            // Perform rollback of stored S3 objects and abort the upload process
            performRollbackOfStoredS3ObjectsAndAbortUpload(context);

        } catch (InvalidUploadZipStructureException exception) {
            // Abort the upload process
            abortUploadProcessDueToErrors(context);
        }

        // Finish the upload process and return the S3ObjectDto for a successful ZIP upload
        finishUpload(upload, TransferStatus.SUCCESSFUL);

        return s3ObjectDtoMapper.s3ObjectToS3ObjectDto(upload.getS3Object());
    }


    /**
     * Handles the ZIP upload for the /uploads/{uploadId}/s3_object endpoint.
     * <p>
     * Note: Exceptions in this process are reported to the context handler and are not thrown.
     *
     * @param context     The {@link KinsectaMeasurementUploadContext} in which this is processed
     * @param requestBody The request body holding the zip data Resource Stream
     *
     * @return An Optional containing the saved Upload instance or nothing if errors occurred
     */
    private Optional<Upload> handleZipUpload(KinsectaMeasurementUploadContext context, Resource requestBody) {
        try {
            // Create File object for the upload zip and save the uploaded zip file from the Resource stream
            File uploadedZipFile = new File(context.getTempDir(), "upload.zip");
            ApiUtil.saveResourceStreamToFile(requestBody, uploadedZipFile);

            // Unzip the zip file in our upload temp dir
            ApiUtil.unzipFileIntoDirectory(uploadedZipFile, context.getTempDir());

            // Upload zip file to S3
            S3Object zipS3Object = s3ObjectService.storeFile(uploadedZipFile);
            Upload upload = context.getUpload();
            upload.setS3Object(zipS3Object);

            // Persist S3Object together with the Upload entity and return as Optional
            Upload savedUploadWithS3Object = saveUpload(upload);
            context.setUpload(savedUploadWithS3Object);
            return Optional.of(savedUploadWithS3Object);

        } catch (IOException e) {
            context.addErrorMessage(String.format("Exception while trying to process or unzip the zip file: %s", e.getMessage()));
            return Optional.empty();

        } catch (StorageException e) {
            context.addErrorMessage(String.format("Exception while trying to store zip file in S3 Storage: %s", e.getMessage()));
            return Optional.empty();
        }
    }

    private void performRollbackOfStoredS3ObjectsAndAbortUpload(KinsectaMeasurementUploadContext context) {
        // Log the errors
        logger.error("The partially processed Upload #{} needs to be rolled back due to errors.", context.getUpload().getId());

        // Delete uploaded KinsectaMeasurement S3 objects, as they are not rolled back by the @Transactional annotation.
        // However, don't delete the uploaded ZIP file from S3 as we want to store it so that other team members can download and investigate errors.
        for (S3Object s3Object : context.getUploadedObjects()) {
            try {
                s3ObjectService.delete(s3Object);
            } catch (StorageException e) {
                logger.error("Error while trying to delete object from S3 Storage during rollback", e);
            }
        }

        // Clean up temp directory and send email notification
        abortUploadProcessDueToErrors(context);
    }

    private void abortUploadProcessDueToErrors(KinsectaMeasurementUploadContext context) {
        // Log the errors
        logger.error("The processing of Upload #{} is aborted due to the following errors:\n- {}", context.getUpload().getId(), String.join("\n- ", context.getErrorMessages()));

        // Set upload to ERRONEOUS state - or leave as CANCELLED if it has been cancelled in the meantime
        TransferStatus cacheUploadStatus = uploadStatusCache.getUploadStatus(context.getUpload().getId());
        Upload finishedUpload = finishUpload(context.getUpload(), cacheUploadStatus.equals(TransferStatus.CANCELLED) ? cacheUploadStatus : TransferStatus.ERRONEOUS);

        // Asynchronously send an email with detailed errors to the upload user
        uploadMailService.sendUploadErrorMessage(finishedUpload, cleanErrorMessagesForEmail(context.getErrorMessages()));

        // Delete upload temp dir
        tempDirectoryHelper.deleteTempSubDirectory(context.getTempDir());

        // Exit the controller with a 410 GONE if the Upload was cancelled by the user
        if (cacheUploadStatus.equals(TransferStatus.CANCELLED)) {
            throw new UploadCancelledException();
        }
        // Exit the controller with a 400 BAD REQUEST
        else {
            throw new ZipUploadException("ZIP upload failed. Check your email inbox for an email with further details.");
        }
    }

    private List<String> cleanErrorMessagesForEmail(List<String> errorMessages) {
        return errorMessages.stream().map(error -> error
            .replace("org.kinsecta.webapp.api.v1.model.", "")
            .replace("org.kinsecta.webapp.api.v1.", "")
            .replace("com.fasterxml.jackson.databind.exc.", "")).toList();
    }

    public S3Object getS3ObjectForUpload(Long uploadId) {
        // Get Upload by ID
        Upload upload = getUpload(uploadId);

        // Assert that the upload isn't finished yet
        if (upload.getStatus().equals(TransferStatus.IN_PROGRESS) || upload.getStatus().equals(TransferStatus.CANCELLED)) {
            throw new ForbiddenException("You are not allowed to download ZIP data of an Upload which hasen't finished uploading yet!");
        }

        // Get S3Object and assert null
        S3Object s3Object = upload.getS3Object();
        if (s3Object == null) {
            throw new NotFoundException(String.format("Could not find S3Object for Upload with id %d", uploadId));
        }
        return s3Object;
    }

    public ResponseEntity<Resource> downloadS3ObjectForUpload(Long uploadId) {
        // Get S3Object
        S3Object s3Object = getS3ObjectForUpload(uploadId);

        // Download S3Object as InputStreamResource
        return s3ObjectService.downloadAttachment(s3Object);
    }

}
