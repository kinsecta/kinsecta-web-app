package org.kinsecta.webapp.api.service;

import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.model.entities.MeasurementWingbeat;
import org.kinsecta.webapp.api.model.repositories.MeasurementWingbeatRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class MeasurementWingbeatService {

    private final MeasurementWingbeatRepository measurementWingbeatRepository;


    public MeasurementWingbeatService(MeasurementWingbeatRepository measurementWingbeatRepository) {
        this.measurementWingbeatRepository = measurementWingbeatRepository;
    }

    public MeasurementWingbeat getMeasurementWingbeat(Long id) {
        return measurementWingbeatRepository.findById(id).orElseThrow(() -> new NotFoundException(String.format("Could not find MeasurementWingbeat with id %d", id)));
    }

    public Optional<MeasurementWingbeat> getOptionalMeasurementWingbeat(Long id) {
        return measurementWingbeatRepository.findById(id);
    }

    public MeasurementWingbeat saveMeasurementWingbeat(MeasurementWingbeat measurementWingbeat) {
        return measurementWingbeatRepository.save(measurementWingbeat);
    }

}
