package org.kinsecta.webapp.api.model.mapping;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(value = "bounding_boxes")
public abstract class KinsectaMeasurementImageMixin {

}
