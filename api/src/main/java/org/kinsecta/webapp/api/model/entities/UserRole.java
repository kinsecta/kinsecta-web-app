package org.kinsecta.webapp.api.model.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;


public enum UserRole {

    ADMIN("ROLE_ADMIN"),
    TEAM_MEMBER("ROLE_TEAM_MEMBER"),
    DATA_COLLECTOR("ROLE_DATA_COLLECTOR"),
    ANONYMOUS("ROLE_ANONYMOUS"),
    DATA_SCIENTIST("ROLE_DATA_SCIENTIST"),
    DATA_RECIPIENT("ROLE_DATA_RECIPIENT");


    private final String value;

    UserRole(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static UserRole fromValue(String value) {
        for (UserRole b : UserRole.values()) {
            if (b.value.equals(value)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }

}
