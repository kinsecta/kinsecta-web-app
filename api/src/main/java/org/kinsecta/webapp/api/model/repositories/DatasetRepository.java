package org.kinsecta.webapp.api.model.repositories;

import org.kinsecta.webapp.api.model.entities.Dataset;
import org.kinsecta.webapp.api.model.entities.Upload;
import org.kinsecta.webapp.api.model.repositories.projections.DatasetCountByDayProjection;
import org.kinsecta.webapp.api.model.repositories.projections.DatasetCountByMonthProjection;
import org.kinsecta.webapp.api.model.repositories.projections.DatasetCountByWeekProjection;
import org.kinsecta.webapp.api.model.repositories.projections.DatasetCountByYearProjection;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;


public interface DatasetRepository extends JpaRepository<Dataset, Long>, CustomDatasetFilterRepository {

    // ---JPA Queries --------------------------------------------------------------------------------------------------

    List<Dataset> findDatasetsByIdIn(List<Long> datasetIds, Sort sort);

    List<Dataset> findDatasetsByUpload(Upload upload);

    List<Dataset> findAllBySensorId(Long sensorId);

    Boolean existsByIdAndUserId(Long datasetId, Long userId);

    @Query(value = """
        SELECT d.id FROM Dataset d
        WHERE :md5Hash = d.md5Hash
        """)
    Optional<Long> findDatasetIdByMd5Hash(String md5Hash);

    @Query(value = """
        SELECT COUNT(*)
        FROM Dataset d
            JOIN d.sensor s
        WHERE YEAR(d.measurementDatetime) = :year
            AND s.showPublic = true
        """)
    Long countDatasetsByYear(Integer year);

    @Query(value = """
        SELECT YEAR(measurement_datetime) AS year,
            COUNT(*) AS count
        FROM dataset
            LEFT JOIN sensor s on dataset.sensor_id = s.id
        WHERE s.show_public = true
        GROUP BY YEAR(measurement_datetime)
        ORDER BY year DESC
        """, nativeQuery = true)
    List<DatasetCountByYearProjection> getCountsForYear();

    @Query(value = """
        SELECT YEAR(measurement_datetime) as year,
            MONTH(measurement_datetime) as month,
            COUNT(*) as count
        FROM dataset
            LEFT JOIN sensor s on dataset.sensor_id = s.id
        WHERE s.show_public = true
        GROUP BY year, month
        ORDER BY year DESC, month DESC
        """, nativeQuery = true)
    List<DatasetCountByMonthProjection> getCountsForMonth();

    @Query(value = """
            SELECT YEAR(measurement_datetime) as year,
                WEEK(measurement_datetime) as week,
                COUNT(*) as count
            FROM dataset
                LEFT JOIN sensor s on dataset.sensor_id = s.id
            WHERE s.show_public = true
            GROUP BY year, week
            ORDER BY year DESC, week DESC
        """, nativeQuery = true)
    List<DatasetCountByWeekProjection> getCountsForWeek();

    @Query(value = """
        SELECT cast(measurement_datetime as date) as date,
        COUNT(*) as count
        FROM dataset
            LEFT JOIN sensor s on dataset.sensor_id = s.id
        WHERE s.show_public = true
        GROUP BY date
        ORDER BY date DESC
        """, nativeQuery = true)
    List<DatasetCountByDayProjection> getCountsForDay();

}
