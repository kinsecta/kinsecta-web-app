package org.kinsecta.webapp.api.model.repositories;

import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.model.entities.UserRole;
import org.kinsecta.webapp.api.model.entities.UserStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

    Optional<User> findByFullName(String fullname);

    Optional<User> findByEmail(String email);

    void deleteUserById(Long id);

    List<User> findAll();

    List<User> findAllByStatus(UserStatus status);

    List<User> findAllByRole(UserRole role);

}
