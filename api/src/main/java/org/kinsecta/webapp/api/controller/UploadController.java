package org.kinsecta.webapp.api.controller;

import org.kinsecta.webapp.api.exception.unchecked.ForbiddenException;
import org.kinsecta.webapp.api.exception.unchecked.StorageRuntimeException;
import org.kinsecta.webapp.api.exception.unchecked.UploadCancelledException;
import org.kinsecta.webapp.api.model.entities.S3Object;
import org.kinsecta.webapp.api.model.entities.TransferStatus;
import org.kinsecta.webapp.api.model.entities.Upload;
import org.kinsecta.webapp.api.model.mapping.S3ObjectDtoMapper;
import org.kinsecta.webapp.api.model.mapping.TransferStatusMapper;
import org.kinsecta.webapp.api.model.mapping.UploadDtoMapper;
import org.kinsecta.webapp.api.s3.StorageException;
import org.kinsecta.webapp.api.service.DeletionService;
import org.kinsecta.webapp.api.service.UploadService;
import org.kinsecta.webapp.api.util.TempDirectoryHelper;
import org.kinsecta.webapp.api.v1.UploadsApi;
import org.kinsecta.webapp.api.v1.model.S3ObjectDto;
import org.kinsecta.webapp.api.v1.model.TransferStatusDto;
import org.kinsecta.webapp.api.v1.model.UploadDto;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import java.util.List;


@Controller
public class UploadController extends BaseController implements UploadsApi {

    private final UploadService uploadService;
    private final UploadDtoMapper uploadDtoMapper;
    private final S3ObjectDtoMapper s3ObjectDtoMapper;
    private final TransferStatusMapper transferStatusMapper;
    private final TempDirectoryHelper tempDirectoryHelper;
    private final DeletionService deletionService;


    public UploadController(UploadService uploadService, UploadDtoMapper uploadDtoMapper, S3ObjectDtoMapper s3ObjectDtoMapper, TransferStatusMapper transferStatusMapper, TempDirectoryHelper tempDirectoryHelper, DeletionService deletionService) {
        this.uploadService = uploadService;
        this.uploadDtoMapper = uploadDtoMapper;
        this.s3ObjectDtoMapper = s3ObjectDtoMapper;
        this.transferStatusMapper = transferStatusMapper;
        this.tempDirectoryHelper = tempDirectoryHelper;
        this.deletionService = deletionService;
    }


    @PostConstruct
    public void init() {
        // create base temp dir
        tempDirectoryHelper.createTempBaseDirectory();
    }


    @Override
    public ResponseEntity<UploadDto> createUpload() {
        Upload upload = uploadService.createUploadForUser(contextUserService.getUser());
        UploadDto uploadDto = uploadDtoMapper.uploadToUploadDto(upload);
        return new ResponseEntity<>(uploadDto, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<UploadDto>> getAllUploads(Integer page, Integer size, String sort, Pageable pageable) {
        Page<Upload> paginatedUploads = uploadService.getAllUploadsForUserIdAndUserRole(contextUserService.getUserId(), contextUserService.getUserRole(), pageable);
        List<UploadDto> dtoList = paginatedUploads.getContent().stream().map(uploadDtoMapper::uploadToUploadDto).toList();
        return returnPaginatedResponse(paginatedUploads, dtoList);
    }

    @Override
    public ResponseEntity<UploadDto> getUpload(Long uploadId) {
        Upload upload = uploadService.getUpload(uploadId);
        UploadDto uploadDto = uploadDtoMapper.uploadToUploadDto(upload);
        return ResponseEntity.ok(uploadDto);
    }

    @Override
    public ResponseEntity<Void> deleteUpload(Long uploadId) {
        try {
            deletionService.deleteUpload(uploadId);
            return ResponseEntity.ok().build();
        } catch (StorageException e) {
            throw new StorageRuntimeException(e);
        }
    }


    @Override
    public ResponseEntity<TransferStatusDto> updateTransferStatusOfUpload(Long uploadId, String body) {
        TransferStatus newTransferStatus = TransferStatus.valueOf(body);
        TransferStatus updatedTransferStatus = uploadService.updateTransferStatusOfUpload(uploadId, newTransferStatus, contextUserService.getUserId(), contextUserService.getUserRole());
        TransferStatusDto transferStatusDto = transferStatusMapper.transferStatusToTransferStatusDto(updatedTransferStatus);
        return ResponseEntity.ok(transferStatusDto);
    }


    @Override
    public ResponseEntity<S3ObjectDto> createS3ObjectForUpload(Long uploadId, Resource body) {
        // Get Upload by ID
        Upload upload = uploadService.getUpload(uploadId);

        // Assert that only the user who created the upload can continue with uploading a ZIP file
        if (!upload.getUser().getId().equals(contextUserService.getUserId())) {
            throw new ForbiddenException("You are not allowed to upload ZIP data to an Upload of another user!");
        }

        // Assert that the upload hasn't been cancelled in the meantime
        if (upload.getStatus().equals(TransferStatus.CANCELLED)) {
            throw new UploadCancelledException();
        }

        // Assert that the upload isn't finished yet
        if (!upload.getStatus().equals(TransferStatus.IN_PROGRESS)) {
            throw new ForbiddenException("You are not allowed to upload ZIP data to an Upload which is already finished!");
        }

        S3ObjectDto s3ObjectDto = uploadService.handleS3ObjectUpload(upload, body);

        return new ResponseEntity<>(s3ObjectDto, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<S3ObjectDto> getS3ObjectForUpload(Long uploadId) {
        S3Object s3Object = uploadService.getS3ObjectForUpload(uploadId);
        S3ObjectDto s3ObjectDto = s3ObjectDtoMapper.s3ObjectToS3ObjectDto(s3Object);
        return ResponseEntity.ok(s3ObjectDto);
    }

    @Override
    public ResponseEntity<Resource> downloadS3ObjectForUpload(Long uploadId) {
        return uploadService.downloadS3ObjectForUpload(uploadId);
    }

}
