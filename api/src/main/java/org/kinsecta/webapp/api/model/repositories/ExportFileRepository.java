package org.kinsecta.webapp.api.model.repositories;

import org.kinsecta.webapp.api.model.entities.Export;
import org.kinsecta.webapp.api.model.entities.ExportFile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface ExportFileRepository extends JpaRepository<ExportFile, Long> {

    List<ExportFile> findExportFilesByExport(Export export);

}
