package org.kinsecta.webapp.api.controller;

import org.kinsecta.webapp.api.exception.unchecked.ForbiddenException;
import org.kinsecta.webapp.api.model.entities.UserRole;
import org.kinsecta.webapp.api.security.ContextUserService;
import org.kinsecta.webapp.api.security.CustomHttpHeaders;
import org.kinsecta.webapp.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;


public class BaseController {

    // don't autowire in constructor here, because we would have to pull the dependencies through every subclass
    @Autowired
    protected UserService userService;
    @Autowired
    protected ContextUserService contextUserService;


    protected boolean isCurrentUserAdmin() {
        return UserRole.ADMIN.equals(contextUserService.getUserRole());
    }

    protected void failIfNonAdminUserTriesToQueryArbitraryUserWithId(Long arbitraryUserId, String context) {
        if (!isCurrentUserAdmin() && !arbitraryUserId.equals(contextUserService.getUserId())) {
            String message = String.format("The current user '%s' with role '%s' is not allowed to query or update '%s' for another user id '%d'",
                contextUserService.getUsername(),
                contextUserService.getUserRole(),
                context,
                arbitraryUserId);
            throw new ForbiddenException(message);
        }
    }

    protected void failIfCurrentUserTriesToQueryArbitraryUserWithId(Long arbitraryUserId) {
        if (!arbitraryUserId.equals(contextUserService.getUserId())) {
            String message = "Querying or updating user data of another user is not allowed in this context. Only querying or updating your own data is allowed.";
            throw new ForbiddenException(message);
        }
    }

    protected <T, S> ResponseEntity<List<S>> returnPaginatedResponse(Page<T> paginatedEntityList, List<S> dtoList) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(CustomHttpHeaders.X_TOTAL_COUNT, String.valueOf(paginatedEntityList.getTotalElements()));
        return new ResponseEntity<>(dtoList, headers, HttpStatus.OK);
    }

}
