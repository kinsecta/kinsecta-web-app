package org.kinsecta.webapp.api.mail;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;


public record EmailAttachment(String name, ByteArrayResource resource, MediaType mediaType) {

}
