package org.kinsecta.webapp.api.exception.checked;

import javax.validation.ConstraintViolation;
import java.util.Set;


/**
 * This custom exception indicates bean validation errors.
 * <p>
 * This Exception MUST be handled where it is thrown as it is not automatically
 * handled by the {@linkplain org.kinsecta.webapp.api.exception.GlobalExceptionHandler GlobalExceptionHandler}.
 */
public class ValidationViolationException extends Exception {

    private final Set<? extends ConstraintViolation<?>> constraintViolationExceptions;


    public ValidationViolationException(Set<? extends ConstraintViolation<?>> constraintViolationExceptions, String message) {
        super(message);
        this.constraintViolationExceptions = constraintViolationExceptions;
    }


    public <T> Set<ConstraintViolation<T>> getViolations(Class<T> clazz) {
        return (Set<ConstraintViolation<T>>) constraintViolationExceptions;
    }

}
