package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.model.entities.S3Object;
import org.kinsecta.webapp.api.model.repositories.S3ObjectRepository;
import org.kinsecta.webapp.api.v1.model.IdObject;
import org.kinsecta.webapp.api.v1.model.S3ObjectDto;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;


@Mapper(
    componentModel = "spring",
    uses = {
        DtoMapperConfig.class
    },
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public abstract class S3ObjectDtoMapper {

    @Autowired
    private S3ObjectRepository s3ObjectRepository;


    public abstract S3Object s3ObjectDtoToS3Object(S3ObjectDto s3ObjectDto);

    public abstract S3ObjectDto s3ObjectToS3ObjectDto(S3Object s3Object);

    public S3Object idObjectToS3Object(IdObject idObject) {
        if (idObject != null) {
            return s3ObjectRepository.getById(idObject.getId());
        }
        return null;
    }

    public IdObject s3ObjectToIdObject(S3Object s3Object) {
        if (s3Object != null) {
            return new IdObject().id(s3Object.getId());
        }
        return null;
    }

}
