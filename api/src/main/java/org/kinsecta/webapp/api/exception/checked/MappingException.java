package org.kinsecta.webapp.api.exception.checked;

/**
 * This custom exception indicates a problem in a Mapstruct mapping process.
 * <p>
 * This Exception MUST be handled where it is thrown as it is not automatically
 * handled by the {@linkplain org.kinsecta.webapp.api.exception.GlobalExceptionHandler GlobalExceptionHandler}.
 */
public class MappingException extends Exception {

    public MappingException(String message) {
        super(message);
    }

}
