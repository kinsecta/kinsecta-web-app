package org.kinsecta.webapp.api.controller;

import org.kinsecta.webapp.api.exception.unchecked.WrongArgumentException;
import org.kinsecta.webapp.api.mail.PasswordResetMailService;
import org.kinsecta.webapp.api.model.entities.ResetToken;
import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.model.entities.UserRole;
import org.kinsecta.webapp.api.model.entities.UserStatus;
import org.kinsecta.webapp.api.model.mapping.UserDtoMapper;
import org.kinsecta.webapp.api.service.ResetTokenService;
import org.kinsecta.webapp.api.util.ValidationTools;
import org.kinsecta.webapp.api.v1.UsersApi;
import org.kinsecta.webapp.api.v1.model.ApiViewDto;
import org.kinsecta.webapp.api.v1.model.UserDto;
import org.kinsecta.webapp.api.v1.model.UserSimpleViewDto;
import org.kinsecta.webapp.api.v1.model.UserStatusDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
import java.util.Set;


@Controller
public class UserController extends BaseController implements UsersApi {

    private final UserDtoMapper userDtoMapper;
    private final ResetTokenService resetTokenService;
    private final PasswordResetMailService passwordResetMailService;

    @Value("${app.account.email-domain-whitelist}")
    private Set<String> emailDomainWhitelist;


    @Autowired
    public UserController(UserDtoMapper userDtoMapper, ResetTokenService resetTokenService, PasswordResetMailService passwordResetMailService) {
        this.userDtoMapper = userDtoMapper;
        this.resetTokenService = resetTokenService;
        this.passwordResetMailService = passwordResetMailService;
    }


    @Override
    public ResponseEntity<List<UserDto>> getAllUsers(Optional<UserStatusDto> status) {
        UserStatus userStatus = userDtoMapper.userStatusDtoToUserStatus(status.orElse(null));
        List<User> userList = userService.getAllUsersOptionallyByStatus(userStatus);
        return ResponseEntity.ok(userDtoMapper.userListToUserDtoList(userList));
    }

    @Override
    public ResponseEntity<UserDto> createUser(@RequestBody UserDto userDto) {
        User createdUser = userDtoMapper.userDtoToUser(userDto);
        if (createdUser.getStatus().equals(UserStatus.DELETED)) {
            throw new WrongArgumentException("A new User cannot be created with a status of 'DELETED'.");
        }
        if (!ValidationTools.isValidEmail(userDto.getEmail(), this.emailDomainWhitelist)) {
            throw new WrongArgumentException(String.format("Cannot create User: The email address '%s' is not valid with regard to the allowed email domain whitelist: %s", userDto.getEmail(), this.emailDomainWhitelist));
        }
        createdUser = userService.createUser(createdUser);
        if (createdUser.getStatus().equals(UserStatus.ACTIVE)) {
            ResetToken resetToken = resetTokenService.createResetToken(createdUser);
            passwordResetMailService.sendPwResetLinkForNewUser(createdUser, resetToken);
        }
        return new ResponseEntity<>(userDtoMapper.userToUserDto(createdUser), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<ApiViewDto>> getAllUserViews() {
        ApiViewDto simpleView = new ApiViewDto();
        simpleView.setView("simple");
        simpleView.setUri("/users/_views/simple");
        simpleView.setDescription("Gets all Users in an array, optionally only active users or inactive. This is a simplified version of the `/users` endpoint for all Non-ADMIN roles. The result is a `UserSimpleViewDto` format, which is basically a stripped down `UserDto` without the `role`, `status` and `email` fields.");
        return ResponseEntity.ok(List.of(simpleView));
    }

    @Override
    public ResponseEntity<List<UserSimpleViewDto>> getAllUsersSimple(Optional<UserStatusDto> status) {
        UserStatus userStatus = userDtoMapper.userStatusDtoToUserStatus(status.orElse(null));
        List<User> userList = userService.getAllUsersOptionallyByStatus(userStatus);
        return ResponseEntity.ok(userList.stream().map(userDtoMapper::userToUserSimpleViewDto).toList());
    }

    @Override
    public ResponseEntity<UserDto> getUser(@PathVariable("userId") Long id) {
        User user = userService.getUser(id);
        return ResponseEntity.ok(userDtoMapper.userToUserDto(user));
    }

    @Override
    public ResponseEntity<UserDto> editUser(@PathVariable("userId") Long id, @RequestBody UserDto userDto) {
        if (id.equals(contextUserService.getUserId()) && !UserRole.ADMIN.equals(userDtoMapper.userRoleDtoToUserRole(userDto.getRole()))) {
            throw new WrongArgumentException("You cannot set your own role to anything other than admin");
        }

        // Get updatable user from DB und with UserDto information
        User user = userService.getUser(id);

        if (user.getStatus().equals(UserStatus.DELETED)) {
            throw new WrongArgumentException("A deleted User cannot be edited.");
        }

        user = userDtoMapper.updateUserWithUserDto(userDto, user);

        if (user.getStatus().equals(UserStatus.DELETED)) {
            throw new WrongArgumentException("The status of a User cannot be changed to 'DELETED'.");
        }

        // check if security details relevant for JwtTokenService changed
        if (!(user.getUsername().equals(userDto.getUsername()) && user.getEmail().equals(userDto.getEmail()))) {
            // update the security details modified timestamp so that all JWT tokens of the specified user render invalid immediately
            user.updateSecurityDetailsModified();
        }

        User savedUser = userService.save(user);
        return ResponseEntity.ok(userDtoMapper.userToUserDto(savedUser));
    }

    @Override
    public ResponseEntity<Void> deleteUser(@PathVariable("userId") Long id) {
        if (id.equals(contextUserService.getUserId())) {
            throw new WrongArgumentException("You cannot delete your own user account!");
        } else {
            userService.deleteUserById(id);
            return ResponseEntity.ok().build();
        }
    }

}
