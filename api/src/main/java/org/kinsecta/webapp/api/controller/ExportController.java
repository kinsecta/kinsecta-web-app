package org.kinsecta.webapp.api.controller;

import org.kinsecta.webapp.api.exception.unchecked.InvalidDownloadTokenException;
import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.exception.unchecked.WrongArgumentException;
import org.kinsecta.webapp.api.model.entities.Export;
import org.kinsecta.webapp.api.model.entities.ExportFile;
import org.kinsecta.webapp.api.model.entities.TransferStatus;
import org.kinsecta.webapp.api.model.entities.UserRole;
import org.kinsecta.webapp.api.model.mapping.ExportDtoMapper;
import org.kinsecta.webapp.api.service.DatasetService;
import org.kinsecta.webapp.api.service.ExportService;
import org.kinsecta.webapp.api.service.S3ObjectService;
import org.kinsecta.webapp.api.v1.ExportsApi;
import org.kinsecta.webapp.api.v1.model.DatasetFilterDto;
import org.kinsecta.webapp.api.v1.model.ExportDto;
import org.kinsecta.webapp.api.v1.model.ExportFileDto;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Controller
public class ExportController extends BaseController implements ExportsApi {

    private final ExportService exportService;
    private final ExportDtoMapper exportDtoMapper;
    private final S3ObjectService s3ObjectService;
    private final DatasetService datasetService;


    public ExportController(ExportService exportService, ExportDtoMapper exportDtoMapper, S3ObjectService s3ObjectService, DatasetService datasetService) {
        this.exportService = exportService;
        this.exportDtoMapper = exportDtoMapper;
        this.s3ObjectService = s3ObjectService;
        this.datasetService = datasetService;
    }


    @Override
    public ResponseEntity<ExportDto> createExport(DatasetFilterDto datasetFilterDto) {
        Export export = new Export();
        export.setStatus(TransferStatus.IN_PROGRESS);

        List<Long> ids = datasetService.getAllDatasetIdsByFilters(datasetFilterDto);
        export.setDatasetIds(ids.stream()
            .map(String::valueOf)
            .collect(Collectors.joining(";")));

        // Check role
        if (contextUserService.getUserRole().equals(UserRole.DATA_COLLECTOR)) {
            List<Long> idsToRemove = new ArrayList<>();
            for (Long id : ids) {
                if (Boolean.FALSE.equals(datasetService.isUserSameAsDatasetUser(contextUserService.getUserId(), id))) {
                    idsToRemove.add(id);
                }
            }
            ids.removeAll(idsToRemove);
            if (ids.isEmpty()) {
                throw new WrongArgumentException(String.format("No exportable datasets remain after removing all datasets from filtered collection which do not belong to your user '%s'", contextUserService.getUsername()));
            }
        }
        // Set current Context user and save new Export and convert to ExportDto
        export.setUser(contextUserService.getUser());
        Export savedExport = exportService.saveExport(export);
        ExportDto savedExportDto = exportDtoMapper.exportToExportDto(savedExport);

        // Start async creation of ExportFiles and email the user once this is finished
        exportService.startAsyncCreationOfExportFilesForExport(savedExport);

        // return 201 CREATED
        return new ResponseEntity<>(savedExportDto, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<ExportFileDto>> getAllExportFilesForExport(Long exportId) {
        // TODO implement this method
        return ExportsApi.super.getAllExportFilesForExport(exportId);
    }

    @Override
    public ResponseEntity<ExportDto> getExport(Long exportId) {
        // TODO implement this method
        return ExportsApi.super.getExport(exportId);
    }

    @Override
    public ResponseEntity<ExportFileDto> getExportFileForExport(Long exportId, Long exportFileId) {
        // TODO implement this method
        return ExportsApi.super.getExportFileForExport(exportId, exportFileId);
    }

    @Override
    @Transactional
    public ResponseEntity<Resource> downloadExportFileForExport(Long exportId, Long exportFileId, String token) {
        // Make sure that an Export with this ID exists and that it has an ExportFile with the specified ID
        Export export = exportService.getExport(exportId);
        ExportFile exportFile = export.getExportFiles().stream().filter(ef -> ef.getId().equals(exportFileId)).findFirst().orElseThrow(() -> new NotFoundException(String.format("Could not find ExportFile with id %d for Export with id %d", exportFileId, exportId)));

        // Validate download token
        if (!export.getDownloadToken().equals(token)) {
            throw new InvalidDownloadTokenException("The download token is invalid!");
        }
        if (export.getExpires().isBefore(LocalDateTime.now())) {
            throw new InvalidDownloadTokenException("The download token has expired.");
        }

        return s3ObjectService.downloadAttachment(exportFile.getS3Object());
    }

}
