package org.kinsecta.webapp.api.model.entities;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Table(name = "insect_species")
public class InsectSpecies {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "genus_id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private InsectGenus genus;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "gbif_id", columnDefinition = "BIGINT UNSIGNED")
    private Long gbifId;

    @Column(name = "created", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW()")
    @Generated(GenerationTime.INSERT)
    @ReadOnlyProperty
    private LocalDateTime created;

    @Column(name = "modified", nullable = false, insertable = false, updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW() ON UPDATE NOW()")
    @Generated(GenerationTime.ALWAYS)
    @ReadOnlyProperty
    private LocalDateTime modified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public InsectGenus getGenus() {
        return genus;
    }

    public void setGenus(InsectGenus genus) {
        this.genus = genus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getGbifId() {
        return gbifId;
    }

    public void setGbifId(Long gbifId) {
        this.gbifId = gbifId;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }


    @Override
    public String toString() {
        return "InsectSpecies{" +
            "id=" + id +
            ", genus=" + genus +
            ", name='" + name + '\'' +
            ", gbifId=" + gbifId +
            ", created=" + created +
            ", modified=" + modified +
            '}';
    }

}
