package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.model.entities.Upload;
import org.kinsecta.webapp.api.v1.model.IdObject;
import org.kinsecta.webapp.api.v1.model.UploadDto;
import org.mapstruct.*;


@Mapper(
    componentModel = "spring",
    uses = {
        DtoMapperConfig.class,
        UserDtoMapper.class,
        S3ObjectDtoMapper.class,
        TransferStatusMapper.class
    },
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public abstract class UploadDtoMapper {

    public abstract Upload uploadDtoToUpload(UploadDto uploadDto);

    public abstract UploadDto uploadToUploadDto(Upload upload);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "user", ignore = true)
    public abstract Upload updateUploadWithUploadDto(UploadDto update, @MappingTarget Upload destination);

    public IdObject uploadToIdObject(Upload upload) {
        return new IdObject().id(upload.getId());
    }

}
