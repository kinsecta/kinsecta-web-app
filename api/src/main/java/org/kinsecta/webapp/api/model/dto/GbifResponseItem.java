package org.kinsecta.webapp.api.model.dto;


/**
 * This is a manually crafted Dto representation of the GBIF API v1 response format as described at
 * <a href="https://www.gbif.org/developer/summary">https://www.gbif.org/developer/summary</a>.
 */
public class GbifResponseItem {

    private Long key;
    private Long nubKey;
    private String canonicalName;
    private String order;
    private String family;
    private String genus;
    private String species;
    private Long classKey;
    private Long orderKey;
    private Long familyKey;
    private Long genusKey;
    private Long speciesKey;
    private String rank;
    private Boolean synonym;


    public Long getKey() {
        return key;
    }

    public void setKey(Long key) {
        this.key = key;
    }

    public Long getNubKey() {
        return nubKey;
    }

    public void setNubKey(Long nubKey) {
        this.nubKey = nubKey;
    }

    public String getCanonicalName() {
        return canonicalName;
    }

    public void setCanonicalName(String canonicalName) {
        this.canonicalName = canonicalName;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getGenus() {
        return genus;
    }

    public void setGenus(String genus) {
        this.genus = genus;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public Long getClassKey() {
        return classKey;
    }

    public void setClassKey(Long classKey) {
        this.classKey = classKey;
    }

    public Long getOrderKey() {
        return orderKey;
    }

    public void setOrderKey(Long orderKey) {
        this.orderKey = orderKey;
    }

    public Long getFamilyKey() {
        return familyKey;
    }

    public void setFamilyKey(Long familyKey) {
        this.familyKey = familyKey;
    }

    public Long getGenusKey() {
        return genusKey;
    }

    public void setGenusKey(Long genusKey) {
        this.genusKey = genusKey;
    }

    public Long getSpeciesKey() {
        return speciesKey;
    }

    public void setSpeciesKey(Long speciesKey) {
        this.speciesKey = speciesKey;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public Boolean getSynonym() {
        return synonym;
    }

    public void setSynonym(Boolean synonym) {
        this.synonym = synonym;
    }

}
