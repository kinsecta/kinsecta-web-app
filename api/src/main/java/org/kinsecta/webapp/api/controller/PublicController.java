package org.kinsecta.webapp.api.controller;

import org.kinsecta.webapp.api.model.entities.Classification;
import org.kinsecta.webapp.api.model.entities.Dataset;
import org.kinsecta.webapp.api.model.mapping.PublicDtoMapper;
import org.kinsecta.webapp.api.model.repositories.projections.*;
import org.kinsecta.webapp.api.service.ClassificationService;
import org.kinsecta.webapp.api.service.DatasetService;
import org.kinsecta.webapp.api.service.DatasetTagService;
import org.kinsecta.webapp.api.service.SensorService;
import org.kinsecta.webapp.api.v1.PublicApi;
import org.kinsecta.webapp.api.v1.model.*;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;


@Controller
public class PublicController extends BaseController implements PublicApi {

    private final SensorService sensorService;
    private final DatasetService datasetService;
    private final DatasetTagService datasetTagService;
    private final ClassificationService classificationService;
    private final PublicDtoMapper publicDtoMapper;

    private final List<String> datasetTagFilter;
    private final List<String> latestDatasetTagFilter;


    public PublicController(SensorService sensorService, DatasetService datasetService, DatasetTagService datasetTagService, ClassificationService classificationService, PublicDtoMapper publicDtoMapper) {
        this.sensorService = sensorService;
        this.datasetService = datasetService;
        this.datasetTagService = datasetTagService;
        this.classificationService = classificationService;
        this.publicDtoMapper = publicDtoMapper;
        this.latestDatasetTagFilter = loadLatestDatasetTagFilter();
        this.datasetTagFilter = loadDatasetTagFilter();
    }


    @Override
    public ResponseEntity<List<PublicSensorDto>> getAllSensorsPublic() {
        List<PublicSensorProjection> projections = sensorService.getAllSensorsPublic();
        List<PublicSensorDto> dtoList = projections.stream().map(publicDtoMapper::toPublicSensorDto).toList();
        return ResponseEntity.ok(dtoList);
    }

    @Override
    public ResponseEntity<List<PublicSensorDetailsDto>> getSensorPublic(Long sensorId) {
        // Assert that requested sensor is public
        sensorService.getSensorPublic(sensorId);

        DatasetFilterDto datasetFilterDto = new DatasetFilterDto();
        datasetFilterDto.setSensorFilters(sensorId.toString());
        datasetFilterDto.setTagFilterList(datasetTagFilter);
        datasetFilterDto.setSensorIsPublic(true);

        Pageable pageable = PageRequest.of(0, 3, Sort.by("measurementDatetime").descending());
        Page<Dataset> page = datasetService.getDatasetsByFilterDto(datasetFilterDto, pageable);

        List<PublicSensorDetailsDto> result = new ArrayList<>();
        page.stream().forEach(dataset -> {
            // TODO: revisit choosing dataset images, when we add classifications to them
            List<Classification> classifications = classificationService.getClassificationsForDatasetId(dataset.getId());
            Optional<Classification> classificationOpt = classifications.stream().min(Comparator.comparing(Classification::getProbability));
            if (classificationOpt.isPresent()) {
                result.add(publicDtoMapper.toPublicSensorDetailsDto(classificationOpt.get()));
            } else {
                result.add(publicDtoMapper.toPublicSensorDetailsDto(dataset));
            }
        });
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<List<PublicDatasetDetailsDto>> getLatestDatasets() {
        DatasetFilterDto datasetFilterDto = new DatasetFilterDto();
        datasetFilterDto.setTagFilterList(latestDatasetTagFilter);
        datasetFilterDto.setSensorIsPublic(true);

        Pageable pageable = PageRequest.of(0, 12, Sort.by("measurementDatetime").descending());
        Page<Dataset> page = datasetService.getDatasetsByFilterDto(datasetFilterDto, pageable);

        List<PublicDatasetDetailsDto> result = new ArrayList<>();
        page.stream().forEach(dataset -> {
            // TODO revisit choosing dataset images, when we add classifications to them
            List<Classification> classifications = classificationService.getClassificationsForDatasetId(dataset.getId());
            Optional<Classification> classificationOpt = classifications.stream().min(Comparator.comparing(Classification::getProbability));
            classificationOpt.ifPresent(classification -> result.add(publicDtoMapper.toPublicDatasetDetailsDto(classification)));
        });
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<String> getDatasetCountForYear(Integer year) {
        Long datasetsByYear = datasetService.countDatasetsByYear(year);
        return ResponseEntity.ok(datasetsByYear.toString());
    }

    @Override
    public ResponseEntity<MeasurementAmountsDto> getTotalDatasetCount(UnitDto unit) {
        MeasurementAmountsDto measurementAmountsDto = new MeasurementAmountsDto();
        measurementAmountsDto.setUnit(unit);
        switch (unit) {
            case YEAR:
                List<DatasetCountByYearProjection> datasetCountByYearProjection = datasetService.countDatasetsByYear();
                measurementAmountsDto.setData(datasetCountByYearProjection.stream().map(publicDtoMapper::toCountDto).toList());
                break;
            case MONTH:
                List<DatasetCountByMonthProjection> datasetCountByMonthProjections = datasetService.countDatasetsByMonth();
                measurementAmountsDto.setData(datasetCountByMonthProjections.stream().map(publicDtoMapper::toCountDto).toList());
                break;
            case WEEK:
                List<DatasetCountByWeekProjection> datasetCountByWeekProjection = datasetService.countDatasetsByWeek();
                measurementAmountsDto.setData(datasetCountByWeekProjection.stream().map(publicDtoMapper::toCountDto).toList());
                break;
            case DAY:
                List<DatasetCountByDayProjection> datasetCountByDayProjection = datasetService.countDatasetsByDay();
                measurementAmountsDto.setData(datasetCountByDayProjection.stream().map(publicDtoMapper::toCountDto).toList());
                break;
        }
        return ResponseEntity.ok(measurementAmountsDto);
    }

    private List<String> loadDatasetTagFilter() {
        List<String> tagFilterList = new ArrayList<>();
        Long fotoId = datasetTagService.getDatasetTag("Fotos").getId();
        Long wingbeatId = datasetTagService.getDatasetTag("Wingbeat").getId();
        tagFilterList.add(String.format("%s", fotoId));
        tagFilterList.add(String.format("-%s", wingbeatId));
        return tagFilterList;
    }

    private List<String> loadLatestDatasetTagFilter() {
        List<String> tagFilterList = new ArrayList<>();
        Long fotoId = datasetTagService.getDatasetTag("Fotos").getId();
        Long wingbeatId = datasetTagService.getDatasetTag("Wingbeat").getId();
        Long mainClassificationMissingId = datasetTagService.getDatasetTag("Haupt-Klassifikation fehlt").getId();
        tagFilterList.add(String.format("%s", fotoId));
        tagFilterList.add(String.format("-%s", wingbeatId));
        tagFilterList.add(String.format("-%s", mainClassificationMissingId));
        return tagFilterList;
    }

    @Override
    public ResponseEntity<Resource> downloadDatasetImageForDatasetPublic(Long datasetId, Long datasetImageId) {
        return datasetService.downloadDatasetImageForDatasetPublic(datasetId, datasetImageId);
    }

}
