package org.kinsecta.webapp.api.exception.unchecked;


/**
 * This custom exception indicates that the download token request parameter is invalid.
 * <p>
 * The exception and especially the response status is handled in our
 * {@linkplain org.kinsecta.webapp.api.exception.GlobalExceptionHandler GlobalExceptionHandler}.
 */
public class InvalidDownloadTokenException extends RuntimeException {

    public InvalidDownloadTokenException(String message) {
        super(message);
    }

}
