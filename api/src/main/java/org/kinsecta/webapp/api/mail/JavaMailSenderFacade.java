package org.kinsecta.webapp.api.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;


@Component
@EnableAsync
public class JavaMailSenderFacade {

    private static final Logger logger = LoggerFactory.getLogger(JavaMailSenderFacade.class);

    private static final String DEV_ENV_DETECTION_PATTERN = "^.*(localhost|\\.devel).*$";

    private static final String LOG_MSG_LOCAL_ENV_NO_REDIRECT = "Email was not sent! Email service is enabled on a local/dev system but no redirect-address was specified. Sending emails from a local/dev system requires to set a redirect-address / catch-all address which will receive all outgoing mail.";
    private static final String LOG_MSG_MAILSERVICE_DISABLED = "Email Service is disabled for this application or in this environment!";
    private static final String LOG_MSG_SENT_SUCCESSFULLY = "Email sent successfully!";

    private final JavaMailSender emailSender;

    @Value("${app.mail.subject-prefix}")
    private String subjectPrefix;

    @Value("${app.mail.send-mail}")
    private boolean mailServiceEnabled;

    @Value("${app.mail.redirect-address}")
    private String mailRedirectAddress;

    @Value("${spring.mail.username}")
    private String mailSender;

    @Value("${app.frontend.base-url}")
    private String frontendBaseURL;

    @Value("${app.name}")
    String appName;

    private String mailFrom;


    public JavaMailSenderFacade(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }


    @PostConstruct
    private void init() {
        mailFrom = String.format("%s <%s>", appName, mailSender);
    }


    @Async
    public void sendMailAsync(String receiverAddress, String subject, String text) {
        sendMail(receiverAddress, subject, text);
    }

    public void sendMail(String receiverAddress, String subject, String text) {
        MimeMessagePreparator preparedMessage = prepareMessage(receiverAddress, subject, text, false, null);
        sendPreparedMessage(preparedMessage);
    }

    @Async
    public void sendMailWithAttachmentAsync(String receiverAddress, String subject, String text, boolean isHtmlMessage, List<EmailAttachment> attachments) {
        sendMailWithAttachment(receiverAddress, subject, text, isHtmlMessage, attachments);
    }

    public void sendMailWithAttachment(String receiverAddress, String subject, String text, boolean isHtmlMessage, List<EmailAttachment> attachments) {
        MimeMessagePreparator preparedMessage = prepareMessage(receiverAddress, subject, text, isHtmlMessage, attachments);
        sendPreparedMessage(preparedMessage);
    }

    private void sendPreparedMessage(MimeMessagePreparator preparedMessage) {
        if (mailSendingIsAllowed()) {
            emailSender.send(preparedMessage);
            logger.info(LOG_MSG_SENT_SUCCESSFULLY);
        } else {
            logger.warn(LOG_MSG_MAILSERVICE_DISABLED);
        }
    }

    private boolean currentEnvIsDevEnv() {
        return frontendBaseURL.matches(DEV_ENV_DETECTION_PATTERN);
    }

    private boolean redirectAddressIsSet() {
        return mailRedirectAddress != null && !mailRedirectAddress.isEmpty();
    }

    private boolean mailSendingIsAllowed() {
        if (currentEnvIsDevEnv() && !redirectAddressIsSet()) {
            logger.error(LOG_MSG_LOCAL_ENV_NO_REDIRECT);
            return false;
        }
        return mailServiceEnabled;
    }

    private String buildMailSubject(String subject, boolean redirectEnabled, String receiverAddress) {
        StringBuilder sb = new StringBuilder();
        // prefix if necessary
        if (subjectPrefix != null && !subjectPrefix.isEmpty()) {
            sb.append(subjectPrefix);
            sb.append(" ");
        }
        // original subject
        sb.append(subject);
        // suffix if necessary
        if (redirectEnabled && !receiverAddress.equals(mailRedirectAddress)) {
            sb.append(" ");
            sb.append(String.format("((dev redirect: email originally addressed to %s))", receiverAddress));
        }
        return sb.toString();
    }

    private MimeMessagePreparator prepareMessage(String receiverAddress, String subject, String text, boolean isHtmlMessage, List<EmailAttachment> attachments) {
        // simplified lambda obtained with an IntelliJ quickfix from "new MimeMessagePreparator()"
        return mimeMessage -> {
            boolean isMultipartMimeMessage = attachments != null && !attachments.isEmpty();
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, isMultipartMimeMessage);
            messageHelper.setFrom(mailFrom);
            messageHelper.setText(text, isHtmlMessage);

            // handle dev env redirects
            if (currentEnvIsDevEnv()) {
                if (redirectAddressIsSet()) {
                    messageHelper.setTo(mailRedirectAddress);
                    String mailSubject = buildMailSubject(subject, true, receiverAddress);
                    messageHelper.setSubject(mailSubject);
                } else {
                    throw new MailSendException(LOG_MSG_LOCAL_ENV_NO_REDIRECT);
                }
            } else {
                messageHelper.setTo(receiverAddress);
                String mailSubject = buildMailSubject(subject, false, receiverAddress);
                messageHelper.setSubject(mailSubject);
            }

            // add attachments if available
            if (isMultipartMimeMessage) {
                for (EmailAttachment attachment : attachments) {
                    messageHelper.addAttachment(attachment.name(), attachment.resource(), attachment.mediaType().toString());
                }
            }
        };
    }

}
