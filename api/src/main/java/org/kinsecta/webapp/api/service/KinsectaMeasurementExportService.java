package org.kinsecta.webapp.api.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.kinsecta.webapp.api.exception.checked.MappingException;
import org.kinsecta.webapp.api.model.entities.Dataset;
import org.kinsecta.webapp.api.model.entities.DatasetImage;
import org.kinsecta.webapp.api.model.entities.S3Object;
import org.kinsecta.webapp.api.model.mapping.KinsectaMeasurementExportMapper;
import org.kinsecta.webapp.api.s3.StorageException;
import org.kinsecta.webapp.api.util.LocalDateTimeConverter;
import org.kinsecta.webapp.api.v1.model.KinsectaMeasurement;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


@Service
public class KinsectaMeasurementExportService {

    private final KinsectaMeasurementExportMapper kinsectaMeasurementExportMapper;
    private final ObjectMapper objectMapper;
    private final S3ObjectService s3ObjectService;


    public KinsectaMeasurementExportService(KinsectaMeasurementExportMapper kinsectaMeasurementExportMapper, ObjectMapper objectMapper, S3ObjectService s3ObjectService) {
        this.kinsectaMeasurementExportMapper = kinsectaMeasurementExportMapper;
        this.objectMapper = objectMapper;
        this.s3ObjectService = s3ObjectService;
    }


    public void generateDirectoriesForDatasets(File tempDirectory, List<Dataset> datasets) throws IOException, StorageException, MappingException {
        for (Dataset dataset : datasets) {
            generateDirectoryForDataset(tempDirectory, dataset);
        }
    }

    private void generateDirectoryForDataset(File tempDirectory, Dataset dataset) throws MappingException, IOException, StorageException {
        File directory = generateEmptyDirectory(tempDirectory, dataset);
        File jsonFile = new File(directory.toString(), "data.json");
        KinsectaMeasurement kinsectaMeasurement = kinsectaMeasurementExportMapper.datasetToKinsectaMeasurement(dataset);
        objectMapper.writeValue(jsonFile, kinsectaMeasurement);
        List<S3Object> s3Objects = collectS3ObjectsToDownload(dataset);
        s3ObjectService.downloadS3ObjectsToLocalDirectory(s3Objects, directory);
    }

    private File generateEmptyDirectory(File tempDirectory, Dataset dataset) {
        DateTimeFormatter dateTimeFormatter = LocalDateTimeConverter.getFormatterForPattern(LocalDateTimeConverter.DATE_TIME_FORMAT_YYYY_MM_DD_UNDERSCORE_HH_MM_SS);
        String dateString = dateTimeFormatter.format(dataset.getMeasurementDatetime());
        String directoryName = String.format("%s_%s", dataset.getSensor().getId(), dateString);
        File directory = new File(tempDirectory.toString(), directoryName);
        int i = 1;
        while (directory.exists()) {
            directory = new File(tempDirectory.toString(), String.format("%s(%s)", directoryName, i));
            i++;
        }
        if (!directory.mkdir()) {
            throw new IllegalStateException(String.format("Cannot create directory for dataset export: %s", directory.getAbsolutePath()));
        }
        return directory;
    }

    private List<S3Object> collectS3ObjectsToDownload(Dataset dataset) {
        List<S3Object> s3ObjectsToDownload = new ArrayList<>();
        if (dataset.getVideoS3Object() != null) {
            s3ObjectsToDownload.add(dataset.getVideoS3Object());
        }
        if (dataset.getMeasurementWingbeat() != null) {
            s3ObjectsToDownload.add(dataset.getMeasurementWingbeat().getWavS3Object());
            if (dataset.getMeasurementWingbeat().getImageS3Object() != null) {
                s3ObjectsToDownload.add(dataset.getMeasurementWingbeat().getImageS3Object());
            }
        }
        for (DatasetImage datasetImage : dataset.getDatasetImages()) {
            s3ObjectsToDownload.add(datasetImage.getS3Object());
        }
        return s3ObjectsToDownload;
    }

}
