package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.model.entities.Classification;
import org.kinsecta.webapp.api.model.util.ServiceLink;
import org.kinsecta.webapp.api.v1.model.ClassificationDto;
import org.mapstruct.*;


@Mapper(
    componentModel = "spring",
    uses = {
        DtoMapperConfig.class,
        DatasetDtoMapper.class,
        DatasetImageDtoMapper.class,
        MeasurementWingbeatDtoMapper.class,
        InsectOrderDtoMapper.class,
        InsectFamilyDtoMapper.class,
        InsectGenusDtoMapper.class,
        InsectSpeciesDtoMapper.class,
        ServiceLink.class
    },
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
@DecoratedWith(ClassificationDtoMapperDecorator.class)
public interface ClassificationDtoMapper {

    ClassificationDto classificationToClassificationDto(Classification classification);

    /**
     * <ul>
     *     <li>insectOrder can be assigned directly using ServiceLink</li>
     *     <li>insectFamily, insectGenus and insectSpecies must be handled within ClassificationDtoMapperDecorator, since @Mapping.source cannot resolve JsonNullable properly</li>
     * </ul>
     */
    @Mapping(source = "insectOrder.id", target = "insectOrder", qualifiedByName = {"ServiceLink", "insectOrderById"})
    @Mapping(target = "insectFamily", ignore = true)
    @Mapping(target = "insectGenus", ignore = true)
    @Mapping(target = "insectSpecies", ignore = true)
    Classification classificationDtoToClassification(ClassificationDto classificationDto);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "id", ignore = true)
    @InheritConfiguration(name = "classificationDtoToClassification")
    Classification updateClassificationWithClassificationDto(ClassificationDto update, @MappingTarget Classification destination);

}
