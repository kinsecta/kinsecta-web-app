package org.kinsecta.webapp.api.service;

import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.exception.unchecked.WrongArgumentException;
import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.model.entities.UserRole;
import org.kinsecta.webapp.api.model.entities.UserStatus;
import org.kinsecta.webapp.api.model.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Service
public class UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;


    @Autowired
    public UserService(UserRepository userRepository, BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }


    @Transactional(readOnly = true)
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<User> getAllUsersByStatus(UserStatus status) {
        return userRepository.findAllByStatus(status);
    }

    @Transactional(readOnly = true)
    public List<User> getAllUsersOptionallyByStatus(UserStatus status) {
        List<User> userList;
        if (status != null) {
            userList = getAllUsersByStatus(UserStatus.valueOf(status.toString()));
        } else {
            userList = getAllUsers();
        }
        return userList;
    }

    @Transactional(readOnly = true)
    public List<User> getAllAdmins() {
        return userRepository.findAllByRole(UserRole.ADMIN);
    }

    @Transactional(readOnly = true)
    public User getUser(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new NotFoundException(String.format("Could not find user with id %d", id)));
    }

    public Optional<User> getOptionalUser(Long id) {
        return userRepository.findById(id);
    }

    @Transactional(readOnly = true)
    public User getUser(String username) {
        return userRepository.findByUsername(username).orElseThrow(() -> new NotFoundException(String.format("Could not find user with username '%s'", username)));
    }

    @Transactional(readOnly = true)
    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(() -> new NotFoundException(String.format("Could not find user with email address '%s'", email)));
    }

    @Transactional
    public User createUser(User user) {
        // Reset ID to null if it is present because we don't want to override an existing user
        user.setId(null);
        // Create a new User with a random default password.
        // User must activate its account by starting the password reset workflow.
        user.setPassword(passwordEncoder.encode(UUID.randomUUID().toString()));
        user.updateSecurityDetailsModified();
        return save(user);
    }

    @Transactional
    public void deleteUserById(Long id) {
        User user = getUser(id);
        // Checking all @OneToMany relations by a count without querying the whole linked data (if it has any)
        // https://stackoverflow.com/a/2913876/1128689
        if ((user.getUploads() == null || user.getUploads().isEmpty())
            && (user.getDatasets() == null || user.getDatasets().isEmpty())
            && (user.getExports() == null || user.getExports().isEmpty())
            && (user.getSensors() == null || user.getSensors().isEmpty())
        ) {
            // Delete User when it has no DB relations
            userRepository.deleteUserById(id);

        } else {
            // Otherwise, override user props and retain user so that uploads, datasets, etc.
            // created by this user still have valid DB constraints.
            UUID uuid = UUID.randomUUID();
            String usernameOverride = "deleted-user-" + uuid;
            String passwordOverride = passwordEncoder.encode(UUID.randomUUID().toString());
            user.setUsername(usernameOverride);
            user.setPassword(passwordOverride);
            user.setEmail(usernameOverride + "@app.kinsecta.org");
            user.setFullName("Deleted User");
            user.setStatus(UserStatus.DELETED);
            user.setDeleted(LocalDateTime.now());
            // update the security details modified timestamp so that all JWT tokens of the specified user render invalid immediately
            user.updateSecurityDetailsModified();
            save(user);
        }
    }

    @Transactional
    public User save(User user) {
        return userRepository.save(user);
    }

    @Transactional
    public User findUserByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow(
            () -> new NotFoundException(String.format("Token contains username '%s' which could not be found in database",
                username)));
    }

    public void checkCurrentUserPasswordMatchesPassword(String password, User currentUser) {
        String userPassword = currentUser.getPassword();
        if (!passwordEncoder.matches(password, userPassword)) {
            throw new WrongArgumentException("Password does not match existing password for user " + currentUser.getUsername());
        }
    }

    public static boolean isActiveUser(User user) {
        return user.getStatus().equals(UserStatus.ACTIVE) && user.getDeleted() == null;
    }

}
