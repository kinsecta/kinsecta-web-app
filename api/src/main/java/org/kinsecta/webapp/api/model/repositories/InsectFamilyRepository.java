package org.kinsecta.webapp.api.model.repositories;

import org.kinsecta.webapp.api.model.entities.InsectFamily;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface InsectFamilyRepository extends JpaRepository<InsectFamily, Long> {

    Optional<InsectFamily> findByGbifId(Long gbifId);

    Optional<InsectFamily> findByName(String name);

    List<InsectFamily> findAllByNameContains(String query);

}
