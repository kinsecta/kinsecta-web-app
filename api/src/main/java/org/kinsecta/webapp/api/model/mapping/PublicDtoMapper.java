package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.model.entities.Classification;
import org.kinsecta.webapp.api.model.entities.Dataset;
import org.kinsecta.webapp.api.model.entities.DatasetImage;
import org.kinsecta.webapp.api.model.repositories.projections.*;
import org.kinsecta.webapp.api.v1.model.CountDto;
import org.kinsecta.webapp.api.v1.model.PublicDatasetDetailsDto;
import org.kinsecta.webapp.api.v1.model.PublicSensorDetailsDto;
import org.kinsecta.webapp.api.v1.model.PublicSensorDto;
import org.mapstruct.*;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Optional;
import java.util.Set;


@Mapper(
    componentModel = MappingConstants.ComponentModel.SPRING,
    uses = {
        DtoMapperConfig.class
    },
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public interface PublicDtoMapper {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Mapping(target = "sensorId", source = "id")
    @Mapping(target = "sensorLocation.name", source = "name")
    @Mapping(target = "sensorLocation.postcode", source = "postcode")
    @Mapping(target = "sensorLocation.city", source = "city")
    @Mapping(target = "sensorLocation.latitude", source = "latitude")
    @Mapping(target = "sensorLocation.longitude", source = "longitude")
    PublicSensorDto toPublicSensorDto(PublicSensorProjection publicSensorProjection);

    @Mapping(target = "datasetId", source = "dataset.id")
    @Mapping(target = "measurementDatetime", source = "dataset.measurementDatetime")
    PublicSensorDetailsDto toPublicSensorDetailsDto(Classification classification);

    @AfterMapping
    default void classificationToPublicSensorDetailsDtoAfterMapping(@MappingTarget PublicSensorDetailsDto publicSensorDetailsDto, Classification classification) {
        if (classification.getInsectOrder() != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(classification.getInsectOrder().getName());
            if (classification.getInsectFamily() != null) {
                sb.append("; ");
                sb.append(classification.getInsectFamily().getName());
            }
            if (classification.getInsectGenus() != null) {
                sb.append("; ");
                sb.append(classification.getInsectGenus().getName());
            }
            if (classification.getInsectSpecies() != null) {
                sb.append("; ");
                sb.append(classification.getInsectSpecies().getName());
            }
            publicSensorDetailsDto.setClassification(sb.toString());
        }
        Set<DatasetImage> datasetImages = classification.getDataset().getDatasetImages();
        if (!datasetImages.isEmpty()) {
            Optional<DatasetImage> datasetImageOptional = datasetImages.stream().min(Comparator.comparing(DatasetImage::getId));
            datasetImageOptional.ifPresent(datasetImage -> publicSensorDetailsDto.setImageId(datasetImage.getId()));
        }
    }

    @Mapping(target = "datasetId", source = "id")
    @Mapping(target = "measurementDatetime", source = "measurementDatetime")
    PublicSensorDetailsDto toPublicSensorDetailsDto(Dataset dataset);

    @AfterMapping
    default void datasetToPublicSensorDetailsDtoAfterMapping(@MappingTarget PublicSensorDetailsDto publicSensorDetailsDto, Dataset dataset) {
        Set<DatasetImage> datasetImages = dataset.getDatasetImages();
        if (!datasetImages.isEmpty()) {
            Optional<DatasetImage> datasetImageOptional = datasetImages.stream().min(Comparator.comparing(DatasetImage::getId));
            datasetImageOptional.ifPresent(datasetImage -> publicSensorDetailsDto.setImageId(datasetImage.getId()));
        }
    }

    @Mapping(target = "count", source = "count")
    @Mapping(target = "key", source = "year")
    CountDto toCountDto(DatasetCountByYearProjection datasetCountByYearProjection);

    @Mapping(target = "count", source = "count")
    CountDto toCountDto(DatasetCountByMonthProjection datasetCountByMonthProjection);

    @AfterMapping
    default void monthToCountDtoAfterMapping(@MappingTarget CountDto countDto, DatasetCountByMonthProjection datasetCountByMonthProjection) {
        countDto.setKey(String.format("%s-%s", datasetCountByMonthProjection.getYear(), datasetCountByMonthProjection.getMonth()));
    }

    @Mapping(target = "count", source = "count")
    CountDto toCountDto(DatasetCountByWeekProjection datasetCountByWeekProjection);

    @AfterMapping
    default void weekToCountDtoAfterMapping(@MappingTarget CountDto countDto, DatasetCountByWeekProjection datasetCountByWeekProjection) {
        countDto.setKey(String.format("%s-%s", datasetCountByWeekProjection.getYear(), datasetCountByWeekProjection.getWeek()));
    }

    @Mapping(target = "count", source = "count")
    CountDto toCountDto(DatasetCountByDayProjection datasetCountByDayProjection);

    @AfterMapping
    default void dayToCountDtoAfterMapping(@MappingTarget CountDto countDto, DatasetCountByDayProjection datasetCountByDayProjection) {
        countDto.setKey(sdf.format(datasetCountByDayProjection.getDate()));
    }

    @Mapping(target = "datasetId", source = "dataset.id")
    @Mapping(target = "measurementDatetime", source = "dataset.measurementDatetime")
    @Mapping(target = "publicSensorLocation", source = "dataset.sensor.sensorLocation")
    PublicDatasetDetailsDto toPublicDatasetDetailsDto(Classification classification);

    @AfterMapping
    default void classificationToPublicDatasetDetailsDtoAfterMapping(@MappingTarget PublicDatasetDetailsDto publicDatasetDetailsDto, Classification classification) {
        if (classification.getInsectOrder() != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(classification.getInsectOrder().getName());
            if (classification.getInsectFamily() != null) {
                sb.append("; ");
                sb.append(classification.getInsectFamily().getName());
            }
            if (classification.getInsectGenus() != null) {
                sb.append("; ");
                sb.append(classification.getInsectGenus().getName());
            }
            if (classification.getInsectSpecies() != null) {
                sb.append("; ");
                sb.append(classification.getInsectSpecies().getName());
            }
            publicDatasetDetailsDto.setClassification(sb.toString());
        }
        Set<DatasetImage> datasetImages = classification.getDataset().getDatasetImages();
        if (!datasetImages.isEmpty()) {
            Optional<DatasetImage> datasetImageOptional = datasetImages.stream().min(Comparator.comparing(DatasetImage::getId));
            datasetImageOptional.ifPresent(datasetImage -> publicDatasetDetailsDto.setImageId(datasetImage.getId()));
        }
    }

    @Mapping(target = "datasetId", source = "dataset.id")
    @Mapping(target = "measurementDatetime", source = "dataset.measurementDatetime")
    @Mapping(target = "publicSensorLocation", source = "dataset.sensor.sensorLocation")
    PublicDatasetDetailsDto toPublicDatasetDetailsDto(Dataset dataset);

    @AfterMapping
    default void datasetToPublicDatasetDetailsDtoAfterMapping(@MappingTarget PublicDatasetDetailsDto publicDatasetDetailsDto, Dataset dataset) {
        Set<DatasetImage> datasetImages = dataset.getDatasetImages();
        if (!datasetImages.isEmpty()) {
            Optional<DatasetImage> datasetImageOptional = datasetImages.stream().min(Comparator.comparing(DatasetImage::getId));
            datasetImageOptional.ifPresent(datasetImage -> publicDatasetDetailsDto.setImageId(datasetImage.getId()));
        }
    }

}
