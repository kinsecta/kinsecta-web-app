package org.kinsecta.webapp.api.service;

import org.kinsecta.webapp.api.exception.unchecked.ForbiddenException;
import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.model.entities.InsectFamily;
import org.kinsecta.webapp.api.model.repositories.InsectFamilyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;


@Service
public class InsectFamilyService {

    private static final Logger logger = LoggerFactory.getLogger(InsectFamilyService.class);

    private final InsectFamilyRepository insectFamilyRepository;


    public InsectFamilyService(InsectFamilyRepository insectFamilyRepository) {
        this.insectFamilyRepository = insectFamilyRepository;
    }


    @Transactional
    public InsectFamily save(InsectFamily insectFamily) {
        return insectFamilyRepository.save(insectFamily);
    }

    @Transactional(readOnly = true)
    public InsectFamily getInsectFamily(Long insectFamilyId) {
        return insectFamilyRepository.findById(insectFamilyId).orElseThrow(() -> new NotFoundException(String.format("Could not find InsectFamily with id %d", insectFamilyId)));
    }

    @Transactional(readOnly = true)
    public InsectFamily getInsectFamily(String insectFamilyName) {
        return insectFamilyRepository.findByName(insectFamilyName).orElseThrow(() -> new NotFoundException(String.format("Could not find InsectFamily with name '%s' ", insectFamilyName)));
    }

    @Transactional(readOnly = true)
    public InsectFamily getInsectFamilyByGbifId(Long gbifId) {
        return insectFamilyRepository.findByGbifId(gbifId).orElseThrow(() -> new NotFoundException(String.format("Could not find InsectFamily with GBIF id %d ", gbifId)));
    }

    @Transactional(readOnly = true)
    public List<InsectFamily> getAllInsectFamilies() {
        return insectFamilyRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<InsectFamily> getAllInsectFamiliesForSearchQuery(String query) {
        String queryDecoded;
        try {
            queryDecoded = URLDecoder.decode(query, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
        return insectFamilyRepository.findAllByNameContains(queryDecoded.toLowerCase());
    }

    @Transactional
    public void delete(Long insectFamilyId) {
        InsectFamily insectFamily = getInsectFamily(insectFamilyId);
        if (!insectFamily.getInsectGenusList().isEmpty()) {
            throw new ForbiddenException(String.format("InsectFamily with id '%s' is linked to one or many InsectGenuses and therefore cannot be deleted", insectFamilyId));
        } else {
            insectFamilyRepository.deleteById(insectFamilyId);
            logger.info("Successfully deleted InsectFamily with id {}", insectFamilyId);
        }
    }

}
