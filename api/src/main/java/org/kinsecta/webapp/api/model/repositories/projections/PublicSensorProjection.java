package org.kinsecta.webapp.api.model.repositories.projections;

import java.math.BigDecimal;


public interface PublicSensorProjection {

    Long getId();

    String getName();

    Integer getPostcode();

    String getCity();

    BigDecimal getLatitude();

    BigDecimal getLongitude();

}
