package org.kinsecta.webapp.api.model.entities;


public enum UserStatus {
    ACTIVE,
    INACTIVE,
    DELETED
}
