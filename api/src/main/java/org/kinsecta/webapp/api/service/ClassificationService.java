package org.kinsecta.webapp.api.service;

import org.kinsecta.webapp.api.exception.unchecked.ForbiddenException;
import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.exception.unchecked.WrongArgumentException;
import org.kinsecta.webapp.api.model.entities.Classification;
import org.kinsecta.webapp.api.model.repositories.ClassificationRepository;
import org.kinsecta.webapp.api.model.repositories.DatasetRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
public class ClassificationService {

    private static final Logger logger = LoggerFactory.getLogger(ClassificationService.class);

    private final ClassificationRepository classificationRepository;
    private final DatasetRepository datasetRepository;
    private final InsectSpeciesService insectSpeciesService;
    private final InsectGenusService insectGenusService;
    private final InsectFamilyService insectFamilyService;
    private final InsectOrderService insectOrderService;


    public ClassificationService(ClassificationRepository classificationRepository, DatasetRepository datasetRepository, InsectSpeciesService insectOrderService, InsectGenusService insectGenusService, InsectFamilyService insectFamilyService, InsectOrderService insectOrderService1) {
        this.classificationRepository = classificationRepository;
        this.datasetRepository = datasetRepository;
        this.insectSpeciesService = insectOrderService;
        this.insectGenusService = insectGenusService;
        this.insectFamilyService = insectFamilyService;
        this.insectOrderService = insectOrderService1;
    }


    @Transactional
    public Classification save(Classification classification) {
        if ((classification.getDataset() != null && (classification.getDatasetImage() != null || classification.getMeasurementWingbeat() != null))
            || (classification.getDatasetImage() != null && (classification.getDataset() != null || classification.getMeasurementWingbeat() != null))
            || (classification.getMeasurementWingbeat() != null && (classification.getDataset() != null || classification.getDatasetImage() != null))
        ) {
            // TODO add a test for this condition
            throw new WrongArgumentException("A Classification must only belong to one of Dataset, DatasetImage or MeasurementWingbeat but not to multiple.");
        }
        if (classification.getInsectOrder() != null) {
            classification.setInsectOrder(insectOrderService.save(classification.getInsectOrder()));
        }
        if (classification.getDataset() != null) {
            classification.getDataset().updateComputedModified();
            datasetRepository.save(classification.getDataset());
        }
        if (classification.getDatasetImage() != null) {
            classification.getDatasetImage().getDataset().updateComputedModified();
            datasetRepository.save(classification.getDatasetImage().getDataset());
        }
        if (classification.getMeasurementWingbeat() != null) {
            classification.getMeasurementWingbeat().getDataset().updateComputedModified();
            datasetRepository.save(classification.getMeasurementWingbeat().getDataset());
        }
        return classificationRepository.save(classification);
    }

    @Transactional(readOnly = true)
    public List<Classification> getAllClassifications() {
        return classificationRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Classification getClassification(Long classificationId) {
        return classificationRepository.findById(classificationId).orElseThrow(() -> new NotFoundException(String.format("Could not find Classification with id %d", classificationId)));
    }

    public Optional<Classification> getOptionalClassification(Long classificationId) {
        return classificationRepository.findById(classificationId);
    }

    @Transactional
    public void deleteClassificationById(Long classificationId) {
        Classification classification = getClassification(classificationId);
        if (classification.getDataset() != null || classification.getDatasetImage() != null || classification.getMeasurementWingbeat() != null) {
            throw new ForbiddenException(String.format("Classification with id '%s' is linked to another entity and therefore cannot be deleted", classification.getId()));
        } else {
            classificationRepository.deleteById(classificationId);
            logger.info("Successfully deleted Classification with id {}", classification);
        }
    }

    public List<Classification> getClassificationsForDatasetId(Long datasetId) {
        return classificationRepository.findAllByDatasetId(datasetId);
    }

    public List<Classification> getClassificationsForDatasetImageId(Long datasetImageId) {
        return classificationRepository.findAllByDatasetImageId(datasetImageId);
    }

    public List<Classification> getClassificationsForMeasurementWingbeatId(Long measurementWingbeatId) {
        return classificationRepository.findAllByMeasurementWingbeatId(measurementWingbeatId);
    }

}
