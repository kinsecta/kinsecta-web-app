package org.kinsecta.webapp.api.model.mapping;

import org.kinsecta.webapp.api.model.entities.Classification;
import org.kinsecta.webapp.api.model.entities.InsectFamily;
import org.kinsecta.webapp.api.model.entities.InsectGenus;
import org.kinsecta.webapp.api.model.entities.InsectSpecies;
import org.kinsecta.webapp.api.service.InsectFamilyService;
import org.kinsecta.webapp.api.service.InsectGenusService;
import org.kinsecta.webapp.api.service.InsectSpeciesService;
import org.kinsecta.webapp.api.v1.model.ClassificationDto;
import org.kinsecta.webapp.api.v1.model.InsectFamilyDto;
import org.kinsecta.webapp.api.v1.model.InsectGenusDto;
import org.kinsecta.webapp.api.v1.model.InsectSpeciesDto;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


public abstract class ClassificationDtoMapperDecorator implements ClassificationDtoMapper {

    @Autowired
    @Qualifier("delegate")
    private ClassificationDtoMapper delegate;

    @Autowired
    private InsectFamilyService insectFamilyService;

    @Autowired
    private InsectGenusService insectGenusService;

    @Autowired
    private InsectSpeciesService insectSpeciesService;

    @Override
    public Classification classificationDtoToClassification(ClassificationDto classificationDto) {
        Classification classification = delegate.classificationDtoToClassification(classificationDto);

        setInsectFamily(classification, classificationDto.getInsectFamily());
        setInsectGenus(classification, classificationDto.getInsectGenus());
        setInsectSpecies(classification, classificationDto.getInsectSpecies());

        return classification;
    }

    private void setInsectSpecies(Classification classification, JsonNullable<InsectSpeciesDto> insectSpecies) {
        if (insectSpecies.isPresent() && insectSpecies.get() != null && insectSpecies.get().getId().isPresent() && insectSpecies.get().getId().get() != null) {
            InsectSpecies insectSpeciesEntity = insectSpeciesService.getInsectSpecies(insectSpecies.get().getId().get());
            classification.setInsectSpecies(insectSpeciesEntity);
        } else if (insectSpecies.isPresent() && insectSpecies.get() == null) {
            classification.setInsectSpecies(null);
        }
    }

    private void setInsectGenus(Classification classification, JsonNullable<InsectGenusDto> insectGenus) {
        if (insectGenus.isPresent() && insectGenus.get() != null && insectGenus.get().getId().isPresent() && insectGenus.get().getId().get() != null) {
            InsectGenus insectGenusEntity = insectGenusService.getInsectGenus(insectGenus.get().getId().get());
            classification.setInsectGenus(insectGenusEntity);
        } else if (insectGenus.isPresent() && insectGenus.get() == null) {
            classification.setInsectGenus(null);
        }
    }

    private void setInsectFamily(Classification classification, JsonNullable<InsectFamilyDto> insectFamily) {
        if (insectFamily.isPresent() && insectFamily.get() != null && insectFamily.get().getId().isPresent() && insectFamily.get().getId().get() != null) {
            InsectFamily insectFamilyEntity = insectFamilyService.getInsectFamily(insectFamily.get().getId().get());
            classification.setInsectFamily(insectFamilyEntity);
        } else if (insectFamily.isPresent() && insectFamily.get() == null) {
            classification.setInsectFamily(null);
        }
    }

    @Override
    public Classification updateClassificationWithClassificationDto(ClassificationDto update, Classification destination) {
        Classification classification = delegate.updateClassificationWithClassificationDto(update, destination);

        setInsectFamily(classification, update.getInsectFamily());
        setInsectGenus(classification, update.getInsectGenus());
        setInsectSpecies(classification, update.getInsectSpecies());

        return classification;
    }

}
