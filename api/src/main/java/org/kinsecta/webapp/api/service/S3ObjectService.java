package org.kinsecta.webapp.api.service;

import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.detect.Detector;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.model.entities.S3Object;
import org.kinsecta.webapp.api.model.repositories.S3ObjectRepository;
import org.kinsecta.webapp.api.s3.S3ClientStorageService;
import org.kinsecta.webapp.api.s3.StorageException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.*;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Service
public class S3ObjectService {

    private final S3ClientStorageService s3ClientStorageService;
    private final S3ObjectRepository s3ObjectRepository;

    private final Detector detector = new DefaultDetector();

    @Value("${app.timezone.default}")
    private String defaultTimezone;


    public S3ObjectService(S3ClientStorageService s3ClientStorageService, S3ObjectRepository s3ObjectRepository) {
        this.s3ClientStorageService = s3ClientStorageService;
        this.s3ObjectRepository = s3ObjectRepository;
    }

    public S3Object getS3Object(Long id) {
        return s3ObjectRepository.findById(id).orElseThrow(() -> new NotFoundException(String.format("Could not find S3Object with id %d", id)));
    }

    public Optional<S3Object> getOptionalS3Object(Long id) {
        return s3ObjectRepository.findById(id);
    }

    public S3Object storeFile(File file) throws IOException, StorageException {
        S3Object s3Object = new S3Object();
        s3Object.setFilename(file.getName());
        s3Object.setMediaType(overrideMediaType(detectMediaType(file)));
        s3Object.setBucket(s3ClientStorageService.getCurrentS3BucketName());
        s3Object.setKey(UUID.randomUUID().toString());
        s3Object.setFilesize(file.length());
        try (FileInputStream fis = new FileInputStream(file)) {
            s3ClientStorageService.store(
                s3Object.getBucket(),
                s3Object.getKey(), fis,
                s3Object.getFilesize());
        }
        return s3ObjectRepository.save(s3Object);
    }

    /**
     * Detects the MediaType / MimeType of a file with Apache Tika.
     *
     * @param file The input File
     *
     * @return A String representation of the detected MediaType
     *
     * @throws IOException if the Tika Detector cannot read the input File
     * @see <a href="https://tika.apache.org/2.3.0/detection.html">https://tika.apache.org/2.3.0/detection.html</a>
     */
    private String detectMediaType(File file) throws IOException {
        Metadata metadata = new Metadata();
        // TikaInputStream sets the TikaCoreProperties.RESOURCE_NAME_KEY when initialized with a file or path
        return detector.detect(TikaInputStream.get(file.toPath(), metadata), metadata).toString();
    }

    private String overrideMediaType(String mediaType) {
        return switch (mediaType) {
            case "audio/vnd.wave" -> "audio/wave";
            default -> mediaType;
        };
    }

    public void delete(S3Object s3Object) throws StorageException {
        s3ClientStorageService.delete(s3Object.getBucket(), s3Object.getKey());
        if (s3Object.getId() != null) {
            s3ObjectRepository.delete(s3Object);
        }
    }

    public InputStream getS3ContentAsStream(S3Object s3Object) throws StorageException {
        return s3ClientStorageService.load(s3Object.getBucket(), s3Object.getKey());
    }

    private HttpHeaders buildDownloadHeaders(S3Object s3Object) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(s3Object.getMediaType()));
        headers.setContentLength(s3Object.getFilesize());
        headers.setLastModified(s3Object.getModified().atZone(ZoneId.of(defaultTimezone)));
        headers.setCacheControl(CacheControl.maxAge(Duration.ofDays(30)).mustRevalidate());
        return headers;
    }

    public ResponseEntity<Resource> downloadAttachment(S3Object s3Object) {
        try {
            InputStreamResource resource = new InputStreamResource(getS3ContentAsStream(s3Object));
            HttpHeaders headers = buildDownloadHeaders(s3Object);
            headers.setContentDisposition(ContentDisposition.attachment().filename(s3Object.getFilename()).build());
            return new ResponseEntity<>(resource, headers, HttpStatus.OK);
        } catch (StorageException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    public ResponseEntity<Resource> downloadInline(S3Object s3Object) {
        try {
            InputStreamResource resource = new InputStreamResource(getS3ContentAsStream(s3Object));
            HttpHeaders headers = buildDownloadHeaders(s3Object);
            headers.setContentDisposition(ContentDisposition.inline().build());
            return new ResponseEntity<>(resource, headers, HttpStatus.OK);
        } catch (StorageException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    public void downloadS3ObjectsToLocalDirectory(List<S3Object> s3Objects, File directory) throws IOException, StorageException {
        for (S3Object s3Object : s3Objects) {
            File file = Paths.get(directory.toString(), s3Object.getFilename()).toFile();
            try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
                s3ClientStorageService.load(s3Object.getBucket(), s3Object.getKey(), fileOutputStream);
            }
        }
    }

}
