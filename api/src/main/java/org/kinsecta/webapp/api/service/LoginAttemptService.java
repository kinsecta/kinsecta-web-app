package org.kinsecta.webapp.api.service;

import org.kinsecta.webapp.api.model.entities.LoginAttempt;
import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.model.repositories.LoginAttemptRepository;
import org.kinsecta.webapp.api.model.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class LoginAttemptService {

    private final LoginAttemptRepository loginAttemptRepository;
    private final UserRepository userRepository;

    @Value("${app.security.max-login-attempts}")
    private Integer maxLoginAttempts;


    public LoginAttemptService(LoginAttemptRepository loginAttemptRepository, UserRepository userRepository) {
        this.loginAttemptRepository = loginAttemptRepository;
        this.userRepository = userRepository;
    }


    public boolean isLocked(User user) {
        LoginAttempt loginAttempt = user.getLoginAttempt();
        if (loginAttempt == null) {
            Optional<LoginAttempt> loginAttemptOpt = loginAttemptRepository.findByUser(user);
            loginAttempt = loginAttemptOpt.orElseGet(() -> createNewLoginAttempt(user));
        }
        return isLocked(loginAttempt);
    }

    public boolean isLocked(LoginAttempt loginAttempt) {
        return loginAttempt.getFailedAttempts() >= maxLoginAttempts;
    }

    private LoginAttempt createNewLoginAttempt(User user) {
        LoginAttempt newLoginAttempt = new LoginAttempt();
        newLoginAttempt.setFailedAttempts(0);
        newLoginAttempt.setUser(user);
        return loginAttemptRepository.save(newLoginAttempt);
    }

    public void loginSucceeded(String username) {
        Optional<User> optUser = userRepository.findByUsername(username);
        optUser.ifPresent(this::loginSucceeded);
    }

    public void loginSucceeded(User user) {
        LoginAttempt loginAttempt = user.getLoginAttempt();
        if (loginAttempt == null) {
            Optional<LoginAttempt> loginAttemptOpt = loginAttemptRepository.findByUser(user);
            loginAttempt = loginAttemptOpt.orElseGet(() -> createNewLoginAttempt(user));
        }
        loginAttempt.setFailedAttempts(0);
        loginAttemptRepository.save(loginAttempt);
    }

    public Optional<LoginAttempt> loginFailed(String username) {
        Optional<User> optUser = userRepository.findByUsername(username);
        if (optUser.isPresent()) {
            User user = optUser.get();
            LoginAttempt loginAttempt = user.getLoginAttempt();
            if (loginAttempt == null) {
                Optional<LoginAttempt> loginAttemptOpt = loginAttemptRepository.findByUser(optUser.get());
                loginAttempt = loginAttemptOpt.orElseGet(() -> createNewLoginAttempt(optUser.get()));
            }
            loginAttempt.setFailedAttempts(loginAttempt.getFailedAttempts() + 1);
            return Optional.of(loginAttemptRepository.save(loginAttempt));
        } else {
            // do nothing because we don't log login attempts for unknown users
            return Optional.empty();
        }
    }

    public Integer getRemainingAttempts(String username) {
        Optional<User> optUser = userRepository.findByUsername(username);
        if (optUser.isPresent()) {
            Optional<LoginAttempt> loginAttemptOpt = loginAttemptRepository.findByUser(optUser.get());
            return loginAttemptOpt.map(this::getRemainingAttempts).orElseGet(() -> maxLoginAttempts);
        } else {
            return null;
        }
    }

    public int getRemainingAttempts(LoginAttempt loginAttempt) {
        Integer failedAttemptsMaybeNull = loginAttempt.getFailedAttempts();
        int failedAttempts = failedAttemptsMaybeNull != null ? failedAttemptsMaybeNull : 0;
        // make sure to always return an int >= 0
        return Math.max(maxLoginAttempts - failedAttempts, 0);
    }

}
