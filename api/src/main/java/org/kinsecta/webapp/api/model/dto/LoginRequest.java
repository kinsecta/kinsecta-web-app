package org.kinsecta.webapp.api.model.dto;

import org.kinsecta.webapp.api.model.entities.UserStatus;


public class LoginRequest {

    private String username;
    private String password;
    private UserStatus status;


    public LoginRequest(String username, String password, UserStatus status) {
        this.username = username;
        this.password = password;
        this.status = status;
    }

    public LoginRequest() {
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

}
