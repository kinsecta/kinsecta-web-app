package org.kinsecta.webapp.api.context;

import org.kinsecta.webapp.api.model.entities.S3Object;
import org.kinsecta.webapp.api.model.entities.Upload;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * This context class is used during processing of an uploaded ZIP file
 * as a temporary object holder to store uploaded or processed objects so
 * that we can roll back uploads or other actions in case of a failure in
 * a later stage of the upload processing.
 */
public class KinsectaMeasurementUploadContext {

    private Upload upload;
    private File tempDir;
    private final List<S3Object> uploadedObjects = new ArrayList<>();
    private final List<String> errorMessages = new ArrayList<>();


    public Upload getUpload() {
        return upload;
    }

    public void setUpload(Upload upload) {
        this.upload = upload;
    }

    public File getTempDir() {
        return tempDir;
    }

    public void setTempDir(File tempDir) {
        this.tempDir = tempDir;
    }

    public List<S3Object> getUploadedObjects() {
        return uploadedObjects;
    }

    public void addUploadedObject(S3Object s3Object) {
        this.uploadedObjects.add(s3Object);
    }

    public List<String> getErrorMessages() {
        return errorMessages;
    }

    public void addErrorMessage(String errorMessage) {
        this.errorMessages.add(errorMessage);
    }

    public boolean hasErrorMessages() {
        return !hasNoErrorMessages();
    }

    public boolean hasNoErrorMessages() {
        return errorMessages.isEmpty();
    }

}
