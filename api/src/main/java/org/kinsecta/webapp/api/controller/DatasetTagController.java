package org.kinsecta.webapp.api.controller;

import org.kinsecta.webapp.api.model.entities.DatasetTag;
import org.kinsecta.webapp.api.model.mapping.DatasetTagDtoMapper;
import org.kinsecta.webapp.api.service.DatasetTagService;
import org.kinsecta.webapp.api.v1.DatasetTagsApi;
import org.kinsecta.webapp.api.v1.model.DatasetTagDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Optional;


@Controller
public class DatasetTagController extends BaseController implements DatasetTagsApi {

    private final DatasetTagDtoMapper datasetTagDtoMapper;
    private final DatasetTagService datasetTagService;


    public DatasetTagController(DatasetTagDtoMapper datasetTagDtoMapper, DatasetTagService datasetTagService) {
        this.datasetTagDtoMapper = datasetTagDtoMapper;
        this.datasetTagService = datasetTagService;
    }


    @Override
    public ResponseEntity<DatasetTagDto> createDatasetTag(DatasetTagDto datasetTagDto) {
        DatasetTag datasetTag = datasetTagDtoMapper.datasetTagDtoToDatasetTag(datasetTagDto);
        DatasetTag createDatasetTag = datasetTagService.createDatasetTag(datasetTag);
        return new ResponseEntity<>(datasetTagDtoMapper.datasetTagToDatasetTagDto(createDatasetTag), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<DatasetTagDto>> getAllDatasetTags(Optional<Boolean> autoTag) {
        List<DatasetTag> datasetTagsList = datasetTagService.getAllDatasetTags(autoTag);
        List<DatasetTagDto> dtoList = datasetTagsList.stream().map(datasetTagDtoMapper::datasetTagToDatasetTagDto).toList();
        return ResponseEntity.ok(dtoList);
    }

    @Override
    public ResponseEntity<DatasetTagDto> getDatasetTag(Long datasetTagId) {
        DatasetTag datasetTag = datasetTagService.getDatasetTag(datasetTagId);
        DatasetTagDto datasetTagDto = datasetTagDtoMapper.datasetTagToDatasetTagDto(datasetTag);
        return ResponseEntity.ok(datasetTagDto);
    }

    @Override
    public ResponseEntity<Void> deleteDatasetTag(Long datasetTagId) {
        datasetTagService.deleteDatasetTagById(datasetTagId);
        return ResponseEntity.ok().build();
    }

}
