package org.kinsecta.webapp.api.exception.unchecked;

/**
 * This custom exception indicates an unsupported Request Query Parameter filter.
 * <p>
 * The exception and especially the response status is handled in our
 * {@linkplain org.kinsecta.webapp.api.exception.GlobalExceptionHandler GlobalExceptionHandler}.
 */
public class UnsupportedQueryFilterException extends RuntimeException {

    public UnsupportedQueryFilterException(String message) {
        super(message);
    }

}
