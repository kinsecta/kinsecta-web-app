UPDATE
    dataset d,
    upload u
SET
    d.user_id = u.user_id
WHERE
    d.upload_id = u.id;

