ALTER TABLE insect_species
    ADD synonym_for_species_id BIGINT UNSIGNED NULL;

ALTER TABLE insect_species
    ADD CONSTRAINT FK_INSECT_SPECIES_ON_SYNONYM_FOR_SPECIES FOREIGN KEY (synonym_for_species_id) REFERENCES insect_species (id);
