CREATE TABLE classification
(
    id                      BIGINT UNSIGNED AUTO_INCREMENT            NOT NULL,
    dataset_id              BIGINT UNSIGNED                           NULL,
    dataset_image_id        BIGINT UNSIGNED                           NULL,
    measurement_wingbeat_id BIGINT UNSIGNED                           NULL,
    insect_order_id         BIGINT UNSIGNED                           NOT NULL,
    insect_family_id        BIGINT UNSIGNED                           NULL,
    insect_genus_id         BIGINT UNSIGNED                           NULL,
    insect_species_id       BIGINT UNSIGNED                           NULL,
    type                    VARCHAR(255)                              NOT NULL,
    probability             DECIMAL(6, 5)                             NOT NULL,
    created                 datetime(3) DEFAULT NOW()                 NOT NULL,
    modified                datetime(3) DEFAULT NOW() ON UPDATE NOW() NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE dataset
(
    id                                    BIGINT UNSIGNED AUTO_INCREMENT            NOT NULL,
    upload_id                             BIGINT UNSIGNED                           NOT NULL,
    user_id                               BIGINT UNSIGNED                           NOT NULL,
    sensor_id                             BIGINT UNSIGNED                           NOT NULL,
    video_s3_object_id                    BIGINT UNSIGNED                           NULL,
    measurement_wingbeat_id               BIGINT UNSIGNED                           NULL,
    measurement_datetime                  datetime(3)                               NOT NULL,
    measurement_size_insect_length        DECIMAL(3, 1)                             NULL,
    measurement_size_insect_width         DECIMAL(3, 1)                             NULL,
    measurement_temperature_value         DECIMAL(3, 1)                             NULL,
    measurement_air_pressure_value        SMALLINT                                  NULL,
    measurement_humidity_value            DECIMAL(4, 1)                             NULL,
    measurement_luminosity_value          DECIMAL(5, 1)                             NULL,
    measurement_spectrum_channel_1        SMALLINT                                  NULL,
    measurement_spectrum_channel_2        SMALLINT                                  NULL,
    measurement_spectrum_channel_3        SMALLINT                                  NULL,
    measurement_spectrum_channel_4        SMALLINT                                  NULL,
    measurement_spectrum_channel_5        SMALLINT                                  NULL,
    measurement_spectrum_channel_6        SMALLINT                                  NULL,
    measurement_spectrum_channel_7        SMALLINT                                  NULL,
    measurement_spectrum_channel_8        SMALLINT                                  NULL,
    measurement_spectrum_channel_ir       SMALLINT                                  NULL,
    measurement_spectrum_clear_light      SMALLINT                                  NULL,
    measurement_particulate_matter_size_1 SMALLINT                                  NULL,
    measurement_particulate_matter_size_2 SMALLINT                                  NULL,
    measurement_particulate_matter_size_3 SMALLINT                                  NULL,
    measurement_particulate_matter_size_4 SMALLINT                                  NULL,
    measurement_particulate_matter_size_5 SMALLINT                                  NULL,
    measurement_particulate_matter_size_6 SMALLINT                                  NULL,
    measurement_rainfall_quantity         DECIMAL(5, 2)                             NULL,
    measurement_wind_sensor_speed         DECIMAL(5, 2)                             NULL,
    measurement_wind_sensor_direction     VARCHAR(10)                               NULL,
    created                               datetime(3) DEFAULT NOW()                 NOT NULL,
    modified                              datetime(3) DEFAULT NOW() ON UPDATE NOW() NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE dataset_image
(
    id           BIGINT UNSIGNED AUTO_INCREMENT            NOT NULL,
    dataset_id   BIGINT UNSIGNED                           NOT NULL,
    s3_object_id BIGINT UNSIGNED                           NOT NULL,
    created      datetime(3) DEFAULT NOW()                 NOT NULL,
    modified     datetime(3) DEFAULT NOW() ON UPDATE NOW() NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE dataset_tag
(
    id       BIGINT UNSIGNED AUTO_INCREMENT            NOT NULL,
    name     VARCHAR(255)                              NOT NULL,
    auto_tag TINYINT(1)  DEFAULT 0                     NOT NULL,
    created  datetime(3) DEFAULT NOW()                 NOT NULL,
    modified datetime(3) DEFAULT NOW() ON UPDATE NOW() NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE datasets_dataset_tags
(
    dataset_id     BIGINT UNSIGNED NOT NULL,
    dataset_tag_id BIGINT UNSIGNED NOT NULL,
    PRIMARY KEY (dataset_id, dataset_tag_id)
);

CREATE TABLE export
(
    id                        BIGINT UNSIGNED AUTO_INCREMENT            NOT NULL,
    user_id                   BIGINT UNSIGNED                           NOT NULL,
    filter_sensor_id          BIGINT UNSIGNED                           NULL,
    filter_sensor_location_id BIGINT UNSIGNED                           NULL,
    filter_start_date         date                                      NOT NULL,
    filter_end_date           date                                      NOT NULL,
    filter_tags               VARCHAR(255)                              NULL,
    status                    VARCHAR(15)                               NOT NULL,
    finished                  datetime(3)                               NULL,
    download_token            VARCHAR(255)                              NULL,
    expires                   datetime(3)                               NULL,
    created                   datetime(3) DEFAULT NOW()                 NOT NULL,
    modified                  datetime(3) DEFAULT NOW() ON UPDATE NOW() NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE export_file
(
    id           BIGINT UNSIGNED AUTO_INCREMENT            NOT NULL,
    export_id    BIGINT UNSIGNED                           NOT NULL,
    s3_object_id BIGINT UNSIGNED                           NOT NULL,
    created      datetime(3) DEFAULT NOW()                 NOT NULL,
    modified     datetime(3) DEFAULT NOW() ON UPDATE NOW() NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE insect_family
(
    id       BIGINT UNSIGNED AUTO_INCREMENT            NOT NULL,
    order_id BIGINT UNSIGNED                           NOT NULL,
    name     VARCHAR(255)                              NOT NULL,
    gbif_id  BIGINT UNSIGNED                           NULL,
    created  datetime(3) DEFAULT NOW()                 NOT NULL,
    modified datetime(3) DEFAULT NOW() ON UPDATE NOW() NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE insect_genus
(
    id        BIGINT UNSIGNED AUTO_INCREMENT            NOT NULL,
    family_id BIGINT UNSIGNED                           NOT NULL,
    name      VARCHAR(255)                              NOT NULL,
    gbif_id   BIGINT UNSIGNED                           NULL,
    created   datetime(3) DEFAULT NOW()                 NOT NULL,
    modified  datetime(3) DEFAULT NOW() ON UPDATE NOW() NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE insect_order
(
    id       BIGINT UNSIGNED AUTO_INCREMENT            NOT NULL,
    name     VARCHAR(255)                              NOT NULL,
    gbif_id  BIGINT UNSIGNED                           NULL,
    created  datetime(3) DEFAULT NOW()                 NOT NULL,
    modified datetime(3) DEFAULT NOW() ON UPDATE NOW() NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE insect_species
(
    id       BIGINT UNSIGNED AUTO_INCREMENT            NOT NULL,
    genus_id BIGINT UNSIGNED                           NOT NULL,
    name     VARCHAR(255)                              NOT NULL,
    gbif_id  BIGINT UNSIGNED                           NULL,
    created  datetime(3) DEFAULT NOW()                 NOT NULL,
    modified datetime(3) DEFAULT NOW() ON UPDATE NOW() NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE measurement_wingbeat
(
    id                 BIGINT UNSIGNED AUTO_INCREMENT            NOT NULL,
    wav_s3_object_id   BIGINT UNSIGNED                           NOT NULL,
    image_s3_object_id BIGINT UNSIGNED                           NULL,
    sample_rate        MEDIUMINT                                 NOT NULL,
    created            datetime(3) DEFAULT NOW()                 NOT NULL,
    modified           datetime(3) DEFAULT NOW() ON UPDATE NOW() NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE s3_object
(
    id         BIGINT UNSIGNED AUTO_INCREMENT            NOT NULL,
    `key`      VARCHAR(255)                              NOT NULL,
    bucket     VARCHAR(255)                              NOT NULL,
    filename   VARCHAR(255)                              NOT NULL,
    media_type VARCHAR(255)                              NOT NULL,
    filesize   BIGINT UNSIGNED                           NOT NULL,
    created    datetime(3) DEFAULT NOW()                 NOT NULL,
    modified   datetime(3) DEFAULT NOW() ON UPDATE NOW() NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE sensor
(
    id                   BIGINT UNSIGNED AUTO_INCREMENT            NOT NULL,
    operator             BIGINT UNSIGNED                           NOT NULL,
    sensor_location_id   BIGINT UNSIGNED                           NOT NULL,
    hardware_platform    VARCHAR(255)                              NOT NULL,
    os                   VARCHAR(255)                              NOT NULL,
    camera_type          VARCHAR(255)                              NOT NULL,
    wingbeat_sensor_type VARCHAR(255)                              NOT NULL,
    bokeh_version        VARCHAR(255)                              NOT NULL,
    created              datetime(3) DEFAULT NOW()                 NOT NULL,
    modified             datetime(3) DEFAULT NOW() ON UPDATE NOW() NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE sensor_location
(
    id          BIGINT UNSIGNED AUTO_INCREMENT            NOT NULL,
    name        VARCHAR(255)                              NOT NULL,
    postcode    MEDIUMINT UNSIGNED                        NOT NULL,
    city        VARCHAR(255)                              NOT NULL,
    latitude    DECIMAL(9, 6)                             NOT NULL,
    longitude   DECIMAL(9, 6)                             NOT NULL,
    uncertainty TINYINT(1)  DEFAULT 0                     NOT NULL,
    created     datetime(3) DEFAULT NOW()                 NOT NULL,
    modified    datetime(3) DEFAULT NOW() ON UPDATE NOW() NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE upload
(
    id           BIGINT UNSIGNED AUTO_INCREMENT            NOT NULL,
    user_id      BIGINT UNSIGNED                           NOT NULL,
    s3_object_id BIGINT UNSIGNED                           NULL,
    status       VARCHAR(15)                               NOT NULL,
    finished     datetime(3)                               NULL,
    created      datetime(3) DEFAULT NOW()                 NOT NULL,
    modified     datetime(3) DEFAULT NOW() ON UPDATE NOW() NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE dataset_tag
    ADD CONSTRAINT uc_datasettag_name UNIQUE (name);

ALTER TABLE classification
    ADD CONSTRAINT FK_CLASSIFICATION_ON_DATASET FOREIGN KEY (dataset_id) REFERENCES dataset (id);

ALTER TABLE classification
    ADD CONSTRAINT FK_CLASSIFICATION_ON_DATASET_IMAGE FOREIGN KEY (dataset_image_id) REFERENCES dataset_image (id);

ALTER TABLE classification
    ADD CONSTRAINT FK_CLASSIFICATION_ON_INSECT_FAMILY FOREIGN KEY (insect_family_id) REFERENCES insect_family (id);

ALTER TABLE classification
    ADD CONSTRAINT FK_CLASSIFICATION_ON_INSECT_GENUS FOREIGN KEY (insect_genus_id) REFERENCES insect_genus (id);

ALTER TABLE classification
    ADD CONSTRAINT FK_CLASSIFICATION_ON_INSECT_ORDER FOREIGN KEY (insect_order_id) REFERENCES insect_order (id);

ALTER TABLE classification
    ADD CONSTRAINT FK_CLASSIFICATION_ON_INSECT_SPECIES FOREIGN KEY (insect_species_id) REFERENCES insect_species (id);

ALTER TABLE classification
    ADD CONSTRAINT FK_CLASSIFICATION_ON_MEASUREMENT_WINGBEAT FOREIGN KEY (measurement_wingbeat_id) REFERENCES measurement_wingbeat (id);

ALTER TABLE dataset_image
    ADD CONSTRAINT FK_DATASETIMAGE_ON_DATASET FOREIGN KEY (dataset_id) REFERENCES dataset (id);

ALTER TABLE dataset_image
    ADD CONSTRAINT FK_DATASETIMAGE_ON_S3_OBJECT FOREIGN KEY (s3_object_id) REFERENCES s3_object (id);

ALTER TABLE dataset
    ADD CONSTRAINT FK_DATASET_ON_MEASUREMENT_WINGBEAT FOREIGN KEY (measurement_wingbeat_id) REFERENCES measurement_wingbeat (id);

ALTER TABLE dataset
    ADD CONSTRAINT FK_DATASET_ON_SENSOR FOREIGN KEY (sensor_id) REFERENCES sensor (id);

ALTER TABLE dataset
    ADD CONSTRAINT FK_DATASET_ON_UPLOAD FOREIGN KEY (upload_id) REFERENCES upload (id);

ALTER TABLE dataset
    ADD CONSTRAINT FK_DATASET_ON_USER FOREIGN KEY (user_id) REFERENCES user (id);

ALTER TABLE dataset
    ADD CONSTRAINT FK_DATASET_ON_VIDEO_S3_OBJECT FOREIGN KEY (video_s3_object_id) REFERENCES s3_object (id);

ALTER TABLE export_file
    ADD CONSTRAINT FK_EXPORTFILE_ON_EXPORT FOREIGN KEY (export_id) REFERENCES export (id);

ALTER TABLE export_file
    ADD CONSTRAINT FK_EXPORTFILE_ON_S3_OBJECT FOREIGN KEY (s3_object_id) REFERENCES s3_object (id);

ALTER TABLE export
    ADD CONSTRAINT FK_EXPORT_ON_FILTER_SENSOR FOREIGN KEY (filter_sensor_id) REFERENCES sensor (id);

ALTER TABLE export
    ADD CONSTRAINT FK_EXPORT_ON_FILTER_SENSOR_LOCATION FOREIGN KEY (filter_sensor_location_id) REFERENCES sensor_location (id);

ALTER TABLE export
    ADD CONSTRAINT FK_EXPORT_ON_USER FOREIGN KEY (user_id) REFERENCES user (id);

ALTER TABLE insect_family
    ADD CONSTRAINT FK_INSECTFAMILY_ON_ORDER FOREIGN KEY (order_id) REFERENCES insect_order (id);

ALTER TABLE insect_genus
    ADD CONSTRAINT FK_INSECTGENUS_ON_FAMILY FOREIGN KEY (family_id) REFERENCES insect_family (id);

ALTER TABLE insect_species
    ADD CONSTRAINT FK_INSECTSPECIES_ON_GENUS FOREIGN KEY (genus_id) REFERENCES insect_genus (id);

ALTER TABLE measurement_wingbeat
    ADD CONSTRAINT FK_MEASUREMENTWINGBEAT_ON_IMAGE_S3_OBJECT FOREIGN KEY (image_s3_object_id) REFERENCES s3_object (id);

ALTER TABLE measurement_wingbeat
    ADD CONSTRAINT FK_MEASUREMENTWINGBEAT_ON_WAV_S3_OBJECT FOREIGN KEY (wav_s3_object_id) REFERENCES s3_object (id);

ALTER TABLE sensor
    ADD CONSTRAINT FK_SENSOR_ON_OPERATOR FOREIGN KEY (operator) REFERENCES user (id);

ALTER TABLE sensor
    ADD CONSTRAINT FK_SENSOR_ON_SENSOR_LOCATION FOREIGN KEY (sensor_location_id) REFERENCES sensor_location (id);

ALTER TABLE upload
    ADD CONSTRAINT FK_UPLOAD_ON_S3_OBJECT FOREIGN KEY (s3_object_id) REFERENCES s3_object (id);

ALTER TABLE upload
    ADD CONSTRAINT FK_UPLOAD_ON_USER FOREIGN KEY (user_id) REFERENCES user (id);

ALTER TABLE datasets_dataset_tags
    ADD CONSTRAINT fk_datdattag_on_dataset FOREIGN KEY (dataset_id) REFERENCES dataset (id);

ALTER TABLE datasets_dataset_tags
    ADD CONSTRAINT fk_datdattag_on_dataset_tag FOREIGN KEY (dataset_tag_id) REFERENCES dataset_tag (id);
