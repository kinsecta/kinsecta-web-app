ALTER TABLE `export`
    DROP FOREIGN KEY `FK_EXPORT_ON_FILTER_SENSOR`,
    DROP FOREIGN KEY `FK_EXPORT_ON_FILTER_SENSOR_LOCATION`,
    DROP COLUMN `filter_sensor_id`,
    DROP COLUMN `filter_sensor_location_id`,
    DROP COLUMN `filter_start_date`,
    DROP COLUMN `filter_end_date`,
    DROP COLUMN `filter_tags`;
