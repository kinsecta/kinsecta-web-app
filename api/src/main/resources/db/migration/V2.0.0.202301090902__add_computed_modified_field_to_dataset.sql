ALTER TABLE dataset
    ADD COLUMN computed_modified datetime(3) null;

UPDATE dataset
    SET dataset.computed_modified = dataset.modified;

ALTER TABLE dataset
    CHANGE COLUMN computed_modified computed_modified datetime(3) default current_timestamp(3) not null on update current_timestamp(3);

