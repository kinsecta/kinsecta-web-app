-- migrate those classifications that reference an insect_species which have a synonym, directly to the insect_species references by the synonym.
update classification as c
    join insect_species as i on c.insect_species_id = i.id
set c.insect_species_id = i.synonym_for_species_id
where i.synonym_for_species_id is not null;

-- remove foreign key constraint
alter table insect_species
    drop foreign key FK_INSECT_SPECIES_ON_SYNONYM_FOR_SPECIES;

-- delete insect species that have a synonym
delete
from insect_species
where synonym_for_species_id is not null;

-- delete column synonym_for_species_id
alter table insect_species
    drop column synonym_for_species_id;


-- remove duplicates from all insects
update insect_order as io
    join insect_order as io2
    on io.gbif_id = io2.gbif_id
    join classification c on io2.id = c.insect_order_id
    join insect_family i on i.order_id = io2.id
set c.insect_order_id = io.id
    and i.order_id = io.id
where io.id < io2.id;

delete
from insect_order
where id in
      (select io2.id
       from insect_order as io
                join insect_order as io2
                     on io.gbif_id = io2.gbif_id
                         and io.id < io2.id);

update insect_family as ifa
    join insect_family as ifa2
    on ifa.gbif_id = ifa2.gbif_id
    join classification c on ifa2.id = c.insect_family_id
    join insect_genus i on i.family_id = ifa2.id
set c.insect_family_id = ifa.id
    and i.family_id = ifa.id
where ifa.id < ifa2.id;

delete
from insect_family
where id in
      (select ifa2.id
       from insect_family as ifa
                join insect_family as ifa2
                     on ifa.gbif_id = ifa2.gbif_id
                         and ifa.id < ifa2.id);

update insect_genus as ig
    join insect_genus as ig2
    on ig.gbif_id = ig2.gbif_id
    join classification c on ig2.id = c.insect_genus_id
    join insect_species i on i.genus_id = ig2.id
set c.insect_genus_id = ig.id
    and i.genus_id = ig.id
where ig.id < ig2.id;

delete
from insect_genus
where id in
      (select ig2.id
       from insect_genus as ig
                join insect_genus as ig2
                     on ig.gbif_id = ig2.gbif_id
                         and ig.id < ig2.id);

update insect_species as isp
    join insect_species as isp2
    on isp.gbif_id = isp2.gbif_id
    join classification c on isp2.id = c.insect_species_id
set c.insect_species_id = isp.id
where isp.id < isp2.id;

delete
from insect_species
where id in
      (select isp2.id
       from insect_species as isp
                join insect_species as isp2
                     on isp.gbif_id = isp2.gbif_id
                         and isp.id < isp2.id);

-- Add unique constraint for all gbif ids.
alter table insect_order
    add constraint insect_order_gbif_id_uc unique (gbif_id);

alter table insect_family
    add constraint insect_family_gbif_id_uc unique (gbif_id);

alter table insect_genus
    add constraint insect_genus_gbif_id_uc unique (gbif_id);

alter table insect_species
    add constraint insect_species_gbif_id_uc unique (gbif_id);
