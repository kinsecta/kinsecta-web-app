alter table dataset
    modify measurement_luminosity decimal(6, 1) unsigned null;

alter table dataset
    modify measurement_spectrum_channel_415nm smallint(6) unsigned null;

alter table dataset
    modify measurement_spectrum_channel_445nm smallint(6) unsigned null;

alter table dataset
    modify measurement_spectrum_channel_480nm smallint(6) unsigned null;

alter table dataset
    modify measurement_spectrum_channel_515nm smallint(6) unsigned null;

alter table dataset
    modify measurement_spectrum_channel_555nm smallint(6) unsigned null;

alter table dataset
    modify measurement_spectrum_channel_590nm smallint(6) unsigned null;

alter table dataset
    modify measurement_spectrum_channel_630nm smallint(6) unsigned null;

alter table dataset
    modify measurement_spectrum_channel_680nm smallint(6) unsigned null;

alter table dataset
    modify measurement_spectrum_near_ir smallint(6) unsigned null;

alter table dataset
    modify measurement_spectrum_clear_light smallint(6) unsigned null;

