alter table sensor
    add show_public tinyint(1) default 0 not null after sensor_location_id;
