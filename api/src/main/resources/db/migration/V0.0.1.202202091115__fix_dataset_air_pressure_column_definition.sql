ALTER TABLE dataset
    MODIFY measurement_air_pressure_value DECIMAL(4, 3) NULL;

