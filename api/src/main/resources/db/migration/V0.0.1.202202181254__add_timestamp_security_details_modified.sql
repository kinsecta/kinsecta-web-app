ALTER TABLE user
    ADD security_details_modified datetime(3) DEFAULT NOW() NOT NULL;
