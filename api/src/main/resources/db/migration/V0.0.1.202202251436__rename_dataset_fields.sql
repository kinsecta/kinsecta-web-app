ALTER TABLE dataset
    CHANGE measurement_air_pressure_value measurement_air_pressure DECIMAL(4, 3) NULL;

ALTER TABLE dataset
    CHANGE measurement_humidity_value measurement_humidity DECIMAL(4, 1) NULL;

ALTER TABLE dataset
    CHANGE measurement_luminosity_value measurement_luminosity DECIMAL(5, 1) NULL;

ALTER TABLE dataset
    CHANGE measurement_rainfall_quantity measurement_rainfall DECIMAL(5, 2) NULL;

ALTER TABLE dataset
    CHANGE measurement_temperature_value measurement_temperature DECIMAL(3, 1) NULL;
