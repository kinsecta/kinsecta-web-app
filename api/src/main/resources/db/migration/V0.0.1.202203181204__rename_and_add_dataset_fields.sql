-- #77 rename columns according to new spectrum keys

ALTER TABLE dataset
    CHANGE measurement_spectrum_channel_1 measurement_spectrum_channel_415nm SMALLINT;

ALTER TABLE dataset
    CHANGE measurement_spectrum_channel_2 measurement_spectrum_channel_445nm SMALLINT;

ALTER TABLE dataset
    CHANGE measurement_spectrum_channel_3 measurement_spectrum_channel_480nm SMALLINT;

ALTER TABLE dataset
    CHANGE measurement_spectrum_channel_4 measurement_spectrum_channel_515nm SMALLINT;

ALTER TABLE dataset
    CHANGE measurement_spectrum_channel_5 measurement_spectrum_channel_555nm SMALLINT;

ALTER TABLE dataset
    CHANGE measurement_spectrum_channel_6 measurement_spectrum_channel_590nm SMALLINT;

ALTER TABLE dataset
    CHANGE measurement_spectrum_channel_7 measurement_spectrum_channel_630nm SMALLINT;

ALTER TABLE dataset
    CHANGE measurement_spectrum_channel_8 measurement_spectrum_channel_680nm SMALLINT;

ALTER TABLE dataset
    CHANGE measurement_spectrum_channel_ir measurement_spectrum_near_ir SMALLINT;

-- #77 rename columns according to new particulate_matter keys

ALTER TABLE dataset
    CHANGE measurement_particulate_matter_size_1 measurement_particulate_matter_particles_03um SMALLINT;

ALTER TABLE dataset
    CHANGE measurement_particulate_matter_size_2 measurement_particulate_matter_particles_05um SMALLINT;

ALTER TABLE dataset
    CHANGE measurement_particulate_matter_size_3 measurement_particulate_matter_particles_10um SMALLINT;

ALTER TABLE dataset
    CHANGE measurement_particulate_matter_size_4 measurement_particulate_matter_particles_25um SMALLINT;

ALTER TABLE dataset
    CHANGE measurement_particulate_matter_size_5 measurement_particulate_matter_particles_50um SMALLINT;

ALTER TABLE dataset
    CHANGE measurement_particulate_matter_size_6 measurement_particulate_matter_particles_100um SMALLINT;


-- #77 add new columns according to new particulate_matter keys

ALTER TABLE dataset
    ADD measurement_particulate_matter_pm1 SMALLINT;

ALTER TABLE dataset
    ADD measurement_particulate_matter_pm2p5 SMALLINT;

ALTER TABLE dataset
    ADD measurement_particulate_matter_pm10 SMALLINT;
