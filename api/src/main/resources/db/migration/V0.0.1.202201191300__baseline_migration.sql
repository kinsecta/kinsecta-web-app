CREATE TABLE IF NOT EXISTS user
(
    id        BIGINT UNSIGNED AUTO_INCREMENT            NOT NULL,
    username  VARCHAR(255)                              NOT NULL,
    password  VARCHAR(255)                              NOT NULL,
    email     VARCHAR(255)                              NOT NULL,
    full_name VARCHAR(255)                              NOT NULL,
    `role`    VARCHAR(255)                              NOT NULL,
    status    VARCHAR(255)                              NOT NULL,
    created   DATETIME(3) DEFAULT NOW()                 NOT NULL,
    modified  DATETIME(3) DEFAULT NOW() ON UPDATE NOW() NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE user
    ADD CONSTRAINT uc_user_email UNIQUE (email),
    ADD CONSTRAINT uc_user_username UNIQUE (username);


CREATE TABLE IF NOT EXISTS login_attempt
(
    id              BIGINT UNSIGNED AUTO_INCREMENT            NOT NULL,
    user_id         BIGINT UNSIGNED                           NOT NULL,
    failed_attempts INT                                       NOT NULL,
    created         DATETIME(3) DEFAULT NOW()                 NOT NULL,
    modified        DATETIME(3) DEFAULT NOW() ON UPDATE NOW() NOT NULL,
    PRIMARY KEY (id)
);


ALTER TABLE login_attempt
    ADD CONSTRAINT uc_loginattempt_user UNIQUE (user_id),
    ADD CONSTRAINT fk_loginattempt_on_user FOREIGN KEY (user_id) REFERENCES user (id);


CREATE TABLE IF NOT EXISTS reset_token
(
    id       BIGINT UNSIGNED AUTO_INCREMENT            NOT NULL,
    user_id  BIGINT UNSIGNED                           NOT NULL,
    token    VARCHAR(64)                               NOT NULL,
    expires  DATETIME(3)                               NOT NULL,
    created  DATETIME(3) DEFAULT NOW()                 NOT NULL,
    modified DATETIME(3) DEFAULT NOW() ON UPDATE NOW() NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE reset_token
    ADD CONSTRAINT uc_resettoken_user UNIQUE (user_id),
    ADD CONSTRAINT uc_resettoken_token UNIQUE (token),
    ADD CONSTRAINT fk_resettoken_on_user FOREIGN KEY (user_id) REFERENCES user (id);
