ALTER TABLE sensor_location
    CHANGE uncertainty gps_masked TINYINT(1) DEFAULT 0 NOT NULL;
