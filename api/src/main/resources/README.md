# Database Migrations

All database migrations go in these folders:

* `db/` - SQL scripts representing dummy data or helper scripts
  * `db/migration/` - migrations which are managed with FlyWay via SpringBoot
* `fixtures/` - SQL scripts representing manual test data
  * `fixtures/migration/` - test migrations which are managed with FlyWay via SpringBoot


## FlyWay

In this project, database versioning is done with FlyWay via SpringBoot.

Read more about FlyWay at https://flywaydb.org/documentation/ and about FlyWay Migrations at https://flywaydb.org/documentation/concepts/migrations.html

### Naming scheme

Each versioned migration must be assigned a unique version. Read more about possible naming schemes at https://flywaydb.org/documentation/concepts/migrations.html#versioned-migrations

We use the following scheme:

```
┌ FlyWay Prefix
│ ┌ FlyWay Version  ┌ Separator   ┌ Suffix
│╭┴───────────────╮╭┤            ╭┴─╮
V1.0.0.202103171406__update-table.sql
 ╰┬──╯ ╰┬─────────╯  ╰─────┬────╯
  │     └ Timestamp        └ Description
  │       YYYYMMDDHHSS
  └ Application version
```

The file name consists of the following parts:

* **FlyWay Prefix:** `V` for _versioned_, `U` for _undo_ and `R` for _repeatable_ migrations
* **FlyWay Version:** Version with dots or underscores separate as many parts as you like (Not to be used for repeatable migrations!)
  * **Application version:** the Maven application version with dots
  * **Timestamp:** a timestamp in the form of `YYYYMMDDHHSS`
* **Separator:** `__` (two underscores)
* **Description:** Underscores or spaces separate the words
* **Suffix:** `.sql`

### Naming scheme for tests

For integration tests, we want to apply test migrations always on top of our default db migrations.

This is ensured with a different naming scheme with a significantly higher Flyway version number:

```
┌ FlyWay Prefix
│     ┌ Separator   ┌ Suffix
│    ╭┤            ╭┴─╮
V1000__update-table.sql
 ╰┬─╯  ╰─────┬────╯
  │          └ Description
  └ FlyWay Version
```

The **Flyway Version** does **not** contain the Application version or a timestamp! It's just a simple integer starting at the value of
* `1000` for the `model` package
* `2000` for the `api` package
* `3000` for the `importer` package

Currently, we don't expect a significant number of versioned integration tests as we plan to put all integration test data in the baseline fixture `V1000__integration_test_data.sql`. However, in case we need separate test migrations we're prepared for 999 additional files per module ;-)
