package org.kinsecta.webapp.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.TestHelperService;
import org.kinsecta.webapp.api.model.entities.ResetToken;
import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.model.mapping.UserDtoMapper;
import org.kinsecta.webapp.api.model.repositories.ResetTokenRepository;
import org.kinsecta.webapp.api.service.ResetTokenService;
import org.kinsecta.webapp.api.service.UserService;
import org.kinsecta.webapp.api.v1.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("it")
@SpringBootTest
@AutoConfigureMockMvc
class AccountControllerIT {

    private static final String ACCOUNT_PATH = "/account";
    private static final String ACCOUNT_PATH_PASSWORD = "/account/password";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserDtoMapper userDtoMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private ResetTokenRepository resetTokenRepository;

    @Autowired
    private ResetTokenService resetTokenService;

    @Autowired
    private TestHelperService testHelperService;


    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "t.fischer")
    void saveAccountProfile() throws Exception {
        User user = userService.getUser(1L);

        UserDto userDto = userDtoMapper.userToUserDto(user);
        userDto.setFullName("Test User");

        String contentString = objectMapper.writeValueAsString(userDto);

        MvcResult mvcResult = mockMvc.perform(put(ACCOUNT_PATH + "/profile")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(contentString)
                .with(csrf()))
            .andExpect(status().isOk())
            .andReturn();

        UserDto returnedDto = testHelperService.convertResponseBodyToDto(mvcResult, UserDto.class);
        assertEquals("Test User", returnedDto.getFullName());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getCurrentAccount() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(ACCOUNT_PATH + "/current"))
            .andExpect(status().isOk())
            .andReturn();

        UserDto returnedDto = testHelperService.convertResponseBodyToDto(mvcResult, UserDto.class);
        assertEquals("t.fischer", returnedDto.getUsername());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "t.fischer")
    void changePassword() throws Exception {
        PasswordChangeDto changeDto = new PasswordChangeDto();
        changeDto.setType(PasswordDto.TypeEnum.PASSWORD_CHANGE);
        changeDto.setCurrentPassword("password");
        changeDto.setNewPassword("1234$ABCD%abcd");
        changeDto.setConfNewPassword("1234$ABCD%abcd");

        String contentString = objectMapper.writeValueAsString(changeDto);

        mockMvc.perform(put(ACCOUNT_PATH_PASSWORD)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(contentString))
            .andExpect(status().isOk());
    }

    @Test
    @Transactional
    void startPasswordResetFlow() throws Exception {
        User user = userService.getUser(1L);

        PasswordResetRequestDto requestDto = new PasswordResetRequestDto();
        requestDto.setUserEmail("t.fischer@goldflam.de");

        String contentString = objectMapper.writeValueAsString(requestDto);

        mockMvc.perform(post(ACCOUNT_PATH_PASSWORD + "/begin_reset")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(contentString))
            .andExpect(status().isOk());

        Optional<ResetToken> optionalResetToken = resetTokenRepository.findByUser(user);
        assertTrue(optionalResetToken.isPresent());
    }

    @Test
    @Transactional
    void resetPassword() throws Exception {
        User user = userService.getUser(2L);
        ResetToken resetToken = resetTokenService.createResetToken(user);
        resetTokenRepository.save(resetToken);

        Optional<ResetToken> optionalResetToken = resetTokenRepository.findByUser(user);
        assertTrue(optionalResetToken.isPresent());

        PasswordResetDto resetDto = new PasswordResetDto();
        resetDto.setType(PasswordDto.TypeEnum.PASSWORD_RESET);
        resetDto.setToken(resetToken.getToken());
        resetDto.setNewPassword("1234$ABCD%abcd");
        resetDto.setConfNewPassword("1234$ABCD%abcd");

        String contentString = objectMapper.writeValueAsString(resetDto);

        mockMvc.perform(put(ACCOUNT_PATH_PASSWORD + "/reset")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(contentString))
            .andExpect(status().isOk());

        Optional<ResetToken> optionalResetTokenAfter = resetTokenRepository.findByUser(user);
        assertTrue(optionalResetTokenAfter.isEmpty());
    }

}
