package org.kinsecta.webapp.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.TestHelperService;
import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.model.entities.DatasetTag;
import org.kinsecta.webapp.api.model.repositories.DatasetTagRepository;
import org.kinsecta.webapp.api.service.DatasetTagService;
import org.kinsecta.webapp.api.v1.model.DatasetTagDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("it")
@SpringBootTest
@AutoConfigureMockMvc
class DatasetTagControllerIT {

    private static final String DATASET_TAGS_PATH = "/dataset_tags";
    private static final String DATASET_TAGS_PATH_SINGLE = DATASET_TAGS_PATH + "/1";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private DatasetTagService datasetTagService;

    @Autowired
    private DatasetTagRepository datasetTagRepository;

    @Autowired
    private TestHelperService testHelperService;


    // helper function
    private DatasetTagDto createDatasetViaMockMvc(String tagName, boolean autoTag) throws Exception {
        DatasetTagDto datasetTagDto = new DatasetTagDto();
        datasetTagDto.setName(tagName);
        datasetTagDto.setAutoTag(autoTag);

        String contentString = objectMapper.writeValueAsString(datasetTagDto);

        MvcResult mvcResult = mockMvc.perform(post(DATASET_TAGS_PATH)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .characterEncoding("UTF-8")
                .content(contentString))
            .andExpect(status().isCreated())
            .andReturn();

        return testHelperService.convertResponseBodyToDto(mvcResult, DatasetTagDto.class);
    }

    // helper function
    private DatasetTag createDatasetTag(String tagName, boolean autoTag) {
        DatasetTag datasetTag = new DatasetTag();
        datasetTag.setName(tagName);
        datasetTag.setAutoTag(autoTag);
        return datasetTagService.save(datasetTag); // this allows saving with autoTag=true, which datasetTagService.createDatasetTag() doesn't
    }


    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void createDatasetTag() throws Exception {
        String tagName = "newDatasetTag";
        DatasetTagDto responseDto = createDatasetViaMockMvc(tagName, true);

        assertTrue(responseDto.getId().isPresent());
        assertEquals(tagName, responseDto.getName());
        assertFalse(responseDto.getAutoTag());
    }


    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void createDatasetTag__withDisallowedAutoTagTrue() throws Exception {
        String tagName = "newDatasetTag";
        DatasetTagDto responseDto = createDatasetViaMockMvc(tagName, false);

        assertTrue(responseDto.getId().isPresent());
        assertEquals(tagName, responseDto.getName());
        assertFalse(responseDto.getAutoTag());  // will create as false
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllDatasetTags__default() throws Exception {
        long tagsCountBefore = datasetTagRepository.count();

        // create two DatasetTags
        createDatasetTag("mitVideo", true);
        createDatasetTag("gute Qualität", true);

        MvcResult mvcResult = mockMvc.perform(get(DATASET_TAGS_PATH))
            .andExpect(status().isOk())
            .andReturn();

        List<DatasetTagDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, DatasetTagDto.class);
        assertEquals(2, returnedDtoList.size() - tagsCountBefore);
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getDatasetTag() throws Exception {
        String datasetTagName = "4k Videos";
        DatasetTag datasetTag4kVideos = createDatasetTag(datasetTagName, false);
        assertNotNull(datasetTag4kVideos.getId());
        Long datasetTagId = datasetTag4kVideos.getId();

        MvcResult mvcResult = mockMvc.perform(get(DATASET_TAGS_PATH + "/" + datasetTagId))
            .andExpect(status().isOk())
            .andReturn();

        DatasetTagDto responseDto = testHelperService.convertResponseBodyToDto(mvcResult, DatasetTagDto.class);

        assertEquals(datasetTagName, responseDto.getName());
        assertEquals(responseDto.getId().get(), datasetTag4kVideos.getId());
    }


    @Test
    @Transactional
    @DisplayName("Assert that DatasetTags with german umlauts are accepted and treated well")
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getDatasetTag__umlautProblems() throws Exception {
        String datasetTagName = "gute Qualität";
        DatasetTag datasetTag = createDatasetTag(datasetTagName, false);
        assertNotNull(datasetTag.getId());
        Long datasetTagId = datasetTag.getId();

        MvcResult mvcResult = mockMvc.perform(get(DATASET_TAGS_PATH + "/" + datasetTagId))
            .andExpect(status().isOk())
            .andReturn();

        DatasetTagDto responseDto = testHelperService.convertResponseBodyToDto(mvcResult, DatasetTagDto.class);

        assertEquals(datasetTagName, responseDto.getName());
        assertEquals(responseDto.getId().get(), datasetTag.getId());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void deleteDatasetTag__withAutoTagFalse() throws Exception {
        // create DatasetTag
        String datasetTagName = "deleteDatasetTagTest";
        DatasetTag datasetTagToBeDeleted = createDatasetTag(datasetTagName, false);
        assertNotNull(datasetTagToBeDeleted.getId());
        Long datasetTagId = datasetTagToBeDeleted.getId();

        // delete DatasetTag
        mockMvc.perform(delete(DATASET_TAGS_PATH + "/" + datasetTagId))
            .andExpect(status().isOk());

        // assert that DatasetTag has been deleted successfully and does no longer exist in the DB
        assertThrows(NotFoundException.class, () -> datasetTagService.getDatasetTag(datasetTagId));
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void deleteDatasetTag__withAutoTagTrue() throws Exception {
        // create DatasetTag
        String datasetTagName = "deleteAutogeneratedDatasetTag";
        DatasetTag datasetTagToBeDeleted = createDatasetTag(datasetTagName, true);
        assertNotNull(datasetTagToBeDeleted.getId());
        Long datasetTagId = datasetTagToBeDeleted.getId();

        // try to delete DatasetTag
        mockMvc.perform(delete(DATASET_TAGS_PATH + "/" + datasetTagId))
            .andExpect(status().isForbidden());

        // assert that DatasetTag with the same ID still exists in the DB
        assertDoesNotThrow(() -> datasetTagService.getDatasetTag(datasetTagId));
    }

}
