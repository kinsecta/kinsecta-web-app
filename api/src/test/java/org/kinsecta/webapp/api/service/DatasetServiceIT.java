package org.kinsecta.webapp.api.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.kinsecta.webapp.api.model.entities.Dataset;
import org.kinsecta.webapp.api.model.entities.DatasetTag;
import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.v1.model.DatasetFilterDto;
import org.kinsecta.webapp.api.v1.model.DatasetTableViewDto;
import org.kinsecta.webapp.api.v1.model.TaxonomyLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@ActiveProfiles("it")
@Transactional
@Sql("/fixtures/datasets.sql")
class DatasetServiceIT {

    @Autowired
    private DatasetService datasetService;

    @Autowired
    private DatasetTagService datasetTagService;

    @Autowired
    private UserService userService;

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private final Pageable pageable = PageRequest.of(0, 20);

    private final Optional<LocalDate> OPTIONAL_LOCALDATE_OF_28_JANUARY_2022 = Optional.of(LocalDate.parse("2022-01-28", formatter));
    private final Optional<LocalDate> OPTIONAL_LOCALDATE_OF_30_JANUARY_2022 = Optional.of(LocalDate.parse("2022-01-30", formatter));
    private final LocalDateTime LOCALDATETIME_MIN_OF_28_JANUARY_2022 = OPTIONAL_LOCALDATE_OF_28_JANUARY_2022.get().atTime(LocalTime.MIN);
    private final LocalDateTime LOCALDATETIME_MAX_OF_30_JANUARY_2022 = OPTIONAL_LOCALDATE_OF_30_JANUARY_2022.get().atTime(LocalTime.MAX);


    // --- helper function ----
    public void checkMeasurementDateTimeOfAllResultsIsBetweenTimeFilter(List<DatasetTableViewDto> datasetTableViewDtos, LocalDateTime startDate, LocalDateTime endDate) {
        for (DatasetTableViewDto datasetTableViewDto : datasetTableViewDtos) {
            if (startDate != null) {
                assertTrue(datasetTableViewDto.getMeasurementDatetime().isAfter(startDate));
            }
            if (endDate != null) {
                assertTrue(datasetTableViewDto.getMeasurementDatetime().isBefore(endDate));
            }
        }
    }

    @Test
    void test_getDatasetsByFilterDto__measurement_start_end_date() {
        DatasetFilterDto filterDto = new DatasetFilterDto();
        filterDto.setMeasurementStartDate(LocalDate.of(2022, Month.JANUARY, 16));
        filterDto.setMeasurementEndDate(LocalDate.of(2022, Month.JANUARY, 28));
        List<Dataset> result = datasetService.getDatasetsByFilterDto(filterDto, pageable).stream().toList();
        assertIdsQueryHasSameDatasets(filterDto, result);
        assertEquals(3, result.size());
    }

    @Test
    void test_getDatasetsByFilterDto__modified_start_end_date() {
        DatasetFilterDto filterDto = new DatasetFilterDto();
        LocalDate today = LocalDate.now();
        filterDto.setModifiedStartDate(today);
        filterDto.setModifiedEndDate(today);
        List<Dataset> result = datasetService.getDatasetsByFilterDto(filterDto, pageable).stream().toList();
        assertIdsQueryHasSameDatasets(filterDto, result);
        assertEquals(10, result.size());

        filterDto.setModifiedEndDate(today.minusDays(1));
        filterDto.setModifiedStartDate(null);
        result = datasetService.getDatasetsByFilterDto(filterDto, pageable).stream().toList();
        assertIdsQueryHasSameDatasets(filterDto, result);
        assertEquals(0, result.size());

        filterDto.setModifiedEndDate(null);
        filterDto.setModifiedStartDate(today.plusDays(1));
        result = datasetService.getDatasetsByFilterDto(filterDto, pageable).stream().toList();
        assertIdsQueryHasSameDatasets(filterDto, result);
        assertEquals(0, result.size());
    }

    @Test
    void test_getDatasetsByFilterDto__taxonomy() {
        DatasetFilterDto filterDto = new DatasetFilterDto();
        filterDto.setTaxonomyLevel(TaxonomyLevel.FAMILY);
        filterDto.setTaxonomyFilters("3;11");
        List<Dataset> result = datasetService.getDatasetsByFilterDto(filterDto, pageable).stream().toList();
        assertIdsQueryHasSameDatasets(filterDto, result);
        assertEquals(2, result.size());
        assertEquals(1L, result.get(0).getId());
        assertEquals(2L, result.get(1).getId());

        filterDto.setTaxonomyFilters("11");
        result = datasetService.getDatasetsByFilterDto(filterDto, pageable).stream().toList();
        assertIdsQueryHasSameDatasets(filterDto, result);
        assertEquals(1, result.size());
        assertEquals(2L, result.get(0).getId());
    }

    @Test
    void test_getDatasetsByFilterDto__sensor() {
        DatasetFilterDto filterDto = new DatasetFilterDto();
        filterDto.setSensorFilters("1");
        List<Dataset> result = datasetService.getDatasetsByFilterDto(filterDto, pageable).stream().toList();
        assertIdsQueryHasSameDatasets(filterDto, result);
        assertEquals(4, result.size());
        assertEquals(1L, result.get(0).getId());
        assertEquals(2L, result.get(1).getId());
        assertEquals(3L, result.get(2).getId());
        assertEquals(5L, result.get(3).getId());
    }

    @Test
    void test_getDatasetsByFilterDto__sensor_location() {
        DatasetFilterDto filterDto = new DatasetFilterDto();
        filterDto.setSensorLocationFilters("5;6");
        List<Dataset> result = datasetService.getDatasetsByFilterDto(filterDto, pageable).stream().toList();
        assertIdsQueryHasSameDatasets(filterDto, result);
        assertThat(result).extracting(Dataset::getId)
            .containsExactlyInAnyOrder(4L, 8L, 9L, 10L, 7L);

    }

    @Test
    void test_getDatasetsByFilterDto__tags() {
        DatasetFilterDto filterDto = new DatasetFilterDto();
        List<String> tagFilters = new ArrayList<>();
        tagFilters.add("101");
        filterDto.setTagFilterList(tagFilters);
        List<Dataset> result = datasetService.getDatasetsByFilterDto(filterDto, pageable).stream().toList();
        assertIdsQueryHasSameDatasets(filterDto, result);
        assertThat(result).extracting(Dataset::getId)
            .containsExactly(3L, 4L, 6L, 7L, 8L);


        tagFilters = new ArrayList<>();
        tagFilters.add("-101");
        filterDto.setTagFilterList(tagFilters);
        result = datasetService.getDatasetsByFilterDto(filterDto, pageable).stream().toList();
        assertIdsQueryHasSameDatasets(filterDto, result);
        assertThat(result).extracting(Dataset::getId)
            .containsExactly(1L, 2L, 5L, 9L, 10L);


        tagFilters = new ArrayList<>();
        tagFilters.add("-101");
        tagFilters.add("3");
        filterDto.setTagFilterList(tagFilters);
        result = datasetService.getDatasetsByFilterDto(filterDto, pageable).stream().toList();
        assertIdsQueryHasSameDatasets(filterDto, result);
        assertThat(result).extracting(Dataset::getId)
            .containsExactlyInAnyOrder(1L, 2L, 5L);

        tagFilters = new ArrayList<>();
        tagFilters.add("1;103");
        filterDto.setTagFilterList(tagFilters);
        result = datasetService.getDatasetsByFilterDto(filterDto, pageable).stream().toList();
        assertIdsQueryHasSameDatasets(filterDto, result);
        assertThat(result).extracting(Dataset::getId)
            .containsExactly(1L, 7L, 8L, 9L);

        tagFilters = new ArrayList<>();
        tagFilters.add("-101;103");
        filterDto.setTagFilterList(tagFilters);
        result = datasetService.getDatasetsByFilterDto(filterDto, pageable).stream().toList();
        assertIdsQueryHasSameDatasets(filterDto, result);
        assertThat(result).extracting(Dataset::getId)
            .containsExactly(1L, 2L, 5L, 7L, 8L, 9L, 10L);


        tagFilters = new ArrayList<>();
        tagFilters.add("-101");
        tagFilters.add("-1");
        filterDto.setTagFilterList(tagFilters);
        result = datasetService.getDatasetsByFilterDto(filterDto, pageable).stream().toList();
        assertIdsQueryHasSameDatasets(filterDto, result);
        assertThat(result).extracting(Dataset::getId)
            .containsExactly(2L, 5L, 9L, 10L);

        tagFilters = new ArrayList<>();
        tagFilters.add("-101;-102");
        filterDto.setTagFilterList(tagFilters);
        result = datasetService.getDatasetsByFilterDto(filterDto, pageable).stream().toList();
        assertIdsQueryHasSameDatasets(filterDto, result);
        assertThat(result).extracting(Dataset::getId)
            .containsExactly(1L, 2L, 3L, 5L, 6L, 7L, 8L, 9L, 10L);
    }

    @Test
    void test_getDatasetsByFilterDto__combinations() {
        DatasetFilterDto filterDto = new DatasetFilterDto();
        filterDto.setTaxonomyLevel(TaxonomyLevel.FAMILY);
        filterDto.setTaxonomyFilters("3;11");

        List<String> tagFilters = new ArrayList<>();
        tagFilters.add("3");
        filterDto.setTagFilterList(tagFilters);
        List<Dataset> result = datasetService.getDatasetsByFilterDto(filterDto, pageable).stream().toList();
        assertIdsQueryHasSameDatasets(filterDto, result);
        assertThat(result).extracting(Dataset::getId)
            .containsExactlyInAnyOrder(1L, 2L);

        tagFilters = new ArrayList<>();
        tagFilters.add("-1");
        filterDto.setTagFilterList(tagFilters);
        result = datasetService.getDatasetsByFilterDto(filterDto, pageable).stream().toList();
        assertIdsQueryHasSameDatasets(filterDto, result);
        assertThat(result).extracting(Dataset::getId)
            .containsExactly(2L);

        filterDto.setTaxonomyLevel(null);
        filterDto.setTaxonomyFilters(null);

        filterDto.setMeasurementStartDate(LocalDate.of(2022, Month.JANUARY, 16));
        filterDto.setMeasurementEndDate(LocalDate.of(2022, Month.JANUARY, 28));
        tagFilters = new ArrayList<>();
        tagFilters.add("-102");
        tagFilters.add("101");
        filterDto.setTagFilterList(tagFilters);
        result = datasetService.getDatasetsByFilterDto(filterDto, pageable).stream().toList();
        assertIdsQueryHasSameDatasets(filterDto, result);
        assertThat(result).extracting(Dataset::getId)
            .containsExactly(3L, 7L);
    }

    @Test
    void test_getDatasetsByFilterDto_pageable() {
        Pageable pageableSize5 = PageRequest.of(0, 5);
        DatasetFilterDto filterDto = new DatasetFilterDto();
        List<Dataset> result = datasetService.getDatasetsByFilterDto(filterDto, pageableSize5).stream().toList();
        assertEquals(5, result.size());

        Sort sort = Sort.by(Sort.Direction.DESC, "measurementDatetime");
        pageableSize5 = PageRequest.of(0, 5, sort);
        result = datasetService.getDatasetsByFilterDto(filterDto, pageableSize5).stream().toList();
        assertThat(result).extracting(Dataset::getId)
            .containsExactly(10L, 2L, 9L, 1L, 8L);

        filterDto.addTagFilterListItem("-1");
        result = datasetService.getDatasetsByFilterDto(filterDto, pageableSize5).stream().toList();
        assertThat(result).extracting(Dataset::getId)
            .containsExactly(10L, 2L, 9L, 8L, 4L);
    }

    @Test
    void testSave_only_update_tags_still_updates_computed_modified() {
        LocalDateTime startOfTest = LocalDateTime.now();
        Dataset dataset = datasetService.getDataset(1L);
        DatasetTag datasetTag = datasetTagService.getDatasetTag(2L);
        dataset.addDatasetTag(datasetTag);
        dataset = datasetService.save(dataset);
        assertTrue(dataset.getComputedModified().isAfter(startOfTest));
    }

    @Test
    void isUserSameAsDatasetUser__same_user() {
        Dataset dataset = datasetService.getDataset(1L);
        User userOfDataset = dataset.getUser();

        assertTrue(datasetService.isUserSameAsDatasetUser(userOfDataset.getId(), dataset.getId()));
    }

    @Test
    void isUserSameAsDatasetUser__different_user() {
        Dataset dataset = datasetService.getDataset(1L);
        User differentUser = userService.getUser(dataset.getUser().getId() + 1L);

        assertFalse(datasetService.isUserSameAsDatasetUser(differentUser.getId(), dataset.getId()));
    }

    @ParameterizedTest
    @MethodSource("provideObjectsForGetAllDatasetIdsByFiltersPageable")
    void getAllDatasetIdsByFiltersPageable(DatasetFilterDto filterDto, Pageable pageable, Long expectedId) {
        List<Long> ids = datasetService.getAllDatasetIdsByFiltersPageable(filterDto, pageable);
        assertEquals(1, ids.size());
        assertEquals(expectedId, ids.get(0));
    }

    private static Stream<Arguments> provideObjectsForGetAllDatasetIdsByFiltersPageable() {
        return Stream.of(
            Arguments.of(new DatasetFilterDto().addTagFilterListItem("101"), PageRequest.of(0, 1), 3L),
            Arguments.of(new DatasetFilterDto().addTagFilterListItem("101"), PageRequest.of(1, 1), 4L),
            Arguments.of(new DatasetFilterDto().addTagFilterListItem("101"), PageRequest.of(2, 1), 6L),
            Arguments.of(new DatasetFilterDto().addTagFilterListItem("101"), PageRequest.of(3, 1), 7L),
            Arguments.of(new DatasetFilterDto().addTagFilterListItem("101"), PageRequest.of(4, 1), 8L),
            // with reverse sorting
            Arguments.of(new DatasetFilterDto().addTagFilterListItem("101"), PageRequest.of(0, 1, Sort.by(Sort.Direction.DESC, "id")), 8L),
            Arguments.of(new DatasetFilterDto().addTagFilterListItem("101"), PageRequest.of(1, 1, Sort.by(Sort.Direction.DESC, "id")), 7L),
            Arguments.of(new DatasetFilterDto().addTagFilterListItem("101"), PageRequest.of(2, 1, Sort.by(Sort.Direction.DESC, "id")), 6L),
            Arguments.of(new DatasetFilterDto().addTagFilterListItem("101"), PageRequest.of(3, 1, Sort.by(Sort.Direction.DESC, "id")), 4L),
            Arguments.of(new DatasetFilterDto().addTagFilterListItem("101"), PageRequest.of(4, 1, Sort.by(Sort.Direction.DESC, "id")), 3L),
            // with different tag
            Arguments.of(new DatasetFilterDto().addTagFilterListItem("102"), PageRequest.of(0, 1), 4L),
            Arguments.of(new DatasetFilterDto().addTagFilterListItem("102"), PageRequest.of(1, 1), 9L),
            Arguments.of(new DatasetFilterDto().addTagFilterListItem("102"), PageRequest.of(2, 1), 10L)
        );
    }

    // --- Helper Functions --------------------------------------------------------------------------------------------

    private void assertIdsQueryHasSameDatasets(DatasetFilterDto filterDto, List<Dataset> datasets) {
        List<Long> datasetIds = datasetService.getAllDatasetIdsByFilters(filterDto);
        assertEquals(datasetIds.size(), datasets.size());
        for (Dataset dataset : datasets) {
            assertTrue(datasetIds.contains(dataset.getId()));
        }
    }

}
