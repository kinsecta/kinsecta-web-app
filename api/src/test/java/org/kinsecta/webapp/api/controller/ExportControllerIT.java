package org.kinsecta.webapp.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.lingala.zip4j.ZipFile;
import org.apache.commons.io.FilenameUtils;
import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.TestHelperService;
import org.kinsecta.webapp.api.mail.ExportMailService;
import org.kinsecta.webapp.api.model.entities.*;
import org.kinsecta.webapp.api.s3.S3ClientStorageService;
import org.kinsecta.webapp.api.s3.StorageException;
import org.kinsecta.webapp.api.service.*;
import org.kinsecta.webapp.api.util.TempDirectoryHelper;
import org.kinsecta.webapp.api.v1.model.ExportDto;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("it")
@SpringBootTest
@AutoConfigureMockMvc
class ExportControllerIT {

    private static final String EXPORTS_PATH = "/exports";
    private static final String EXPORTS_PATH_SINGLE = EXPORTS_PATH + "/1";
    private static final String EXPORTS_PATH_SINGLE_EXPORT_FILES = EXPORTS_PATH_SINGLE + "/export_files";
    private static final String EXPORTS_PATH_SINGLE_EXPORT_FILES_SINGLE = EXPORTS_PATH_SINGLE_EXPORT_FILES + "/1";
    private static final String EXPORTS_PATH_SINGLE_EXPORT_FILES_SINGLE_DOWNLOAD_PATTERN = "/exports/%s/export_files/%s/download?token=%s";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private DatasetService datasetService;

    @Autowired
    private DatasetImageService datasetImageService;

    @Autowired
    private TestHelperService testHelperService;

    @Autowired
    private TempDirectoryHelper tempDirectoryHelper;

    @Autowired
    private ExportService exportService;

    @Autowired
    private S3ClientStorageService s3ClientStorageService;

    @Autowired
    private S3ObjectService s3ObjectService;

    @Autowired
    private UserService userService;

    @MockBean
    private ExportMailService exportMailServiceMock;

    @Captor
    private ArgumentCaptor<Export> exportCaptor;

    @Captor
    private ArgumentCaptor<List<String>> errorMessageCaptor;

    @Value("${app.export.download.expiration}")
    private Integer expireHours;

    @Value("${app.export.max-datasets}")
    private Integer maxDatasetsPerExport;


    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void createExport() throws Exception {
        try {
            Dataset dataset1 = datasetService.getDataset(1L);
            Dataset dataset2 = datasetService.getDataset(2L);

            List<DatasetImage> datasetImages1 = datasetImageService.getDatasetImagesForDataset(dataset1);
            // upload necessary s3 objects
            testHelperService.uploadTestFilesAsS3Object(
                dataset1.getVideoS3Object(),
                datasetImages1.get(0).getS3Object(),
                datasetImages1.get(1).getS3Object(),
                dataset2.getMeasurementWingbeat().getImageS3Object(),
                dataset2.getMeasurementWingbeat().getWavS3Object());

            MvcResult mvcResult = mockMvc.perform(post(EXPORTS_PATH))
                .andExpect(status().isCreated())
                .andReturn();

            ExportDto returnedExportDto = testHelperService.convertResponseBodyToDto(mvcResult, ExportDto.class);
            Export export = exportService.getExport(returnedExportDto.getId().get());
            List<ExportFile> exportFiles = new ArrayList<>(export.getExportFiles());
            assertEquals(1, exportFiles.size());

            S3Object zipObject = exportFiles.get(0).getS3Object();
            assertTrue(testHelperService.existsS3ObjectInBucketStartingWith(zipObject.getBucket(), zipObject.getKey()));

            // assert zip file contains all the files
            File testTempDir = tempDirectoryHelper.createRandomTempSubDirectory();
            File zipFile = new File(testTempDir.getAbsolutePath(), zipObject.getFilename());
            try (FileOutputStream fos = new FileOutputStream(zipFile)) {
                s3ClientStorageService.load(zipObject.getBucket(), zipObject.getKey(), fos);
            }
            File extractedDir = new File(testTempDir, FilenameUtils.getBaseName(zipFile.getName()));
            new ZipFile(zipFile).extractAll(extractedDir.getAbsolutePath());
            List<String> fileListZipDirectory = Arrays.stream(extractedDir.list()).toList();
            assertEquals(2, fileListZipDirectory.size());

            File directory1 = new File(extractedDir.getAbsolutePath(), "1_2022-01-29_13-59-12");
            assertTrue(directory1.exists());
            assertTrue(directory1.isDirectory());
            List<String> fileList1 = Arrays.stream(directory1.list()).toList();
            assertTrue(fileList1.contains("data.json"));
            assertTrue(fileList1.contains(dataset1.getVideoS3Object().getFilename()));
            assertTrue(fileList1.contains(datasetImages1.get(0).getS3Object().getFilename()));
            assertTrue(fileList1.contains(datasetImages1.get(1).getS3Object().getFilename()));

            File directory2 = new File(extractedDir.getAbsolutePath(), "1_2022-01-29_14-31-20");
            assertTrue(directory2.exists());
            assertTrue(directory2.isDirectory());
            List<String> fileList2 = Arrays.stream(directory2.list()).toList();
            assertTrue(fileList2.contains("data.json"));
            assertTrue(fileList2.contains(dataset2.getMeasurementWingbeat().getImageS3Object().getFilename()));
            assertTrue(fileList2.contains(dataset2.getMeasurementWingbeat().getWavS3Object().getFilename()));

            // assert expires is ~24h in the future
            LocalDateTime now = LocalDateTime.now();
            Duration differenceToExpires = Duration.between(now, export.getExpires());
            assertFalse(differenceToExpires.isNegative());
            assertTrue(differenceToExpires.toMinutes() >= (expireHours * 60) - 1);
            assertTrue(differenceToExpires.toMinutes() <= (expireHours * 60) + 1);
            assertNotNull(export.getDownloadToken());
            assertEquals(TransferStatus.SUCCESSFUL, export.getStatus());

            // assert export file was saved in db
            assertEquals(1, export.getExportFiles().size());

            // assert mail is being sent
            verify(exportMailServiceMock).sendExportFinishedMessage(exportCaptor.capture(), eq(false), eq(maxDatasetsPerExport));
            Export capturedExport = exportCaptor.getValue();
            assertEquals(capturedExport.getId(), export.getId());

        } finally {
            testHelperService.cleanS3Buckets();
            testHelperService.cleanWorkingDirectory();
        }
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void createExport_filter_dataset() throws Exception {
        try {
            Dataset dataset1 = datasetService.getDataset(1L);
            Dataset dataset2 = datasetService.getDataset(2L);

            List<DatasetImage> datasetImages1 = datasetImageService.getDatasetImagesForDataset(dataset1);
            // upload necessary s3 objects
            testHelperService.uploadTestFilesAsS3Object(
                dataset1.getVideoS3Object(),
                datasetImages1.get(0).getS3Object(),
                datasetImages1.get(1).getS3Object(),
                dataset2.getMeasurementWingbeat().getImageS3Object(),
                dataset2.getMeasurementWingbeat().getWavS3Object());

            String queryParams = "?taxonomyLevel=GENUS"
                + "&taxonomyFilters=4";

            MvcResult mvcResult = mockMvc.perform(post(EXPORTS_PATH + queryParams))
                .andExpect(status().isCreated())
                .andReturn();

            ExportDto returnedExportDto = testHelperService.convertResponseBodyToDto(mvcResult, ExportDto.class);
            Export export = exportService.getExport(returnedExportDto.getId().get());
            List<ExportFile> exportFiles = new ArrayList<>(export.getExportFiles());
            assertEquals(1, exportFiles.size());

            S3Object zipObject = exportFiles.get(0).getS3Object();
            assertTrue(testHelperService.existsS3ObjectInBucketStartingWith(zipObject.getBucket(), zipObject.getKey()));

            // assert zip file contains all the files
            File testTempDir = tempDirectoryHelper.createRandomTempSubDirectory();
            File zipFile = new File(testTempDir.getAbsolutePath(), zipObject.getFilename());
            try (FileOutputStream fos = new FileOutputStream(zipFile)) {
                s3ClientStorageService.load(zipObject.getBucket(), zipObject.getKey(), fos);
            }
            File extractedDir = new File(testTempDir, FilenameUtils.getBaseName(zipFile.getName()));
            new ZipFile(zipFile).extractAll(extractedDir.getAbsolutePath());
            List<String> fileListZipDirectory = Arrays.stream(extractedDir.list()).toList();
            assertEquals(1, fileListZipDirectory.size());

            File directory = new File(extractedDir.getAbsolutePath(), "1_2022-01-29_13-59-12");
            assertTrue(directory.exists());
            assertTrue(directory.isDirectory());
            List<String> fileList1 = Arrays.stream(directory.list()).toList();
            assertTrue(fileList1.contains("data.json"));
            assertTrue(fileList1.contains(dataset1.getVideoS3Object().getFilename()));
            assertTrue(fileList1.contains(datasetImages1.get(0).getS3Object().getFilename()));
            assertTrue(fileList1.contains(datasetImages1.get(1).getS3Object().getFilename()));

            // assert expires is ~24h in the future
            LocalDateTime now = LocalDateTime.now();
            Duration differenceToExpires = Duration.between(now, export.getExpires());
            assertFalse(differenceToExpires.isNegative());
            assertTrue(differenceToExpires.toMinutes() >= (expireHours * 60) - 1);
            assertTrue(differenceToExpires.toMinutes() <= (expireHours * 60) + 1);
            assertNotNull(export.getDownloadToken());
            assertEquals(TransferStatus.SUCCESSFUL, export.getStatus());

            // assert export file was saved in db
            assertEquals(1, export.getExportFiles().size());

            // assert mail is being sent
            verify(exportMailServiceMock).sendExportFinishedMessage(exportCaptor.capture(), eq(false), eq(maxDatasetsPerExport));
            Export capturedExport = exportCaptor.getValue();
            assertEquals(capturedExport.getId(), export.getId());

        } finally {
            testHelperService.cleanS3Buckets();
            testHelperService.cleanWorkingDirectory();
        }
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void createExport__export_failed_email() throws Exception {
        ExportDto exportDto = new ExportDto();
        exportDto.setDatasetIds(JsonNullable.of("1;2"));

        // perform POST and expect to return CREATED
        mockMvc.perform(post(EXPORTS_PATH)
                .content(objectMapper.writeValueAsString(exportDto))
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());

        // verify that the async export task fails with an S3 exception because the ExportFile cannot be loaded
        verify(exportMailServiceMock).sendExportFailedMessage(exportCaptor.capture(), errorMessageCaptor.capture());
        assertEquals(TransferStatus.ERRONEOUS, exportCaptor.getValue().getStatus());
        assertEquals(1, errorMessageCaptor.getValue().size());
        assertThat(errorMessageCaptor.getValue())
            .allMatch(s -> s.startsWith("Failed to load file '"));
    }


    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getExport() throws Exception {
        mockMvc.perform(get(EXPORTS_PATH_SINGLE)).andExpect(status().isNotImplemented());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllExportFilesForExport() throws Exception {
        mockMvc.perform(get(EXPORTS_PATH_SINGLE_EXPORT_FILES)).andExpect(status().isNotImplemented());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getExportFileForExport() throws Exception {
        mockMvc.perform(get(EXPORTS_PATH_SINGLE_EXPORT_FILES_SINGLE)).andExpect(status().isNotImplemented());
    }

    @Test
    void downloadExportFileForExport() throws Exception {
        File zipFile = new File("src/test/resources/zipped/data_wingbeat_single.zip");
        User user = userService.getUser(1L);
        String downloadToken = "AbCxYz";
        LocalDateTime expires = LocalDateTime.now().plusHours(24);
        Export export = createExportWithExportFile(user, zipFile, downloadToken, expires);
        Long exportFileId = export.getExportFiles().stream().findFirst().get().getId();

        String downloadEndpoint = getExportsPathSingleExportFilesSingleDownload(export.getId(), exportFileId, downloadToken);
        testHelperService.fileDownloadTest(mockMvc, downloadEndpoint, zipFile, "application/zip", "attachment; filename=\"data_wingbeat_single.zip\"", true);
    }

    @Test
    void downloadExportFileForExport__wrong_token() throws Exception {
        try {
            File zipFile = new File("src/test/resources/zipped/data_wingbeat_single.zip");
            User user = userService.getUser(1L);
            LocalDateTime expires = LocalDateTime.now().plusHours(24);
            Export export = createExportWithExportFile(user, zipFile, "AbCxYz", expires);
            Long exportFileId = export.getExportFiles().stream().findFirst().get().getId();

            String downloadEndpoint = getExportsPathSingleExportFilesSingleDownload(export.getId(), exportFileId, "xxx-wrong-token-xxx");
            mockMvc.perform(get(downloadEndpoint))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.message").value("The download token is invalid!"));

        } finally {
            testHelperService.cleanWorkingDirectory();
            testHelperService.cleanS3Buckets();
        }
    }

    @Test
    void downloadExportFileForExport__expired() throws Exception {
        try {
            File zipFile = new File("src/test/resources/zipped/data_wingbeat_single.zip");
            User user = userService.getUser(1L);
            String downloadToken = "AbCxYz";
            LocalDateTime expires = LocalDateTime.now().minusHours(1);
            Export export = createExportWithExportFile(user, zipFile, downloadToken, expires);
            Long exportFileId = export.getExportFiles().stream().findFirst().get().getId();

            String downloadEndpoint = getExportsPathSingleExportFilesSingleDownload(export.getId(), exportFileId, downloadToken);
            mockMvc.perform(get(downloadEndpoint))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.message").value("The download token has expired."));

        } finally {
            testHelperService.cleanWorkingDirectory();
            testHelperService.cleanS3Buckets();
        }
    }


    private String getExportsPathSingleExportFilesSingleDownload(Long exportId, Long exportFileId, String token) {
        return String.format(EXPORTS_PATH_SINGLE_EXPORT_FILES_SINGLE_DOWNLOAD_PATTERN, exportId, exportFileId, token);
    }

    private Export createExportWithExportFile(User user, File zipFile, String downloadToken, LocalDateTime expires) throws IOException, StorageException {
        Export export = new Export();
        export.setUser(user);
        export.setDatasetIds("1");
        export.setStatus(TransferStatus.SUCCESSFUL);
        export.setFinished(LocalDateTime.now());
        export.setExpires(expires);
        export.setDownloadToken(downloadToken);
        export = exportService.saveExport(export);
        ExportFile exportFile = new ExportFile();
        S3Object s3Object = s3ObjectService.storeFile(zipFile);
        exportFile.setS3Object(s3Object);
        export.addExportFile(exportFile);
        return exportService.saveExport(export);
    }

}
