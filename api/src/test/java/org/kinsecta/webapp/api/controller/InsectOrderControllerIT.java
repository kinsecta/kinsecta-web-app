package org.kinsecta.webapp.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.TestHelperService;
import org.kinsecta.webapp.api.model.entities.InsectOrder;
import org.kinsecta.webapp.api.service.InsectOrderService;
import org.kinsecta.webapp.api.v1.model.InsectOrderDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("it")
@SpringBootTest
@AutoConfigureMockMvc
class InsectOrderControllerIT {

    private static final String INSECT_ORDERS_PATH = "/insect_orders";
    private static final String INSECT_ORDERS_PATH_SEARCH = INSECT_ORDERS_PATH + "/search";
    private static final String INSECT_ORDERS_PATH_SINGLE = INSECT_ORDERS_PATH + "/1";

    private final String testName = "Test InsectOrder";
    private final String testNameUrlEncoded = URLEncoder.encode(testName, StandardCharsets.UTF_8.toString());

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TestHelperService testHelperService;

    @Autowired
    private InsectOrderService insectOrderService;


    InsectOrderControllerIT() throws UnsupportedEncodingException {}


    // Helper Function
    private InsectOrder createNewInsectOrder() throws Exception {
        InsectOrder insectOrder = new InsectOrder();
        insectOrder.setName(testName);
        insectOrder.setGbifId(99991L);

        return insectOrderService.save(insectOrder);
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllInsectOrders() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(INSECT_ORDERS_PATH))
            .andExpect(status().isOk())
            .andReturn();

        List<InsectOrderDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, InsectOrderDto.class);
        assertEquals(5, returnedDtoList.size());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void searchInsectOrders__query_exact_match() throws Exception {
        InsectOrder created = createNewInsectOrder();

        MvcResult mvcResult = mockMvc.perform(get(INSECT_ORDERS_PATH_SEARCH + "?query=" + testNameUrlEncoded))
            .andExpect(status().isOk())
            .andReturn();

        List<InsectOrderDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, InsectOrderDto.class);
        assertEquals(1, returnedDtoList.size());
        InsectOrderDto resultDto = returnedDtoList.get(0);
        assertEquals(created.getName(), resultDto.getName());
        assertTrue(resultDto.getId().isPresent());
        assertEquals(created.getId(), resultDto.getId().get());
        assertTrue(resultDto.getGbifId().isPresent());
        assertEquals(created.getGbifId(), resultDto.getGbifId().get());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void searchInsectOrders__query_partial_match_ignorecase() throws Exception {
        InsectOrder created = createNewInsectOrder();

        MvcResult mvcResult = mockMvc.perform(get(INSECT_ORDERS_PATH_SEARCH + "?query=test"))
            .andExpect(status().isOk())
            .andReturn();

        List<InsectOrderDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, InsectOrderDto.class);
        assertEquals(1, returnedDtoList.size());
        InsectOrderDto resultDto = returnedDtoList.get(0);
        assertEquals(created.getName(), resultDto.getName());
        assertTrue(resultDto.getId().isPresent());
        assertEquals(created.getId(), resultDto.getId().get());
        assertTrue(resultDto.getGbifId().isPresent());
        assertEquals(created.getGbifId(), resultDto.getGbifId().get());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getInsectOrder() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(INSECT_ORDERS_PATH_SINGLE))
            .andExpect(status().isOk())
            .andReturn();

        InsectOrderDto resultDto = testHelperService.convertResponseBodyToDto(mvcResult, InsectOrderDto.class);
        assertEquals(1L, resultDto.getId().get());
    }

}
