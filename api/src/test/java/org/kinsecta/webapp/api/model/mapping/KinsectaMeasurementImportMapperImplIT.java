package org.kinsecta.webapp.api.model.mapping;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.exception.checked.MappingException;
import org.kinsecta.webapp.api.model.entities.Classification;
import org.kinsecta.webapp.api.model.entities.ClassificationType;
import org.kinsecta.webapp.api.model.entities.Dataset;
import org.kinsecta.webapp.api.v1.model.KinsectaMeasurement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
@ActiveProfiles("it")
@Transactional
class KinsectaMeasurementImportMapperImplIT {

    @Autowired
    KinsectaMeasurementImportMapper kinsectaMeasurementImportMapper;

    @Autowired
    ObjectMapper objectMapper;


    @Test
    void testKinsectaMeasurementToDataset() throws IOException, MappingException {
        // Read a valid JSON file and map to KinsectaMeasurement class
        File jsonFile = Paths.get("src/test/resources/exchange-dto/31-exchange-spec-valid.json").toFile();
        KinsectaMeasurement kinsectaMeasurement = objectMapper.readValue(jsonFile, KinsectaMeasurement.class);

        // Create Dataset entity by mapping from KinsectaMeasurement
        Dataset dataset = kinsectaMeasurementImportMapper.kinsectaMeasurementToDataset(kinsectaMeasurement);

        // Perform assertions on the created Dataset entity
        assertEquals(LocalDateTime.of(2022, 5, 18, 14, 15, 31), dataset.getMeasurementDatetime());
        assertEquals(0, new BigDecimal("34.4").compareTo(dataset.getMeasurementHumidity()));
        assertEquals(345, dataset.getMeasurementSpectrumClearLight());
        assertEquals("NE", dataset.getMeasurementWindSensorDirection());
        assertEquals(96000, dataset.getMeasurementWingbeat().getSampleRate());

        List<Classification> classifications = new ArrayList<>(dataset.getClassifications());
        assertEquals(4, classifications.size());

        for (Classification classification : classifications) {
            if (classification.getType().equals(ClassificationType.GROUNDTRUTH)) {
                // only matches 1 Classification
                assertEquals(dataset, classification.getDataset());
                assertEquals(BigDecimal.valueOf(1), classification.getProbability());
                assertEquals(4L, classification.getInsectOrder().getId());
                assertEquals(16L, classification.getInsectFamily().getId());
                assertEquals(22L, classification.getInsectGenus().getId());
                assertEquals(25L, classification.getInsectSpecies().getId());

            } else if (classification.getType().equals(ClassificationType.HUMAN)) {
                // only matches 1 Classification
                assertEquals(dataset, classification.getDataset());
                assertEquals(22L, classification.getInsectGenus().getId());

            } else if (classification.getType().equals(ClassificationType.MACHINE)) {
                // matches 2 Classifications, so only check what they have in common
                assertEquals(BigDecimal.valueOf(0.346), classification.getProbability());

            } else {
                Assertions.fail();
            }
        }

    }

}
