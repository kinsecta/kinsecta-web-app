package org.kinsecta.webapp.api.security;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.kinsecta.webapp.api.model.entities.LoginAttempt;
import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.model.entities.UserRole;
import org.kinsecta.webapp.api.model.entities.UserStatus;
import org.kinsecta.webapp.api.service.LoginAttemptService;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class UserDetailsImplTest {

    @Mock
    LoginAttemptService loginAttemptService;

    @Mock
    LoginAttempt loginAttempt;


    @Test
    void testUserDetailsImpl() {
        String username = "my_username";
        String password = "my_$3cr3t_password_Ä$å";
        UserRole role = UserRole.TEAM_MEMBER;
        UserStatus status = UserStatus.ACTIVE;
        Collection<? extends GrantedAuthority> grantedAuthorities = List.of(new SimpleGrantedAuthority(role.getValue()));
        boolean accountIsLocked = false;
        boolean accountIsEnabled = true;

        User user = spy(new User());
        user.setUsername(username);
        user.setPassword(password);
        user.setRole(role);
        user.setStatus(status);

        doReturn(loginAttempt).when(user).getLoginAttempt();
        when(loginAttemptService.isLocked(loginAttempt)).thenReturn(accountIsLocked);

        UserDetailsImpl userDetails = new UserDetailsImpl(loginAttemptService);
        userDetails.setUser(user);

        assertEquals(user, userDetails.getUser());
        assertEquals(username, userDetails.getUsername());
        assertEquals(password, userDetails.getPassword());
        assertEquals(grantedAuthorities, userDetails.getAuthorities());
        assertTrue(userDetails.isAccountNonExpired());
        assertEquals(!accountIsLocked, userDetails.isAccountNonLocked());
        assertTrue(userDetails.isCredentialsNonExpired());
        assertEquals(accountIsEnabled, userDetails.isEnabled());

        // test user not returning a LoginAttempt object but null, which means that no LoginAttempt for User was  found in the DB meaning the User is not locked
        doReturn(null).when(user).getLoginAttempt();
        assertTrue(userDetails.isAccountNonLocked());
    }

}
