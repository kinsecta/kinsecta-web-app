package org.kinsecta.webapp.api;

import org.apache.commons.lang3.LocaleUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Locale;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ActiveProfiles("it")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ApiApplicationIT {

    @Value("${app.timezone.default}")
    private String defaultTimezone;

    @Value("${app.locales.default}")
    private String defaultLocale;


    @Test
    void contextLoads() {
    }

    @Test
    void init_checkDefaultLocaleAndTimezone() {
        Locale localeFromProperties = LocaleUtils.toLocale(defaultLocale);
        assertEquals(localeFromProperties, Locale.getDefault());

        TimeZone timezoneFromProperties = TimeZone.getTimeZone(defaultTimezone);
        assertEquals(timezoneFromProperties, TimeZone.getDefault());
    }

}
