package org.kinsecta.webapp.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.TestHelperService;
import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.service.SensorLocationService;
import org.kinsecta.webapp.api.v1.model.ApiViewDto;
import org.kinsecta.webapp.api.v1.model.SensorLocationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("it")
@SpringBootTest
@AutoConfigureMockMvc
class SensorLocationControllerIT {

    private static final String SENSOR_LOCATIONS_PATH = "/sensor_locations";
    private static final String SENSOR_LOCATIONS_PATH_SINGLE = SENSOR_LOCATIONS_PATH + "/1";
    private static final String SENSOR_LOCATIONS_PATH_VIEWS = SENSOR_LOCATIONS_PATH + "/_views";
    private static final String SENSOR_LOCATIONS_PATH_VIEWS_SIMPLE = SENSOR_LOCATIONS_PATH_VIEWS + "/simple";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private SensorLocationService sensorLocationService;

    @Autowired
    private TestHelperService testHelperService;


    // Helper Function
    private SensorLocationDto createNewSensorLocation() throws Exception {
        SensorLocationDto dto = new SensorLocationDto();
        dto.setName("Eberhardt Karls Universität");
        dto.setPostcode(72072);
        dto.setCity("Tübingen");
        dto.setLatitude(BigDecimal.valueOf(48.521637));
        dto.setLongitude(BigDecimal.valueOf(9.057645));
        dto.setGpsMasked(false);

        String contentString = objectMapper.writeValueAsString(dto);
        MvcResult mvcResult = mockMvc.perform(
                post(SENSOR_LOCATIONS_PATH)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .content(contentString))
            .andExpect(status().isCreated())
            .andReturn();

        return testHelperService.convertResponseBodyToDto(mvcResult, SensorLocationDto.class);
    }


    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void createSensorLocation() throws Exception {
        SensorLocationDto created = createNewSensorLocation();
        assertEquals(6L, created.getId().get());
        assertEquals("Eberhardt Karls Universität", created.getName());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllSensorLocations() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(SENSOR_LOCATIONS_PATH))
            .andExpect(status().isOk())
            .andReturn();

        List<SensorLocationDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, SensorLocationDto.class);

        assertEquals(5, returnedDtoList.size());
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void getAllSensorLocationViews() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(SENSOR_LOCATIONS_PATH_VIEWS))
            .andExpect(status().isOk())
            .andReturn();

        List<ApiViewDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, ApiViewDto.class);

        assertEquals(1, returnedDtoList.size());
        assertEquals("simple", returnedDtoList.get(0).getView());
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void getAllSensorLocationsSimple() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(SENSOR_LOCATIONS_PATH_VIEWS_SIMPLE))
            .andExpect(status().isOk())
            .andReturn();

        List<SensorLocationDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, SensorLocationDto.class);

        assertEquals(5, returnedDtoList.size());
        for (SensorLocationDto sensorLocationDto : returnedDtoList) {
            assertTrue(sensorLocationDto.getId().isPresent());
            assertNotNull(sensorLocationDto.getName());
            assertNotNull(sensorLocationDto.getPostcode());
            assertNotNull(sensorLocationDto.getCity());
        }
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getSensorLocation() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(SENSOR_LOCATIONS_PATH_SINGLE))
            .andExpect(status().isOk())
            .andReturn();

        SensorLocationDto sensorLocationDto = testHelperService.convertResponseBodyToDto(mvcResult, SensorLocationDto.class);
        assertEquals(1L, sensorLocationDto.getId().get());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void editSensorLocation() throws Exception {
        SensorLocationDto created = createNewSensorLocation();
        created.setName("New Name");
        created.setCity("New City");

        String contentString = objectMapper.writeValueAsString(created);
        MvcResult mvcResult = mockMvc.perform(
                put(SENSOR_LOCATIONS_PATH + "/" + created.getId().get().toString())
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .content(contentString))
            .andExpect(status().isOk())
            .andReturn();

        SensorLocationDto edited = testHelperService.convertResponseBodyToDto(mvcResult, SensorLocationDto.class);

        assertEquals(created.getId().get(), edited.getId().get());
        assertEquals("New Name", edited.getName());
        assertEquals("New City", edited.getCity());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void deleteSensorLocation() throws Exception {
        SensorLocationDto created = createNewSensorLocation();
        Long sensorLocationId = created.getId().get();
        mockMvc.perform(delete(SENSOR_LOCATIONS_PATH + "/" + sensorLocationId))
            .andExpect(status().isOk());

        assertThrows(NotFoundException.class, () -> sensorLocationService.getSensorLocation(sensorLocationId));
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void deleteSensorLocation_linkedToSensor() throws Exception {
        mockMvc.perform(delete(SENSOR_LOCATIONS_PATH_SINGLE))
            .andExpect(status().isForbidden())
            .andExpect(jsonPath("$.message").value("Cannot delete SensorLocation with id 1 because some Sensors are linked to it."));

        assertEquals(1L, sensorLocationService.getSensorLocation(1L).getId());
    }

}
