package org.kinsecta.webapp.api.controller.roletesting;

import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.TestHelperService;
import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.model.entities.Upload;
import org.kinsecta.webapp.api.service.UploadService;
import org.kinsecta.webapp.api.v1.model.TransferStatusDto;
import org.kinsecta.webapp.api.v1.model.UploadDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("it")
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class UploadControllerRoleIT {

    private static final String UPLOADS_PATH = "/uploads";

    private static final String UPLOADS_PATH_SINGLE = UPLOADS_PATH + "/1";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UploadService uploadService;

    @Autowired
    private TestHelperService testHelperService;


    // ------- createUpload --------------------------------------------------------------------------------------------
    void createUpload(String username) throws Exception {
        MvcResult mvcResult = mockMvc.perform(post(UPLOADS_PATH))
            .andExpect(status().isCreated())
            .andReturn();

        UploadDto uploadDto = testHelperService.convertResponseBodyToDto(mvcResult, UploadDto.class);
        assertEquals(TransferStatusDto.IN_PROGRESS, uploadDto.getStatus());
        Upload upload = uploadService.getUpload(uploadDto.getId());
        assertEquals(username, upload.getUser().getUsername());
    }


    @Test
    @Transactional
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void createUpload__Admin() throws Exception {
        createUpload("t.fischer");
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void createUpload__TeamMember() throws Exception {
        createUpload("f.koeninger");
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void createUpload__DataCollector() throws Exception {
        createUpload("a.goldflam");
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void createUpload__DataRecipient() throws Exception {
        createUpload("a.wonnleben");
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void createUpload__DataScientist() throws Exception {
        mockMvc.perform(post(UPLOADS_PATH))
            .andExpect(status().isForbidden());
    }


    // ----- getAllUploads ---------------------------------------------------------------------------------------------

    void getAllUploads(int expectedNumberOfDatasets) throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(UPLOADS_PATH))
            .andExpect(status().isOk())
            .andReturn();

        List<UploadDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, UploadDto.class);
        assertEquals(expectedNumberOfDatasets, returnedDtoList.size());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getAllUploads__Admin() throws Exception {
        getAllUploads(7);
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllUploads__TeamMember() throws Exception {
        getAllUploads(7);
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void getAllUploads__DataCollector() throws Exception {
        getAllUploads(1);
    }

    @Test
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void getAllUploads__DataRecipient() throws Exception {
        getAllUploads(7);
    }

    @Test
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void getAllUploads__DataScientist() throws Exception {
        mockMvc.perform(get(UPLOADS_PATH))
            .andExpect(status().isForbidden());
    }


    // ---- getUpload --------------------------------------------------------------------------------------------------

    void getUpload_AndExpectForbidden() throws Exception {
        mockMvc.perform(get(UPLOADS_PATH_SINGLE))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getUpload__Admin() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(UPLOADS_PATH_SINGLE))
            .andExpect(status().isOk())
            .andReturn();

        UploadDto uploadDto = testHelperService.convertResponseBodyToDto(mvcResult, UploadDto.class);
        assertEquals(1L, uploadDto.getId());
        assertEquals(TransferStatusDto.IN_PROGRESS, uploadDto.getStatus());
        assertEquals(2L, uploadDto.getUser().getId().get());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getUpload__TeamMember() throws Exception {
        getUpload_AndExpectForbidden();
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void getUpload__DataCollector() throws Exception {
        getUpload_AndExpectForbidden();
    }

    @Test
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void getUpload__DataRecipient() throws Exception {
        getUpload_AndExpectForbidden();
    }

    @Test
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void getUpload__DataScientist() throws Exception {
        getUpload_AndExpectForbidden();
    }


    // ---- deleteUpload -----------------------------------------------------------------------------------------------

    void deleteUpload_AndExpectForbidden() throws Exception {
        mockMvc.perform(delete(UPLOADS_PATH_SINGLE))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void deleteUpload__Admin() throws Exception {
        mockMvc.perform(delete(UPLOADS_PATH_SINGLE))
            .andExpect(status().isOk());

        assertThrows(NotFoundException.class, () -> uploadService.getUpload(1L));
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void deleteUpload__TeamMember() throws Exception {
        deleteUpload_AndExpectForbidden();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void deleteUpload__DataCollector() throws Exception {
        deleteUpload_AndExpectForbidden();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void deleteUpload__DataRecipient() throws Exception {
        deleteUpload_AndExpectForbidden();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void deleteUpload__DataScientist() throws Exception {
        deleteUpload_AndExpectForbidden();
    }

}
