package org.kinsecta.webapp.api.model.mapping;

import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.model.entities.TransferStatus;
import org.kinsecta.webapp.api.model.entities.Upload;
import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.model.entities.UserRole;
import org.kinsecta.webapp.api.model.repositories.UserRepository;
import org.kinsecta.webapp.api.security.ContextUserService;
import org.kinsecta.webapp.api.service.UserService;
import org.kinsecta.webapp.api.v1.model.TransferStatusDto;
import org.kinsecta.webapp.api.v1.model.UploadDto;
import org.kinsecta.webapp.api.v1.model.UserSimpleViewDto;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;


@SpringBootTest
@ActiveProfiles("it")
@Transactional
class UploadDtoMapperImplIT {

    @Autowired
    UploadDtoMapper uploadDtoMapper;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @MockBean
    private ContextUserService contextUserServiceMock;


    @Test
    void testUploadDtoToUpload() {
        UserSimpleViewDto userSimpleViewDto = new UserSimpleViewDto();
        userSimpleViewDto.setId(JsonNullable.of(1L));
        userSimpleViewDto.setUsername("t.fischer");
        userSimpleViewDto.setFullName("Tobias Fischer (from simple)");

        UploadDto uploadDto = new UploadDto();
        uploadDto.setId(3L);
        uploadDto.setFinished(JsonNullable.of(null));
        uploadDto.setStatus(TransferStatusDto.IN_PROGRESS);
        uploadDto.setUser(userSimpleViewDto);

        // Test UploadDtoMapper.uploadDtoToUpload()
        Upload upload = uploadDtoMapper.uploadDtoToUpload(uploadDto);

        assertEquals(Long.valueOf(3), upload.getId());
        assertNull(upload.getFinished());
        assertEquals(TransferStatus.IN_PROGRESS, upload.getStatus());
        assertEquals("Tobias Fischer", upload.getUser().getFullName());
        assertEquals(1L, upload.getUser().getId());

        uploadDto.setFinished(JsonNullable.of(LocalDateTime.of(2022, 1, 1, 10, 0)));
        uploadDto.setStatus(TransferStatusDto.ERRONEOUS);

        upload = uploadDtoMapper.uploadDtoToUpload(uploadDto);
        assertEquals(LocalDateTime.of(2022, 1, 1, 10, 0), upload.getFinished());
        assertEquals(TransferStatus.ERRONEOUS, upload.getStatus());
    }

    @Test
    void testUploadToUploadDto() {
        User user = new User();
        user.setId(4L);
        user.setRole(UserRole.ADMIN);

        Upload upload = new Upload();
        upload.setId(2L);
        upload.setFinished(null);
        upload.setUser(user);
        upload.setStatus(TransferStatus.IN_PROGRESS);

        // Test UploadDtoMapper.uploadToUploadDto()
        mockContextUserServiceWithUser(user);
        UploadDto uploadDto = uploadDtoMapper.uploadToUploadDto(upload);

        assertEquals(2L, uploadDto.getId());
        assertEquals(JsonNullable.of(null), uploadDto.getFinished());
        assertEquals(4L, uploadDto.getUser().getId().get());
        assertEquals(TransferStatusDto.IN_PROGRESS, uploadDto.getStatus());
    }

    @Test
    void testUpdateUploadWithUploadDto() {
        LocalDateTime localDateTime = LocalDateTime.of(2022, 1, 1, 10, 0);

        UserSimpleViewDto userSimpleViewDto = new UserSimpleViewDto();
        userSimpleViewDto.setId(JsonNullable.of(5L));

        UploadDto uploadDto = new UploadDto();
        uploadDto.setId(3L);
        uploadDto.setFinished(JsonNullable.of(localDateTime));
        uploadDto.setStatus(TransferStatusDto.ERRONEOUS);
        uploadDto.setUser(userSimpleViewDto);

        Upload upload = new Upload();
        upload.setId(2L);
        upload.setFinished(null);
        User user = new User();
        user.setId(8L);
        upload.setUser(user);
        upload.setStatus(TransferStatus.IN_PROGRESS);

        // Test UploadDtoMapper.updateUploadWithUploadDto()
        // everything except id and user id should be updated (id has exception in annotation)
        uploadDtoMapper.updateUploadWithUploadDto(uploadDto, upload);

        assertEquals(Long.valueOf(2), upload.getId());
        assertEquals(8L, upload.getUser().getId());
        assertEquals(localDateTime, upload.getFinished());
        assertEquals(TransferStatus.ERRONEOUS, upload.getStatus());

        // undefined should not alter the upload
        uploadDto.setFinished(JsonNullable.undefined());
        uploadDtoMapper.updateUploadWithUploadDto(uploadDto, upload);
        assertEquals(localDateTime, upload.getFinished());

        // null should set the field to null
        uploadDto.setFinished(JsonNullable.of(null));
        uploadDtoMapper.updateUploadWithUploadDto(uploadDto, upload);
        assertNull(upload.getFinished());
    }


    // --- Helper Functions --------------------------------------------------------------------------------------------

    private void mockContextUserServiceWithUser(User contextUser) {
        when(contextUserServiceMock.getUserFromContext()).thenReturn(contextUser);
        ReflectionTestUtils.setField(contextUserServiceMock, "user", contextUser);
        when(contextUserServiceMock.getUser()).thenReturn(contextUser);
        when(contextUserServiceMock.getUserId()).thenReturn(contextUser.getId());
        when(contextUserServiceMock.getUserRole()).thenReturn(contextUser.getRole());
    }

}
