package org.kinsecta.webapp.api.service;

import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.exception.unchecked.WrongArgumentException;
import org.kinsecta.webapp.api.model.entities.*;
import org.kinsecta.webapp.api.model.repositories.ResetTokenRepository;
import org.kinsecta.webapp.api.model.repositories.UploadRepository;
import org.kinsecta.webapp.api.model.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@ActiveProfiles("it")
class UserServiceIT {

    @Autowired
    private UserService userService;
    @Autowired
    private ResetTokenService resetTokenService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ResetTokenRepository resetTokenRepository;
    @Autowired
    private UploadRepository uploadRepository;


    private User constructNewUser(String username) {
        User user = new User();
        user.setUsername(username);
        user.setEmail(username + "@example.com");
        user.setFullName("Test User Name");
        user.setRole(UserRole.TEAM_MEMBER);
        user.setStatus(UserStatus.ACTIVE);
        return user;
    }

    private User constructNewUserWithPassword(String username) {
        User user = constructNewUser(username);
        // the bcrypt hash for "Password!123"
        user.setPassword("$2a$10$C6aV/sWWw4BHJ0fp6n1drOWtYFbG0FTshzKoQK0G5JzEzen.xMIwW");
        return user;
    }


    @Test
    @Transactional
    void createUser() {
        User user = constructNewUser("test-user-1");
        // password is auto-created in the userService.createUser() method
        User createdUser = userService.createUser(user);

        assertNotNull(createdUser.getId());
        assertEquals("test-user-1", createdUser.getUsername());
        assertNotNull(createdUser.getPassword());
        assertNull(createdUser.getDeleted());

        // #55 assert that the 'created' and 'modified' timestamps, which are created by the DB,
        // are not null in the saved entity returned from repository.save()
        assertNotNull(createdUser.getCreated());
        assertNotNull(createdUser.getModified());
    }

    @Test
    void checkCurrentUserPasswordMatchesPassword__matches() {
        User user = constructNewUserWithPassword("test-user-2");
        assertDoesNotThrow(() -> userService.checkCurrentUserPasswordMatchesPassword("Password!123", user));
    }

    @Test
    void checkCurrentUserPasswordMatchesPassword__doesNotMatch() {
        User user = constructNewUserWithPassword("test-user-3");
        assertThrows(WrongArgumentException.class, () -> userService.checkCurrentUserPasswordMatchesPassword("test123", user));
    }

    @Test
    @Transactional(readOnly = true)
    void getUser__byId_exists() {
        User user = assertDoesNotThrow(() -> userService.getUser(1L));

        assertEquals("t.fischer", user.getUsername());

        // #55 check that 'created' and 'modified' timestamps are included as well
        assertNotNull(user.getCreated());
        assertNotNull(user.getModified());
    }

    @Test
    @Transactional(readOnly = true)
    void getUser__byId_doesntExist() {
        assertThrows(NotFoundException.class, () -> userService.getUser(999999L));
    }

    @Test
    @Transactional(readOnly = true)
    void getUser__byUsername_exists() {
        User user = assertDoesNotThrow(() -> userService.getUser("t.fischer"));

        assertEquals(1L, user.getId());

        // #55 check that 'created' and 'modified' timestamps are included as well
        assertNotNull(user.getCreated());
        assertNotNull(user.getModified());
    }

    @Test
    @Transactional(readOnly = true)
    void getUser__byUsername_doesntExist() {
        assertThrows(NotFoundException.class, () -> userService.getUser("non-existing-user"));
    }

    @Test
    @Transactional(readOnly = true)
    void getUserByEmail__exists() {
        User user = assertDoesNotThrow(() -> userService.getUserByEmail("t.fischer@goldflam.de"));

        assertEquals(1L, user.getId());
        assertEquals("t.fischer", user.getUsername());

        // #55 check that 'created' and 'modified' timestamps are included as well
        assertNotNull(user.getCreated());
        assertNotNull(user.getModified());
    }

    @Test
    @Transactional(readOnly = true)
    void getUserByEmail__doesntExist() {
        assertThrows(NotFoundException.class, () -> userService.getUserByEmail("test@example.org"));
    }

    @Test
    @Transactional
    void deleteUserById__noConstraints() {
        User user = userService.createUser(constructNewUser("test-user-4"));
        Long id = user.getId();
        userService.deleteUserById(id);
        assertEquals(Optional.empty(), userRepository.findById(id));
    }

    @Test
    void deleteUserById__hasResetTokenConstraint() {
        // create user
        User user = userService.createUser(constructNewUser("test-user-5"));
        Long id = user.getId();

        // create a ResetToken constraint
        ResetToken resetToken = resetTokenService.save(resetTokenService.createResetToken(user));

        // delete user
        userService.deleteUserById(id);

        // query deleted user and reset token repo
        Optional<User> queryUser = userRepository.findById(id);
        Optional<ResetToken> queryToken = resetTokenRepository.findById(resetToken.getId());

        // assert that user is not present anymore
        assertEquals(Optional.empty(), queryUser);
        // assert that reset token is not present anymore
        assertEquals(Optional.empty(), queryToken);
    }

    @Test
    void deleteUserById__hasUploadConstraint() {
        // create user
        // password is auto-created in the userService.createUser() method
        User user = userService.createUser(constructNewUser("test-user-6"));
        Long id = user.getId();

        // create an upload constraint
        Upload upload = new Upload();
        upload.setUser(user);
        upload.setStatus(TransferStatus.IN_PROGRESS);
        uploadRepository.save(upload);

        // delete user
        userService.deleteUserById(id);

        // query deleted user
        Optional<User> queryUser = userRepository.findById(id);

        // assert that user is present
        assertTrue(queryUser.isPresent());

        // assert that user properties have been overridden
        assertNotEquals(user.getUsername(), queryUser.get().getUsername());
        assertNotEquals(user.getEmail(), queryUser.get().getEmail());
        assertNotEquals(user.getEmail(), queryUser.get().getEmail());
        assertNotEquals(user.getFullName(), queryUser.get().getFullName());

        // assert that UserStatus and deleted are set correctly
        assertEquals(UserStatus.DELETED, queryUser.get().getStatus());
        assertNotNull(queryUser.get().getDeleted());
    }

    @Test
    @Transactional
    void isActiveUser__true() {
        User user = constructNewUser("test-user");
        assertTrue(UserService.isActiveUser(user));
    }

    @Test
    @Transactional
    void isActiveUser__false() {
        User user = constructNewUser("test-user");
        user.setStatus(UserStatus.INACTIVE);
        assertFalse(UserService.isActiveUser(user));
    }

    @Test
    @Transactional
    void isActiveUser__false_because_deleted() {
        User user = constructNewUser("test-user");
        user.setDeleted(LocalDateTime.now());
        assertFalse(UserService.isActiveUser(user));
    }

}

