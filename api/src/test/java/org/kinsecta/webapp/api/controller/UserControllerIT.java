package org.kinsecta.webapp.api.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.TestHelperService;
import org.kinsecta.webapp.api.mail.PasswordResetMailService;
import org.kinsecta.webapp.api.model.entities.ResetToken;
import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.model.mapping.UserDtoMapper;
import org.kinsecta.webapp.api.service.ResetTokenService;
import org.kinsecta.webapp.api.service.UserService;
import org.kinsecta.webapp.api.v1.model.*;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("it")
@SpringBootTest
@AutoConfigureMockMvc
class UserControllerIT {

    private static final String USERS_PATH = "/users";
    private static final String USERS_PATH_SINGLE = USERS_PATH + "/1";
    private static final String USERS_PATH_VIEWS = USERS_PATH + "/_views";
    private static final String USERS_PATH_VIEWS_SIMPLE = USERS_PATH_VIEWS + "/simple";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private UserDtoMapper userDtoMapper;

    @Autowired
    private TestHelperService testHelperService;

    @Autowired
    private ResetTokenService resetTokenService;

    @MockBean
    private PasswordResetMailService passwordResetMailServiceMock;

    @Captor
    private ArgumentCaptor<User> userCaptor;

    @Captor
    private ArgumentCaptor<ResetToken> resetTokenCaptor;


    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void getAllUsers_withDataCollectorRole() throws Exception {
        mockMvc.perform(get(USERS_PATH)).andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getAllUsers_withAdminRole() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(USERS_PATH))
            .andExpect(status().isOk())
            .andReturn();

        List<UserDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, UserDto.class);

        assertEquals(7, returnedDtoList.size());
        for (UserDto userDto : returnedDtoList) {
            assertTrue(userDto.getId().isPresent());
            assertNotNull(userDto.getUsername());
            assertNotNull(userDto.getFullName());
            assertNotNull(userDto.getEmail());
            assertNotNull(userDto.getRole());
            assertNotNull(userDto.getStatus());
        }
    }

    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getAllUsers_withAdminRole_withStatusFilter() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(USERS_PATH + "?status=ACTIVE"))
            .andExpect(status().isOk())
            .andReturn();

        List<UserDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, UserDto.class);

        assertEquals(5, returnedDtoList.size());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getAllUsers_withAdminRole_withInvalidStatusFilter() throws Exception {
        mockMvc.perform(get(USERS_PATH + "?status=aktiv")).andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllUserViews() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(USERS_PATH_VIEWS))
            .andExpect(status().isOk())
            .andReturn();

        List<ApiViewDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, ApiViewDto.class);

        assertEquals(1, returnedDtoList.size());
        assertEquals("simple", returnedDtoList.get(0).getView());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getAllUsersSimple_withAdminRole() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(USERS_PATH_VIEWS_SIMPLE))
            .andExpect(status().isOk())
            .andReturn();

        List<UserSimpleViewDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, UserSimpleViewDto.class);

        assertEquals(7, returnedDtoList.size());
        for (UserSimpleViewDto UserSimpleViewDto : returnedDtoList) {
            assertTrue(UserSimpleViewDto.getId().isPresent());
            assertNotNull(UserSimpleViewDto.getUsername());
            assertNotNull(UserSimpleViewDto.getFullName());
        }
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllUsersSimple_withTeamMemberRole() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(USERS_PATH_VIEWS_SIMPLE))
            .andExpect(status().isOk())
            .andReturn();

        List<UserSimpleViewDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, UserSimpleViewDto.class);

        assertEquals(7, returnedDtoList.size());
        for (UserSimpleViewDto userSimpleViewDto : returnedDtoList) {
            assertTrue(userSimpleViewDto.getId().isPresent());
            // Assert name details for own MockUser but not for others
            if (userSimpleViewDto.getId().get().equals(3L)) {
                assertNotNull(userSimpleViewDto.getUsername());
                assertNotNull(userSimpleViewDto.getFullName());
            } else {
                assertNull(userSimpleViewDto.getUsername());
                assertNull(userSimpleViewDto.getFullName());
            }
        }
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllUsersSimple_withTeamMemberRole_withStatusFilter() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(USERS_PATH_VIEWS_SIMPLE + "?status=ACTIVE"))
            .andExpect(status().isOk())
            .andReturn();

        List<UserSimpleViewDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, UserSimpleViewDto.class);

        assertEquals(5, returnedDtoList.size());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void createUser() throws Exception {
        UserDto userDto = new UserDto();
        userDto.setUsername("test");
        userDto.setEmail("test@goldflam.de");
        userDto.setFullName("Test User");
        userDto.setRole(UserRoleDto.TEAM_MEMBER);
        userDto.setStatus(UserStatusDto.ACTIVE);

        String contentString = objectMapper.writeValueAsString(userDto);

        MvcResult mvcResult = mockMvc.perform(post(USERS_PATH)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(contentString)
                .with(csrf()))
            .andExpect(status().isCreated())
            .andReturn();
        UserDto returnedDto = testHelperService.convertResponseBodyToDto(mvcResult, UserDto.class);
        User user = userService.getUser(returnedDto.getId().get());
        Optional<ResetToken> resetTokenOpt = resetTokenService.getResetTokenForUser(user);
        assertTrue(resetTokenOpt.isPresent());
        verify(passwordResetMailServiceMock).sendPwResetLinkForNewUser(userCaptor.capture(), resetTokenCaptor.capture());
        assertEquals(user, userCaptor.getValue());
        assertEquals(resetTokenOpt.get(), resetTokenCaptor.getValue());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void createUser_email_invalid() throws Exception {
        UserDto userDto = new UserDto();
        userDto.setUsername("test2");
        userDto.setEmail("test@example.org");
        userDto.setFullName("Test User 2");
        userDto.setRole(UserRoleDto.TEAM_MEMBER);
        userDto.setStatus(UserStatusDto.ACTIVE);

        String contentString = objectMapper.writeValueAsString(userDto);

        mockMvc.perform(post(USERS_PATH)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(contentString)
                .with(csrf()))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.message").value("Cannot create User: The email address 'test@example.org' is not valid with regard to the allowed email domain whitelist: [goldflam.de]"));
    }

    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getUser() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(USERS_PATH_SINGLE))
            .andExpect(status().isOk())
            .andReturn();

        UserDto returnedDto = testHelperService.convertResponseBodyToDto(mvcResult, UserDto.class);
        assertEquals(1, returnedDto.getId().get());
        assertEquals("t.fischer", returnedDto.getUsername());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void editUser() throws Exception {
        User user = userService.getUser(2L);

        UserDto userDto = userDtoMapper.userToUserDto(user);
        userDto.setFullName("Test User");
        userDto.setStatus(UserStatusDto.INACTIVE);

        String contentString = objectMapper.writeValueAsString(userDto);

        MvcResult mvcResult = mockMvc.perform(put(USERS_PATH + "/2")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(contentString)
                .with(csrf()))
            .andExpect(status().isOk())
            .andReturn();

        UserDto returnedDto = testHelperService.convertResponseBodyToDto(mvcResult, UserDto.class);
        assertEquals(2, returnedDto.getId().get());
        assertEquals("Test User", returnedDto.getFullName());
        assertEquals(UserStatusDto.INACTIVE, returnedDto.getStatus());
    }

    @Test
    @DisplayName("Trying to edit an already deleted User")
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void editUser_deleted() throws Exception {
        // retrieve deleted user
        User user = userService.getUser(5L);

        // edit deleted user
        UserDto userDto = userDtoMapper.userToUserDto(user);
        userDto.setFullName("Test User");
        userDto.setStatus(UserStatusDto.ACTIVE);

        String contentString = objectMapper.writeValueAsString(userDto);

        mockMvc.perform(put(USERS_PATH + "/5")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(contentString)
                .with(csrf()))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.message").value("A deleted User cannot be edited."));

        // re-retrieve original user again and assert that nothing has changed.
        User actualUser = userService.getUser(5L);
        assertEquals(user, actualUser);
    }

    @Test
    @DisplayName("Trying to change the UserStatus to deleted for an inactive User")
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void editUser_to_deleted() throws Exception {
        // retrieve inactive user
        User user = userService.getUser(2L);

        // edit inactive user
        UserDto userDto = userDtoMapper.userToUserDto(user);
        userDto.setFullName("Test User");
        userDto.setStatus(UserStatusDto.DELETED);

        String contentString = objectMapper.writeValueAsString(userDto);

        mockMvc.perform(put(USERS_PATH + "/2")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(contentString)
                .with(csrf()))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.message").value("The status of a User cannot be changed to 'DELETED'."));

        // re-retrieve original user again and assert that nothing has changed.
        User actualUser = userService.getUser(2L);
        assertEquals(user, actualUser);
    }

    @Test
    @Transactional
    @DisplayName("Delete a User which doesn't have any Uploads or other constraints in our general 'V1000__integration_test_data.sql' fixtures.")
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void deleteUser__noForeignKeyConstraints() throws Exception {
        mockMvc.perform(delete(USERS_PATH + "/4"))
            .andExpect(status().isOk());
    }

}
