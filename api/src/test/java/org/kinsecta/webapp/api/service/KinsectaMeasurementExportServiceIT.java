package org.kinsecta.webapp.api.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.TestHelperService;
import org.kinsecta.webapp.api.exception.checked.MappingException;
import org.kinsecta.webapp.api.model.entities.Dataset;
import org.kinsecta.webapp.api.model.entities.DatasetImage;
import org.kinsecta.webapp.api.s3.S3ClientException;
import org.kinsecta.webapp.api.s3.StorageException;
import org.kinsecta.webapp.api.util.TempDirectoryHelper;
import org.kinsecta.webapp.api.v1.model.DatasetTagDto;
import org.kinsecta.webapp.api.v1.model.KinsectaMeasurement;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.tuple;
import static org.junit.jupiter.api.Assertions.*;


@ActiveProfiles("it")
@SpringBootTest
@Transactional
class KinsectaMeasurementExportServiceIT {

    @Autowired
    private KinsectaMeasurementExportService kinsectaMeasurementExportService;

    @Autowired
    private TestHelperService testHelperService;

    @Autowired
    private TempDirectoryHelper tempDirectoryHelper;

    @Autowired
    private DatasetService datasetService;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    void testGenerateDirectoriesForDatasets() throws IOException, StorageException, MappingException, S3ClientException {
        try {
            Dataset dataset1 = datasetService.getDataset(1L);
            List<DatasetImage> dataset1Images = new ArrayList<>(dataset1.getDatasetImages());
            Dataset dataset2 = datasetService.getDataset(2L);

            testHelperService.uploadTestFilesAsS3Object(
                dataset1.getVideoS3Object(),
                dataset1Images.get(0).getS3Object(),
                dataset1Images.get(1).getS3Object(),
                dataset2.getMeasurementWingbeat().getImageS3Object(),
                dataset2.getMeasurementWingbeat().getWavS3Object());

            File tempDirectory = tempDirectoryHelper.createRandomTempSubDirectory();
            kinsectaMeasurementExportService.generateDirectoriesForDatasets(tempDirectory, Arrays.asList(dataset1, dataset2));

            File directory1 = new File(tempDirectory.toString(), "1_2022-01-29_13-59-12");
            assertTrue(directory1.exists());
            assertTrue(directory1.isDirectory());
            List<String> fileList1 = Arrays.stream(directory1.list()).toList();
            assertTrue(fileList1.contains("data.json"));
            assertTrue(fileList1.contains(dataset1.getVideoS3Object().getFilename()));
            assertTrue(fileList1.contains(dataset1Images.get(0).getS3Object().getFilename()));
            assertTrue(fileList1.contains(dataset1Images.get(1).getS3Object().getFilename()));

            File directory2 = new File(tempDirectory.toString(), "1_2022-01-29_14-31-20");
            assertTrue(directory2.exists());
            assertTrue(directory2.isDirectory());
            List<String> fileList2 = Arrays.stream(directory2.list()).toList();
            assertTrue(fileList2.contains("data.json"));
            assertTrue(fileList2.contains(dataset2.getMeasurementWingbeat().getImageS3Object().getFilename()));
            assertTrue(fileList2.contains(dataset2.getMeasurementWingbeat().getWavS3Object().getFilename()));

        } finally {
            testHelperService.cleanWorkingDirectory();
            testHelperService.cleanS3Buckets();
        }
    }

    @Test
    void testGenerateDirectoriesForDatasets__same_directory_name() throws IOException, StorageException, MappingException, S3ClientException {
        try {
            Dataset dataset = datasetService.getDataset(1L);
            List<DatasetImage> datasetImages = new ArrayList<>(dataset.getDatasetImages());

            testHelperService.uploadTestFilesAsS3Object(
                dataset.getVideoS3Object(),
                datasetImages.get(0).getS3Object(),
                datasetImages.get(1).getS3Object());

            File tempDirectory = tempDirectoryHelper.createRandomTempSubDirectory();
            kinsectaMeasurementExportService.generateDirectoriesForDatasets(tempDirectory, Arrays.asList(dataset, dataset, dataset));

            File directory1 = new File(tempDirectory.toString(), "1_2022-01-29_13-59-12");
            assertTrue(directory1.exists());
            assertTrue(directory1.isDirectory());
            List<String> fileList1 = Arrays.stream(directory1.list()).toList();
            assertTrue(fileList1.contains("data.json"));
            assertTrue(fileList1.contains(dataset.getVideoS3Object().getFilename()));
            assertTrue(fileList1.contains(datasetImages.get(0).getS3Object().getFilename()));
            assertTrue(fileList1.contains(datasetImages.get(1).getS3Object().getFilename()));

            File dataJsonOfDirectory1 = new File(directory1.toString(), "data.json");
            KinsectaMeasurement kinsectaMeasurement = objectMapper.readValue(dataJsonOfDirectory1, KinsectaMeasurement.class);

            // #132 check for missing unit
            assertNotNull(kinsectaMeasurement.getSpectrum().getUnit());
            assertTrue(kinsectaMeasurement.getSpectrum().getUnit().isEmpty());

            // #140 check for dataset tags
            assertNotNull(kinsectaMeasurement.getTags());
            List<DatasetTagDto> datasetTagDtos = kinsectaMeasurement.getTags();
            assertEquals(2, datasetTagDtos.size());

            assertThat(datasetTagDtos)
                .extracting(DatasetTagDto::getId, DatasetTagDto::getName, DatasetTagDto::getAutoTag)
                .containsExactlyInAnyOrder(
                    tuple(JsonNullable.of(1L), "Video", true),
                    tuple(JsonNullable.of(3L), "Fotos", true)
                );

            File directory2 = new File(tempDirectory.toString(), "1_2022-01-29_13-59-12(1)");
            assertTrue(directory2.exists());
            assertTrue(directory2.isDirectory());
            List<String> fileList2 = Arrays.stream(directory2.list()).toList();
            assertTrue(fileList2.contains("data.json"));
            assertTrue(fileList2.contains(dataset.getVideoS3Object().getFilename()));
            assertTrue(fileList2.contains(datasetImages.get(0).getS3Object().getFilename()));
            assertTrue(fileList2.contains(datasetImages.get(1).getS3Object().getFilename()));

            File directory3 = new File(tempDirectory.toString(), "1_2022-01-29_13-59-12(2)");
            assertTrue(directory3.exists());
            assertTrue(directory3.isDirectory());
            List<String> fileList3 = Arrays.stream(directory3.list()).toList();
            assertTrue(fileList3.contains("data.json"));
            assertTrue(fileList3.contains(dataset.getVideoS3Object().getFilename()));
            assertTrue(fileList3.contains(datasetImages.get(0).getS3Object().getFilename()));
            assertTrue(fileList3.contains(datasetImages.get(1).getS3Object().getFilename()));

        } finally {
            testHelperService.cleanWorkingDirectory();
            testHelperService.cleanS3Buckets();
        }
    }

}
