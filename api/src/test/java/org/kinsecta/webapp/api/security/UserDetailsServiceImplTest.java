package org.kinsecta.webapp.api.security;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.model.entities.UserRole;
import org.kinsecta.webapp.api.model.entities.UserStatus;
import org.kinsecta.webapp.api.model.repositories.UserRepository;
import org.kinsecta.webapp.api.service.LoginAttemptService;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class UserDetailsServiceImplTest {

    @Mock
    UserRepository userRepository;

    @Mock
    LoginAttemptService loginAttemptService;


    @Test
    void loadUserByUsername__userIsFound() {
        String username = "my_username";
        String password = "my_$3cr3t_password_Ä$å";
        UserRole role = UserRole.TEAM_MEMBER;
        UserStatus status = UserStatus.ACTIVE;

        User user = spy(new User());
        user.setUsername(username);
        user.setPassword(password);
        user.setRole(role);
        user.setStatus(status);

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));

        UserDetailsServiceImpl userDetailsService = getUserDetailsService();
        UserDetailsImpl actualUserDetails = userDetailsService.loadUserByUsername(username);
        assertEquals(user, actualUserDetails.getUser());
    }

    @Test
    void loadUserByUsername__userNotFound() {
        String username = "not_existing_username";

        when(userRepository.findByUsername(username)).thenReturn(Optional.empty());

        UserDetailsServiceImpl userDetailsService = getUserDetailsService();
        assertThrows(UsernameNotFoundException.class, () -> userDetailsService.loadUserByUsername(username));
    }


    private UserDetailsServiceImpl getUserDetailsService() {
        UserDetailsImpl userDetails = spy(new UserDetailsImpl(loginAttemptService));
        return new UserDetailsServiceImpl(userRepository, userDetails);
    }

}
