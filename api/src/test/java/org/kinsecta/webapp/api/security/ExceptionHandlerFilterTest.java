package org.kinsecta.webapp.api.security;

import com.auth0.jwt.exceptions.InvalidClaimException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.kinsecta.webapp.api.config.ObjectMapperConfig;
import org.kinsecta.webapp.api.v1.model.JsonError;
import org.kinsecta.webapp.api.v1.model.JsonErrorCause;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class ExceptionHandlerFilterTest {

    @Mock
    HttpServletRequest request;

    @Mock
    FilterChain filterChain;

    HttpServletResponse response = new MockHttpServletResponse();

    @Captor
    ArgumentCaptor<JsonError> jsonErrorCaptor;

    ObjectMapper objectMapper = new ObjectMapperConfig().objectMapper();


    private JsonError mockDoFilterInternalWithExceptionAndExpect(RuntimeException exception, Throwable cause, HttpStatus expectedOverriddenHttpStatus, String expectedOverriddenMessage) throws ServletException, IOException {
        // Configure mocks and Argument Captor
        doThrow(exception).when(filterChain).doFilter(request, response);
        when(request.getServletPath()).thenReturn("/login");

        // Run test method
        ExceptionHandlerFilter exceptionHandlerFilter = spy(new ExceptionHandlerFilter(objectMapper));
        exceptionHandlerFilter.doFilterInternal(request, response, filterChain);

        // Retrieve argument passed to convertObjectToJson() method (= errorAttributes)
        verify(exceptionHandlerFilter).convertObjectToJson(jsonErrorCaptor.capture());
        JsonError capturedJsonError = jsonErrorCaptor.getValue();

        // Asserts for response character encoding and content type
        assertEquals(ExceptionHandlerFilter.RESPONSE_ENCODING, response.getCharacterEncoding());
        assertEquals(ExceptionHandlerFilter.RESPONSE_CONTENT_TYPE + ";charset=" + ExceptionHandlerFilter.RESPONSE_ENCODING, response.getContentType());

        // Assert overridden status code
        assertEquals(expectedOverriddenHttpStatus.value(), capturedJsonError.getStatus(), "Captured status code does not match the expected overridden status code");

        // Assert overridden message, therefore expect additional error attributes
        if (expectedOverriddenMessage != null) {
            assertEquals(expectedOverriddenMessage, capturedJsonError.getMessage(), "Captured message does not match the expected overridden message");
            assertNotNull(capturedJsonError.getOriginalMessage());
            assertEquals(exception.getMessage(), capturedJsonError.getOriginalMessage(), "Captured original message does not match the expected original message");
        } else {
            assertNull(capturedJsonError.getOriginalMessage());
        }

        // Assertions on the cause child object if present
        if (cause != null) {
            JsonErrorCause errorCause = capturedJsonError.getCause();
            assertEquals(cause.getClass().getName(), errorCause.getException());
            assertEquals(cause.getMessage(), errorCause.getMessage());
        }

        // Return captured errorAttributes
        return capturedJsonError;
    }

    @Test
    void doFilterInternal__with_SignatureVerificationException() throws ServletException, IOException {
        // Configure mock exception
        NoSuchAlgorithmException cause = new NoSuchAlgorithmException("cause message");
        SignatureVerificationException exception = new SignatureVerificationException(mock(com.auth0.jwt.algorithms.Algorithm.class), cause);

        // Run test method and expect 401 UNAUTHORIZED in the response and an overridden message
        mockDoFilterInternalWithExceptionAndExpect(exception, cause, HttpStatus.UNAUTHORIZED, "JWT secret token has changed. Re-login required!");
    }

    @Test
    void doFilterInternal__with_TokenExpiredException() throws ServletException, IOException {
        // Configure mock exception
        TokenExpiredException exception = new TokenExpiredException(String.format("The Token has expired on %s.", new Date()), new Date().toInstant());

        // Run test method and expect 401 UNAUTHORIZED in the response and NO overridden message
        mockDoFilterInternalWithExceptionAndExpect(exception, null, HttpStatus.UNAUTHORIZED, null);
    }

    @Test
    void doFilterInternal__with_InvalidClaimException() throws ServletException, IOException {
        // Configure mock exception
        InvalidClaimException exception = new InvalidClaimException(String.format("The Claim '%s' value doesn't match the required one.", JwtTokenService.CLAIM_SECURITY_DETAILS_MODIFIED));

        // Run test method and expect 401 UNAUTHORIZED in the response and NO overridden message
        mockDoFilterInternalWithExceptionAndExpect(exception, null, HttpStatus.UNAUTHORIZED, null);
    }

    @Test
    void doFilterInternal__with_DisabledException() throws ServletException, IOException {
        // Configure mock exception
        DisabledException exception = new DisabledException(JwtAuthenticationFailureHandler.MESSAGE_USER_DISABLED);

        // Run test method and expect 401 UNAUTHORIZED in the response and NO overridden message
        mockDoFilterInternalWithExceptionAndExpect(exception, null, HttpStatus.UNAUTHORIZED, null);
    }

    @Test
    void doFilterInternal__with_LockedException() throws ServletException, IOException {
        // Configure mock exception
        LockedException exception = new LockedException(JwtAuthenticationFailureHandler.MESSAGE_USER_LOCKED);

        // Run test method and expect 401 UNAUTHORIZED in the response and NO overridden message
        mockDoFilterInternalWithExceptionAndExpect(exception, null, HttpStatus.UNAUTHORIZED, null);
    }

}
