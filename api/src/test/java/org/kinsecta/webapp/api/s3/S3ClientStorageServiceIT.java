package org.kinsecta.webapp.api.s3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.TestHelperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;


@ActiveProfiles("it")
@SpringBootTest
@Transactional
class S3ClientStorageServiceIT {

    private final String resourcePath = "src/test/resources";

    @Autowired
    private S3ClientStorageService s3ClientStorageService;

    @Autowired
    private S3Client s3Client;

    @Autowired
    private TestHelperService testHelperService;


    /**
     * Loads an image from the test resources and converts it to a byte array
     */
    private byte[] getFileAsBytes(String path) throws IOException, URISyntaxException {
        URL imageUrl = ClassLoader.getSystemResource(path);
        return Files.readAllBytes(Path.of(imageUrl.toURI()));
    }


    @AfterEach
    private void cleanUpAfterEachTest() {
        try {
            testHelperService.cleanS3Buckets();
        } catch (S3ClientException e) {
            e.printStackTrace();
        }
    }


    @Test
    void loadAnS3ObjectAndSaveAsFile__viaOutputStream() throws StorageException, IOException, S3ClientException {
        Path inputFilePath = Paths.get(resourcePath, "s3/test-image.jpg");
        String s3TestBucket = s3ClientStorageService.getCurrentS3BucketName();
        String s3Key = inputFilePath.getFileName().toString();
        File outputFile = Paths.get(resourcePath, "s3/test-image-temp.jpg").toFile();

        try (OutputStream out = new FileOutputStream(outputFile)) {
            s3Client.uploadObject(s3TestBucket, s3Key, inputFilePath.toFile());

            s3ClientStorageService.load(s3TestBucket, s3Key, out);
            out.close();

            assertTrue(outputFile.isFile());
            assertTrue(outputFile.length() > 1000);
        } finally {
            outputFile.delete();
        }
    }

    @Test
    void loadAnS3ObjectAndReturn__viaInputStream() throws StorageException, IOException, S3ClientException {
        Path inputFilePath = Paths.get(resourcePath, "s3/test-image.jpg");
        String s3TestBucket = s3ClientStorageService.getCurrentS3BucketName();
        String s3Key = inputFilePath.getFileName().toString();
        File outputFile = Paths.get(resourcePath, "s3/test-image-temp.jpg").toFile();

        try {
            s3Client.uploadObject(s3TestBucket, s3Key, inputFilePath.toFile());
            InputStream inputStream = s3ClientStorageService.load(s3TestBucket, s3Key);
            Files.copy(inputStream, outputFile.toPath());
            inputStream.close();

            assertTrue(outputFile.isFile());
            assertTrue(outputFile.length() > 1000);
        } finally {
            outputFile.delete();
        }
    }

    @Test
    @DisplayName("Upload a file with a multipart request to the S3 storage provider")
    void uploadMultipartFile() throws IOException, URISyntaxException, StorageException, S3ClientException {
        String path = "s3/test-image.png";
        String name = "test-image.png";
        String originalName = "test-image-original.png";
        String contentType = "image/png";
        byte[] content = getFileAsBytes(path);

        MultipartFile mFile = new MockMultipartFile(name, originalName, contentType, content);
        String s3TestBucket = s3ClientStorageService.getCurrentS3BucketName();
        String s3Key = s3ClientStorageService.store(s3TestBucket, originalName, mFile);
        assertNotNull(s3Key);
        assertFalse(s3Key.isEmpty());
        assertTrue(testHelperService.existsS3ObjectInBucketStartingWith(s3TestBucket, s3Key));
    }

    @Test
    @DisplayName("Upload a file to the S3 storage provider")
    void testUploadFile() throws IOException, StorageException, S3ClientException {
        File inputFile = Paths.get(resourcePath, "s3/test-image.jpg").toFile();
        String s3Key = inputFile.getName();
        String s3TestBucket = s3ClientStorageService.getCurrentS3BucketName();
        try (InputStream inputStream = new FileInputStream(inputFile)) {
            String returnedS3Key = s3ClientStorageService.store(s3TestBucket, s3Key, inputStream, inputFile.length());
            assertEquals(s3Key, returnedS3Key);
            assertTrue(testHelperService.existsS3ObjectInBucketStartingWith(s3TestBucket, s3Key));
        }
    }

    @Test
    @DisplayName("Delete previously created S3 object")
    void deleteFile() throws StorageException, S3ClientException {
        Path inputFilePath = Paths.get(resourcePath, "s3/test-image.jpg");
        String s3TestBucket = s3ClientStorageService.getCurrentS3BucketName();
        String s3Key = inputFilePath.getFileName().toString();
        s3Client.uploadObject(s3TestBucket, s3Key, inputFilePath.toFile());
        s3ClientStorageService.delete(s3TestBucket, s3Key);
        assertFalse(testHelperService.existsS3ObjectInBucketStartingWith(s3TestBucket, s3Key));
    }

    @Test
    @DisplayName("Deleting a non-existing S3 object should not throw an error")
    void deleteNonExistingFile() {
        String s3TestBucket = s3ClientStorageService.getCurrentS3BucketName();
        assertDoesNotThrow(() -> s3ClientStorageService.delete(s3TestBucket, "s3_non_existing_key"));
    }

}
