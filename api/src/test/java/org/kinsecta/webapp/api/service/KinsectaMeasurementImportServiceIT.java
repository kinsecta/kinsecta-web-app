package org.kinsecta.webapp.api.service;

import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.exception.checked.JsonDeserializationException;
import org.kinsecta.webapp.api.exception.checked.MappingException;
import org.kinsecta.webapp.api.exception.checked.ValidationViolationException;
import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.model.entities.Dataset;
import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.model.mapping.KinsectaMeasurementImportMapper;
import org.kinsecta.webapp.api.v1.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.validation.ConstraintViolation;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;


@ActiveProfiles("it")
@SpringBootTest
class KinsectaMeasurementImportServiceIT {

    private final String resourcePath = "src/test/resources";

    @Autowired
    private KinsectaMeasurementImportService kinsectaMeasurementImportService;

    @Autowired
    private KinsectaMeasurementImportMapper kinsectaMeasurementImportMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private UploadService uploadService;


    @Test
    void testDataFolderNameRegex() {
        String regex = KinsectaMeasurementImportService.DATA_FOLDER_NAME_REGEX;
        assertTrue("0001_2022-01-29_13-59-12".matches(regex));
        assertTrue("12_1995-12-29_00-00-00".matches(regex));
        assertFalse("0001_22-01-29_13-59-12".matches(regex));
        assertFalse("abc_2022-01-29_13-59-12".matches(regex));
        assertFalse("2022-01-29_13-59-12".matches(regex));
        assertFalse("0001-2022-01-29-13-59-12".matches(regex));
    }

    @Test
    void mapJsonStringToKinsectaMeasurementAndValidate__valid() throws IOException, ValidationViolationException, JsonDeserializationException {
        // Parse JSON file as KinsectaMeasurement
        File jsonFile = Paths.get(resourcePath + "/exchange-dto/31-exchange-spec-valid.json").toFile();
        KinsectaMeasurement kinsectaMeasurement = kinsectaMeasurementImportService.mapJsonFileToKinsectaMeasurementAndValidate(jsonFile);
        assertNotNull(kinsectaMeasurement);

        // check 'user_id'
        assertEquals(1L, kinsectaMeasurement.getUserId());

        // check 'sensor_id'
        assertEquals(1L, kinsectaMeasurement.getSensorId());

        // check 'date_time'
        assertEquals(LocalDateTime.of(2022, 5, 18, 14, 15, 31), kinsectaMeasurement.getDateTime());

        // check 'video' object for specific file
        assertEquals("c6bb8592-4b43-4a41-b052-2f8e83153f07.h264", kinsectaMeasurement.getVideo().getFilename());

        // check 'images' object for 2 files
        assertEquals(2, kinsectaMeasurement.getImages().size());
        assertEquals("d6bc8535-4a13-4a41-b052-2k9d80024k17.jpg", kinsectaMeasurement.getImages().get(0).getFilename());
        assertEquals("t9f39253-0j84-3l98-e428-c4a480123g09.jpg", kinsectaMeasurement.getImages().get(1).getFilename());

        // check 'wingbeat' object
        assertEquals("c6ee8592-5k2g-9s10-v456-8d7h5124543f12.wav", kinsectaMeasurement.getWingbeat().getFilename());
        assertEquals(96000, kinsectaMeasurement.getWingbeat().getSampleRate());
        assertEquals("t9f39253-0j84-3l98-e428-c4a480123g09.png", kinsectaMeasurement.getWingbeat().getPngFilename());

        // check 'main_classifications' object
        assertEquals(4, kinsectaMeasurement.getMainClassifications().size());
        // ... check first classification
        KinsectaMeasurementClassification expectedFirstClassification = new KinsectaMeasurementClassification()
            .type(ClassificationTypeDto.GROUNDTRUTH)
            .probability(new BigDecimal(1))
            .gender(ClassificationGenderDto.FEMALE)
            .order(new KinsectaMeasurementClassificationInsectOrder().name("Lepidoptera").gbifId(729L))
            .family(new KinsectaMeasurementClassificationInsect().name("Hesperiidae").gbifId(5264L))
            .genus(new KinsectaMeasurementClassificationInsect().name("Achalarus").gbifId(2492321L))
            .species(new KinsectaMeasurementClassificationInsect().name("lyciades").gbifId(5231190L));
        assertEquals(expectedFirstClassification, kinsectaMeasurement.getMainClassifications().get(0));
        // ... check second classification
        KinsectaMeasurementClassification expectedSecondClassification = new KinsectaMeasurementClassification()
            .type(ClassificationTypeDto.MACHINE)
            .probability(new BigDecimal("0.346"))
            .gender(ClassificationGenderDto.UNSPECIFIED)
            .order(new KinsectaMeasurementClassificationInsectOrder().name("Lepidoptera").gbifId(729L))
            .family(new KinsectaMeasurementClassificationInsect().name("Hesperiidae").gbifId(5264L))
            .genus(new KinsectaMeasurementClassificationInsect().name("Achalarus"))
            .species(new KinsectaMeasurementClassificationInsect().name("casica").gbifId(5231190L));
        assertEquals(expectedSecondClassification, kinsectaMeasurement.getMainClassifications().get(1));
        // ... check third classification
        KinsectaMeasurementClassification expectedThirdClassification = new KinsectaMeasurementClassification()
            .type(ClassificationTypeDto.HUMAN)
            .probability(new BigDecimal(1))
            .gender(ClassificationGenderDto.FEMALE)
            .order(new KinsectaMeasurementClassificationInsectOrder().name("Lepidoptera").gbifId(729L))
            .family(new KinsectaMeasurementClassificationInsect().name("Hesperiidae").gbifId(5264L))
            .genus(new KinsectaMeasurementClassificationInsect().name("Achalarus").gbifId(2492321L))
            .species(new KinsectaMeasurementClassificationInsect().name("lyciades").gbifId(null));
        assertEquals(expectedThirdClassification, kinsectaMeasurement.getMainClassifications().get(2));
        // ... check fourth classification
        KinsectaMeasurementClassification expectedFourthClassification = new KinsectaMeasurementClassification()
            .type(ClassificationTypeDto.MACHINE)
            .probability(new BigDecimal("0.346"))
            .gender(ClassificationGenderDto.MALE)
            .order(new KinsectaMeasurementClassificationInsectOrder().name("Lepidoptera").gbifId(729L))
            .family(new KinsectaMeasurementClassificationInsect().name("Hesperiidae").gbifId(5264L))
            .genus(new KinsectaMeasurementClassificationInsect().name("Achalarus").gbifId(2492321L));
        expectedFourthClassification.setSpecies(null);
        assertEquals(expectedFourthClassification, kinsectaMeasurement.getMainClassifications().get(3));

        // ... check other random properties
        assertEquals(new BigDecimal("34.4"), kinsectaMeasurement.getHumidity().getValue().get());
        assertEquals(new BigDecimal("1.012"), kinsectaMeasurement.getAirPressure().getValue().get());
        assertEquals("g/cbm", kinsectaMeasurement.getHumidity().getUnit());
        assertEquals("Reutlingen", kinsectaMeasurement.getSensorLocation().getName());
        assertFalse(kinsectaMeasurement.getSensorLocation().getGpsMasked());
    }

    @Test
    void mapJsonStringToKinsectaMeasurementAndValidate__invalid() throws IOException, JsonDeserializationException {
        // Parse JSON file as KinsectaMeasurement
        File jsonFile = Paths.get(resourcePath + "/exchange-dto/31-exchange-spec-invalid.json").toFile();
        boolean hasConstraintViolations = false;
        try {
            kinsectaMeasurementImportService.mapJsonFileToKinsectaMeasurementAndValidate(jsonFile);

        } catch (ValidationViolationException e) {
            hasConstraintViolations = true;
            // Retrieve Constraint Violations
            Set<ConstraintViolation<KinsectaMeasurement>> violationsSet = e.getViolations(KinsectaMeasurement.class);

            // Check that we have 3 violations
            assertEquals(3, violationsSet.size());

            // Need to sort here because its not deterministic
            // Sort by property path because it's different on all violations
            List<ConstraintViolation<KinsectaMeasurement>> violations = violationsSet.stream()
                .sorted(Comparator.comparing(o -> o.getPropertyPath().toString())).toList();

            // check 1. violation
            ConstraintViolation<KinsectaMeasurement> violation1 = violations.get(0);
            assertEquals("{javax.validation.constraints.DecimalMin.message}", violation1.getMessageTemplate());
            assertEquals("rainfall.quantity", violation1.getPropertyPath().toString());
            assertEquals(new BigDecimal("-2"), violation1.getInvalidValue());

            // check 2. violation
            ConstraintViolation<KinsectaMeasurement> violation2 = violations.get(1);
            assertEquals("{javax.validation.constraints.NotNull.message}", violation2.getMessageTemplate());
            assertEquals("sensorLocation.gpsMasked", violation2.getPropertyPath().toString());
            assertNull(violation2.getInvalidValue());

            // check 3. violation
            ConstraintViolation<KinsectaMeasurement> violation4 = violations.get(2);
            assertEquals("{javax.validation.constraints.NotNull.message}", violation4.getMessageTemplate());
            assertEquals("userId", violation4.getPropertyPath().toString());
            assertNull(violation4.getInvalidValue());
        }

        // check that the mapping failed with Constraint Violations
        assertTrue(hasConstraintViolations);
    }

    @Test
    void mapJsonStringToKinsectaMeasurementAndValidate__allowed_nullable_fields() throws IOException, ValidationViolationException, JsonDeserializationException {
        // Parse JSON file as KinsectaMeasurement
        File jsonFile = Paths.get(resourcePath + "/exchange-dto/99-allowed-null-values.json").toFile();
        KinsectaMeasurement kinsectaMeasurement = kinsectaMeasurementImportService.mapJsonFileToKinsectaMeasurementAndValidate(jsonFile);
        // assert that the JSON file is mapped and validated successfully
        assertNotNull(kinsectaMeasurement);
        assertNotNull(kinsectaMeasurement.getVideo());
        assertNotNull(kinsectaMeasurement.getImages());
        assertEquals(3, kinsectaMeasurement.getImages().size());
        assertNull(kinsectaMeasurement.getWingbeat());
        assertNotNull(kinsectaMeasurement.getMainClassifications());
        assertEquals(0, kinsectaMeasurement.getMainClassifications().size());
    }

    @Test
    void mapJsonStringToKinsectaMeasurementAndValidate__allowed_nullable_fields_2() throws IOException, ValidationViolationException, JsonDeserializationException {
        // Parse JSON file as KinsectaMeasurement
        File jsonFile = Paths.get(resourcePath + "/exchange-dto/99-allowed-null-values-2.json").toFile();
        KinsectaMeasurement kinsectaMeasurement = kinsectaMeasurementImportService.mapJsonFileToKinsectaMeasurementAndValidate(jsonFile);
        // assert that the JSON file is mapped and validated successfully
        assertNotNull(kinsectaMeasurement);
        assertNull(kinsectaMeasurement.getVideo());
        assertNull(kinsectaMeasurement.getImages());
        assertNotNull(kinsectaMeasurement.getWingbeat());
        assertNotNull(kinsectaMeasurement.getMainClassifications());
        assertEquals(1, kinsectaMeasurement.getMainClassifications().size());

        KinsectaMeasurementClassification classification = kinsectaMeasurement.getMainClassifications().get(0);
        assertEquals(ClassificationTypeDto.GROUNDTRUTH, classification.getType());
        assertEquals(ClassificationGenderDto.UNSPECIFIED, classification.getGender());
        assertEquals(BigDecimal.ONE, classification.getProbability());
        assertNotNull(classification.getOrder());
        assertNull(classification.getFamily());
        assertNull(classification.getGenus());
        assertNull(classification.getSpecies());
    }

    @Test
    void mapJsonStringToKinsectaMeasurementAndValidate__forbidden_null_values() throws IOException, JsonDeserializationException {
        // Parse JSON file as KinsectaMeasurement
        File jsonFile = Paths.get(resourcePath + "/exchange-dto/99-forbidden-null-values.json").toFile();
        boolean hasConstraintViolations = false;
        try {
            kinsectaMeasurementImportService.mapJsonFileToKinsectaMeasurementAndValidate(jsonFile);

        } catch (ValidationViolationException e) {
            hasConstraintViolations = true;
            // Retrieve Constraint Violations
            Set<ConstraintViolation<KinsectaMeasurement>> violationsSet = e.getViolations(KinsectaMeasurement.class);

            // Check that we have 23 violations
            assertEquals(23, violationsSet.size());

            List<String> expectedNotNullViolationPaths = List.of("wingbeat.filename", "particulateMatter.unit",
                "mainClassifications[0].type", "dateTime", "userId", "rainfall.unit", "humidity.unit",
                "temperature.unit", "wingbeat.sampleRate", "airPressure.unit", "spectrum.unit",
                "mainClassifications[0].order.name", "luminosity.unit", "sensorLocation.postcode",
                "sensorId", "windSensor.unit", "sizeInsect.unit", "mainClassifications[0].gender",
                "mainClassifications[0].probability", "sensorLocation.longitude", "sensorLocation.latitude",
                "sensorLocation.gpsMasked", "sensorLocation.name");

            // Check that we have 23 NotNull violations
            assertEquals(expectedNotNullViolationPaths.size(), violationsSet.size());

            // Need to sort here because its not deterministic
            // Sort by property path because it's different on all violations
            List<ConstraintViolation<KinsectaMeasurement>> violations = violationsSet.stream()
                .sorted(Comparator.comparing(o -> o.getPropertyPath().toString())).toList();

            for (ConstraintViolation<KinsectaMeasurement> violation : violations) {
                assertEquals("{javax.validation.constraints.NotNull.message}", violation.getMessageTemplate());
                assertTrue(expectedNotNullViolationPaths.contains(violation.getPropertyPath().toString()));
            }
        }

        // check that the mapping failed with Constraint Violations
        assertTrue(hasConstraintViolations);
    }

    @Test
    void mapJsonStringToKinsectaMeasurementAndSaveDataset__set_user_from_JSON_valid_userId() throws IOException, ValidationViolationException, JsonDeserializationException, MappingException {
        // Parse JSON file as KinsectaMeasurement
        File jsonFile = Paths.get(resourcePath + "/exchange-dto/105-valid-userId.json").toFile();
        KinsectaMeasurement kinsectaMeasurement = kinsectaMeasurementImportService.mapJsonFileToKinsectaMeasurementAndValidate(jsonFile);
        User uploadUser = uploadService.getUpload(1L).getUser();
        Dataset dataset = kinsectaMeasurementImportMapper.kinsectaMeasurementToDataset(kinsectaMeasurement);
        // Test the logic from KinsectaMeasurementImportService.processKinsectaMeasurementDirectory
        Long kinsectaMeasurementUserId = kinsectaMeasurement.getUserId();
        Optional<User> optionalUserOfKinsectaMeasurement = userService.getOptionalUser(kinsectaMeasurementUserId);
        if (optionalUserOfKinsectaMeasurement.isPresent()) {
            dataset.setUser(optionalUserOfKinsectaMeasurement.get());
        } else {
            dataset.setUser(uploadUser);
        }

        assertEquals(optionalUserOfKinsectaMeasurement.orElseThrow(() -> new NotFoundException(String.format("Couldn't find user with id %d", kinsectaMeasurementUserId))), dataset.getUser());
    }

    @Test
    void mapJsonStringToKinsectaMeasurementAndSaveDataset__set_user_from_Upload_invalid_userId() throws IOException, ValidationViolationException, JsonDeserializationException, MappingException {
        // Parse JSON file as KinsectaMeasurement
        File jsonFile = Paths.get(resourcePath + "/exchange-dto/105-invalid-userId.json").toFile();
        KinsectaMeasurement kinsectaMeasurement = kinsectaMeasurementImportService.mapJsonFileToKinsectaMeasurementAndValidate(jsonFile);
        User uploadUser = uploadService.getUpload(1L).getUser();
        Dataset dataset = kinsectaMeasurementImportMapper.kinsectaMeasurementToDataset(kinsectaMeasurement);
        // Test the logic from KinsectaMeasurementImportService.processKinsectaMeasurementDirectory
        Long kinsectaMeasurementUserId = kinsectaMeasurement.getUserId();
        Optional<User> optionalUserOfKinsectaMeasurement = userService.getOptionalUser(kinsectaMeasurementUserId);
        if (optionalUserOfKinsectaMeasurement.isPresent()) {
            dataset.setUser(optionalUserOfKinsectaMeasurement.get());
        } else {
            dataset.setUser(uploadUser);
        }
        assertTrue(optionalUserOfKinsectaMeasurement.isEmpty());
        assertEquals(uploadUser, dataset.getUser());
    }

    @Test
    void testGenerateHashForDirectory() throws IOException, JsonDeserializationException, ValidationViolationException {
        File jsonFile = Paths.get(resourcePath + "/exchange-dto/31-exchange-spec-valid.json").toFile();
        KinsectaMeasurement kinsectaMeasurement = kinsectaMeasurementImportService.mapJsonFileToKinsectaMeasurementAndValidate(jsonFile);
        File directory = Paths.get(resourcePath + "/s3").toFile();

        String hash = kinsectaMeasurementImportService.generateHashForDirectory(directory, kinsectaMeasurement);
        assertEquals("ef5662bb8edfd3fe9e46851d834a7b0b", hash);

        // change something and make sure it's a different hash
        kinsectaMeasurement.getImages().get(0).setFilename("test.jpg");
        String updatedHash1 = kinsectaMeasurementImportService.generateHashForDirectory(directory, kinsectaMeasurement);
        assertEquals("2dad47591e90edd6ff98907230c4bb0b", updatedHash1);

        // change something and make sure it's a different hash
        kinsectaMeasurement.getMainClassifications().get(0).getOrder().setName("testOrder");
        String updatedHash2 = kinsectaMeasurementImportService.generateHashForDirectory(directory, kinsectaMeasurement);
        assertEquals("0a2e1dffabc6310d0cbd5f78ea6b003a", updatedHash2);
    }

}
