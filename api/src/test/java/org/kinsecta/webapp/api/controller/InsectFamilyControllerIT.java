package org.kinsecta.webapp.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.TestHelperService;
import org.kinsecta.webapp.api.model.entities.InsectFamily;
import org.kinsecta.webapp.api.service.InsectFamilyService;
import org.kinsecta.webapp.api.service.InsectOrderService;
import org.kinsecta.webapp.api.v1.model.InsectFamilyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("it")
@SpringBootTest
@AutoConfigureMockMvc
class InsectFamilyControllerIT {

    private static final String INSECT_FAMILIES_PATH = "/insect_families";
    private static final String INSECT_FAMILIES_PATH_SEARCH = INSECT_FAMILIES_PATH + "/search";
    private static final String INSECT_FAMILIES_PATH_SINGLE = INSECT_FAMILIES_PATH + "/1";

    private final String testName = "Test InsectFamily";
    private final String testNameUrlEncoded = URLEncoder.encode(testName, StandardCharsets.UTF_8.toString());

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TestHelperService testHelperService;

    @Autowired
    private InsectFamilyService insectFamilyService;

    @Autowired
    private InsectOrderService insectOrderService;


    InsectFamilyControllerIT() throws UnsupportedEncodingException {}


    // Helper Function
    public InsectFamily createNewInsectFamily() throws Exception {
        InsectFamily insectFamily = new InsectFamily();
        insectFamily.setName(testName);
        insectFamily.setGbifId(99991L);
        insectFamily.setOrder(insectOrderService.getInsectOrder(1L));

        return insectFamilyService.save(insectFamily);
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllInsectFamilies() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(INSECT_FAMILIES_PATH))
            .andExpect(status().isOk())
            .andReturn();

        List<InsectFamilyDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, InsectFamilyDto.class);
        assertEquals(16, returnedDtoList.size());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void searchInsectFamilies__query_exact_match() throws Exception {
        InsectFamily created = createNewInsectFamily();

        MvcResult mvcResult = mockMvc.perform(get(INSECT_FAMILIES_PATH_SEARCH + "?query=" + testNameUrlEncoded))
            .andExpect(status().isOk())
            .andReturn();

        List<InsectFamilyDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, InsectFamilyDto.class);
        assertEquals(1, returnedDtoList.size());
        InsectFamilyDto resultDto = returnedDtoList.get(0);
        assertEquals(created.getName(), resultDto.getName());
        assertTrue(resultDto.getId().isPresent());
        assertEquals(created.getId(), resultDto.getId().get());
        assertTrue(resultDto.getGbifId().isPresent());
        assertEquals(created.getGbifId(), resultDto.getGbifId().get());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void searchInsectFamilies__query_partial_match_ignorecase() throws Exception {
        InsectFamily created = createNewInsectFamily();

        MvcResult mvcResult = mockMvc.perform(get(INSECT_FAMILIES_PATH_SEARCH + "?query=test"))
            .andExpect(status().isOk())
            .andReturn();

        List<InsectFamilyDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, InsectFamilyDto.class);
        assertEquals(1, returnedDtoList.size());
        InsectFamilyDto resultDto = returnedDtoList.get(0);
        assertEquals(created.getName(), resultDto.getName());
        assertTrue(resultDto.getId().isPresent());
        assertEquals(created.getId(), resultDto.getId().get());
        assertTrue(resultDto.getGbifId().isPresent());
        assertEquals(created.getGbifId(), resultDto.getGbifId().get());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getInsectFamily() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(INSECT_FAMILIES_PATH_SINGLE))
            .andExpect(status().isOk())
            .andReturn();

        InsectFamilyDto resultDto = testHelperService.convertResponseBodyToDto(mvcResult, InsectFamilyDto.class);
        assertEquals(1L, resultDto.getId().get());
    }

}
