package org.kinsecta.webapp.api.controller;

import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.TestHelperService;
import org.kinsecta.webapp.api.model.dto.GbifResponseItem;
import org.kinsecta.webapp.api.model.entities.InsectFamily;
import org.kinsecta.webapp.api.model.entities.InsectGenus;
import org.kinsecta.webapp.api.model.entities.InsectOrder;
import org.kinsecta.webapp.api.model.entities.InsectSpecies;
import org.kinsecta.webapp.api.service.GbifApiService;
import org.kinsecta.webapp.api.service.InsectSpeciesService;
import org.kinsecta.webapp.api.v1.model.GbifInsectDto;
import org.kinsecta.webapp.api.v1.model.TaxonomyLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("it")
@SpringBootTest
@AutoConfigureMockMvc
class GbifInsectControllerIT {

    @SpyBean
    private GbifApiService mockGbifApiService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TestHelperService testHelperService;

    @Autowired
    private InsectSpeciesService insectSpeciesService;

    private final Long gbifId = 8293292L;

    private static final String GBIF_INSECTS_PATH_SINGLE = "/gbif_insects/%s";


    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.rossiar")
    void getGbifInsect() throws Exception {
        GbifResponseItem gbifResponseItem = createGbifResponseItemForId8293292();
        doReturn(gbifResponseItem).when(mockGbifApiService).getGbifResponseItemForGbifId(gbifId);

        MvcResult mvcResult = mockMvc.perform(get(String.format(GBIF_INSECTS_PATH_SINGLE, gbifId)))
            .andExpect(status().isOk())
            .andReturn();

        GbifInsectDto gbifInsectDto = testHelperService.convertResponseBodyToDto(mvcResult, GbifInsectDto.class);
        assertEquals(gbifId, gbifInsectDto.getGbifId());
        assertEquals("Titanus Giganteus", gbifInsectDto.getName());
        assertEquals(TaxonomyLevel.SPECIES, gbifInsectDto.getTaxonomyLevel());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.rossiar")
    void postGbifInsect() throws Exception {
        GbifResponseItem gbifResponseItem = createGbifResponseItemForId8293292();
        doReturn(gbifResponseItem).when(mockGbifApiService).getGbifResponseItemForGbifId(gbifId);

        MvcResult mvcResult = mockMvc.perform(post(String.format(GBIF_INSECTS_PATH_SINGLE, gbifId)))
            .andExpect(status().isOk())
            .andReturn();

        GbifInsectDto gbifInsectDto = testHelperService.convertResponseBodyToDto(mvcResult, GbifInsectDto.class);
        assertEquals(gbifId, gbifInsectDto.getGbifId());
        assertEquals("Titanus Giganteus", gbifInsectDto.getName());
        assertEquals(TaxonomyLevel.SPECIES, gbifInsectDto.getTaxonomyLevel());

        InsectSpecies storedInsectSpecies = insectSpeciesService.getInsectSpeciesByGbifId(gbifId);
        assertNotNull(storedInsectSpecies);
        assertEquals(gbifResponseItem.getSpecies(), storedInsectSpecies.getName());
        assertEquals(gbifResponseItem.getSpeciesKey(), storedInsectSpecies.getGbifId());
        InsectGenus insectGenus = storedInsectSpecies.getGenus();
        assertNotNull(insectGenus);
        assertEquals(gbifResponseItem.getGenus(), insectGenus.getName());
        assertEquals(gbifResponseItem.getGenusKey(), insectGenus.getGbifId());
        InsectFamily insectFamily = insectGenus.getFamily();
        assertNotNull(insectFamily);
        assertEquals(gbifResponseItem.getFamily(), insectFamily.getName());
        assertEquals(gbifResponseItem.getFamilyKey(), insectFamily.getGbifId());
        InsectOrder insectOrder = insectFamily.getOrder();
        assertNotNull(insectOrder);
        assertEquals(gbifResponseItem.getOrder(), insectOrder.getName());
        assertEquals(gbifResponseItem.getOrderKey(), insectOrder.getGbifId());
    }

    private GbifResponseItem createGbifResponseItemForId8293292() {
        GbifResponseItem gbifResponseItem = new GbifResponseItem();
        gbifResponseItem.setKey(8293292L);
        gbifResponseItem.setNubKey(8293292L);
        gbifResponseItem.setCanonicalName("Titanus Giganteus");
        gbifResponseItem.setOrder("Coleoptera");
        gbifResponseItem.setFamily("Cerambycidae");
        gbifResponseItem.setGenus("Titanus");
        gbifResponseItem.setSpecies("Titanus giganteus");
        gbifResponseItem.setOrderKey(1470L);
        gbifResponseItem.setFamilyKey(5602L);
        gbifResponseItem.setGenusKey(1133874L);
        gbifResponseItem.setSpeciesKey(8293292L);
        gbifResponseItem.setRank("SPECIES");
        gbifResponseItem.setSynonym(false);
        return gbifResponseItem;
    }

}
