package org.kinsecta.webapp.api.security;

import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.AlgorithmMismatchException;
import com.auth0.jwt.exceptions.InvalidClaimException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import nl.altindag.log.LogCaptor;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.model.entities.UserRole;
import org.kinsecta.webapp.api.model.entities.UserStatus;
import org.kinsecta.webapp.api.service.LoginAttemptService;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
class JwtTokenServiceTest {

    String jwtSecret = "UTxK%[VOWdR{aso#'sm3&G8IwK(f|ZYy?Z|$r`l=>L[g0Y<Ps&XC9Z";
    String modifiedJwtSecret = "y?Z|$r`l=>L[gVOWdY<Ps&UTxK%[G8Iaso#'sm3&0XC9ZwK(f|ZYR{";
    User user = new User();
    User modifiedUser = new User();

    String username = "my_username";

    UserDetailsImpl userDetails;
    UserDetailsImpl modifiedUserDetails;

    MockHttpServletResponse response = new MockHttpServletResponse();

    LogCaptor logCaptor = LogCaptor.forClass(JwtTokenService.class);

    // must be initialized and not use @Mock otherwise the @Spy on the JwtTokenService class won't work
    UserDetailsServiceImpl userDetailsService = mock(UserDetailsServiceImpl.class);

    @Spy
    JwtTokenService jwtTokenService = new JwtTokenService(userDetailsService);

    @Mock
    LoginAttemptService loginAttemptService;


    @BeforeAll
    private void beforeAll() {
        // Create valid User
        user.setId(1L);
        user.setUsername(username);
        user.setRole(UserRole.TEAM_MEMBER);
        user.setStatus(UserStatus.ACTIVE);
        user.setSecurityDetailsModified(LocalDateTime.now().minus(2, ChronoUnit.HOURS));
        userDetails = spy(new UserDetailsImpl(loginAttemptService));
        userDetails.setUser(user);

        // Create valid User with updated Security Details
        modifiedUser.setId(user.getId());
        modifiedUser.setUsername(username);
        modifiedUser.setRole(UserRole.DATA_COLLECTOR);
        modifiedUser.setSecurityDetailsModified(LocalDateTime.now().minus(15, ChronoUnit.MINUTES));
        modifiedUserDetails = new UserDetailsImpl(loginAttemptService);
        modifiedUserDetails.setUser(modifiedUser);
    }

    private void setJwtAlgorithm(Algorithm algorithm) {
        ReflectionTestUtils.setField(jwtTokenService, "algorithm", algorithm);
    }

    private void setJwtExpirationInMs(long jwtExpirationMs) {
        ReflectionTestUtils.setField(jwtTokenService, "jwtExpirationMs", jwtExpirationMs);
    }

    /**
     * This method is run before each test to reset overridden properties and fields from previous tests
     */
    @BeforeEach
    private void beforeEach() {
        // Set private fields with Reflection
        setJwtAlgorithm(Algorithm.HMAC512(jwtSecret));
        setJwtExpirationInMs(3600000L);
    }


    @Test
    void createJwtToken__valid() {
        assertTrue(jwtTokenService.createJwtToken(userDetails).length() > 200);
    }


    @Test
    void createJwtToken__invalid() {
        // Set an invalid JWT Algorithm
        setJwtAlgorithm(null);
        assertThrows(IllegalArgumentException.class, () -> jwtTokenService.createJwtToken(userDetails));
    }

    @Test
    void createJwtTokenAndAddToResponse__valid() {
        jwtTokenService.createJwtTokenAndAddToResponse(userDetails, response);
        String authHeader = response.getHeader(HttpHeaders.AUTHORIZATION);
        assertNotNull(authHeader);
        assertTrue(authHeader.length() > 200);
    }

    @Test
    void createJwtTokenAndReturnInAuthorizationHeader__valid() {
        HttpHeaders httpHeaders = jwtTokenService.createJwtTokenAndReturnInAuthorizationHeader(user);
        List<String> allAuthHeaders = httpHeaders.get(HttpHeaders.AUTHORIZATION);
        assertEquals(1, allAuthHeaders.size());
        String authHeader = allAuthHeaders.get(0);
        assertNotNull(authHeader);
        assertTrue(authHeader.length() > 200);
    }

    @Test
    void verifyJwt__valid() {
        String jwt = jwtTokenService.createJwtToken(user);
        assertDoesNotThrow(() -> jwtTokenService.verifyJwt(jwt));
    }

    @Test
    void verifyJwtWithClaims__valid() {
        String jwt = jwtTokenService.createJwtToken(user);
        assertDoesNotThrow(() -> jwtTokenService.verifyJwtWithClaims(jwt, userDetails));
    }

    @Test
    void verifyJwtWithClaims__invalid_changedClaims() {
        String jwt = jwtTokenService.createJwtToken(user);
        assertThrows(InvalidClaimException.class, () -> jwtTokenService.verifyJwtWithClaims(jwt, modifiedUserDetails));
    }

    @Test
    void verifyJwtWithClaims__invalid_changedJwtSecret() {
        // Create JWT with old secret
        String jwt = jwtTokenService.createJwtToken(user);
        // Change secret
        setJwtAlgorithm(Algorithm.HMAC512(modifiedJwtSecret));
        // Validate old JWT
        assertThrows(SignatureVerificationException.class, () -> jwtTokenService.verifyJwtWithClaims(jwt, userDetails));
    }

    @Test
    void verifyJwtWithClaims__invalid_changedAlgorithm() {
        // Create JWT with old secret
        String jwt = jwtTokenService.createJwtToken(user);
        // Change algorithm
        setJwtAlgorithm(Algorithm.HMAC256(jwtSecret));
        // Validate old JWT
        assertThrows(AlgorithmMismatchException.class, () -> jwtTokenService.verifyJwtWithClaims(jwt, userDetails));
    }

    @Test
    void verifyJwtWithClaims__invalid_tokenExpired() throws InterruptedException {
        // create a token which only lasts 1s
        setJwtExpirationInMs(1000L);
        String jwt = jwtTokenService.createJwtToken(user);
        // sleep 3 seconds to wait for token expiration
        TimeUnit.SECONDS.sleep(3);
        // Validate old JWT
        assertThrows(TokenExpiredException.class, () -> jwtTokenService.verifyJwtWithClaims(jwt, userDetails));
    }

    @Test
    void verifyDecodedJwtWithClaims__valid() {
        String jwt = jwtTokenService.createJwtToken(user);
        DecodedJWT decodedJWT = jwtTokenService.verifyJwt(jwt);
        assertDoesNotThrow(() -> jwtTokenService.verifyDecodedJwtWithClaims(decodedJWT, userDetails));
    }

    @Test
    void verifyJwtAndGetAuthentication__validJwt() {
        String encodedJwt = "alskdjc8ew5nvdi89345gfvwe943vewkrf0934tmve";
        DecodedJWT decodedJWT = mock(DecodedJWT.class);
        Collection<? extends GrantedAuthority> grantedAuthorities = List.of(new SimpleGrantedAuthority(UserRole.ADMIN.getValue()));

        doReturn(decodedJWT).when(jwtTokenService).verifyJwt(encodedJwt);
        doReturn(username).when(decodedJWT).getSubject();
        when(userDetailsService.loadUserByUsername(username)).thenReturn(userDetails);
        when(userDetails.isAccountNonLocked()).thenReturn(true);
        doReturn(decodedJWT).when(jwtTokenService).verifyDecodedJwtWithClaims(decodedJWT, userDetails);
        when(userDetails.getUsername()).thenReturn(username);
        doReturn(grantedAuthorities).when(userDetails).getAuthorities();

        UsernamePasswordAuthenticationToken authentication = jwtTokenService.verifyJwtAndGetAuthentication(encodedJwt);

        assertInstanceOf(UserDetailsImpl.class, authentication.getPrincipal());
        UserDetailsImpl userDetailsImpl = (UserDetailsImpl) authentication.getPrincipal();
        assertEquals(username, userDetailsImpl.getUsername());
        assertNotNull(userDetailsImpl.getUser());
        assertNotNull(userDetailsImpl.getUser().getId());
        assertNull(authentication.getCredentials());
        assertEquals(grantedAuthorities, authentication.getAuthorities());
    }

    @Test
    void verifyJwtAndGetAuthentication__validJwtUserAccountLockedOrUserInactive() {
        String encodedJwt = "alskdjc8ew5nvdi89345gfvwe943vewkrf0934tmve";
        DecodedJWT decodedJWT = mock(DecodedJWT.class);
        Collection<? extends GrantedAuthority> grantedAuthorities = List.of(new SimpleGrantedAuthority(UserRole.ADMIN.getValue()));

        doReturn(decodedJWT).when(jwtTokenService).verifyJwt(encodedJwt);
        when(decodedJWT.getSubject()).thenReturn(username);
        when(userDetailsService.loadUserByUsername(username)).thenReturn(userDetails);
        when(userDetails.isEnabled()).thenReturn(false);

        assertThrows(DisabledException.class, () -> jwtTokenService.verifyJwtAndGetAuthentication(encodedJwt));
        assertTrue(logCaptor.getLogs().contains(String.format("Request with valid JWT was blocked because the UserStatus of the decoded user '%s' is no longer 'ACTIVE'.", username)));

        when(userDetails.isEnabled()).thenReturn(true);
        when(userDetails.isAccountNonLocked()).thenReturn(false);

        assertThrows(LockedException.class, () -> jwtTokenService.verifyJwtAndGetAuthentication(encodedJwt));
        assertTrue(logCaptor.getLogs().contains(String.format("Request with valid JWT was blocked because the decoded user '%s' was found to be blocked for too many failed login attempts.", username)));
    }

}
