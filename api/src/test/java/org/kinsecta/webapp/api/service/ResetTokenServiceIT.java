package org.kinsecta.webapp.api.service;

import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.exception.unchecked.WrongArgumentException;
import org.kinsecta.webapp.api.model.entities.ResetToken;
import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.model.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


@ActiveProfiles("it")
@SpringBootTest
class ResetTokenServiceIT {

    @Autowired
    private ResetTokenService resetTokenService;

    @Autowired
    private UserRepository userRepository;

    @Test
    @Transactional
    void createResetToken() {
        User user = userRepository.getById(1L);
        ResetToken resetToken = resetTokenService.createResetToken(user);

        assertNotNull(resetToken.getToken());
        // Assert resetToken expiration date is before now + 24h (+1 minute for flaky test uncertainties)
        assertTrue(resetToken.getExpires().isBefore(LocalDateTime.now().plusHours(24).plusMinutes(1)));
        assertEquals("t.fischer", resetToken.getUser().getUsername());
    }

    @Test
    void createNewToken() {
        assertNotNull(ResetTokenService.createNewToken());
        assertEquals(64, ResetTokenService.createNewToken().length());
    }

    @Test
    @Transactional
    void validateResetToken__valid() {
        User user = new User();
        user.setId(1L);
        ResetToken resetToken = resetTokenService.createResetToken(user);
        Optional<ResetToken> optionalResetToken = Optional.of(resetToken);
        assertDoesNotThrow(() -> ResetTokenService.validateResetToken(optionalResetToken));
    }

    @Test
    void validateResetToken__empty() {
        Optional<ResetToken> optionalResetToken = Optional.empty();
        assertThrows(WrongArgumentException.class, () -> ResetTokenService.validateResetToken(optionalResetToken));
    }

    @Test
    @Transactional
    void validateResetToken__expired() {
        User user = new User();
        user.setId(1L);
        ResetToken resetToken = resetTokenService.createResetToken(user);
        resetToken.setExpires(LocalDateTime.now().minusHours(1));
        Optional<ResetToken> optionalResetToken = Optional.of(resetToken);
        assertThrows(WrongArgumentException.class, () -> ResetTokenService.validateResetToken(optionalResetToken));
    }

}
