package org.kinsecta.webapp.api.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.altindag.log.LogCaptor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.kinsecta.webapp.api.config.ObjectMapperConfig;
import org.kinsecta.webapp.api.model.entities.LoginAttempt;
import org.kinsecta.webapp.api.service.LoginAttemptService;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.security.core.AuthenticationException;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.kinsecta.webapp.api.security.JwtAuthenticationFailureHandler.ATTRIBUTE_AUTH_USERNAME;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class JwtAuthenticationFailureHandlerTest {

    LogCaptor logCaptor = LogCaptor.forClass(JwtAuthenticationFailureHandler.class);

    String username = "my_username";

    @Mock
    LoginAttemptService loginAttemptService;

    ObjectMapper objectMapper = new ObjectMapperConfig().objectMapper();

    @Mock
    LoginAttempt loginAttempt;

    MockHttpServletRequest servletRequest = new MockHttpServletRequest();
    MockHttpServletResponse servletResponse = new MockHttpServletResponse();
    @Spy
    MockHttpSession session = new MockHttpSession();

    @Captor
    ArgumentCaptor<Map<String, Object>> responseCaptor;


    private JwtAuthenticationFailureHandler setupSpyJwtAuthenticationFailureHandler() {
        return spy(new JwtAuthenticationFailureHandler(loginAttemptService, objectMapper));
    }


    @Test
    void onAuthenticationFailure__disabledException() throws IOException {
        DisabledException exception = new DisabledException("mock message");
        mockOnAuthenticationFailureWithExceptionAndExpectations(exception, 3, JwtAuthenticationFailureHandler.MESSAGE_USER_DISABLED);
    }

    @Test
    void onAuthenticationFailure__lockedException() throws IOException {
        LockedException exception = new LockedException("mock message");
        mockOnAuthenticationFailureWithExceptionAndExpectations(exception, 3, JwtAuthenticationFailureHandler.MESSAGE_USER_LOCKED);
    }

    @Test
    void onAuthenticationFailure__unhandledAuthException() throws IOException {
        ProviderNotFoundException exception = new ProviderNotFoundException("mock message");
        mockOnAuthenticationFailureWithExceptionAndExpectations(exception, 3, JwtAuthenticationFailureHandler.MESSAGE_AUTH_FAILED);
        logCaptor.getWarnLogs().forEach(System.out::println);
        assertTrue(logCaptor.getWarnLogs().contains(String.format("Authentication failed with unhandled AuthenticationException of type '%s' with message: %s", exception.getClass().getSimpleName(), exception.getMessage())));
    }

    @Test
    void onAuthenticationFailure__badCredentialsException_OneAttemptLeft() throws IOException {
        BadCredentialsException exception = new BadCredentialsException("mock message");
        int remainingLoginAttempts = 1;
        String expectedMessage = JwtAuthenticationFailureHandler.MESSAGE_BAD_CREDENTIALS;
        Map<String, Object> capturedErrorAttributes = mockOnAuthenticationFailureWithExceptionAndExpectations(exception, remainingLoginAttempts, expectedMessage);

        // if remaining login attempts > 0 assert a 'remainingLoginAttempts' property in the error attributes
        assertEquals(2, capturedErrorAttributes.size());
        assertTrue(capturedErrorAttributes.containsKey("remainingLoginAttempts"));
        assertEquals(remainingLoginAttempts, capturedErrorAttributes.get("remainingLoginAttempts"));
    }

    @Test
    void onAuthenticationFailure__badCredentialsException_NoAttemptsLeft() throws IOException {
        BadCredentialsException exception = new BadCredentialsException("mock message");
        int remainingLoginAttempts = 0;
        String expectedMessage = JwtAuthenticationFailureHandler.MESSAGE_BAD_CREDENTIALS_NOW_LOCKED;
        Map<String, Object> capturedErrorAttributes = mockOnAuthenticationFailureWithExceptionAndExpectations(exception, remainingLoginAttempts, expectedMessage);

        // if remaining login attempts = 0, the 'remainingLoginAttempts' property in the error attributes must not be available
        assertEquals(1, capturedErrorAttributes.size());
    }


    private Map<String, Object> mockOnAuthenticationFailureWithExceptionAndExpectations(AuthenticationException exception, int remainingLoginAttempts, String expectedMessage) throws IOException {
        servletRequest.setSession(session);
        JwtAuthenticationFailureHandler failureHandler = setupSpyJwtAuthenticationFailureHandler();

        // prerequisites
        lenient().when(session.getAttribute(ATTRIBUTE_AUTH_USERNAME)).thenReturn(username);
        lenient().when(loginAttemptService.loginFailed(username)).thenReturn(Optional.of(loginAttempt));
        lenient().when(loginAttemptService.getRemainingAttempts(loginAttempt)).thenReturn(remainingLoginAttempts);

        // run test
        failureHandler.onAuthenticationFailure(servletRequest, servletResponse, exception);

        assertEquals(HttpStatus.FORBIDDEN.value(), servletResponse.getStatus());

        // Retrieve argument passed to convertObjectToJson() method (= errorAttributes)
        verify(failureHandler).convertObjectToJson(responseCaptor.capture());
        Map<String, Object> capturedErrorAttributes = responseCaptor.getValue();

        assertTrue(capturedErrorAttributes.size() > 0);
        assertTrue(capturedErrorAttributes.containsKey("message"));
        assertEquals(expectedMessage, capturedErrorAttributes.get("message"));

        return capturedErrorAttributes;
    }

}
