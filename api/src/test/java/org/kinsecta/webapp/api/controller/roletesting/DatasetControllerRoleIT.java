package org.kinsecta.webapp.api.controller.roletesting;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.TestHelperService;
import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.model.entities.*;
import org.kinsecta.webapp.api.model.mapping.ClassificationDtoMapper;
import org.kinsecta.webapp.api.model.mapping.DatasetImageDtoMapper;
import org.kinsecta.webapp.api.model.mapping.DatasetTagDtoMapper;
import org.kinsecta.webapp.api.model.repositories.DatasetImageRepository;
import org.kinsecta.webapp.api.service.ClassificationService;
import org.kinsecta.webapp.api.service.DatasetService;
import org.kinsecta.webapp.api.service.DatasetTagService;
import org.kinsecta.webapp.api.service.S3ObjectService;
import org.kinsecta.webapp.api.v1.model.*;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("it")
@SpringBootTest
@AutoConfigureMockMvc
public class DatasetControllerRoleIT {

    private static final String DATASETS_PATH = "/datasets";

    private static final String DATASETS_PATH_VIEWS = DATASETS_PATH + "/_views";
    private static final String DATASETS_PATH_VIEWS_TABLE = DATASETS_PATH_VIEWS + "/table";
    private static final String DATASETS_PATH_VIEWS_ID_ONLY = DATASETS_PATH_VIEWS + "/id_only";

    private static final String DATASETS_PATH_SINGLE = DATASETS_PATH + "/1";

    private static final String DATASETS_PATH_SINGLE_DATASET_TAGS = DATASETS_PATH_SINGLE + "/dataset_tags";

    private static final String DATASETS_PATH_SINGLE_CLASSIFICATIONS = DATASETS_PATH_SINGLE + "/classifications";
    private static final String DATASETS_PATH_SINGLE_CLASSIFICATIONS_SINGLE = DATASETS_PATH_SINGLE_CLASSIFICATIONS + "/1";

    private static final String DATASETS_PATH_SINGLE_SIZE_INSECT = DATASETS_PATH_SINGLE + "/size_insect";

    private static final String DATASETS_PATH_SINGLE_VIDEO_S3 = DATASETS_PATH_SINGLE + "/video_s3_object";
    private static final String DATASETS_PATH_SINGLE_VIDEO_S3_DOWNLOAD = DATASETS_PATH_SINGLE_VIDEO_S3 + "/download";

    private static final String DATASETS_PATH_SINGLE_DATASET_IMAGES = DATASETS_PATH_SINGLE + "/dataset_images";
    private static final String DATASETS_PATH_SINGLE_DATASET_IMAGES_SINGLE = DATASETS_PATH_SINGLE_DATASET_IMAGES + "/1";


    private static final String DATASETS_PATH_SINGLE_WINGBEAT = DATASETS_PATH_SINGLE + "/measurement_wingbeat";
    private static final String DATASETS_PATH_SINGLE_WINGBEAT_WAV = DATASETS_PATH_SINGLE_WINGBEAT + "/wav_s3_object";
    private static final String DATASETS_PATH_SINGLE_WINGBEAT_WAV_DOWNLOAD = DATASETS_PATH_SINGLE_WINGBEAT_WAV + "/download";
    private static final String DATASETS_PATH_SINGLE_WINGBEAT_IMAGE = DATASETS_PATH_SINGLE_WINGBEAT + "/image_s3_object";
    private static final String DATASETS_PATH_SINGLE_WINGBEAT_IMAGE_DOWNLOAD = DATASETS_PATH_SINGLE_WINGBEAT_IMAGE + "/download";
    public static final int STATUS_CODE_403_FORBIDDEN = 403;
    public static final int STATUS_CODE_200_OK = 200;

    private final String resourcePath = "src/test/resources";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TestHelperService testHelperService;

    @Autowired
    private S3ObjectService s3ObjectService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private DatasetService datasetService;

    @Autowired
    private DatasetTagService datasetTagService;

    @Autowired
    private DatasetImageRepository datasetImageRepository;

    @Autowired
    private ClassificationService classificationService;

    @Autowired
    private ClassificationDtoMapper classificationDtoMapper;

    @Autowired
    private DatasetImageDtoMapper datasetImageDtoMapper;

    @Autowired
    private DatasetTagDtoMapper datasetTagDtoMapper;


    // -------- getAllDatasets -----------------------------------------------------------------------------------------

    MvcResult getAllDatasets(int statusCode) throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH))
            .andExpect(status().is(statusCode))
            .andReturn();
        return mvcResult;
    }

    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getAllDatasets__Admin() throws Exception {
        MvcResult mvcResult = getAllDatasets(STATUS_CODE_200_OK);
        List<DatasetDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, DatasetDto.class);
        assertEquals(2, returnedDtoList.size());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllDatasets__TeamMember() throws Exception {
        MvcResult mvcResult = getAllDatasets(STATUS_CODE_200_OK);
        List<DatasetDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, DatasetDto.class);
        assertEquals(2, returnedDtoList.size());
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void getAllDatasets__DataCollector() throws Exception {
        getAllDatasets(STATUS_CODE_403_FORBIDDEN); // is Forbidden
    }

    @Test
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void getAllDatasets__DataScientist() throws Exception {
        getAllDatasets(STATUS_CODE_403_FORBIDDEN); // is Forbidden
    }

    @Test
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void getAllDatasets__DataRecipient() throws Exception {
        getAllDatasets(STATUS_CODE_403_FORBIDDEN); // is Forbidden
    }

    // -------- getDataset ---------------------------------------------------------------------------------------------

    void getDataset() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH_SINGLE))
            .andExpect(status().isOk())
            .andReturn();

        DatasetDto datasetDto = testHelperService.convertResponseBodyToDto(mvcResult, DatasetDto.class);
        assertEquals(1L, datasetDto.getId());
    }


    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getDataset__Admin() throws Exception {
        getDataset();
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getDataset__TeamMember() throws Exception {
        getDataset();
    }


    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void getDataset__DataCollector() throws Exception {
        getDataset();
    }

    @Test
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void getDataset__DataScientist() throws Exception {
        getDataset();
    }

    @Test
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void getDataset__DataRecipient() throws Exception {
        getDataset();
    }


    // -------- deleteDataset ------------------------------------------------------------------------------------------

    void deleteDataset(int statusCode) throws Exception {
        mockMvc.perform(delete(DATASETS_PATH_SINGLE))
            .andExpect(status().is(statusCode));
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void deleteDataset__Admin() throws Exception {
        deleteDataset(STATUS_CODE_200_OK);

        // assert that Dataset has been deleted successfully and does no longer exist in the DB
        assertThrows(NotFoundException.class, () -> datasetService.getDataset(1L));
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void deleteDataset__TeamMember() throws Exception {
        deleteDataset(STATUS_CODE_200_OK);

        // assert that Dataset has been deleted successfully and does no longer exist in the DB
        assertThrows(NotFoundException.class, () -> datasetService.getDataset(1L));
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void deleteDataset__DataCollector() throws Exception {
        deleteDataset(STATUS_CODE_403_FORBIDDEN);
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void deleteDataset__DataScientist() throws Exception {
        deleteDataset(STATUS_CODE_403_FORBIDDEN);
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void deleteDataset__DataRecipient() throws Exception {
        deleteDataset(STATUS_CODE_403_FORBIDDEN);
    }

    // -------- getAllDatasetsTableView --------------------------------------------------------------------------------

    void getAllDatasetsTableView() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH_VIEWS_TABLE)).andExpect(status().isOk()).andReturn();
        List<DatasetTableViewDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, DatasetTableViewDto.class);
        assertEquals(2, returnedDtoList.size());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getAllDatasetsTableView__Admin() throws Exception {
        getAllDatasetsTableView();
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllDatasetsTableView__TeamMember() throws Exception {
        getAllDatasetsTableView();
    }


    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void getAllDatasetsTableView_DataCollector() throws Exception {
        getAllDatasetsTableView();
    }

    @Test
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void getAllDatasetsTableView__DataScientist() throws Exception {
        getAllDatasetsTableView();
    }

    @Test
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void getAllDatasetsTableView__DataRecipient() throws Exception {
        getAllDatasetsTableView();
    }


    // -------- getAllDatasetsIdOnlyView --------------------------------------------------------------------------------

    void getAllDatasetsIdOnlyView() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH_VIEWS_ID_ONLY)
                .param("startDate", "2022-01-29")
                .param("endDate", "2022-01-29")
                .param("sensorId", "1"))
            .andExpect(status().isOk())
            .andReturn();

        IdListDto idListDto = testHelperService.convertResponseBodyToDto(mvcResult, IdListDto.class);
        assertEquals(2, idListDto.getIds().size());
        assertEquals(1L, idListDto.getIds().get(0));
        assertEquals(2L, idListDto.getIds().get(1));
    }

    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getAllDatasetsIdOnlyView__Admin() throws Exception {
        getAllDatasetsIdOnlyView();
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllDatasetsIdOnlyView__TeamMember() throws Exception {
        getAllDatasetsIdOnlyView();
    }


    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void getAllDatasetsIdOnlyView_DataCollector() throws Exception {
        getAllDatasetsIdOnlyView();
    }

    @Test
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void getAllDatasetsIdOnlyView__DataScientist() throws Exception {
        getAllDatasetsIdOnlyView();
    }

    @Test
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void getAllDatasetsIdOnlyView__DataRecipient() throws Exception {
        getAllDatasetsIdOnlyView();
    }


    // -------- getAllDatasetTagsForDataset ----------------------------------------------------------------------------

    void getAllDatasetTagsForDataset() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH_SINGLE_DATASET_TAGS))
            .andExpect(status().isOk())
            .andReturn();

        List<DatasetTagDto> datasetTagList = testHelperService.convertResponseBodyToDtoList(mvcResult, DatasetTagDto.class);
        assertEquals(2, datasetTagList.size());
    }


    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getAllDatasetTagsForDataset__Admin() throws Exception {
        getAllDatasetTagsForDataset();
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllDatasetTagsForDataset__TeamMember() throws Exception {
        getAllDatasetTagsForDataset();
    }


    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void getAllDatasetTagsForDataset_DataCollector() throws Exception {
        getAllDatasetTagsForDataset();
    }

    @Test
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void getAllDatasetTagsForDataset__DataScientist() throws Exception {
        getAllDatasetTagsForDataset();
    }

    @Test
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void getAllDatasetTagsForDataset__DataRecipient() throws Exception {
        getAllDatasetTagsForDataset();
    }


    // -------- linkDatasetTagToDataset --------------------------------------------------------------------------------

    void linkDatasetTagToDataset() throws Exception {
        DatasetTag datasetTag = new DatasetTag();
        String tagName = "My Tag";
        datasetTag.setName(tagName);
        DatasetTag createdDatasetTag = datasetTagService.createDatasetTag(datasetTag);
        mockMvc.perform(put(DATASETS_PATH_SINGLE_DATASET_TAGS + "/" + createdDatasetTag.getId()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(createdDatasetTag.getId()))
            .andExpect(jsonPath("$.auto_tag").value(false))
            .andExpect(jsonPath("$.name").value(tagName));

        Dataset datasetAfter = datasetService.getDataset(1L);
        Set<DatasetTag> datasetTags = datasetAfter.getDatasetTags();
        assertTrue(datasetTags.contains(createdDatasetTag));
    }

    void linkDatasetTagToDataset__andExpectForbidden() throws Exception {
        DatasetTag datasetTag = new DatasetTag();
        String tagName = "My Tag";
        datasetTag.setName(tagName);
        DatasetTag createdDatasetTag = datasetTagService.createDatasetTag(datasetTag);
        mockMvc.perform(put(DATASETS_PATH_SINGLE_DATASET_TAGS + "/" + createdDatasetTag.getId()))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void linkDatasetTagToDataset__Admin() throws Exception {
        linkDatasetTagToDataset();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void linkDatasetTagToDataset__TeamMember() throws Exception {
        linkDatasetTagToDataset();
    }


    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void linkDatasetTagToDataset_DataCollector() throws Exception {
        linkDatasetTagToDataset__andExpectForbidden();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void linkDatasetTagToDataset__DataScientist() throws Exception {
        linkDatasetTagToDataset__andExpectForbidden();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void linkDatasetTagToDataset__DataRecipient() throws Exception {
        linkDatasetTagToDataset__andExpectForbidden();
    }


    // -------- unlinkDatasetTagToDataset ------------------------------------------------------------------------------

    void unlinkDatasetTagFromDataset(int statusCode) throws Exception {
        DatasetTag datasetTag = new DatasetTag();
        String tagName = "My Tag";
        datasetTag.setName(tagName);
        DatasetTag createdDatasetTag = datasetService.createDatasetTagForDataset(1L, datasetTag);
        mockMvc.perform(delete(DATASETS_PATH_SINGLE_DATASET_TAGS + "/" + createdDatasetTag.getId()))
            .andExpect(status().is(statusCode));
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void unlinkDatasetTagFromDataset__Admin() throws Exception {
        unlinkDatasetTagFromDataset(STATUS_CODE_200_OK);
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void unlinkDatasetTagFromDataset__TeamMember() throws Exception {
        unlinkDatasetTagFromDataset(STATUS_CODE_200_OK);
    }


    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void unlinkDatasetTagFromDataset_DataCollector() throws Exception {
        unlinkDatasetTagFromDataset(STATUS_CODE_403_FORBIDDEN);
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void unlinkDatasetTagFromDataset__DataScientist() throws Exception {
        unlinkDatasetTagFromDataset(STATUS_CODE_403_FORBIDDEN);
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void unlinkDatasetTagFromDataset__DataRecipient() throws Exception {
        unlinkDatasetTagFromDataset(STATUS_CODE_403_FORBIDDEN);
    }


    // -------- createClassificationForDataset -------------------------------------------------------------------------
    void createClassificationForDataset() throws Exception {
        Classification classification = classificationService.getClassification(1L);
        ClassificationDto classificationDto = classificationDtoMapper.classificationToClassificationDto(classification);
        classificationDto.setId(JsonNullable.undefined());
        classificationDto.setDataset(JsonNullable.undefined());

        String contentString = objectMapper.writeValueAsString(classificationDto);
        mockMvc.perform(post(DATASETS_PATH_SINGLE_CLASSIFICATIONS)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(contentString))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.id").isNumber())
            .andExpect(jsonPath("$.dataset.id").value(1));
    }

    void createClassificationForDataset__andExpectForbidden() throws Exception {
        Classification classification = classificationService.getClassification(1L);
        ClassificationDto classificationDto = classificationDtoMapper.classificationToClassificationDto(classification);
        classificationDto.setId(JsonNullable.undefined());
        classificationDto.setDataset(JsonNullable.undefined());

        String contentString = objectMapper.writeValueAsString(classificationDto);
        mockMvc.perform(post(DATASETS_PATH_SINGLE_CLASSIFICATIONS)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(contentString))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void createClassificationForDataset__Admin() throws Exception {
        createClassificationForDataset();

    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void createClassificationForDataset__TeamMember() throws Exception {
        createClassificationForDataset();
    }


    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void createClassificationForDataset_DataCollector() throws Exception {
        createClassificationForDataset__andExpectForbidden();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void createClassificationForDataset__DataScientist() throws Exception {
        createClassificationForDataset__andExpectForbidden();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void createClassificationForDataset__DataRecipient() throws Exception {
        createClassificationForDataset__andExpectForbidden();
    }


    // -------- getAllClassificationsForDataset ------------------------------------------------------------------------

    void getAllClassificationsForDataset() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH_SINGLE_CLASSIFICATIONS))
            .andExpect(status().isOk())
            .andReturn();

        List<ClassificationDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, ClassificationDto.class);

        assertEquals(1, returnedDtoList.size());

        // #79 double check for duplicates
        Dataset dataset = datasetService.getDataset(1L);
        assertEquals(1, dataset.getClassifications().size());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getAllClassificationsForDataset__Admin() throws Exception {
        getAllClassificationsForDataset();

    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllClassificationsForDataset__TeamMember() throws Exception {
        getAllClassificationsForDataset();
    }


    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void getAllClassificationsForDataset_DataCollector() throws Exception {
        getAllClassificationsForDataset();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void getAllClassificationsForDataset__DataScientist() throws Exception {
        getAllClassificationsForDataset();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void getAllClassificationsForDataset__DataRecipient() throws Exception {
        getAllClassificationsForDataset();
    }


    // -------- linkClassificationToDataset__new_link ------------------------------------------------------------------

    void linkClassificationToDataset() throws Exception {
        mockMvc.perform(put(DATASETS_PATH_SINGLE_CLASSIFICATIONS + "/3")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.dataset.id").value(1));
    }

    void linkClassificationToDataset__andExpectForbidden() throws Exception {
        mockMvc.perform(put(DATASETS_PATH_SINGLE_CLASSIFICATIONS + "/3")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void linkClassificationToDataset__Admin() throws Exception {
        linkClassificationToDataset();

    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void linkClassificationToDataset__TeamMember() throws Exception {
        linkClassificationToDataset();
    }


    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void linkClassificationToDataset_DataCollector() throws Exception {
        linkClassificationToDataset__andExpectForbidden();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void linkClassificationToDataset__DataScientist() throws Exception {
        linkClassificationToDataset__andExpectForbidden();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void linkClassificationToDataset__DataRecipient() throws Exception {
        linkClassificationToDataset__andExpectForbidden();
    }


    // -------- unlinkClassificationFromDataset -----------------------------------------------------------------------


    void unlinkClassificationFromDataset(int statusCode) throws Exception {
        mockMvc.perform(delete(DATASETS_PATH_SINGLE_CLASSIFICATIONS_SINGLE))
            .andExpect(status().is(statusCode));
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void unlinkClassificationFromDataset__Admin() throws Exception {
        unlinkClassificationFromDataset(STATUS_CODE_200_OK);

        assertThrows(NotFoundException.class, () -> classificationService.getClassification(1L));
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void unlinkClassificationFromDataset__TeamMember() throws Exception {
        unlinkClassificationFromDataset(STATUS_CODE_200_OK);

        assertThrows(NotFoundException.class, () -> classificationService.getClassification(1L));
    }


    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void unlinkClassificationFromDataset_DataCollector() throws Exception {
        unlinkClassificationFromDataset(STATUS_CODE_403_FORBIDDEN);
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void unlinkClassificationFromDataset__DataScientist() throws Exception {
        unlinkClassificationFromDataset(STATUS_CODE_403_FORBIDDEN);
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void unlinkClassificationFromDataset__DataRecipient() throws Exception {
        unlinkClassificationFromDataset(STATUS_CODE_403_FORBIDDEN);
    }

    // ----- editSizeInsectOfDataset -----------------------------------------------------------------------------------

    void editSizeInsectOfDataset(int statusCode) throws Exception {
        InsectSizeDto insectSizeDto = new InsectSizeDto();
        insectSizeDto.setWidth(BigDecimal.valueOf(4.5));
        insectSizeDto.setLength(BigDecimal.valueOf(8.7));

        String contentString = objectMapper.writeValueAsString(insectSizeDto);
        mockMvc.perform(put(DATASETS_PATH_SINGLE_SIZE_INSECT)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(contentString))
            .andExpect(status().is(statusCode));
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void editSizeInsectOfDataset__Admin() throws Exception {
        editSizeInsectOfDataset(STATUS_CODE_200_OK);

        Dataset dataset = datasetService.getDataset(1L);
        assertEquals(BigDecimal.valueOf(4.5), dataset.getMeasurementSizeInsectWidth());
        assertEquals(BigDecimal.valueOf(8.7), dataset.getMeasurementSizeInsectLength());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void editSizeInsectOfDataset__TeamMember() throws Exception {
        editSizeInsectOfDataset(STATUS_CODE_200_OK);

        Dataset dataset = datasetService.getDataset(1L);
        assertEquals(BigDecimal.valueOf(4.5), dataset.getMeasurementSizeInsectWidth());
        assertEquals(BigDecimal.valueOf(8.7), dataset.getMeasurementSizeInsectLength());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void editSizeInsectOfDataset_DataCollector() throws Exception {
        editSizeInsectOfDataset(STATUS_CODE_403_FORBIDDEN);
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void editSizeInsectOfDataset__DataScientist() throws Exception {
        editSizeInsectOfDataset(STATUS_CODE_403_FORBIDDEN);
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void editSizeInsectOfDataset__DataRecipient() throws Exception {
        editSizeInsectOfDataset(STATUS_CODE_403_FORBIDDEN);
    }


    // ----- getVideoS3ObjectForDataset --------------------------------------------------------------------------------

    void getVideoS3ObjectForDataset() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH_SINGLE))
            .andExpect(status().isOk())
            .andReturn();

        DatasetDto datasetDto = testHelperService.convertResponseBodyToDto(mvcResult, DatasetDto.class);

        MvcResult s3MvcResult = mockMvc.perform(get(DATASETS_PATH_SINGLE_VIDEO_S3))
            .andExpect(status().isOk())
            .andReturn();

        S3ObjectDto videoS3ObjectDto = testHelperService.convertResponseBodyToDto(s3MvcResult, S3ObjectDto.class);

        assertEquals(videoS3ObjectDto.getId(), datasetDto.getVideoS3Object().get().getId());
        assertEquals("video/h264", videoS3ObjectDto.getMediaType());
        assertEquals("41d2cd7e-8103-11ec-893a-b827eb353b23.h264", videoS3ObjectDto.getFilename());
    }


    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getVideoS3ObjectForDataset__Admin() throws Exception {
        getVideoS3ObjectForDataset();
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getVideoS3ObjectForDataset__TeamMember() throws Exception {
        getVideoS3ObjectForDataset();
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void getVideoS3ObjectForDataset_DataCollector() throws Exception {
        getVideoS3ObjectForDataset();
    }

    @Test
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void getVideoS3ObjectForDataset__DataScientist() throws Exception {
        getVideoS3ObjectForDataset();
    }

    @Test
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void getVideoS3ObjectForDataset__DataRecipient() throws Exception {
        getVideoS3ObjectForDataset();
    }


    // ----- downloadVideoS3ObjectForDataset ---------------------------------------------------------------------------


    void downloadVideoS3ObjectForDataset() throws Exception {
        String filename = "video.mp4";
        String mediaType = "video/mp4";
        File inputFile = Paths.get(resourcePath, "s3", filename).toFile();

        // Update Dataset 1 (which per IT test data already has a video S3 Object attached), so we don't have to manually create a Dataset - which is a nightmare
        S3Object s3Object = s3ObjectService.storeFile(inputFile);
        Dataset dataset = datasetService.getDataset(1L);
        dataset.setVideoS3Object(s3Object);
        datasetService.save(dataset);

        testHelperService.fileDownloadTest(mockMvc, DATASETS_PATH_SINGLE_VIDEO_S3_DOWNLOAD, inputFile, mediaType, "inline", true);
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void downloadVideoS3ObjectForDataset__Admin() throws Exception {
        downloadVideoS3ObjectForDataset();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void downloadVideoS3ObjectForDataset__TeamMember() throws Exception {
        downloadVideoS3ObjectForDataset();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void downloadVideoS3ObjectForDataset_DataCollector() throws Exception {
        downloadVideoS3ObjectForDataset();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void downloadVideoS3ObjectForDataset__DataScientist() throws Exception {
        downloadVideoS3ObjectForDataset();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void downloadVideoS3ObjectForDataset__DataRecipient() throws Exception {
        downloadVideoS3ObjectForDataset();
    }


    // -------- getAllDatasetImagesForDataset --------------------------------------------------------------------------

    void getAllDatasetImagesForDataset() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH_SINGLE_DATASET_IMAGES))
            .andExpect(status().isOk())
            .andReturn();

        List<DatasetImageDto> imageList = testHelperService.convertResponseBodyToDtoList(mvcResult, DatasetImageDto.class);
        assertEquals(2, imageList.size());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getAllDatasetImagesForDataset__Admin() throws Exception {
        getAllDatasetImagesForDataset();
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllDatasetImagesForDataset__TeamMember() throws Exception {
        getAllDatasetImagesForDataset();
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void getAllDatasetImagesForDataset_DataCollector() throws Exception {
        getAllDatasetImagesForDataset();
    }

    @Test
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void getAllDatasetImagesForDataset__DataScientist() throws Exception {
        getAllDatasetImagesForDataset();
    }

    @Test
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void getAllDatasetImagesForDataset__DataRecipient() throws Exception {
        getAllDatasetImagesForDataset();
    }


    // -------- getDatasetImageForDataset ------------------------------------------------------------------------------

    MvcResult getDatasetImageForDataset(int statusCode) throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH_SINGLE))
            .andExpect(status().isOk())
            .andReturn();

        DatasetDto datasetDto = testHelperService.convertResponseBodyToDto(mvcResult, DatasetDto.class);

        MvcResult s3MvcResult = mockMvc.perform(get(DATASETS_PATH_SINGLE_DATASET_IMAGES_SINGLE))
            .andExpect(status().is(statusCode))
            .andReturn();

        return s3MvcResult;
    }

    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getDatasetImageForDataset__Admin() throws Exception {
        MvcResult s3MvcResult = getDatasetImageForDataset(STATUS_CODE_200_OK);


        S3ObjectDto imageS3Object = testHelperService.convertResponseBodyToDto(s3MvcResult, S3ObjectDto.class);

        assertEquals("image/jpeg", imageS3Object.getMediaType());
        assertEquals(3, imageS3Object.getId());
        assertEquals("4394aa1a-8103-11ec-893a-b827eb353b23.png", imageS3Object.getFilename());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getDatasetImageForDataset__TeamMember() throws Exception {
        MvcResult s3MvcResult = getDatasetImageForDataset(STATUS_CODE_200_OK);


        S3ObjectDto imageS3Object = testHelperService.convertResponseBodyToDto(s3MvcResult, S3ObjectDto.class);

        assertEquals("image/jpeg", imageS3Object.getMediaType());
        assertEquals(3, imageS3Object.getId());
        assertEquals("4394aa1a-8103-11ec-893a-b827eb353b23.png", imageS3Object.getFilename());
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void getDatasetImageForDataset_DataCollector() throws Exception {
        getDatasetImageForDataset(STATUS_CODE_403_FORBIDDEN);
    }

    @Test
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void getDatasetImageForDataset__DataScientist() throws Exception {
        getDatasetImageForDataset(STATUS_CODE_403_FORBIDDEN);
    }

    @Test
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void getDatasetImageForDataset__DataRecipient() throws Exception {
        getDatasetImageForDataset(STATUS_CODE_403_FORBIDDEN);
    }


    // -------- downloadDatasetImageForDataset -------------------------------------------------------------------------

    void downloadDatasetImageForDataset() throws Exception {
        String filename = "test-image.jpg";
        String mediaType = "image/jpeg";
        File inputFile = Paths.get(resourcePath, "s3", filename).toFile();

        // Update Dataset 1 (which per IT test data already has a DatasetImage attached), so we don't have to manually create a Dataset - which is a nightmare
        Dataset dataset = datasetService.getDataset(1L);
        S3Object s3Object = s3ObjectService.storeFile(inputFile);
        DatasetImageDto datasetImageDto = new DatasetImageDto();
        datasetImageDto.setS3Object(new IdObject().id(s3Object.getId()));
        DatasetImage datasetImage = datasetImageDtoMapper.datasetImageDtoToDatasetImage(datasetImageDto);
        dataset.addDatasetImage(datasetImage);
        dataset = datasetService.save(dataset);
        long lastDatasetImageId = dataset.getDatasetImages().stream().map(DatasetImage::getId).max(Long::compareTo).get();

        testHelperService.fileDownloadTest(mockMvc, DATASETS_PATH_SINGLE_DATASET_IMAGES + "/" + lastDatasetImageId + "/download", inputFile, mediaType, "inline", true);
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void downloadDatasetImageForDataset__Admin() throws Exception {
        downloadDatasetImageForDataset();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void downloadDatasetImageForDataset__TeamMember() throws Exception {
        downloadDatasetImageForDataset();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void downloadDatasetImageForDataset_DataCollector() throws Exception {
        downloadDatasetImageForDataset();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void downloadDatasetImageForDataset__DataScientist() throws Exception {
        downloadDatasetImageForDataset();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void downloadDatasetImageForDataset__DataRecipient() throws Exception {
        downloadDatasetImageForDataset();
    }


    // -------- getMeasurementWingbeatForDataset -----------------------------------------------------------------------

    void getMeasurementWingbeatForDataset() throws Exception {
        mockMvc.perform(get(DATASETS_PATH + "/2/measurement_wingbeat"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(1));
    }

    @Test
    @Transactional(readOnly = true)
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getMeasurementWingbeatForDataset__Admin() throws Exception {
        getMeasurementWingbeatForDataset();
    }

    @Test
    @Transactional(readOnly = true)
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getMeasurementWingbeatForDataset__TeamMember() throws Exception {
        getMeasurementWingbeatForDataset();
    }

    @Test
    @Transactional(readOnly = true)
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void getMeasurementWingbeatForDataset_DataCollector() throws Exception {
        getMeasurementWingbeatForDataset();
    }

    @Test
    @Transactional(readOnly = true)
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void getMeasurementWingbeatForDataset__DataScientist() throws Exception {
        getMeasurementWingbeatForDataset();
    }

    @Test
    @Transactional(readOnly = true)
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void getMeasurementWingbeatForDataset__DataRecipient() throws Exception {
        getMeasurementWingbeatForDataset();
    }


    // -------- getMeasurementWingbeatWavForDataset --------------------------------------------------------------------


    void getMeasurementWingbeatWavForDataset() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH + "/2"))
            .andExpect(status().isOk())
            .andReturn();

        DatasetDto datasetDto = testHelperService.convertResponseBodyToDto(mvcResult, DatasetDto.class);

        MvcResult s3MvcResult = mockMvc.perform(get(DATASETS_PATH + "/2/measurement_wingbeat/wav_s3_object"))
            .andExpect(status().isOk())
            .andReturn();

        S3ObjectDto wavS3Object = testHelperService.convertResponseBodyToDto(s3MvcResult, S3ObjectDto.class);

        assertEquals("audio/wave", wavS3Object.getMediaType());
        assertEquals(6, wavS3Object.getId());
        assertEquals("be9efd38-8107-11ec-893a-b827eb353b23.wav", wavS3Object.getFilename());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getMeasurementWingbeatWavForDataset__Admin() throws Exception {
        getMeasurementWingbeatWavForDataset();
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getMeasurementWingbeatWavForDataset__TeamMember() throws Exception {
        getMeasurementWingbeatWavForDataset();
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void getMeasurementWingbeatWavForDataset_DataCollector() throws Exception {
        getMeasurementWingbeatWavForDataset();
    }

    @Test
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void getMeasurementWingbeatWavForDataset__DataScientist() throws Exception {
        getMeasurementWingbeatWavForDataset();
    }

    @Test
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void getMeasurementWingbeatWavForDataset__DataRecipient() throws Exception {
        getMeasurementWingbeatWavForDataset();
    }

    // -------- downloadMeasurementWingbeatWavAndImageForDataset -------------------------------------------------------

    void downloadMeasurementWingbeatWavAndImageForDataset() throws Exception {
        String wavFilename = "wingbeat.wav";
        String wavMediaType = "audio/wave";
        File wavInputFile = Paths.get(resourcePath, "s3", wavFilename).toFile();
        S3Object wavS3Object = s3ObjectService.storeFile(wavInputFile);

        String imageFilename = "wingbeat.png";
        String imageMediaType = "image/png";
        File imageInputFile = Paths.get(resourcePath, "s3", imageFilename).toFile();
        S3Object imageS3Object = s3ObjectService.storeFile(imageInputFile);

        // Update Dataset 1 (which per IT test data already has a DatasetImage attached), so we don't have to manually create a Dataset - which is a nightmare
        Dataset dataset = datasetService.getDataset(1L);
        MeasurementWingbeat measurementWingbeat = new MeasurementWingbeat();
        measurementWingbeat.setWavS3Object(wavS3Object);
        measurementWingbeat.setImageS3Object(imageS3Object);
        measurementWingbeat.setSampleRate(96000);
        dataset.setMeasurementWingbeat(measurementWingbeat);
        datasetService.save(dataset);

        testHelperService.fileDownloadTest(mockMvc, DATASETS_PATH_SINGLE_WINGBEAT_WAV_DOWNLOAD, wavInputFile, wavMediaType, "inline", false);
        testHelperService.fileDownloadTest(mockMvc, DATASETS_PATH_SINGLE_WINGBEAT_IMAGE_DOWNLOAD, imageInputFile, imageMediaType, "inline", true);
    }


    @Test
    @Transactional
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void downloadMeasurementWingbeatWavAndImageForDataset__Admin() throws Exception {
        downloadMeasurementWingbeatWavAndImageForDataset();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void downloadMeasurementWingbeatWavAndImageForDataset__TeamMember() throws Exception {
        downloadMeasurementWingbeatWavAndImageForDataset();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void downloadMeasurementWingbeatWavAndImageForDataset_DataCollector() throws Exception {
        downloadMeasurementWingbeatWavAndImageForDataset();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void downloadMeasurementWingbeatWavAndImageForDataset__DataScientist() throws Exception {
        downloadMeasurementWingbeatWavAndImageForDataset();
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void downloadMeasurementWingbeatWavAndImageForDataset__DataRecipient() throws Exception {
        downloadMeasurementWingbeatWavAndImageForDataset();
    }


    // -------- getMeasurementWingbeatImageForDataset ------------------------------------------------------------------

    void getMeasurementWingbeatImageForDataset() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH + "/2"))
            .andExpect(status().isOk())
            .andReturn();

        DatasetDto datasetDto = testHelperService.convertResponseBodyToDto(mvcResult, DatasetDto.class);

        MvcResult s3MvcResult = mockMvc.perform(get(DATASETS_PATH + "/2/measurement_wingbeat/image_s3_object"))
            .andExpect(status().isOk())
            .andReturn();

        S3ObjectDto imageS3Object = testHelperService.convertResponseBodyToDto(s3MvcResult, S3ObjectDto.class);

        assertEquals("image/png", imageS3Object.getMediaType());
        assertEquals(7, imageS3Object.getId());
        assertEquals("c0ff562c-8107-11ec-893a-b827eb353b23.png", imageS3Object.getFilename());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getMeasurementWingbeatImageForDataset__Admin() throws Exception {
        getMeasurementWingbeatImageForDataset();
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getMeasurementWingbeatImageForDataset__TeamMember() throws Exception {
        getMeasurementWingbeatImageForDataset();
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void getMeasurementWingbeatImageForDataset_DataCollector() throws Exception {
        getMeasurementWingbeatImageForDataset();
    }

    @Test
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void getMeasurementWingbeatImageForDataset__DataScientist() throws Exception {
        getMeasurementWingbeatImageForDataset();
    }

    @Test
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void getMeasurementWingbeatImageForDataset__DataRecipient() throws Exception {
        getMeasurementWingbeatImageForDataset();
    }

}

