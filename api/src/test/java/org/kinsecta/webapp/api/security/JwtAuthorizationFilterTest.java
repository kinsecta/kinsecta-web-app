package org.kinsecta.webapp.api.security;

import nl.altindag.log.LogCaptor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.kinsecta.webapp.api.model.entities.UserRole;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class JwtAuthorizationFilterTest {

    LogCaptor logCaptor = LogCaptor.forClass(JwtAuthorizationFilter.class);

    @Mock
    JwtTokenService jwtTokenService;

    MockHttpServletRequest servletRequest = new MockHttpServletRequest();
    MockHttpServletResponse servletResponse = new MockHttpServletResponse();

    @Spy
    FilterChain filterChain;


    private JwtAuthorizationFilter setupSpyJwtAuthorizationFilter() {
        return spy(new JwtAuthorizationFilter(jwtTokenService));
    }


    @Test
    void doFilterInternal__noAuthorizationHeader() throws ServletException, IOException {
        logCaptor.setLogLevelToDebug();

        JwtAuthorizationFilter jwtAuthorizationFilter = setupSpyJwtAuthorizationFilter();
        jwtAuthorizationFilter.doFilterInternal(servletRequest, servletResponse, filterChain);

        // Assert that no Authorization header is present
        assertNull(servletRequest.getHeader(HttpHeaders.AUTHORIZATION));
        assertTrue(logCaptor.getDebugLogs().contains("Skipped JwtAuthorizationFilter because 'Authorization' header is not present in request or it is present but doesn't start with 'Bearer '"));
        verify(filterChain).doFilter(servletRequest, servletResponse);
    }


    @Test
    void doFilterInternal__withValidJwt() throws ServletException, IOException {
        JwtAuthorizationFilter jwtAuthorizationFilter = setupSpyJwtAuthorizationFilter();
        String bearerToken = JwtTokenService.TOKEN_PREFIX + "ValidJwt";
        servletRequest.addHeader(HttpHeaders.AUTHORIZATION, bearerToken);
        UsernamePasswordAuthenticationToken expectedAuthentication = new UsernamePasswordAuthenticationToken("my_username", "my_password");

        doReturn(expectedAuthentication).when(jwtAuthorizationFilter).getAuthenticationFromBearerToken(bearerToken);
        jwtAuthorizationFilter.doFilterInternal(servletRequest, servletResponse, filterChain);

        // Assert that Authorization header is present, 'getAuthenticationFromBearerToken()' method is called and returns an Authentication object and the next filter is called
        assertNotNull(servletRequest.getHeader(HttpHeaders.AUTHORIZATION));
        verify(jwtAuthorizationFilter).getAuthenticationFromBearerToken(bearerToken);
        verify(filterChain).doFilter(servletRequest, servletResponse);
    }


    @Test
    void getAuthenticationFromBearerToken__validJwt() {
        JwtAuthorizationFilter jwtAuthorizationFilter = setupSpyJwtAuthorizationFilter();
        String username = "my_username";
        String jwt = "alskdjc8ew5nvdi89345gfvwe943vewkrf0934tmve";
        String bearerToken = JwtTokenService.TOKEN_PREFIX + jwt;
        Collection<? extends GrantedAuthority> grantedAuthorities = List.of(new SimpleGrantedAuthority(UserRole.ADMIN.getValue()));


        when(jwtTokenService.verifyJwtAndGetAuthentication(jwt)).thenReturn(new UsernamePasswordAuthenticationToken(username, null, grantedAuthorities));

        UsernamePasswordAuthenticationToken authentication = jwtAuthorizationFilter.getAuthenticationFromBearerToken(bearerToken);

        assertEquals(username, authentication.getPrincipal());
        assertNull(authentication.getCredentials());
        assertEquals(grantedAuthorities, authentication.getAuthorities());
    }

}
