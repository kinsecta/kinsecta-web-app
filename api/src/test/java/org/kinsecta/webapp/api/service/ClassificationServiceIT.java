package org.kinsecta.webapp.api.service;

import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.model.entities.Classification;
import org.kinsecta.webapp.api.model.entities.ClassificationGender;
import org.kinsecta.webapp.api.model.entities.ClassificationType;
import org.kinsecta.webapp.api.model.entities.Dataset;
import org.kinsecta.webapp.api.model.repositories.InsectOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertTrue;


@SpringBootTest
@ActiveProfiles("it")
@Transactional
public class ClassificationServiceIT {

    @Autowired
    private ClassificationService classificationService;

    @Autowired
    private DatasetService datasetService;

    @Autowired
    private InsectOrderRepository insectOrderRepository;

    @Test
    void testSave_updates_dataset_computed_modified() {
        LocalDateTime beforeSave = LocalDateTime.now();

        Dataset dataset = datasetService.getDataset(1L);
        Classification classification = createBasicClassification();
        dataset.addClassification(classification);
        classificationService.save(classification);
        dataset = datasetService.getDataset(1L);
        assertTrue(dataset.getComputedModified().isAfter(beforeSave));

        beforeSave = LocalDateTime.now();
        classification = createBasicClassification();
        dataset.getDatasetImages().iterator().next().addClassification(classification);
        classificationService.save(classification);
        dataset = datasetService.getDataset(1L);
        assertTrue(dataset.getComputedModified().isAfter(beforeSave));

        beforeSave = LocalDateTime.now();
        dataset = datasetService.getDataset(2L);
        classification = createBasicClassification();
        dataset.getMeasurementWingbeat().addClassification(classification);
        classificationService.save(classification);
        dataset = datasetService.getDataset(2L);
        assertTrue(dataset.getComputedModified().isAfter(beforeSave));
    }

    private Classification createBasicClassification() {
        Classification classification = new Classification();
        classification.setInsectOrder(insectOrderRepository.findById(1L).get());
        classification.setType(ClassificationType.HUMAN);
        classification.setGender(ClassificationGender.UNSPECIFIED);
        classification.setProbability(new BigDecimal("0.5"));
        return classification;
    }

}
