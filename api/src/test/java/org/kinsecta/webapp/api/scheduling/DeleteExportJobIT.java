package org.kinsecta.webapp.api.scheduling;

import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.mail.ExportMailService;
import org.kinsecta.webapp.api.model.entities.Export;
import org.kinsecta.webapp.api.s3.StorageException;
import org.kinsecta.webapp.api.service.DeletionService;
import org.kinsecta.webapp.api.service.ExportService;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;


@ActiveProfiles("it")
@SpringBootTest
@Transactional
class DeleteExportJobIT {

    @Autowired
    private DeleteExportJob deleteExportJob;

    @Autowired
    private ExportService exportService;

    @SpyBean
    private DeletionService deletionServiceSpy;

    @MockBean
    private ExportMailService exportMailServiceMock;

    @Captor
    private ArgumentCaptor<Export> exportCaptor;

    @Captor
    private ArgumentCaptor<String> errorMessageCaptor;


    @Test
    void testDeleteExpiredExports_normal() {
        deleteExportJob.deleteExpiredExports();
        assertTrue(exportService.getOptionalExport(1L).isEmpty());
        assertTrue(exportService.getOptionalExport(2L).isPresent());
    }

    @Test
    void testException() throws StorageException {
        // throw exception on delete
        Mockito.doThrow(new StorageException("Exception message")).when(deletionServiceSpy).deleteExport(any());
        deleteExportJob.deleteExpiredExports();

        // assert mail will be sent
        verify(exportMailServiceMock).sendExportDeletionFailedMessageToAdmin(exportCaptor.capture(), errorMessageCaptor.capture());

        assertEquals(1L, exportCaptor.getValue().getId());
        assertEquals("Exception message", errorMessageCaptor.getValue());
        assertTrue(exportService.getOptionalExport(1L).isPresent());
        assertTrue(exportService.getOptionalExport(2L).isPresent());
    }

}
