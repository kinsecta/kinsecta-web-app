package org.kinsecta.webapp.api.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.io.FileUtils;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.kinsecta.webapp.api.TestHelperService;
import org.kinsecta.webapp.api.mail.UploadMailService;
import org.kinsecta.webapp.api.model.entities.*;
import org.kinsecta.webapp.api.service.DatasetImageService;
import org.kinsecta.webapp.api.service.DatasetService;
import org.kinsecta.webapp.api.service.UploadService;
import org.kinsecta.webapp.api.service.UserService;
import org.kinsecta.webapp.api.util.TempDirectoryHelper;
import org.kinsecta.webapp.api.v1.model.S3ObjectDto;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * This test class must not have a @Transactional annotation, because it would interfere with the @Transactional annotations
 * that are tested within this test class. This makes the assertions somewhat more inconvenient.
 */
@ActiveProfiles("it")
@SpringBootTest
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UploadControllerTransactionIT {

    private static final String LOCAL_RESOURCE_PATH = "src/test/resources/zipped";

    private static final String UPLOADS_PATH = "/uploads";
    private static final String UPLOADS_PATH_SINGLE_S3 = "/s3_object";
    private static final String UPLOADS_PATH_SINGLE_S3_DOWNLOAD = "/download";

    @Autowired
    private Flyway flyway;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TempDirectoryHelper tempDirectoryHelper;

    @Autowired
    private UserService userService;

    @Autowired
    private UploadService uploadService;

    @Autowired
    private DatasetService datasetService;

    @Autowired
    private DatasetImageService datasetImageService;

    @Autowired
    private TestHelperService testHelperService;

    @MockBean
    private UploadMailService uploadMailServiceMock;

    @Captor
    private ArgumentCaptor<List<String>> errorMessageCaptor;

    @Captor
    private ArgumentCaptor<Upload> uploadCaptor;

    @AfterAll
    void cleanUp() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "f.rossiar")
    void createS3ObjectForUpload__withVideo() throws Exception {
        try {
            User user = userService.getUser(2L);
            Upload upload = uploadService.createUploadForUser(user);
            File zipFile = Paths.get(LOCAL_RESOURCE_PATH, "data_dummie_single.zip").toFile();

            // Upload ZIP via REST API
            performUploadWithBasicAssertions(zipFile, upload, status().isCreated(), TransferStatus.SUCCESSFUL);

            // Retrieve whole Upload object from DB
            Upload savedUpload = uploadService.getUpload(upload.getId());

            // Check Datasets created from Upload
            List<Dataset> datasets = datasetService.getDatasetsForUpload(savedUpload);
            assertEquals(1, datasets.size());

            // Assertions on first Dataset (order matches alphanumeric subdirectory order inside ZIP file)
            Dataset dataset = datasets.get(0);
            assertEquals(savedUpload.getId(), dataset.getUpload().getId());
            assertEquals(1L, dataset.getSensor().getId());
            assertEquals("148ebe218dd77159e74c94f836c4b265", dataset.getMd5Hash());
            assertEquals("video/mp4", dataset.getVideoS3Object().getMediaType());
            assertTrue(testHelperService.existsS3ObjectInBucketStartingWith(dataset.getVideoS3Object().getBucket(), dataset.getVideoS3Object().getKey()));
            List<DatasetImage> datasetImages = datasetImageService.getDatasetImagesForDataset(dataset);
            assertEquals(2, datasetImages.size());
            assertEquals("image/jpeg", datasetImages.get(0).getS3Object().getMediaType());
            assertEquals("image/jpeg", datasetImages.get(1).getS3Object().getMediaType());
            assertTrue(testHelperService.existsS3ObjectInBucketStartingWith(datasetImages.get(0).getS3Object().getBucket(), datasetImages.get(0).getS3Object().getKey()));
            assertTrue(testHelperService.existsS3ObjectInBucketStartingWith(datasetImages.get(1).getS3Object().getBucket(), datasetImages.get(1).getS3Object().getKey()));
            assertNull(dataset.getMeasurementWingbeat());
            assertEquals(0, dataset.getMeasurementSpectrumChannel415nm());
            assertEquals(0, new BigDecimal("21.2").compareTo(dataset.getMeasurementTemperature()));
            assertEquals("SSE", dataset.getMeasurementWindSensorDirection());
            assertEquals(1, dataset.getClassifications().size());

            // Assertions for Auto Tags
            List<String> expectedAutoTags = List.of(DatasetAutoTag.VIDEO.getTagName(), DatasetAutoTag.IMAGES.getTagName());
            assertAutoTagging(expectedAutoTags, dataset);

            // #79 double check for duplicates
            // ReLoad Dataset explicitly because when calling getDataset() it would return 2 classifications (duplicates) instead of 1
            Dataset datasetNew = datasetService.getDataset(dataset.getId());
            assertEquals(1, datasetNew.getClassifications().size());

            // Assert that temp dir got cleaned
            assertTempDirGotCleaned();

        } finally {
            testHelperService.clearAllHashesForUpload();
            testHelperService.cleanWorkingDirectory();
            testHelperService.cleanS3Buckets();
        }
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "f.rossiar")
    void createS3ObjectForUpload__withWingbeat() throws Exception {
        File singleWingbeat = Paths.get(LOCAL_RESOURCE_PATH, "data_wingbeat_single.zip").toFile();
        File singleWingbeatNoClassification = Paths.get(LOCAL_RESOURCE_PATH, "data_wingbeat_single_no_classification.zip").toFile();
        List<File> wingbeatUploads = List.of(singleWingbeat, singleWingbeatNoClassification);

        for (File zipFile : wingbeatUploads) {
            try {
                User user = userService.getUser(2L);
                Upload upload = uploadService.createUploadForUser(user);


                // Upload ZIP via REST API
                performUploadWithBasicAssertions(zipFile, upload, status().isCreated(), TransferStatus.SUCCESSFUL);

                // Retrieve whole Upload object from DB
                Upload savedUpload = uploadService.getUpload(upload.getId());

                // Check Datasets created from Upload
                List<Dataset> datasets = datasetService.getDatasetsForUpload(savedUpload);
                assertEquals(1, datasets.size());

                // Assertions on first Dataset (order matches alphanumeric subdirectory order inside ZIP file)
                Dataset dataset = datasets.get(0);
                assertEquals(savedUpload.getId(), dataset.getUpload().getId());
                assertEquals(1L, dataset.getSensor().getId());
                if (zipFile.getName().endsWith("no_classification.zip")) {
                    assertEquals("86d6ef176bda88f36014e3b3e28427f5", dataset.getMd5Hash());
                } else {
                    assertEquals("0767ef62cb94bad3ba354364b03da2a1", dataset.getMd5Hash());
                }
                assertNull(dataset.getVideoS3Object());
                List<DatasetImage> datasetImages = datasetImageService.getDatasetImagesForDataset(dataset);
                assertTrue(datasetImages.isEmpty());
                assertEquals("audio/wave", dataset.getMeasurementWingbeat().getWavS3Object().getMediaType());
                assertTrue(testHelperService.existsS3ObjectInBucketStartingWith(dataset.getMeasurementWingbeat().getWavS3Object().getBucket(), dataset.getMeasurementWingbeat().getWavS3Object().getKey()));
                assertEquals("image/png", dataset.getMeasurementWingbeat().getImageS3Object().getMediaType());
                assertTrue(testHelperService.existsS3ObjectInBucketStartingWith(dataset.getMeasurementWingbeat().getImageS3Object().getBucket(), dataset.getMeasurementWingbeat().getImageS3Object().getKey()));
                assertEquals(96000, dataset.getMeasurementWingbeat().getSampleRate());
                assertEquals(0, dataset.getMeasurementSpectrumChannel445nm());
                assertEquals(0, new BigDecimal("16.7").compareTo(dataset.getMeasurementTemperature()));
                assertEquals("S", dataset.getMeasurementWindSensorDirection());

                // Assertion for Classifications
                if (zipFile.getName().endsWith("no_classification.zip")) {
                    assertEquals(0, dataset.getClassifications().size());
                } else {
                    assertEquals(1, dataset.getClassifications().size());
                }

                // Assertions for Auto Tags
                List<String> expectedAutoTags = new ArrayList<>();
                expectedAutoTags.add(DatasetAutoTag.WINGBEAT.getTagName());
                if (zipFile.getName().endsWith("no_classification.zip")) {
                    expectedAutoTags.add(DatasetAutoTag.MISSING_MAIN_CLASSIFICATION.getTagName());
                }
                assertAutoTagging(expectedAutoTags, dataset);

                // Assert that temp dir got cleaned
                assertTempDirGotCleaned();

            } finally {
                testHelperService.clearAllHashesForUpload();
                testHelperService.cleanWorkingDirectory();
                testHelperService.cleanS3Buckets();
            }
        }
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "f.rossiar")
    void createS3ObjectForUpload__twoDatasets() throws Exception {
        try {
            User user = userService.getUser(2L);
            Upload upload = uploadService.createUploadForUser(user);
            File zipFile = Paths.get(LOCAL_RESOURCE_PATH, "data_two_datasets.zip").toFile();

            // Upload ZIP via REST API
            performUploadWithBasicAssertions(zipFile, upload, status().isCreated(), TransferStatus.SUCCESSFUL);

            // Retrieve whole Upload object from DB
            Upload savedUpload = uploadService.getUpload(upload.getId());

            // Check Datasets created from Upload
            List<Dataset> datasets = datasetService.getDatasetsForUpload(savedUpload);
            datasets.sort(Comparator.comparing(Dataset::getId));
            assertEquals(2, datasets.size());

            boolean enteredFirstIf = false;
            boolean enteredSecondIf = false;

            for (Dataset dataset : datasets) {
                // Assertions on the first Dataset with Video
                if (dataset.getVideoS3Object() != null) {
                    assertEquals(savedUpload.getId(), dataset.getUpload().getId());
                    assertEquals(1L, dataset.getSensor().getId());
                    assertEquals("148ebe218dd77159e74c94f836c4b265", dataset.getMd5Hash());
                    assertEquals("video/mp4", dataset.getVideoS3Object().getMediaType());
                    assertTrue(testHelperService.existsS3ObjectInBucketStartingWith(dataset.getVideoS3Object().getBucket(), dataset.getVideoS3Object().getKey()));
                    List<DatasetImage> datasetImages1 = datasetImageService.getDatasetImagesForDataset(dataset);
                    assertEquals(2, datasetImages1.size());
                    assertEquals("image/jpeg", datasetImages1.get(0).getS3Object().getMediaType());
                    assertEquals("image/jpeg", datasetImages1.get(1).getS3Object().getMediaType());
                    assertTrue(testHelperService.existsS3ObjectInBucketStartingWith(datasetImages1.get(0).getS3Object().getBucket(), datasetImages1.get(0).getS3Object().getKey()));
                    assertTrue(testHelperService.existsS3ObjectInBucketStartingWith(datasetImages1.get(1).getS3Object().getBucket(), datasetImages1.get(1).getS3Object().getKey()));
                    assertNull(dataset.getMeasurementWingbeat());
                    assertEquals(0, dataset.getMeasurementSpectrumChannel415nm());
                    assertEquals(0, new BigDecimal("21.2").compareTo(dataset.getMeasurementTemperature()));
                    assertEquals("SSE", dataset.getMeasurementWindSensorDirection());

                    // Assertions for Auto Tags
                    List<String> expectedAutoTags = List.of(DatasetAutoTag.VIDEO.getTagName(), DatasetAutoTag.IMAGES.getTagName());
                    assertAutoTagging(expectedAutoTags, dataset);

                    enteredFirstIf = true;
                }
                // Assertions on the second Dataset with Wingbeat Data
                else if (dataset.getMeasurementWingbeat() != null) {
                    assertEquals(savedUpload.getId(), dataset.getUpload().getId());
                    assertEquals(1L, dataset.getSensor().getId());
                    assertEquals("1af1637f7b61a768747b9536ff3b0df4", dataset.getMd5Hash());
                    assertNull(dataset.getVideoS3Object());
                    List<DatasetImage> datasetImages2 = datasetImageService.getDatasetImagesForDataset(dataset);
                    assertTrue(datasetImages2.isEmpty());
                    assertEquals("audio/wave", dataset.getMeasurementWingbeat().getWavS3Object().getMediaType());
                    assertTrue(testHelperService.existsS3ObjectInBucketStartingWith(dataset.getMeasurementWingbeat().getWavS3Object().getBucket(), dataset.getMeasurementWingbeat().getWavS3Object().getKey()));
                    assertEquals("image/png", dataset.getMeasurementWingbeat().getImageS3Object().getMediaType());
                    assertTrue(testHelperService.existsS3ObjectInBucketStartingWith(dataset.getMeasurementWingbeat().getImageS3Object().getBucket(), dataset.getMeasurementWingbeat().getImageS3Object().getKey()));
                    assertEquals(96000, dataset.getMeasurementWingbeat().getSampleRate());
                    assertEquals(0, dataset.getMeasurementSpectrumChannel415nm());
                    assertEquals(0, new BigDecimal("16.7").compareTo(dataset.getMeasurementTemperature()));
                    assertEquals("S", dataset.getMeasurementWindSensorDirection());

                    // Assertions for Auto Tags
                    List<String> expectedAutoTags = List.of(DatasetAutoTag.WINGBEAT.getTagName());
                    assertAutoTagging(expectedAutoTags, dataset);

                    enteredSecondIf = true;
                }
            }

            assertTrue(enteredFirstIf);
            assertTrue(enteredSecondIf);

            // Assert that temp dir got cleaned
            assertTempDirGotCleaned();

        } finally {
            testHelperService.clearAllHashesForUpload();
            testHelperService.cleanWorkingDirectory();
            testHelperService.cleanS3Buckets();
        }
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "f.rossiar")
    void createS3ObjectForUpload__ignore_bounding_box_and_wingbeat_classifications() throws Exception {
        try {
            // TODO: remove or change this test, once bounding boxes and/or wingbeat classifications are implemented
            User user = userService.getUser(2L);
            Upload upload = uploadService.createUploadForUser(user);
            File zipFile = Paths.get(LOCAL_RESOURCE_PATH, "ignore_bounding_box_and_wingbeat_classifications.zip").toFile();

            // Upload ZIP via REST API
            performUploadWithBasicAssertions(zipFile, upload, status().isCreated(), TransferStatus.SUCCESSFUL);

            // Retrieve whole Upload object from DB
            Upload savedUpload = uploadService.getUpload(upload.getId());

            // Check Datasets created from Upload
            List<Dataset> datasets = datasetService.getDatasetsForUpload(savedUpload);
            assertEquals(1, datasets.size());

            // Assertions on first Dataset (order matches alphanumeric subdirectory order inside ZIP file)
            Dataset dataset = datasets.get(0);
            assertEquals(savedUpload.getId(), dataset.getUpload().getId());
            assertEquals(1L, dataset.getSensor().getId());
            assertEquals("31283a4c6bc18690d28b269bd55ec64b", dataset.getMd5Hash());
            assertNull(dataset.getVideoS3Object());
            assertEquals(0, dataset.getDatasetImages().size());
            assertNull(dataset.getMeasurementWingbeat());
            assertEquals(0, dataset.getMeasurementSpectrumChannel415nm());
            assertEquals(0, new BigDecimal("21.2").compareTo(dataset.getMeasurementTemperature()));
            assertEquals("SSE", dataset.getMeasurementWindSensorDirection());
            assertEquals(1, dataset.getClassifications().size());

            // Assert that temp dir got cleaned
            assertTempDirGotCleaned();

        } finally {
            testHelperService.clearAllHashesForUpload();
            testHelperService.cleanWorkingDirectory();
            testHelperService.cleanS3Buckets();
        }
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "f.rossiar")
    void createS3ObjectForUpload__issue_212() throws Exception {
        try {
            User user = userService.getUser(2L);
            Upload upload = uploadService.createUploadForUser(user);
            File zipFile = Paths.get(LOCAL_RESOURCE_PATH, "212-upload-2024-06-09.zip").toFile();

            // Upload ZIP via REST API
            performUploadWithBasicAssertions(zipFile, upload, status().isCreated(), TransferStatus.SUCCESSFUL);

            // Retrieve whole Upload object from DB
            Upload savedUpload = uploadService.getUpload(upload.getId());

            // Check Datasets created from Upload
            List<Dataset> datasets = datasetService.getDatasetsForUpload(savedUpload);
            assertEquals(1, datasets.size());

            // Assertions on first Dataset (order matches alphanumeric subdirectory order inside ZIP file)
            Dataset dataset = datasets.get(0);
            assertEquals(savedUpload.getId(), dataset.getUpload().getId());
            assertEquals(3L, dataset.getSensor().getId());
            assertEquals("b05e70a1f30680d969c041a2617dd313", dataset.getMd5Hash());

            assertEquals("video/mp4", dataset.getVideoS3Object().getMediaType());
            assertTrue(testHelperService.existsS3ObjectInBucketStartingWith(dataset.getVideoS3Object().getBucket(), dataset.getVideoS3Object().getKey()));

            List<DatasetImage> datasetImages = datasetImageService.getDatasetImagesForDataset(dataset);
            assertEquals(3, datasetImages.size());
            assertEquals("image/png", datasetImages.get(0).getS3Object().getMediaType());
            assertEquals("image/png", datasetImages.get(1).getS3Object().getMediaType());
            assertEquals("image/png", datasetImages.get(2).getS3Object().getMediaType());
            assertTrue(testHelperService.existsS3ObjectInBucketStartingWith(datasetImages.get(0).getS3Object().getBucket(), datasetImages.get(0).getS3Object().getKey()));
            assertTrue(testHelperService.existsS3ObjectInBucketStartingWith(datasetImages.get(1).getS3Object().getBucket(), datasetImages.get(1).getS3Object().getKey()));
            assertTrue(testHelperService.existsS3ObjectInBucketStartingWith(datasetImages.get(2).getS3Object().getBucket(), datasetImages.get(2).getS3Object().getKey()));

            assertNull(dataset.getMeasurementWingbeat());
            assertEquals(0, new BigDecimal("6932.1").compareTo(dataset.getMeasurementLuminosity()));
            assertEquals(60487, dataset.getMeasurementSpectrumChannel415nm());
            assertEquals(65535, dataset.getMeasurementSpectrumChannel445nm());
            assertEquals(65535, dataset.getMeasurementSpectrumNearIr());
            assertEquals(0, new BigDecimal("25.0").compareTo(dataset.getMeasurementTemperature()));
            assertNull(dataset.getMeasurementWindSensorDirection());
            assertNull(dataset.getMeasurementWindSensorSpeed());

            // Assert that temp dir got cleaned
            assertTempDirGotCleaned();

        } finally {
            testHelperService.clearAllHashesForUpload();
            testHelperService.cleanWorkingDirectory();
            testHelperService.cleanS3Buckets();
        }
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "f.rossiar")
    void createS3ObjectForUpload__invalidJson() throws Exception {
        try {
            User user = userService.getUser(2L);
            Upload upload = uploadService.createUploadForUser(user);
            File zipFile = Paths.get(LOCAL_RESOURCE_PATH, "invalid_json.zip").toFile();

            // Upload ZIP via REST API
            performUploadWithBasicAssertions(zipFile, upload, status().isBadRequest(), TransferStatus.ERRONEOUS);

            // Verify that email notification has been triggered and list of errors matches our expectation
            verifyEmailNotificationAndErrorMessages(user, List.of("[0001_2022-01-29_13-59-12] An error occurred while trying to deserialize a JSON file: UnrecognizedPropertyException: Unrecognized field \"id_user\" (class KinsectaMeasurement), not marked as ignorable (18 known properties: \"sensor_id\", \"wingbeat\", \"size_insect\", \"main_classifications\", \"user_id\", \"spectrum\", \"particulate_matter\", \"humidity\", \"temperature\", \"images\", \"tags\", \"air_pressure\", \"video\", \"date_time\", \"luminosity\", \"rainfall\", \"wind_sensor\", \"sensor_location\"])\n" +
                " at [Source: (File); line: 2, column: 17] (through reference chain: KinsectaMeasurement[\"id_user\"])"));

            // Assert that temp dir got cleaned
            assertTempDirGotCleaned();

        } finally {
            testHelperService.cleanWorkingDirectory();
            testHelperService.cleanS3Buckets();
        }
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "f.rossiar")
    void createS3ObjectForUpload__nonExistentInsectOrder() throws Exception {
        try {
            User user = userService.getUser(2L);
            Upload upload = uploadService.createUploadForUser(user);
            File zipFile = Paths.get(LOCAL_RESOURCE_PATH, "non_existant_insect_order.zip").toFile();

            // Upload ZIP via REST API
            performUploadWithBasicAssertions(zipFile, upload, status().isBadRequest(), TransferStatus.ERRONEOUS);

            // Verify that email notification has been triggered and list of errors matches our expectation
            verifyEmailNotificationAndErrorMessages(user, List.of("[0001_2022-01-29_13-59-12] An error occurred while mapping a JSON structure: Could not find InsectOrder for Gbif Id 'null' or name 'Bla'"));

            // Assert that temp dir got cleaned
            assertTempDirGotCleaned();

        } finally {
            testHelperService.cleanWorkingDirectory();
            testHelperService.cleanS3Buckets();
        }
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "f.rossiar")
    void createS3ObjectForUpload__invalidDate() throws Exception {
        try {
            User user = userService.getUser(2L);
            Upload upload = uploadService.createUploadForUser(user);
            File zipFile = Paths.get(LOCAL_RESOURCE_PATH, "invalid_date.zip").toFile();

            // Upload ZIP via REST API
            performUploadWithBasicAssertions(zipFile, upload, status().isBadRequest(), TransferStatus.ERRONEOUS);

            // Verify that email notification has been triggered and list of errors matches our expectation
            verifyEmailNotificationAndErrorMessages(user, List.of("[0001_2022-02-55_13-59-12] An error occurred while trying to format the datetime from a given folder: unable to parse '2022-02-55_13-59-12' as LocalDateTime, supported formats are 'yyyy-MM-dd_HH-mm-ss', 'yyyy-MM-dd HH:mm:ss', 'yyyy-MM-ddTHH:mm:ss'"));

            // Assert that temp dir got cleaned
            assertTempDirGotCleaned();

        } finally {
            testHelperService.cleanWorkingDirectory();
            testHelperService.cleanS3Buckets();
        }
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "f.rossiar")
    void createS3ObjectForUpload__validation_failures() throws Exception {
        try {
            User user = userService.getUser(2L);
            Upload upload = uploadService.createUploadForUser(user);
            File zipFile = Paths.get(LOCAL_RESOURCE_PATH, "validation_failures.zip").toFile();

            // Upload ZIP via REST API
            performUploadWithBasicAssertions(zipFile, upload, status().isBadRequest(), TransferStatus.ERRONEOUS);

            // Verify that email notification has been triggered and list of errors matches our expectation
            verifyEmailNotificationAndErrorMessages(user, List.of(
                "[0001_2022-01-29_13-59-12] JSON validation error for the 'data.json' metadata file at property path wingbeat.filename: size must be between 1 and 255",
                "[0001_2022-01-29_13-59-12] JSON validation error for the 'data.json' metadata file at property path temperature.unit: must not be null"));
            // Assert that temp dir got cleaned
            assertTempDirGotCleaned();

        } finally {
            testHelperService.cleanWorkingDirectory();
            testHelperService.cleanS3Buckets();
        }
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "f.rossiar")
    void createS3ObjectForUpload__testRollbackOnError() throws Exception {
        try {
            User user = userService.getUser(2L);
            Upload upload = uploadService.createUploadForUser(user);
            File zipFile = Paths.get(LOCAL_RESOURCE_PATH, "one_valid_one_invalid.zip").toFile();

            // Upload ZIP via REST API
            Upload savedUpload = performUploadWithBasicAssertions(zipFile, upload, status().isBadRequest(), TransferStatus.ERRONEOUS);

            // Assert that the only object in S3 storage is the uploaded zip and no other entities have been persisted in the DB
            assertNothingButTheZipHasBeenStored(savedUpload);

            // Verify that email notification has been triggered and list of errors matches our expectation
            verifyEmailNotificationAndErrorMessages(user, List.of("[0001_2022-01-29_13-59-12] An error occurred while trying to deserialize a JSON file: UnrecognizedPropertyException: Unrecognized field \"id_user\" (class KinsectaMeasurement), not marked as ignorable (18 known properties: \"sensor_id\", \"wingbeat\", \"size_insect\", \"main_classifications\", \"user_id\", \"spectrum\", \"particulate_matter\", \"humidity\", \"temperature\", \"images\", \"tags\", \"air_pressure\", \"video\", \"date_time\", \"luminosity\", \"rainfall\", \"wind_sensor\", \"sensor_location\"])\n" +
                " at [Source: (File); line: 2, column: 17] (through reference chain: KinsectaMeasurement[\"id_user\"])"));

            // Assert that temp dir got cleaned
            assertTempDirGotCleaned();

        } finally {
            testHelperService.cleanWorkingDirectory();
            testHelperService.cleanS3Buckets();
        }
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "f.rossiar")
    void createS3ObjectForUpload__noMeasurementSubfoldersInZip() throws Exception {
        try {
            User user = userService.getUser(2L);
            Upload upload = uploadService.createUploadForUser(user);
            File zipFile = Paths.get(LOCAL_RESOURCE_PATH, "no_matching_measurement_subfolders.zip").toFile();

            // Upload ZIP via REST API
            Upload savedUpload = performUploadWithBasicAssertions(zipFile, upload, status().isBadRequest(), TransferStatus.ERRONEOUS);

            // Assert that the only object in S3 storage is the uploaded zip and no other entities have been persisted in the DB
            assertNothingButTheZipHasBeenStored(savedUpload);

            // Verify that email notification has been triggered and list of errors matches our expectation
            verifyEmailNotificationAndErrorMessages(user, List.of("No subfolders found in ZIP file which match the required directory naming scheme for a single KInsecta Measurement"));

            // Assert that temp dir got cleaned
            assertTempDirGotCleaned();

        } finally {
            testHelperService.cleanWorkingDirectory();
            testHelperService.cleanS3Buckets();
        }
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "f.rossiar")
    void createS3ObjectForUpload__109_noDataJsonFile() throws Exception {
        try {
            User user = userService.getUser(2L);
            Upload upload = uploadService.createUploadForUser(user);
            File zipFile = Paths.get(LOCAL_RESOURCE_PATH, "109-no-data-json.zip").toFile();

            // Upload ZIP via REST API
            Upload savedUpload = performUploadWithBasicAssertions(zipFile, upload, status().isBadRequest(), TransferStatus.ERRONEOUS);

            // Assert that the only object in S3 storage is the uploaded zip and no other entities have been persisted in the DB
            assertNothingButTheZipHasBeenStored(savedUpload);

            // Verify that email notification has been triggered and list of errors matches our expectation
            verifyEmailNotificationAndErrorMessages(user, List.of("[0001_2022-01-29_14-31-20] KInsecta Measurement metadata file 'data.json' not found"));

            // Assert that temp dir got cleaned
            assertTempDirGotCleaned();

        } finally {
            testHelperService.cleanWorkingDirectory();
            testHelperService.cleanS3Buckets();
        }
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "f.rossiar")
    void createS3ObjectForUpload__109_nonExistantSensor() throws Exception {
        try {
            User user = userService.getUser(2L);
            Upload upload = uploadService.createUploadForUser(user);
            File zipFile = Paths.get(LOCAL_RESOURCE_PATH, "109-non-existant-sensor.zip").toFile();

            // Upload ZIP via REST API
            Upload savedUpload = performUploadWithBasicAssertions(zipFile, upload, status().isBadRequest(), TransferStatus.ERRONEOUS);

            // Assert that the only object in S3 storage is the uploaded zip and no other entities have been persisted in the DB
            assertNothingButTheZipHasBeenStored(savedUpload);

            // Verify that email notification has been triggered and list of errors matches our expectation
            verifyEmailNotificationAndErrorMessages(user, List.of("[0033_2022-04-08_10-09-00] Could not find Sensor with ID '33'"));

            // Assert that temp dir got cleaned
            assertTempDirGotCleaned();

        } finally {
            testHelperService.cleanWorkingDirectory();
        }
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "f.rossiar")
    void createS3ObjectForUpload__109_nonMatchingSubfolderMetadata() throws Exception {
        try {
            User user = userService.getUser(2L);
            Upload upload = uploadService.createUploadForUser(user);
            File zipFile = Paths.get(LOCAL_RESOURCE_PATH, "109-non-matching-subfolder-metadata.zip").toFile();

            // Upload ZIP via REST API
            Upload savedUpload = performUploadWithBasicAssertions(zipFile, upload, status().isBadRequest(), TransferStatus.ERRONEOUS);

            // Assert that the only object in S3 storage is the uploaded zip and no other entities have been persisted in the DB
            assertNothingButTheZipHasBeenStored(savedUpload);

            // Verify that email notification has been triggered and list of errors matches our expectation
            verifyEmailNotificationAndErrorMessages(user, List.of(
                "[0001_2022-04-08_10-39-00] Sensor ID '2' in data.json does not match Sensor ID part in measurement subfolder name '1'",
                "[0001_2022-04-08_10-40-00] Timestamp '2022-04-08T10:39' in data.json does not match timestamp part in measurement subfolder name '2022-04-08T10:40'"));

            // Assert that temp dir got cleaned
            assertTempDirGotCleaned();

        } finally {
            testHelperService.cleanWorkingDirectory();
        }
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "f.rossiar")
    void createS3ObjectForUpload__duplicate_gets_rejected_and_rolled_back() throws Exception {
        File zipFile = Paths.get(LOCAL_RESOURCE_PATH, "data_dummie_single.zip").toFile();
        try {
            User user = userService.getUser(2L);
            Upload upload = uploadService.createUploadForUser(user);

            // Upload ZIP via REST API
            performUploadWithBasicAssertions(zipFile, upload, status().isCreated(), TransferStatus.SUCCESSFUL);

            // upload again, should fail now
            Upload upload2 = uploadService.createUploadForUser(user);
            Upload savedUpload = performUploadWithBasicAssertions(zipFile, upload2, status().isBadRequest(), TransferStatus.ERRONEOUS);

            assertNoDatasetsHaveBeenStored(savedUpload);

            // Verify that email notification has been triggered and list of errors matches our expectation
            verifyEmailNotificationAndErrorMessages(user, List.of(
                "[0001_2022-01-29_13-59-12] Hashing the current folder/dataset produced the duplicate hash '148ebe218dd77159e74c94f836c4b265'. This folder has already been imported as Dataset ID 3"));

            // Assert that temp dir got cleaned
            assertTempDirGotCleaned();

        } finally {
            testHelperService.cleanWorkingDirectory();
            testHelperService.cleanS3Buckets();
            testHelperService.clearAllHashesForUpload();
        }
    }


    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "f.rossiar")
    void createS3ObjectForUpload__tags_in_data_json_rejected_and_rolled_back() throws Exception {
        File zipFile = Paths.get(LOCAL_RESOURCE_PATH, "data_wingbeat_single_no_classification_with_tags.zip").toFile();
        try {
            User user = userService.getUser(2L);
            Upload upload = uploadService.createUploadForUser(user);

            // Upload ZIP via REST API
            Upload savedUpload = performUploadWithBasicAssertions(zipFile, upload, status().isBadRequest(), TransferStatus.ERRONEOUS);

            assertNoDatasetsHaveBeenStored(savedUpload);

            // Verify that email notification has been triggered and list of errors matches our expectation
            verifyEmailNotificationAndErrorMessages(user, List.of("[0001_2022-01-29_14-31-20] The key 'tags' is specified in the data.json, which indicates that the dataset has been exported and already exists in the system."));

            // Assert that temp dir got cleaned
            assertTempDirGotCleaned();

        } finally {
            testHelperService.cleanWorkingDirectory();
            testHelperService.cleanS3Buckets();
            testHelperService.clearAllHashesForUpload();
        }
    }


    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "f.rossiar")
    void getS3ObjectForUpload() throws Exception {
        try {
            User user = userService.getUser(2L);
            Upload upload = uploadService.createUploadForUser(user);
            File zipFile = Paths.get(LOCAL_RESOURCE_PATH, "data_dummie_single.zip").toFile();

            Upload savedUpload = performUploadWithBasicAssertions(zipFile, upload, status().isCreated(), TransferStatus.SUCCESSFUL);

            MvcResult mvcResult = mockMvc.perform(get(getUploadsPathSingleS3(upload.getId())))
                .andExpect(status().isOk())
                .andReturn();

            // Read API response as S3ObjectDto
            S3ObjectDto s3ObjectDto = mvcResultAsS3ObjectDtoWithBasicAssertions(mvcResult, savedUpload);
            assertEquals("upload.zip", s3ObjectDto.getFilename());
            assertEquals(zipFile.length(), s3ObjectDto.getFilesize());

        } finally {
            testHelperService.cleanWorkingDirectory();
            testHelperService.cleanS3Buckets();
            testHelperService.clearAllHashesForUpload();
        }

    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "f.rossiar")
    void downloadS3ObjectForUpload() throws Exception {
        try {
            User user = userService.getUser(2L);
            Upload upload = uploadService.createUploadForUser(user);
            File zipFile = Paths.get(LOCAL_RESOURCE_PATH, "data_dummie_single.zip").toFile();

            performUploadWithBasicAssertions(zipFile, upload, status().isCreated(), TransferStatus.SUCCESSFUL);

            String downloadEndpoint = getUploadsPathSingleS3Download(upload.getId());
            testHelperService.fileDownloadTest(mockMvc, downloadEndpoint, zipFile, "application/zip", "attachment; filename=\"upload.zip\"", true);

        } finally {
            testHelperService.cleanWorkingDirectory();
            testHelperService.cleanS3Buckets();
            testHelperService.clearAllHashesForUpload();
        }
    }


    private Upload performUploadWithBasicAssertions(File zipFile, Upload upload, ResultMatcher expectedResult, TransferStatus expectedTransferStatus) throws Exception {
        // Upload ZIP via REST API
        byte[] requestBody = FileUtils.readFileToByteArray(zipFile);
        MvcResult mvcResult = mockMvc.perform(post(getUploadsPathSingleS3(upload.getId()))
                .content(requestBody)
                .contentType("application/zip"))
            .andExpect(expectedResult)
            .andReturn();

        // Retrieve whole Upload object from DB
        Upload savedUpload = uploadService.getUpload(upload.getId());

        // Check returned S3ObjectDto when expected result is 200 OK
        if (expectedResult.equals(status().isOk())) {
            mvcResultAsS3ObjectDtoWithBasicAssertions(mvcResult, savedUpload);
        }

        // Assert status is SUCCESSFUL and finished dates are set reasonably
        assertEquals(expectedTransferStatus, savedUpload.getStatus());
        assertTrue(savedUpload.getFinished().isBefore(LocalDateTime.now()));
        assertTrue(savedUpload.getFinished().isAfter(LocalDateTime.now().minus(2, ChronoUnit.MINUTES)));

        // Assert S3 bucket contains upload ZIP
        assertTrue(testHelperService.existsS3ObjectInBucketStartingWith(savedUpload.getS3Object().getBucket(), savedUpload.getS3Object().getKey()));

        return savedUpload;
    }

    private S3ObjectDto mvcResultAsS3ObjectDtoWithBasicAssertions(MvcResult mvcResult, Upload savedUpload) throws JsonProcessingException, UnsupportedEncodingException {
        // Read API response as S3ObjectDto
        S3ObjectDto s3ObjectDto = testHelperService.convertResponseBodyToDto(mvcResult, S3ObjectDto.class);

        // Assert correct mimetype recognition of uploaded zip file
        assertEquals("application/zip", s3ObjectDto.getMediaType());

        // Assert Upload ID matches S3Object Upload ID
        assertEquals(s3ObjectDto.getId(), savedUpload.getS3Object().getId());

        return s3ObjectDto;
    }

    private void verifyEmailNotificationAndErrorMessages(User user, List<String> expectedErrors) {
        // Verify that email notification has been triggered and capture the passed arguments
        verify(uploadMailServiceMock).sendUploadErrorMessage(uploadCaptor.capture(), errorMessageCaptor.capture());

        // Assert that email was sent to correct user
        assertEquals(user.getId(), uploadCaptor.getValue().getUser().getId());
        // Assert that error messages match expected
        List<String> errorMessages = errorMessageCaptor.getValue();
        assertEquals(expectedErrors.size(), errorMessages.size(), "\nExpected error messages:\n" + String.join("\n", expectedErrors) + "\n\nActual error messages:\n" + String.join("\n", errorMessages) + "\n");
        for (String expectedError : expectedErrors) {
            assertTrue(errorMessages.contains(expectedError), "Error messages list does not contain the expected error: " + expectedError);
        }
    }

    private void assertNoDatasetsHaveBeenStored(Upload savedUpload) {
        // Assert no Datasets have been created from Upload
        List<Dataset> datasets = datasetService.getDatasetsForUpload(savedUpload);
        assertEquals(0, datasets.size());
    }

    private void assertNothingButTheZipHasBeenStored(Upload savedUpload) {
        // Assert that the only object in S3 storage is the uploaded zip
        List<String> allObjects = testHelperService.listAllS3Objects();
        assertEquals(1, allObjects.size());
        assertEquals(savedUpload.getS3Object().getKey(), allObjects.get(0));

        assertNoDatasetsHaveBeenStored(savedUpload);
    }

    private void assertTempDirGotCleaned() {
        File tempDir = tempDirectoryHelper.getTempBaseDir();
        assertNotNull(tempDir.list());
        assertEquals(0, tempDir.list().length);
    }

    private void assertAutoTagging(List<String> expectedAutoTags, Dataset dataset) {
        assertEquals(expectedAutoTags.size(), dataset.getDatasetTags().size());
        for (DatasetTag datasetTag : dataset.getDatasetTags()) {
            assertTrue(expectedAutoTags.contains(datasetTag.getName()));
        }
    }


    private String getUploadsPathSingle(Long id) {
        return UPLOADS_PATH + "/" + id;
    }

    private String getUploadsPathSingleS3(Long id) {
        return getUploadsPathSingle(id) + UPLOADS_PATH_SINGLE_S3;
    }

    private String getUploadsPathSingleS3Download(Long id) {
        return getUploadsPathSingleS3(id) + UPLOADS_PATH_SINGLE_S3_DOWNLOAD;
    }

}
