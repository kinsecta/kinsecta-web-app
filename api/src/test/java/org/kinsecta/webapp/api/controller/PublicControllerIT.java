package org.kinsecta.webapp.api.controller;

import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.TestHelperService;
import org.kinsecta.webapp.api.v1.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.tuple;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ActiveProfiles("it")
@SpringBootTest
@AutoConfigureMockMvc
class PublicControllerIT {

    private static final String PUBLIC_PATH = "/public";
    private static final String SENSORS_PATH = PUBLIC_PATH + "/sensors";
    private static final String SENSORS_PATH_ID_1 = SENSORS_PATH + "/1";
    private static final String SENSORS_PATH_ID_2 = SENSORS_PATH + "/2";
    private static final String STATS_PATH = PUBLIC_PATH + "/stats";
    private static final String MEASUREMENTS_PATH = STATS_PATH + "/measurements";
    private static final String MEASUREMENTS_YEAR_PATH = MEASUREMENTS_PATH + "/year";
    private static final String MEASUREMENTS_MONTH_PATH = MEASUREMENTS_PATH + "/month";
    private static final String MEASUREMENTS_WEEK_PATH = MEASUREMENTS_PATH + "/week";
    private static final String MEASUREMENTS_DAY_PATH = MEASUREMENTS_PATH + "/day";
    private static final String YEAR_PATH = MEASUREMENTS_PATH + "/year";
    private static final String YEAR_SINGLE_PATH = YEAR_PATH + "/2022";
    private static final String DATASETS_PATH = PUBLIC_PATH + "/datasets";
    private static final String DATASETS_PATH_LATEST = DATASETS_PATH + "/latest";
    private static final String IMAGES_PATH = PUBLIC_PATH + "/images";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TestHelperService testHelperService;


    @Test
    @Transactional
    @Sql("/fixtures/datasets.sql")
    void getLatestDatasets() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH_LATEST))
            .andExpect(status().isOk())
            .andReturn();

        List<PublicDatasetDetailsDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, PublicDatasetDetailsDto.class);
        assertThat(returnedDtoList)
            .extracting(PublicDatasetDetailsDto::getDatasetId)
            .containsExactly(2L, 1L);
    }

    @Test
    void getImageForDataset__wrong_dataset_id() throws Exception {
        String url = IMAGES_PATH + "/2/1";
        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.message").value("DatasetImage with id 1 does not belong to dataset with id 2"));
    }

    @Test
    void getAllSensorsPublic() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(SENSORS_PATH))
            .andExpect(status().isOk())
            .andReturn();

        List<PublicSensorDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, PublicSensorDto.class);
        assertThat(returnedDtoList)
            .extracting(
                PublicSensorDto::getSensorId,
                publicSensorDto -> publicSensorDto.getSensorLocation().getName(),
                publicSensorDto -> publicSensorDto.getSensorLocation().getPostcode(),
                publicSensorDto -> publicSensorDto.getSensorLocation().getCity(),
                publicSensorDto -> publicSensorDto.getSensorLocation().getLatitude(),
                publicSensorDto -> publicSensorDto.getSensorLocation().getLongitude()
            )
            .containsExactlyInAnyOrder(
                tuple(1L, "Berliner Hochschule für Technik (BHT)", 13353, "Berlin", new BigDecimal("52.543762"), new BigDecimal("13.352350")),
                tuple(3L, "Umweltbildungszentrum Listhof", 72770, "Reutlingen", new BigDecimal("48.474556"), new BigDecimal("9.174845")),
                tuple(4L, "Umweltbildungszentrum Listhof", 72770, "Reutlingen", new BigDecimal("48.472000"), new BigDecimal("9.174000"))
            );
    }

    @Test
    @Transactional
    @Sql("/fixtures/datasets.sql")
    void getSensorPublic() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(SENSORS_PATH_ID_1))
            .andExpect(status().isOk())
            .andReturn();

        List<PublicSensorDetailsDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, PublicSensorDetailsDto.class);

        assertThat(returnedDtoList)
            .extracting(
                PublicSensorDetailsDto::getDatasetId,
                PublicSensorDetailsDto::getMeasurementDatetime,
                PublicSensorDetailsDto::getClassification,
                PublicSensorDetailsDto::getImageId
            )
            .containsExactly(
                tuple(2L, LocalDateTime.of(2022, Month.JANUARY, 29, 14, 31, 20), "Hymenoptera; Vespidae; Vespula; Vespula vulgaris", null),
                tuple(1L, LocalDateTime.of(2022, Month.JANUARY, 29, 13, 59, 12), "Diptera; Drosophilidae; Drosophila; Drosophila melanogaster", 1L),
                tuple(3L, LocalDateTime.of(2022, Month.JANUARY, 28, 13, 59, 12), null, null)
            );
    }

    @Test
    void getSensorPublic_notPublic() throws Exception {
        mockMvc.perform(get(SENSORS_PATH_ID_2))
            .andExpect(status().isNotFound())
            .andExpect(jsonPath("$.message").value("Could not find Sensor with id 2. It may not be publicly available."));
        ;
    }

    @Test
    @Transactional
    @Sql("/fixtures/datasets.sql")
    void getDatasetCountForYear() throws Exception {
        mockMvc.perform(get(YEAR_SINGLE_PATH))
            .andExpect(status().isOk())
            .andExpect(content().contentType("text/plain;charset=UTF-8"))
            .andExpect(content().string("4"));
    }

    @Test
    @Transactional
    @Sql("/fixtures/datasets.sql")
    void getTotalDatasetCount_year() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(MEASUREMENTS_YEAR_PATH))
            .andExpect(status().isOk())
            .andReturn();
        MeasurementAmountsDto result = testHelperService.convertResponseBodyToDto(mvcResult, MeasurementAmountsDto.class);
        assertEquals(UnitDto.YEAR, result.getUnit());
        assertThat(result.getData())
            .extracting(CountDto::getKey, CountDto::getCount)
            .containsExactlyInAnyOrder(
                tuple("2022", 4),
                tuple("2021", 1)
            );
    }

    @Test
    @Transactional
    @Sql("/fixtures/datasets.sql")
    void getTotalDatasetCount_month() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(MEASUREMENTS_MONTH_PATH))
            .andExpect(status().isOk())
            .andReturn();
        MeasurementAmountsDto result = testHelperService.convertResponseBodyToDto(mvcResult, MeasurementAmountsDto.class);
        assertEquals(UnitDto.MONTH, result.getUnit());
        assertThat(result.getData())
            .extracting(CountDto::getKey, CountDto::getCount)
            .containsExactlyInAnyOrder(
                tuple("2022-1", 4),
                tuple("2021-1", 1)
            );
    }

    @Test
    @Transactional
    @Sql("/fixtures/datasets.sql")
    void getTotalDatasetCount_week() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(MEASUREMENTS_WEEK_PATH))
            .andExpect(status().isOk())
            .andReturn();

        MeasurementAmountsDto result = testHelperService.convertResponseBodyToDto(mvcResult, MeasurementAmountsDto.class);
        assertEquals(UnitDto.WEEK, result.getUnit());
        assertThat(result.getData())
            .extracting(CountDto::getKey, CountDto::getCount)
            .containsExactlyInAnyOrder(
                tuple("2021-4", 1),
                tuple("2022-2", 1),
                tuple("2022-4", 3)
            );
    }

    @Test
    @Transactional
    @Sql("/fixtures/datasets.sql")
    void getTotalDatasetCount_day() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(MEASUREMENTS_DAY_PATH))
            .andExpect(status().isOk())
            .andReturn();
        MeasurementAmountsDto result = testHelperService.convertResponseBodyToDto(mvcResult, MeasurementAmountsDto.class);
        assertEquals(UnitDto.DAY, result.getUnit());
        assertThat(result.getData())
            .extracting(CountDto::getKey, CountDto::getCount)
            .containsExactlyInAnyOrder(
                tuple("2022-01-29", 2),
                tuple("2022-01-28", 1),
                tuple("2022-01-14", 1),
                tuple("2021-01-28", 1)
            );
    }

}
