package org.kinsecta.webapp.api.util;

import org.junit.jupiter.api.Test;

import java.time.DateTimeException;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class LocalDateTimeConverterTest {

    @Test
    void convert() {
        LocalDateTimeConverter converter = new LocalDateTimeConverter();
        assertEquals(LocalDateTime.of(2021, 12, 2, 10, 37, 15), converter.convert("2021-12-02 10:37:15"));
        assertEquals(LocalDateTime.of(2021, 12, 2, 10, 37, 15), converter.convert("2021-12-02T10:37:15"));
    }

    @Test
    void convert__unsupported_format() {
        LocalDateTimeConverter converter = new LocalDateTimeConverter();
        assertThrows(DateTimeException.class, () -> converter.convert("2021-12-02 10:37"));
        assertThrows(DateTimeException.class, () -> converter.convert("02.12.2012 10:37:15"));
    }

}
