package org.kinsecta.webapp.api.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.TestHelperService;
import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.model.entities.*;
import org.kinsecta.webapp.api.model.mapping.ClassificationDtoMapper;
import org.kinsecta.webapp.api.model.mapping.DatasetImageDtoMapper;
import org.kinsecta.webapp.api.model.mapping.DatasetTagDtoMapper;
import org.kinsecta.webapp.api.model.repositories.DatasetImageRepository;
import org.kinsecta.webapp.api.service.ClassificationService;
import org.kinsecta.webapp.api.service.DatasetService;
import org.kinsecta.webapp.api.service.DatasetTagService;
import org.kinsecta.webapp.api.service.S3ObjectService;
import org.kinsecta.webapp.api.v1.model.*;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("it")
@SpringBootTest
@AutoConfigureMockMvc
class DatasetControllerIT {

    private static final String DATASETS_PATH = "/datasets";

    private static final String DATASETS_PATH_VIEWS = DATASETS_PATH + "/_views";
    private static final String DATASETS_PATH_VIEWS_TABLE = DATASETS_PATH_VIEWS + "/table";
    private static final String DATASETS_PATH_VIEWS_ID_ONLY = DATASETS_PATH_VIEWS + "/id_only";

    private static final String DATASETS_PATH_SINGLE = DATASETS_PATH + "/1";

    private static final String DATASETS_PATH_SINGLE_DATASET_TAGS = DATASETS_PATH_SINGLE + "/dataset_tags";
    private static final String DATASETS_PATH_SINGLE_DATASET_TAGS_SINGLE = DATASETS_PATH_SINGLE_DATASET_TAGS + "/1";

    private static final String DATASETS_PATH_SINGLE_CLASSIFICATIONS = DATASETS_PATH_SINGLE + "/classifications";
    private static final String DATASETS_PATH_SINGLE_CLASSIFICATIONS_SINGLE = DATASETS_PATH_SINGLE_CLASSIFICATIONS + "/1";

    private static final String DATASETS_PATH_SINGLE_SIZE_INSECT = DATASETS_PATH_SINGLE + "/size_insect";

    private static final String DATASETS_PATH_SINGLE_VIDEO_S3 = DATASETS_PATH_SINGLE + "/video_s3_object";
    private static final String DATASETS_PATH_SINGLE_VIDEO_S3_DOWNLOAD = DATASETS_PATH_SINGLE_VIDEO_S3 + "/download";

    private static final String DATASETS_PATH_SINGLE_DATASET_IMAGES = DATASETS_PATH_SINGLE + "/dataset_images";
    private static final String DATASETS_PATH_SINGLE_DATASET_IMAGES_SINGLE = DATASETS_PATH_SINGLE_DATASET_IMAGES + "/1";
    private static final String DATASETS_PATH_SINGLE_DATASET_IMAGES_SINGLE_DOWNLOAD = DATASETS_PATH_SINGLE_DATASET_IMAGES_SINGLE + "/download";

    private static final String DATASETS_PATH_SINGLE_WINGBEAT = DATASETS_PATH_SINGLE + "/measurement_wingbeat";
    private static final String DATASETS_PATH_SINGLE_WINGBEAT_WAV = DATASETS_PATH_SINGLE_WINGBEAT + "/wav_s3_object";
    private static final String DATASETS_PATH_SINGLE_WINGBEAT_WAV_DOWNLOAD = DATASETS_PATH_SINGLE_WINGBEAT_WAV + "/download";
    private static final String DATASETS_PATH_SINGLE_WINGBEAT_IMAGE = DATASETS_PATH_SINGLE_WINGBEAT + "/image_s3_object";
    private static final String DATASETS_PATH_SINGLE_WINGBEAT_IMAGE_DOWNLOAD = DATASETS_PATH_SINGLE_WINGBEAT_IMAGE + "/download";

    private final String resourcePath = "src/test/resources";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TestHelperService testHelperService;

    @Autowired
    private S3ObjectService s3ObjectService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private DatasetService datasetService;

    @Autowired
    private DatasetTagService datasetTagService;

    @Autowired
    private DatasetImageRepository datasetImageRepository;

    @Autowired
    private ClassificationService classificationService;

    @Autowired
    private ClassificationDtoMapper classificationDtoMapper;

    @Autowired
    private DatasetImageDtoMapper datasetImageDtoMapper;

    @Autowired
    private DatasetTagDtoMapper datasetTagDtoMapper;


    // --- Datasets ----------------------------------------------------------------------------------------------------

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllDatasets() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH)).andExpect(status().isOk()).andReturn();
        List<DatasetDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, DatasetDto.class);
        assertEquals(2, returnedDtoList.size());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllDatasets__withSize_valid() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH + "?size=100")).andExpect(status().isOk()).andReturn();
        List<DatasetDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, DatasetDto.class);
        assertEquals(2, returnedDtoList.size());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllDatasets__withSize_invalid() throws Exception {
        mockMvc.perform(get(DATASETS_PATH + "?size=101")).andExpect(status().isBadRequest()).andReturn();

    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllDatasets__withSort_valid() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH + "?sort=id,desc")).andExpect(status().isOk()).andReturn();
        List<DatasetDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, DatasetDto.class);
        assertEquals(2, returnedDtoList.size());
        assertTrue(returnedDtoList.get(0).getId() > returnedDtoList.get(1).getId());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getDataset() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH_SINGLE))
            .andExpect(status().isOk())
            .andReturn();

        DatasetDto datasetDto = testHelperService.convertResponseBodyToDto(mvcResult, DatasetDto.class);
        assertEquals(1L, datasetDto.getId());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void deleteDataset() throws Exception {
        mockMvc.perform(delete(DATASETS_PATH_SINGLE))
            .andExpect(status().isOk());

        // assert that Dataset has been deleted successfully and does no longer exist in the DB
        assertThrows(NotFoundException.class, () -> datasetService.getDataset(1L));
    }


    // --- Dataset Views -----------------------------------------------------------------------------------------------

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllDatasetViews() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH_VIEWS))
            .andExpect(status().isOk())
            .andReturn();

        List<ApiViewDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, ApiViewDto.class);

        assertEquals(2, returnedDtoList.size());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllDatasetsTableView() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH_VIEWS_TABLE)).andExpect(status().isOk()).andReturn();
        List<DatasetTableViewDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, DatasetTableViewDto.class);
        assertEquals(2, returnedDtoList.size());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllDatasetsTableView_with_filters() throws Exception {
        String queryParams = "?taxonomyLevel=GENUS"
            + "&taxonomyFilters=5;8;17";
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH_VIEWS_TABLE + queryParams))
            .andExpect(status().isOk())
            .andReturn();
        List<DatasetTableViewDto> result = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<DatasetTableViewDto>>() {});
        assertEquals(1, result.size());
        assertEquals(2L, result.get(0).getId());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllDatasetsTableView_missing_taxonomy_level() throws Exception {
        String queryParams = "?taxonomyFilters=5;8;17";
        mockMvc.perform(get(DATASETS_PATH_VIEWS_TABLE + queryParams))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.message").value("There are taxonomy filters present, but no taxonomy level has been chosen."));
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllDatasetsTableView_invalid_sensor_location_string() throws Exception {
        String queryParams = "?sensorLocationFilters=-1";
        mockMvc.perform(get(DATASETS_PATH_VIEWS_TABLE + queryParams))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.message").value("Sensor Location IDs are not passed as a semicolon separated list of positive integers."));
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllDatasetsTableView_invalid_tag_string() throws Exception {
        String queryParams = "?tagFilterList[0]=-3"
            + "&tagFilterList[1]=2;";
        mockMvc.perform(get(DATASETS_PATH_VIEWS_TABLE + queryParams))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.message").value("DatasetTag IDs are not passed as semicolon separated lists of positive or negative integers or there is an empty list among them"));
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllDatasetsTableView_empty_tag_string() throws Exception {
        String queryParams = "?tagFilterList[0]=-3"
            + "&tagFilterList[1]=";
        mockMvc.perform(get(DATASETS_PATH_VIEWS_TABLE + queryParams))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.message").value("DatasetTag IDs are not passed as semicolon separated lists of positive or negative integers or there is an empty list among them"));
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllDatasetsIdOnlyView() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH_VIEWS_ID_ONLY)
                .param("measurementStartDate", "2022-01-29")
                .param("measurementEndDate", "2022-01-29")
                .param("sensorFilters", "1"))
            .andExpect(status().isOk())
            .andReturn();

        IdListDto idListDto = testHelperService.convertResponseBodyToDto(mvcResult, IdListDto.class);
        assertEquals(2, idListDto.getIds().size());
        assertEquals(1L, idListDto.getIds().get(0));
        assertEquals(2L, idListDto.getIds().get(1));
    }


    // --- DatasetTags subresource of Dataset --------------------------------------------------------------------------

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void createDatasetTagForDataset() throws Exception {
        DatasetTagDto datasetTagDto = new DatasetTagDto();
        String tagName = "Test Tag";
        datasetTagDto.setName(tagName);
        datasetTagDto.setAutoTag(false);

        String contentString = objectMapper.writeValueAsString(datasetTagDto);
        mockMvc.perform(post(DATASETS_PATH_SINGLE_DATASET_TAGS)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(contentString))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.id").isNumber())
            .andExpect(jsonPath("$.name").value(tagName));
    }

    @Test
    @Transactional(readOnly = true)
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllDatasetTagsForDataset() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH_SINGLE_DATASET_TAGS))
            .andExpect(status().isOk())
            .andReturn();

        List<DatasetTagDto> datasetTagList = testHelperService.convertResponseBodyToDtoList(mvcResult, DatasetTagDto.class);
        assertEquals(2, datasetTagList.size());
    }

    @Test
    @Transactional(readOnly = true)
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getDatasetTagForDataset() throws Exception {
        mockMvc.perform(get(DATASETS_PATH_SINGLE_DATASET_TAGS_SINGLE))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").isNumber())
            .andExpect(jsonPath("$.auto_tag").isBoolean())
            .andExpect(jsonPath("$.name").isString());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void linkDatasetTagToDataset() throws Exception {
        DatasetTag datasetTag = new DatasetTag();
        String tagName = "My Tag";
        datasetTag.setName(tagName);
        DatasetTag createdDatasetTag = datasetTagService.createDatasetTag(datasetTag);
        mockMvc.perform(put(DATASETS_PATH_SINGLE_DATASET_TAGS + "/" + createdDatasetTag.getId()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(createdDatasetTag.getId()))
            .andExpect(jsonPath("$.auto_tag").value(false))
            .andExpect(jsonPath("$.name").value(tagName));

        Dataset datasetAfter = datasetService.getDataset(1L);
        Set<DatasetTag> datasetTags = datasetAfter.getDatasetTags();
        assertTrue(datasetTags.contains(createdDatasetTag));
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void unlinkDatasetTagFromDataset() throws Exception {
        DatasetTag datasetTag = new DatasetTag();
        String tagName = "My Tag";
        datasetTag.setName(tagName);
        DatasetTag createdDatasetTag = datasetService.createDatasetTagForDataset(1L, datasetTag);
        mockMvc.perform(delete(DATASETS_PATH_SINGLE_DATASET_TAGS + "/" + createdDatasetTag.getId()))
            .andExpect(status().isOk());
    }


    // --- Classification subresource of Dataset -----------------------------------------------------------------------

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void createClassificationForDataset() throws Exception {
        Classification classification = classificationService.getClassification(1L);
        ClassificationDto classificationDto = classificationDtoMapper.classificationToClassificationDto(classification);
        classificationDto.setId(JsonNullable.undefined());
        classificationDto.setDataset(JsonNullable.undefined());

        String contentString = objectMapper.writeValueAsString(classificationDto);
        mockMvc.perform(post(DATASETS_PATH_SINGLE_CLASSIFICATIONS)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(contentString))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.id").isNumber())
            .andExpect(jsonPath("$.dataset.id").value(1));
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void createClassificationForDataset_but_species_not_set() throws Exception {
        Classification classification = classificationService.getClassification(1L);
        ClassificationDto classificationDto = classificationDtoMapper.classificationToClassificationDto(classification);
        classificationDto.setId(JsonNullable.undefined());
        classificationDto.setDataset(JsonNullable.undefined());
        classificationDto.setInsectSpecies(JsonNullable.of(null));

        String contentString = objectMapper.writeValueAsString(classificationDto);
        MvcResult mvcResult = mockMvc.perform(post(DATASETS_PATH_SINGLE_CLASSIFICATIONS)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(contentString))
            .andExpect(status().isCreated())
            .andReturn();

        ClassificationDto result = testHelperService.convertResponseBodyToDto(mvcResult, ClassificationDto.class);
        assertNotNull(result.getId());
        assertEquals(1, result.getDataset().get().getId());
        assertEquals(classification.getInsectOrder().getId(), result.getInsectOrder().getId().get());
        assertEquals(classification.getInsectFamily().getId(), result.getInsectFamily().get().getId().get());
        assertEquals(classification.getInsectGenus().getId(), result.getInsectGenus().get().getId().get());
        assertNull(result.getInsectSpecies().get());
    }

    @Test
    @Transactional(readOnly = true)
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllClassificationsForDataset() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH_SINGLE_CLASSIFICATIONS))
            .andExpect(status().isOk())
            .andReturn();

        List<ClassificationDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, ClassificationDto.class);

        assertEquals(1, returnedDtoList.size());

        // #79 double check for duplicates
        Dataset dataset = datasetService.getDataset(1L);
        assertEquals(1, dataset.getClassifications().size());
    }

    @Test
    @Transactional(readOnly = true)
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getClassificationForDataset() throws Exception {
        mockMvc.perform(get(DATASETS_PATH_SINGLE_CLASSIFICATIONS_SINGLE))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(1))
            .andExpect(jsonPath("$.dataset.id").value(1));
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void linkClassificationToDataset__already_linked() throws Exception {
        mockMvc.perform(put(DATASETS_PATH_SINGLE_CLASSIFICATIONS_SINGLE)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.dataset.id").value(1));
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void linkClassificationToDataset__new_link() throws Exception {
        mockMvc.perform(put(DATASETS_PATH_SINGLE_CLASSIFICATIONS + "/3")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.dataset.id").value(1));
    }

    @Test
    @Transactional(readOnly = true)
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void linkClassificationToDataset__classification_already_linked() throws Exception {
        mockMvc.perform(put(DATASETS_PATH_SINGLE_CLASSIFICATIONS + "/2")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.message").value("A Classification which already belongs to another Dataset, DatasetImage or MeasurementWingbeat cannot be linked with this Dataset."));
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void unlinkClassificationFromDataset() throws Exception {
        mockMvc.perform(delete(DATASETS_PATH_SINGLE_CLASSIFICATIONS_SINGLE))
            .andExpect(status().isOk());

        assertThrows(NotFoundException.class, () -> classificationService.getClassification(1L));
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void linkClassificationToDataset__edit_classification_remove_insect_family() throws Exception {
        Classification classification = classificationService.getClassification(1L);
        ClassificationDto classificationDto = classificationDtoMapper.classificationToClassificationDto(classification);
        classificationDto.setId(JsonNullable.undefined());
        classificationDto.setDataset(JsonNullable.undefined());
        classificationDto.setInsectFamily(JsonNullable.of(null));
        classificationDto.setInsectGenus(JsonNullable.of(null));
        classificationDto.setInsectSpecies(JsonNullable.of(null));

        String contentString = objectMapper.writeValueAsString(classificationDto);
        MvcResult mvcResult = mockMvc.perform(put(DATASETS_PATH_SINGLE_CLASSIFICATIONS_SINGLE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(contentString))
            .andExpect(status().isOk())
            .andReturn();

        ClassificationDto result = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), ClassificationDto.class);
        assertNotNull(result.getInsectOrder());
        assertNull(result.getInsectFamily().get());
        assertNull(result.getInsectGenus().get());
        assertNull(result.getInsectSpecies().get());
    }

    // --- SizeInsect subresource of Dataset ---------------------------------------------------------------------------

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void editSizeInsectOfDataset() throws Exception {
        InsectSizeDto insectSizeDto = new InsectSizeDto();
        insectSizeDto.setWidth(BigDecimal.valueOf(4.5));
        insectSizeDto.setLength(BigDecimal.valueOf(8.7));

        String contentString = objectMapper.writeValueAsString(insectSizeDto);
        mockMvc.perform(put(DATASETS_PATH_SINGLE_SIZE_INSECT)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(contentString))
            .andExpect(status().isOk());

        Dataset dataset = datasetService.getDataset(1L);
        assertEquals(BigDecimal.valueOf(4.5), dataset.getMeasurementSizeInsectWidth());
        assertEquals(BigDecimal.valueOf(8.7), dataset.getMeasurementSizeInsectLength());
    }


    // --- VideoS3Object subresource of Dataset ------------------------------------------------------------------------

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getVideoS3ObjectForDataset() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH_SINGLE))
            .andExpect(status().isOk())
            .andReturn();

        DatasetDto datasetDto = testHelperService.convertResponseBodyToDto(mvcResult, DatasetDto.class);

        MvcResult s3MvcResult = mockMvc.perform(get(DATASETS_PATH_SINGLE_VIDEO_S3))
            .andExpect(status().isOk())
            .andReturn();

        S3ObjectDto videoS3ObjectDto = testHelperService.convertResponseBodyToDto(s3MvcResult, S3ObjectDto.class);

        assertEquals(videoS3ObjectDto.getId(), datasetDto.getVideoS3Object().get().getId());
        assertEquals("video/h264", videoS3ObjectDto.getMediaType());
        assertEquals("41d2cd7e-8103-11ec-893a-b827eb353b23.h264", videoS3ObjectDto.getFilename());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void deleteVideoS3ObjectFromDataset() throws Exception {
        mockMvc.perform(delete(DATASETS_PATH_SINGLE_VIDEO_S3)).andExpect(status().isNotImplemented());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void downloadVideoS3ObjectForDataset() throws Exception {
        String filename = "video.mp4";
        String mediaType = "video/mp4";
        File inputFile = Paths.get(resourcePath, "s3", filename).toFile();

        // Update Dataset 1 (which per IT test data already has a video S3 Object attached), so we don't have to manually create a Dataset - which is a nightmare
        S3Object s3Object = s3ObjectService.storeFile(inputFile);
        Dataset dataset = datasetService.getDataset(1L);
        dataset.setVideoS3Object(s3Object);
        datasetService.save(dataset);

        testHelperService.fileDownloadTest(mockMvc, DATASETS_PATH_SINGLE_VIDEO_S3_DOWNLOAD, inputFile, mediaType, "inline", true);
    }


    // --- DatasetImages subresource of Dataset ------------------------------------------------------------------------

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllDatasetImagesForDataset() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH_SINGLE_DATASET_IMAGES))
            .andExpect(status().isOk())
            .andReturn();

        List<DatasetImageDto> imageList = testHelperService.convertResponseBodyToDtoList(mvcResult, DatasetImageDto.class);
        assertEquals(2, imageList.size());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getDatasetImageForDataset() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH_SINGLE))
            .andExpect(status().isOk())
            .andReturn();

        DatasetDto datasetDto = testHelperService.convertResponseBodyToDto(mvcResult, DatasetDto.class);

        MvcResult s3MvcResult = mockMvc.perform(get(DATASETS_PATH_SINGLE_DATASET_IMAGES_SINGLE))
            .andExpect(status().isOk())
            .andReturn();

        S3ObjectDto imageS3Object = testHelperService.convertResponseBodyToDto(s3MvcResult, S3ObjectDto.class);

        assertEquals("image/jpeg", imageS3Object.getMediaType());
        assertEquals(3, imageS3Object.getId());
        assertEquals("4394aa1a-8103-11ec-893a-b827eb353b23.png", imageS3Object.getFilename());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void linkDatasetImageToDataset() throws Exception {
        mockMvc.perform(put(DATASETS_PATH_SINGLE_DATASET_IMAGES_SINGLE)).andExpect(status().isNotImplemented());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void deleteDatasetImageFromDataset() throws Exception {
        mockMvc.perform(delete(DATASETS_PATH_SINGLE_DATASET_IMAGES_SINGLE)).andExpect(status().isNotImplemented());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void downloadDatasetImageForDataset() throws Exception {
        String filename = "test-image.jpg";
        String mediaType = "image/jpeg";
        File inputFile = Paths.get(resourcePath, "s3", filename).toFile();

        // Update Dataset 1 (which per IT test data already has a DatasetImage attached), so we don't have to manually create a Dataset - which is a nightmare
        Dataset dataset = datasetService.getDataset(1L);
        S3Object s3Object = s3ObjectService.storeFile(inputFile);
        DatasetImageDto datasetImageDto = new DatasetImageDto();
        datasetImageDto.setS3Object(new IdObject().id(s3Object.getId()));
        DatasetImage datasetImage = datasetImageDtoMapper.datasetImageDtoToDatasetImage(datasetImageDto);
        dataset.addDatasetImage(datasetImage);
        dataset = datasetService.save(dataset);
        long lastDatasetImageId = dataset.getDatasetImages().stream().map(DatasetImage::getId).max(Long::compareTo).get();

        testHelperService.fileDownloadTest(mockMvc, DATASETS_PATH_SINGLE_DATASET_IMAGES + "/" + lastDatasetImageId + "/download", inputFile, mediaType, "inline", true);
    }


    // --- MeasurementWingbeat subresource of Dataset ------------------------------------------------------------------

    @Test
    @Transactional(readOnly = true)
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getMeasurementWingbeatForDataset() throws Exception {
        mockMvc.perform(get(DATASETS_PATH + "/2/measurement_wingbeat"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(1));
    }

    @Test
    @Transactional(readOnly = true)
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getMeasurementWingbeatForDataset__no_wingbeat_data() throws Exception {
        mockMvc.perform(get(DATASETS_PATH_SINGLE_WINGBEAT))
            .andExpect(status().isNotFound())
            .andExpect(jsonPath("$.message").value("No Wingbeat Measurement found for Dataset with id 1"));
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void deleteMeasurementWingbeatFromDataset() throws Exception {
        mockMvc.perform(delete(DATASETS_PATH_SINGLE_WINGBEAT)).andExpect(status().isNotImplemented());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getMeasurementWingbeatWavForDataset() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH + "/2"))
            .andExpect(status().isOk())
            .andReturn();

        DatasetDto datasetDto = testHelperService.convertResponseBodyToDto(mvcResult, DatasetDto.class);

        MvcResult s3MvcResult = mockMvc.perform(get(DATASETS_PATH + "/2/measurement_wingbeat/wav_s3_object"))
            .andExpect(status().isOk())
            .andReturn();

        S3ObjectDto wavS3Object = testHelperService.convertResponseBodyToDto(s3MvcResult, S3ObjectDto.class);

        assertEquals("audio/wave", wavS3Object.getMediaType());
        assertEquals(6, wavS3Object.getId());
        assertEquals("be9efd38-8107-11ec-893a-b827eb353b23.wav", wavS3Object.getFilename());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void downloadMeasurementWingbeatWavAndImageForDataset() throws Exception {
        String wavFilename = "wingbeat.wav";
        String wavMediaType = "audio/wave";
        File wavInputFile = Paths.get(resourcePath, "s3", wavFilename).toFile();
        S3Object wavS3Object = s3ObjectService.storeFile(wavInputFile);

        String imageFilename = "wingbeat.png";
        String imageMediaType = "image/png";
        File imageInputFile = Paths.get(resourcePath, "s3", imageFilename).toFile();
        S3Object imageS3Object = s3ObjectService.storeFile(imageInputFile);

        // Update Dataset 1 (which per IT test data already has a DatasetImage attached), so we don't have to manually create a Dataset - which is a nightmare
        Dataset dataset = datasetService.getDataset(1L);
        MeasurementWingbeat measurementWingbeat = new MeasurementWingbeat();
        measurementWingbeat.setWavS3Object(wavS3Object);
        measurementWingbeat.setImageS3Object(imageS3Object);
        measurementWingbeat.setSampleRate(96000);
        dataset.setMeasurementWingbeat(measurementWingbeat);
        datasetService.save(dataset);

        testHelperService.fileDownloadTest(mockMvc, DATASETS_PATH_SINGLE_WINGBEAT_WAV_DOWNLOAD, wavInputFile, wavMediaType, "inline", false);
        testHelperService.fileDownloadTest(mockMvc, DATASETS_PATH_SINGLE_WINGBEAT_IMAGE_DOWNLOAD, imageInputFile, imageMediaType, "inline", true);
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getMeasurementWingbeatImageForDataset() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(DATASETS_PATH + "/2"))
            .andExpect(status().isOk())
            .andReturn();

        DatasetDto datasetDto = testHelperService.convertResponseBodyToDto(mvcResult, DatasetDto.class);

        MvcResult s3MvcResult = mockMvc.perform(get(DATASETS_PATH + "/2/measurement_wingbeat/image_s3_object"))
            .andExpect(status().isOk())
            .andReturn();

        S3ObjectDto imageS3Object = testHelperService.convertResponseBodyToDto(s3MvcResult, S3ObjectDto.class);

        assertEquals("image/png", imageS3Object.getMediaType());
        assertEquals(7, imageS3Object.getId());
        assertEquals("c0ff562c-8107-11ec-893a-b827eb353b23.png", imageS3Object.getFilename());
    }

}
