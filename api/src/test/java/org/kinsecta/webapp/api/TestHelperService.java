package org.kinsecta.webapp.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.kinsecta.webapp.api.model.entities.Dataset;
import org.kinsecta.webapp.api.model.entities.S3Object;
import org.kinsecta.webapp.api.s3.S3Client;
import org.kinsecta.webapp.api.s3.S3ClientException;
import org.kinsecta.webapp.api.s3.S3ClientStorageService;
import org.kinsecta.webapp.api.s3.StorageException;
import org.kinsecta.webapp.api.service.DatasetService;
import org.kinsecta.webapp.api.util.TempDirectoryHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@Service
public class TestHelperService {

    private final TempDirectoryHelper tempDirectoryHelper;
    private final S3Client s3Client;
    private final S3ClientStorageService s3ClientStorageService;
    private final DatasetService datasetService;
    private final ObjectMapper objectMapper;

    @Value("${app.s3storage.bucket-name.list}")
    private String[] s3BucketNames;

    private final File testFile = Paths.get("src/test/resources/s3/test-image.jpg").toFile();


    public TestHelperService(TempDirectoryHelper tempDirectoryHelper, S3Client s3Client, S3ClientStorageService s3ClientStorageService, DatasetService datasetService, ObjectMapper objectMapper) {
        this.tempDirectoryHelper = tempDirectoryHelper;
        this.s3Client = s3Client;
        this.s3ClientStorageService = s3ClientStorageService;
        this.datasetService = datasetService;
        this.objectMapper = objectMapper;
    }


    public void cleanWorkingDirectory() {
        tempDirectoryHelper.cleanTempBaseDirectory();
    }

    public boolean existsS3ObjectInBucketStartingWith(String bucketName, String prefix) throws S3ClientException {
        return s3Client.hasObjectsThatStartWith(bucketName, prefix);
    }

    public void cleanS3Buckets() throws S3ClientException {
        for (String bucketName : s3BucketNames) {
            List<String> allObjectKeys = s3Client.listAllObjects(bucketName);
            for (String s3Key : allObjectKeys) {
                s3Client.deleteObject(bucketName, s3Key);
            }
        }
    }

    public List<String> listAllS3Objects() {
        List<String> result = new ArrayList<>();
        for (String bucketName : s3BucketNames) {
            List<String> allObjectKeys = s3Client.listAllObjects(bucketName);
            result.addAll(allObjectKeys);
        }
        return result;
    }

    public void uploadTestFilesAsS3Object(S3Object... s3Objects) throws IOException, StorageException {
        for (S3Object s3Object : s3Objects) {
            uploadTestFileAsS3Object(s3Object);
        }
    }

    public void uploadTestFileAsS3Object(S3Object s3Object) throws IOException, StorageException {
        try (FileInputStream fileInputStream = new FileInputStream(testFile)) {
            s3ClientStorageService.store(s3Object.getBucket(), s3Object.getKey(), fileInputStream, testFile.length());
        }
    }

    public void fileDownloadTest(MockMvc mockMvc, String restEndpoint, File testFile, String expectedMediaType, String expectedContentDisposition, boolean cleanS3) throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(restEndpoint))
            .andExpect(status().isOk())
            .andExpect(header().longValue(HttpHeaders.CONTENT_LENGTH, testFile.length()))
            .andExpect(header().string(HttpHeaders.CONTENT_DISPOSITION, expectedContentDisposition))
            .andExpect(header().string(HttpHeaders.CONTENT_TYPE, expectedMediaType))
            .andExpect(content().contentType(MediaType.valueOf(expectedMediaType)))
            .andReturn();

        try {
            assertTrue(mvcResult.getResponse().getContentAsByteArray().length > 0);
            File downloadedFile = new File(tempDirectoryHelper.getTempBaseDir(), testFile.getName());
            byte[] contentAsByteArray = mvcResult.getResponse().getContentAsByteArray();
            FileUtils.writeByteArrayToFile(downloadedFile, contentAsByteArray);

            assertTrue(downloadedFile.exists());
            assertEquals(testFile.length(), downloadedFile.length());

        } finally {
            cleanWorkingDirectory();
            if (cleanS3) {
                cleanS3Buckets();
            }
        }
    }


    public <T> T convertResponseBodyToDto(MvcResult mvcResult, Class<T> targetDto) throws UnsupportedEncodingException, JsonProcessingException {
        String responseContent = mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8);
        return objectMapper.readValue(responseContent, targetDto);
    }

    public <T> List<T> convertResponseBodyToDtoList(MvcResult mvcResult, Class<T> targetDto) throws UnsupportedEncodingException, JsonProcessingException {
        String responseContent = mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8);
        return objectMapper.readValue(responseContent, objectMapper.getTypeFactory().constructCollectionType(List.class, targetDto));
    }


    public <T> void assertActualListContainsAllExpectedItemsIgnoreOrder(List<T> expectedItems, List<T> actualItems) {
        assertEquals(expectedItems.size(), actualItems.size());
        assertTrue(actualItems.containsAll(expectedItems));
    }

    public void clearAllHashesForUpload() {
        for (Dataset dataset : datasetService.getAllDatasets()) {
            dataset.setMd5Hash(null);
            datasetService.save(dataset);
        }
    }

}
