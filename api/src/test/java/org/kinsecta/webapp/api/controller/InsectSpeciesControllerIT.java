package org.kinsecta.webapp.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.TestHelperService;
import org.kinsecta.webapp.api.model.entities.InsectSpecies;
import org.kinsecta.webapp.api.service.InsectGenusService;
import org.kinsecta.webapp.api.service.InsectSpeciesService;
import org.kinsecta.webapp.api.v1.model.InsectSpeciesDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("it")
@SpringBootTest
@AutoConfigureMockMvc
class InsectSpeciesControllerIT {

    private static final String INSECT_SPECIES_PATH = "/insect_species";
    private static final String INSECT_SPECIES_PATH_SEARCH = INSECT_SPECIES_PATH + "/search";
    private static final String INSECT_SPECIES_PATH_SINGLE = INSECT_SPECIES_PATH + "/1";

    private final String testName = "Test InsectSpecies";
    private final String testNameUrlEncoded = URLEncoder.encode(testName, StandardCharsets.UTF_8.toString());

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TestHelperService testHelperService;

    @Autowired
    private InsectSpeciesService insectSpeciesService;

    @Autowired
    private InsectGenusService insectGenusService;


    InsectSpeciesControllerIT() throws UnsupportedEncodingException {}


    // Helper Function
    private InsectSpecies createNewInsectSpecies() throws Exception {
        InsectSpecies insectSpecies = new InsectSpecies();
        insectSpecies.setName(testName);
        insectSpecies.setGbifId(99991L);
        insectSpecies.setGenus(insectGenusService.getInsectGenus(1L));

        return insectSpeciesService.save(insectSpecies);
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllInsectSpecies() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(INSECT_SPECIES_PATH))
            .andExpect(status().isOk())
            .andReturn();

        List<InsectSpeciesDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, InsectSpeciesDto.class);
        assertEquals(24, returnedDtoList.size());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void searchInsectSpecies__query_exact_match() throws Exception {
        InsectSpecies created = createNewInsectSpecies();

        MvcResult mvcResult = mockMvc.perform(get(INSECT_SPECIES_PATH_SEARCH + "?query=" + testNameUrlEncoded))
            .andExpect(status().isOk())
            .andReturn();

        List<InsectSpeciesDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, InsectSpeciesDto.class);
        assertEquals(1, returnedDtoList.size());
        InsectSpeciesDto resultDto = returnedDtoList.get(0);
        assertEquals(created.getName(), resultDto.getName());
        assertTrue(resultDto.getId().isPresent());
        assertEquals(created.getId(), resultDto.getId().get());
        assertTrue(resultDto.getGbifId().isPresent());
        assertEquals(created.getGbifId(), resultDto.getGbifId().get());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void searchInsectSpecies__query_partial_match_ignorecase() throws Exception {
        InsectSpecies created = createNewInsectSpecies();

        MvcResult mvcResult = mockMvc.perform(get(INSECT_SPECIES_PATH_SEARCH + "?query=test"))
            .andExpect(status().isOk())
            .andReturn();

        List<InsectSpeciesDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, InsectSpeciesDto.class);
        assertEquals(1, returnedDtoList.size());
        InsectSpeciesDto resultDto = returnedDtoList.get(0);
        assertEquals(created.getName(), resultDto.getName());
        assertTrue(resultDto.getId().isPresent());
        assertEquals(created.getId(), resultDto.getId().get());
        assertTrue(resultDto.getGbifId().isPresent());
        assertEquals(created.getGbifId(), resultDto.getGbifId().get());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getInsectSpecies() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(INSECT_SPECIES_PATH_SINGLE))
            .andExpect(status().isOk())
            .andReturn();

        InsectSpeciesDto resultDto = testHelperService.convertResponseBodyToDto(mvcResult, InsectSpeciesDto.class);
        assertEquals(1L, resultDto.getId().get());
    }

}
