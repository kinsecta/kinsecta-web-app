package org.kinsecta.webapp.api.controller;

import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.exception.unchecked.ForbiddenException;
import org.kinsecta.webapp.api.model.entities.UserRole;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;


@ActiveProfiles("it")
@SpringBootTest
@AutoConfigureMockMvc
class BaseControllerIT extends BaseController {

    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getUserFromContext__adminUser() {
        assertEquals("t.fischer", contextUserService.getUsername());
        assertEquals(UserRole.ADMIN, contextUserService.getUserRole());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void isCurrentUserAdmin__true() {
        assertTrue(isCurrentUserAdmin());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void isCurrentUserAdmin__false() {
        assertFalse(isCurrentUserAdmin());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void failIfNonAdminUserTriesToQueryArbitraryUserWithId__okay() {
        assertDoesNotThrow(() -> failIfNonAdminUserTriesToQueryArbitraryUserWithId(3L, "test context"));
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void failIfNonAdminUserTriesToQueryArbitraryUserWithId__failed() {
        assertThrows(ForbiddenException.class, () -> failIfNonAdminUserTriesToQueryArbitraryUserWithId(1L, "test context"));
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void failIfCurrentUserTriesToQueryArbitraryUserWithId__okay() {
        assertDoesNotThrow(() -> failIfCurrentUserTriesToQueryArbitraryUserWithId(3L));
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void failIfCurrentUserTriesToQueryArbitraryUserWithId__failed() {
        assertThrows(ForbiddenException.class, () -> failIfNonAdminUserTriesToQueryArbitraryUserWithId(1L, "test context"));
    }

}
