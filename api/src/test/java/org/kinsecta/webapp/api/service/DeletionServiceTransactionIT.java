package org.kinsecta.webapp.api.service;

import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.kinsecta.webapp.api.TestHelperService;
import org.kinsecta.webapp.api.model.entities.*;
import org.kinsecta.webapp.api.model.repositories.ExportRepository;
import org.kinsecta.webapp.api.model.repositories.UploadRepository;
import org.kinsecta.webapp.api.s3.S3ClientException;
import org.kinsecta.webapp.api.s3.S3ClientStorageService;
import org.kinsecta.webapp.api.s3.StorageException;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;


/**
 * This test class must not have a @Transactional annotation, because it would interfere with the @Transactional annotations
 * that are tested within this test class. This makes the assertions somewhat more inconvenient.
 */
@ActiveProfiles("it")
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DeletionServiceTransactionIT {

    private final File testFile = Paths.get("src/test/resources/s3/test.txt").toFile();

    @Autowired
    private Flyway flyway;

    @Autowired
    private UserService userService;

    @Autowired
    private UploadService uploadService;

    @Autowired
    private DatasetService datasetService;

    @Autowired
    private SensorService sensorService;

    @Autowired
    private S3ObjectService s3ObjectService;

    @Autowired
    private DatasetTagService datasetTagService;

    @Autowired
    private ExportService exportService;

    @Autowired
    private ExportFileService exportFileService;

    @Autowired
    private InsectSpeciesService insectSpeciesService;

    @Autowired
    private TestHelperService testHelperService;

    @Autowired
    private DatasetImageService datasetImageService;

    @SpyBean
    private S3ClientStorageService s3ClientStorageServiceSpy;

    @Autowired
    private MeasurementWingbeatService measurementWingbeatService;

    @Autowired
    private ClassificationService classificationService;

    @Autowired
    private DeletionService deletionService;

    @Autowired
    private UploadRepository uploadRepository;

    @Autowired
    private ExportRepository exportRepository;

    private Long datasetTagId1;
    private Long datasetTagId2;


    @BeforeAll
    public void setUp() {
        DatasetTag datasetTag1 = new DatasetTag();
        datasetTag1.setName("Test-Tag-1");
        datasetTagService.createDatasetTag(datasetTag1);
        datasetTagId1 = datasetTag1.getId();

        DatasetTag datasetTag2 = new DatasetTag();
        datasetTag2.setName("Test-Tag-2");
        datasetTagService.createDatasetTag(datasetTag2);
        datasetTagId2 = datasetTag2.getId();
    }

    @AfterAll
    void cleanUp() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    void testDeleteDataset__dont_delete_upload() throws IOException, StorageException, S3ClientException {
        User user = userService.getUser(2L);
        try {
            Upload upload = uploadService.createUploadForUser(user);
            upload.setS3Object(s3ObjectService.storeFile(testFile));
            upload.addDataset(createNewDataset());
            upload.addDataset(createNewDataset());
            Upload savedUpload = uploadService.saveUpload(upload);
            Dataset datasetToDelete = savedUpload.getDatasets().get(0);
            Dataset datasetToKeep = savedUpload.getDatasets().get(1);

            // assert these have been saved
            assertTrue(datasetService.getOptionalDataset(datasetToKeep.getId()).isPresent());
            assertTrue(datasetService.getOptionalDataset(datasetToDelete.getId()).isPresent());

            Long uploadId = savedUpload.getId();
            List<Long> datasetIdsToKeep = new ArrayList<>();
            List<Long> datasetImageIdsToKeep = new ArrayList<>();
            List<Long> measurementWingbeatIdsToKeep = new ArrayList<>();
            List<Long> classificationIdsToKeep = new ArrayList<>();
            List<S3Object> s3ObjectsToKeep = new ArrayList<>();
            collectIdsAndS3Objects(List.of(datasetToKeep), datasetIdsToKeep, datasetImageIdsToKeep, measurementWingbeatIdsToKeep, classificationIdsToKeep, s3ObjectsToKeep);

            // upload is not deleted so expect the Upload S3Object to be kept
            s3ObjectsToKeep.add(savedUpload.getS3Object());

            List<Long> datasetIdsToDelete = new ArrayList<>();
            List<Long> datasetImageIdsToDelete = new ArrayList<>();
            List<Long> measurementWingbeatIdsToDelete = new ArrayList<>();
            List<Long> classificationIdsToDelete = new ArrayList<>();
            List<S3Object> s3ObjectsToDelete = new ArrayList<>();
            collectIdsAndS3Objects(List.of(datasetToDelete), datasetIdsToDelete, datasetImageIdsToDelete, measurementWingbeatIdsToDelete, classificationIdsToDelete, s3ObjectsToDelete);

            // assert these have been uploaded
            List<String> allExpectedS3Objects = new ArrayList<>();
            allExpectedS3Objects.addAll(s3ObjectsToKeep.stream().map(S3Object::getKey).toList());
            allExpectedS3Objects.addAll(s3ObjectsToDelete.stream().map(S3Object::getKey).toList());
            List<String> allActualS3Objects = testHelperService.listAllS3Objects();
            assertEquals(allExpectedS3Objects.stream().sorted().collect(Collectors.joining()), allActualS3Objects.stream().sorted().collect(Collectors.joining()));

            // delete one dataset
            deletionService.deleteDataset(datasetToDelete.getId());

            // upload and other dataset should still exist
            assertAllExist(uploadId, datasetIdsToKeep, datasetImageIdsToKeep, measurementWingbeatIdsToKeep, classificationIdsToKeep, s3ObjectsToKeep);
            // all entities from other dataset should be deleted
            assertNoneExist(null, datasetIdsToDelete, datasetImageIdsToDelete, measurementWingbeatIdsToDelete, classificationIdsToDelete, s3ObjectsToDelete);

            // assert s3 files match s3ObjectsToKeep
            allExpectedS3Objects = s3ObjectsToKeep.stream().map(S3Object::getKey).toList();
            allActualS3Objects = testHelperService.listAllS3Objects();
            assertEquals(allExpectedS3Objects.stream().sorted().collect(Collectors.joining()), allActualS3Objects.stream().sorted().collect(Collectors.joining()));
            assertDatasetTagsStillExist();
            assertInsectsStillExist();

        } finally {
            testHelperService.cleanS3Buckets();
        }
    }

    @Test
    void testDeleteDataset__also_delete_upload() throws IOException, StorageException, S3ClientException {
        User user = userService.getUser(2L);
        try {
            Upload upload = uploadService.createUploadForUser(user);
            upload.setS3Object(s3ObjectService.storeFile(testFile));
            upload.addDataset(createNewDataset());
            Upload savedUpload = uploadService.saveUpload(upload);
            Dataset datasetToDelete = savedUpload.getDatasets().get(0);

            // assert this has been saved
            assertTrue(datasetService.getOptionalDataset(datasetToDelete.getId()).isPresent());

            Long uploadId = savedUpload.getId();
            List<Long> datasetIdsToDelete = new ArrayList<>();
            List<Long> datasetImageIdsToDelete = new ArrayList<>();
            List<Long> measurementWingbeatIdsToDelete = new ArrayList<>();
            List<Long> classificationIdsToDelete = new ArrayList<>();
            List<S3Object> s3ObjectsToDelete = new ArrayList<>();
            collectIdsAndS3Objects(List.of(datasetToDelete), datasetIdsToDelete, datasetImageIdsToDelete, measurementWingbeatIdsToDelete, classificationIdsToDelete, s3ObjectsToDelete);

            // upload should get deleted so expect the Upload S3Object to be deleted as well
            s3ObjectsToDelete.add(savedUpload.getS3Object());

            // assert these have been uploaded
            List<String> allExpectedS3Objects = new ArrayList<>(s3ObjectsToDelete.stream().map(S3Object::getKey).toList());
            List<String> allActualS3Objects = testHelperService.listAllS3Objects();
            assertEquals(allExpectedS3Objects.stream().sorted().collect(Collectors.joining()), allActualS3Objects.stream().sorted().collect(Collectors.joining()));

            // delete one dataset
            deletionService.deleteDataset(datasetToDelete.getId());

            // all entities from other dataset should be deleted
            assertNoneExist(uploadId, datasetIdsToDelete, datasetImageIdsToDelete, measurementWingbeatIdsToDelete, classificationIdsToDelete, s3ObjectsToDelete);

            // assert s3 files match s3ObjectsToKeep
            allActualS3Objects = testHelperService.listAllS3Objects();
            assertEquals(0, allActualS3Objects.size());
            assertDatasetTagsStillExist();
            assertInsectsStillExist();

        } finally {
            testHelperService.cleanS3Buckets();
        }
    }

    @Test
    void testDeleteDataset__rollback_transaction() throws IOException, StorageException, S3ClientException {
        User user = userService.getUser(2L);
        try {
            Upload upload = uploadService.createUploadForUser(user);
            upload.setS3Object(s3ObjectService.storeFile(testFile));
            upload.addDataset(createNewDataset());
            Upload savedUpload = uploadService.saveUpload(upload);
            Dataset datasetToDelete = savedUpload.getDatasets().get(0);

            // assert this has been saved
            assertTrue(datasetService.getOptionalDataset(datasetToDelete.getId()).isPresent());

            Long uploadId = savedUpload.getId();
            List<Long> datasetIdsToDelete = new ArrayList<>();
            List<Long> datasetImageIdsToDelete = new ArrayList<>();
            List<Long> measurementWingbeatIdsToDelete = new ArrayList<>();
            List<Long> classificationIdsToDelete = new ArrayList<>();
            List<S3Object> s3ObjectsToDelete = new ArrayList<>();
            collectIdsAndS3Objects(List.of(datasetToDelete), datasetIdsToDelete, datasetImageIdsToDelete, measurementWingbeatIdsToDelete, classificationIdsToDelete, s3ObjectsToDelete);

            // upload should get deleted so expect the Upload S3Object to be deleted as well
            s3ObjectsToDelete.add(savedUpload.getS3Object());

            // assert these have been uploaded
            List<String> allExpectedS3Objects = new ArrayList<>(s3ObjectsToDelete.stream().map(S3Object::getKey).toList());
            List<String> allActualS3Objects = testHelperService.listAllS3Objects();
            assertEquals(allExpectedS3Objects.stream().sorted().collect(Collectors.joining()), allActualS3Objects.stream().sorted().collect(Collectors.joining()));

            // throw exception when deleting s3 objects
            Mockito.doThrow(StorageException.class).when(s3ClientStorageServiceSpy).delete(any(), any());

            // delete one dataset
            Dataset finalDatasetToDelete = datasetToDelete;
            assertThrows(StorageException.class, () -> deletionService.deleteDataset(finalDatasetToDelete.getId()));

            // all entities should still exist because they were rolled back
            assertAllExist(uploadId, datasetIdsToDelete, datasetImageIdsToDelete, measurementWingbeatIdsToDelete, classificationIdsToDelete, s3ObjectsToDelete);

            // assert s3 files still exist
            allActualS3Objects = testHelperService.listAllS3Objects();
            assertEquals(allExpectedS3Objects.stream().sorted().collect(Collectors.joining()), allActualS3Objects.stream().sorted().collect(Collectors.joining()));
            assertDatasetTagsStillExist();
            assertInsectsStillExist();

        } finally {
            testHelperService.cleanS3Buckets();
        }
    }

    @Test
    void testDeleteUpload_normal() throws IOException, StorageException, S3ClientException {
        User user = userService.getUser(2L);
        try {
            Upload upload = uploadService.createUploadForUser(user);
            upload.setS3Object(s3ObjectService.storeFile(testFile));
            upload.addDataset(createNewDataset());
            upload.addDataset(createNewDataset());
            Upload savedUpload = uploadService.saveUpload(upload);

            Dataset dataset1 = savedUpload.getDatasets().get(0);
            Dataset dataset2 = savedUpload.getDatasets().get(1);

            // assert these have been saved
            assertTrue(datasetService.getOptionalDataset(dataset1.getId()).isPresent());
            assertTrue(datasetService.getOptionalDataset(dataset2.getId()).isPresent());

            Long uploadId = savedUpload.getId();
            List<Long> datasetIdsToDelete = new ArrayList<>();
            List<Long> datasetImageIdsToDelete = new ArrayList<>();
            List<Long> measurementWingbeatIdsToDelete = new ArrayList<>();
            List<Long> classificationIdsToDelete = new ArrayList<>();
            List<S3Object> s3ObjectsToDelete = new ArrayList<>();
            collectIdsAndS3Objects(List.of(dataset1, dataset2), datasetIdsToDelete, datasetImageIdsToDelete, measurementWingbeatIdsToDelete, classificationIdsToDelete, s3ObjectsToDelete);

            // upload should get deleted so expect the Upload S3Object to be deleted as well
            s3ObjectsToDelete.add(savedUpload.getS3Object());

            // assert these have been uploaded
            List<String> allExpectedS3Objects = new ArrayList<>(s3ObjectsToDelete.stream().map(S3Object::getKey).toList());
            List<String> allActualS3Objects = testHelperService.listAllS3Objects();
            assertEquals(allExpectedS3Objects.stream().sorted().collect(Collectors.joining()), allActualS3Objects.stream().sorted().collect(Collectors.joining()));

            // delete upload
            deletionService.deleteUpload(uploadId);

            // all entities from upload should be deleted
            assertNoneExist(uploadId, datasetIdsToDelete, datasetImageIdsToDelete, measurementWingbeatIdsToDelete, classificationIdsToDelete, s3ObjectsToDelete);

            // assert s3 files have been deleted
            allActualS3Objects = testHelperService.listAllS3Objects();
            assertEquals(0, allActualS3Objects.size());
            assertDatasetTagsStillExist();
            assertInsectsStillExist();

        } finally {
            testHelperService.cleanS3Buckets();
        }
    }

    @Test
    void testDeleteUpload_rollback_transaction() throws IOException, StorageException, S3ClientException {
        User user = userService.getUser(2L);
        try {
            Upload upload = uploadService.createUploadForUser(user);
            upload.setS3Object(s3ObjectService.storeFile(testFile));
            upload.addDataset(createNewDataset());
            upload.addDataset(createNewDataset());
            Upload savedUpload = uploadService.saveUpload(upload);

            Dataset dataset1 = savedUpload.getDatasets().get(0);
            Dataset dataset2 = savedUpload.getDatasets().get(1);

            // assert these have been saved
            assertTrue(datasetService.getOptionalDataset(dataset1.getId()).isPresent());
            assertTrue(datasetService.getOptionalDataset(dataset2.getId()).isPresent());

            Long uploadId = savedUpload.getId();
            List<Long> datasetIdsToDelete = new ArrayList<>();
            List<Long> datasetImageIdsToDelete = new ArrayList<>();
            List<Long> measurementWingbeatIdsToDelete = new ArrayList<>();
            List<Long> classificationIdsToDelete = new ArrayList<>();
            List<S3Object> s3ObjectsToDelete = new ArrayList<>();
            collectIdsAndS3Objects(List.of(dataset1, dataset2), datasetIdsToDelete, datasetImageIdsToDelete, measurementWingbeatIdsToDelete, classificationIdsToDelete, s3ObjectsToDelete);

            // upload should get deleted so expect the Upload S3Object to be deleted as well
            s3ObjectsToDelete.add(savedUpload.getS3Object());

            // assert these have been uploaded
            List<String> allExpectedS3Objects = new ArrayList<>(s3ObjectsToDelete.stream().map(S3Object::getKey).toList());
            List<String> allActualS3Objects = testHelperService.listAllS3Objects();
            assertEquals(allExpectedS3Objects.stream().sorted().collect(Collectors.joining()), allActualS3Objects.stream().sorted().collect(Collectors.joining()));

            // throw exception when deleting s3 objects
            Mockito.doThrow(StorageException.class).when(s3ClientStorageServiceSpy).delete(any(), any());

            // delete upload
            assertThrows(StorageException.class, () -> deletionService.deleteUpload(uploadId));

            // all entities from upload should still exist because they were rolled back
            assertAllExist(uploadId, datasetIdsToDelete, datasetImageIdsToDelete, measurementWingbeatIdsToDelete, classificationIdsToDelete, s3ObjectsToDelete);

            // assert s3 files have not been deleted
            allActualS3Objects = testHelperService.listAllS3Objects();
            assertEquals(allExpectedS3Objects.stream().sorted().collect(Collectors.joining()), allActualS3Objects.stream().sorted().collect(Collectors.joining()));
            assertDatasetTagsStillExist();
            assertInsectsStillExist();

        } finally {
            testHelperService.cleanS3Buckets();
        }
    }

    @Test
    void testDeleteExport_normal() throws IOException, StorageException, S3ClientException {
        User user = userService.getUser(2L);

        try {
            Export export = createNewExport(user);

            List<Long> exportFileIds = new ArrayList<>();
            List<S3Object> s3ObjectsToDelete = new ArrayList<>();
            collectIdsAndS3Objects(export, exportFileIds, s3ObjectsToDelete);

            // assert these have been uploaded
            List<String> allExpectedS3Objects = new ArrayList<>(s3ObjectsToDelete.stream().map(S3Object::getKey).toList());
            List<String> allActualS3Objects = testHelperService.listAllS3Objects();
            assertEquals(allExpectedS3Objects.stream().sorted().collect(Collectors.joining()), allActualS3Objects.stream().sorted().collect(Collectors.joining()));

            // delete export
            deletionService.deleteExport(export.getId());

            assertNoneExist(export.getId(), exportFileIds, s3ObjectsToDelete);

            // assert s3 files have been deleted
            allActualS3Objects = testHelperService.listAllS3Objects();
            assertEquals(0, allActualS3Objects.size());

        } finally {
            testHelperService.cleanS3Buckets();
        }
    }

    @Test
    void testDeleteExport_rollback_transaction() throws IOException, StorageException, S3ClientException {
        User user = userService.getUser(2L);

        try {
            Export export = createNewExport(user);

            List<Long> exportFileIds = new ArrayList<>();
            List<S3Object> s3ObjectsToDelete = new ArrayList<>();
            collectIdsAndS3Objects(export, exportFileIds, s3ObjectsToDelete);

            // assert these have been uploaded
            List<String> allExpectedS3Objects = new ArrayList<>(s3ObjectsToDelete.stream().map(S3Object::getKey).toList());
            List<String> allActualS3Objects = testHelperService.listAllS3Objects();
            assertEquals(allExpectedS3Objects.stream().sorted().collect(Collectors.joining()), allActualS3Objects.stream().sorted().collect(Collectors.joining()));

            // throw exception when deleting s3 objects
            Mockito.doThrow(StorageException.class).when(s3ClientStorageServiceSpy).delete(any(), any());

            // delete export
            assertThrows(StorageException.class, () -> deletionService.deleteExport(export.getId()));

            assertAllExist(export.getId(), exportFileIds, s3ObjectsToDelete);

            // assert s3 files have been deleted
            allActualS3Objects = testHelperService.listAllS3Objects();
            assertEquals(allExpectedS3Objects.stream().sorted().collect(Collectors.joining()), allActualS3Objects.stream().sorted().collect(Collectors.joining()));

        } finally {
            testHelperService.cleanS3Buckets();
        }
    }

    private Dataset createNewDataset() throws IOException, StorageException {
        User user = userService.getUser(2L);
        Dataset dataset = new Dataset();
        dataset.setUser(user);
        dataset.setSensor(sensorService.getSensor(1L));
        dataset.setVideoS3Object(s3ObjectService.storeFile(testFile));
        Set<DatasetTag> datasetTags = new HashSet<>();
        datasetTags.add(datasetTagService.getDatasetTag(datasetTagId1));
        datasetTags.add(datasetTagService.getDatasetTag(datasetTagId2));
        dataset.setDatasetTags(datasetTags);
        dataset.addClassification(createNewClassification());
        dataset.addClassification(createNewClassification());
        dataset.addDatasetImage(createNewDatasetImage());
        dataset.addDatasetImage(createNewDatasetImage());
        dataset.setMeasurementWingbeat(createNewMeasurementWingbeat());
        dataset.setMeasurementDatetime(LocalDateTime.now());
        return dataset;
    }

    private Classification createNewClassification() {
        Classification classification = new Classification();
        InsectSpecies insectSpecies = insectSpeciesService.getInsectSpecies(1L);
        classification.setInsectSpecies(insectSpecies);
        classification.setInsectGenus(insectSpecies.getGenus());
        classification.setInsectFamily(insectSpecies.getGenus().getFamily());
        classification.setInsectOrder(insectSpecies.getGenus().getFamily().getOrder());
        classification.setType(ClassificationType.MACHINE);
        classification.setGender(ClassificationGender.MALE);
        classification.setProbability(new BigDecimal("0.5"));
        return classification;
    }

    private DatasetImage createNewDatasetImage() throws IOException, StorageException {
        DatasetImage datasetImage = new DatasetImage();
        datasetImage.setS3Object(s3ObjectService.storeFile(testFile));
        datasetImage.addClassification(createNewClassification());
        datasetImage.addClassification(createNewClassification());
        return datasetImage;
    }

    private MeasurementWingbeat createNewMeasurementWingbeat() throws IOException, StorageException {
        MeasurementWingbeat measurementWingbeat = new MeasurementWingbeat();
        measurementWingbeat.setWavS3Object(s3ObjectService.storeFile(testFile));
        measurementWingbeat.setImageS3Object(s3ObjectService.storeFile(testFile));
        measurementWingbeat.setSampleRate(12);
        measurementWingbeat.addClassification(createNewClassification());
        measurementWingbeat.addClassification(createNewClassification());
        return measurementWingbeat;
    }

    private Export createNewExport(User user) throws IOException, StorageException {
        Export export = new Export();
        export.setUser(user);
        export.setStatus(TransferStatus.SUCCESSFUL);
        export = exportService.saveExport(export);

        ExportFile exportFile = new ExportFile();
        exportFile.setS3Object(s3ObjectService.storeFile(testFile));
        export.addExportFile(exportFile);

        ExportFile exportFile2 = new ExportFile();
        exportFile2.setS3Object(s3ObjectService.storeFile(testFile));
        export.addExportFile(exportFile2);

        return exportService.saveExport(export);
    }

    private void collectIdsAndS3Objects(List<Dataset> datasets, List<Long> datasetIds, List<Long> datasetImageIds,
                                        List<Long> measurementWingbeatIds, List<Long> classificationIds, List<S3Object> s3Objects) {
        for (Dataset dataset : datasets) {
            datasetIds.add(dataset.getId());
            s3Objects.add(dataset.getVideoS3Object());
            for (DatasetImage datasetImage : datasetImageService.getDatasetImagesForDataset(dataset)) {
                datasetImageIds.add(datasetImage.getId());
                s3Objects.add(datasetImage.getS3Object());
                for (Classification classification : classificationService.getClassificationsForDatasetImageId(datasetImage.getId())) {
                    classificationIds.add(classification.getId());
                }
            }
            MeasurementWingbeat measurementWingbeat = dataset.getMeasurementWingbeat();
            measurementWingbeatIds.add(measurementWingbeat.getId());
            s3Objects.add(measurementWingbeat.getWavS3Object());
            if (measurementWingbeat.getImageS3Object() != null) {
                s3Objects.add(measurementWingbeat.getImageS3Object());
            }
            for (Classification classification : classificationService.getClassificationsForMeasurementWingbeatId(measurementWingbeat.getId())) {
                classificationIds.add(classification.getId());
            }
            for (Classification classification : classificationService.getClassificationsForDatasetId(dataset.getId())) {
                classificationIds.add(classification.getId());
            }
        }
    }

    private void collectIdsAndS3Objects(Export export, List<Long> exportFileIds, List<S3Object> s3Objects) {
        for (ExportFile exportFile : export.getExportFiles()) {
            exportFileIds.add(exportFile.getId());
            s3Objects.add(exportFile.getS3Object());
        }
    }

    private void assertAllExist(Long uploadId, List<Long> datasetIds, List<Long> datasetImageIds,
                                List<Long> measurementWingbeatIds, List<Long> classificationIds,
                                List<S3Object> s3Objects) {
        if (uploadId != null) {
            assertTrue(uploadService.getOptionalUpload(uploadId).isPresent());
        }
        for (Long datasetId : datasetIds) {
            assertTrue(datasetService.getOptionalDataset(datasetId).isPresent());
        }
        for (Long datasetImageId : datasetImageIds) {
            assertTrue(datasetImageService.getOptionalDatasetImage(datasetImageId).isPresent());
        }
        for (Long measurementWingbeatId : measurementWingbeatIds) {
            assertTrue(measurementWingbeatService.getOptionalMeasurementWingbeat(measurementWingbeatId).isPresent());
        }
        for (Long classificationId : classificationIds) {
            assertTrue(classificationService.getOptionalClassification(classificationId).isPresent());
        }
        for (S3Object s3Object : s3Objects) {
            assertTrue(s3ObjectService.getOptionalS3Object(s3Object.getId()).isPresent());
        }
    }

    private void assertNoneExist(Long uploadId, List<Long> datasetIds, List<Long> datasetImageIds,
                                 List<Long> measurementWingbeatIds, List<Long> classificationIds,
                                 List<S3Object> s3Objects) {
        if (uploadId != null) {
            assertTrue(uploadService.getOptionalUpload(uploadId).isEmpty());
        }
        for (Long datasetId : datasetIds) {
            assertTrue(datasetService.getOptionalDataset(datasetId).isEmpty());
        }
        for (Long datasetImageId : datasetImageIds) {
            assertTrue(datasetImageService.getOptionalDatasetImage(datasetImageId).isEmpty());
        }
        for (Long measurementWingbeatId : measurementWingbeatIds) {
            assertTrue(measurementWingbeatService.getOptionalMeasurementWingbeat(measurementWingbeatId).isEmpty());
        }
        for (Long classificationId : classificationIds) {
            assertTrue(classificationService.getOptionalClassification(classificationId).isEmpty());
        }
        for (S3Object s3Object : s3Objects) {
            Long s3ObjectId = s3Object.getId();
            assertTrue(s3ObjectService.getOptionalS3Object(s3ObjectId).isEmpty());
        }
    }

    private void assertAllExist(Long exportId, List<Long> exportFileIds, List<S3Object> s3Objects) {
        if (exportId != null) {
            assertTrue(exportService.getOptionalExport(exportId).isPresent());
        }
        for (Long exportFileId : exportFileIds) {
            assertTrue(exportFileService.getOptionalExportFile(exportFileId).isPresent());
        }
        for (S3Object s3Object : s3Objects) {
            Long s3ObjectId = s3Object.getId();
            assertTrue(s3ObjectService.getOptionalS3Object(s3ObjectId).isPresent());
        }
    }

    private void assertNoneExist(Long exportId, List<Long> exportFileIds, List<S3Object> s3Objects) {
        if (exportId != null) {
            assertTrue(exportService.getOptionalExport(exportId).isEmpty());
        }
        for (Long exportFileId : exportFileIds) {
            assertTrue(exportFileService.getOptionalExportFile(exportFileId).isEmpty());
        }
        for (S3Object s3Object : s3Objects) {
            Long s3ObjectId = s3Object.getId();
            assertTrue(s3ObjectService.getOptionalS3Object(s3ObjectId).isEmpty());
        }
    }

    private void assertDatasetTagsStillExist() {
        datasetTagService.getDatasetTag(datasetTagId1);
        datasetTagService.getDatasetTag(datasetTagId2);
    }

    private void assertInsectsStillExist() {
        InsectSpecies insectSpecies = insectSpeciesService.getInsectSpecies(1L);
        // genus and co must still exist because they are not optional for insect species
    }

}
