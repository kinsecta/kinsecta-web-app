package org.kinsecta.webapp.api.util;

import org.junit.jupiter.api.Test;

import java.time.DateTimeException;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class LocalDateConverterTest {

    @Test
    void convert() {
        LocalDateConverter converter = new LocalDateConverter();
        assertEquals(LocalDate.of(2021, 12, 2), converter.convert("2021-12-02"));
    }

    @Test
    void convert__unsupported_format() {
        LocalDateConverter converter = new LocalDateConverter();
        assertThrows(DateTimeException.class, () -> converter.convert("02.12.2012"));
    }

}
