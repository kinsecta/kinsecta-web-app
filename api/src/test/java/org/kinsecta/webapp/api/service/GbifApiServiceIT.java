package org.kinsecta.webapp.api.service;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.kinsecta.webapp.api.exception.unchecked.GbifApiException;
import org.kinsecta.webapp.api.exception.unchecked.GbifApiResponseDataException;
import org.kinsecta.webapp.api.model.dto.GbifResponseItem;
import org.kinsecta.webapp.api.model.entities.InsectFamily;
import org.kinsecta.webapp.api.model.entities.InsectGenus;
import org.kinsecta.webapp.api.model.entities.InsectOrder;
import org.kinsecta.webapp.api.model.entities.InsectSpecies;
import org.kinsecta.webapp.api.v1.model.TaxonomyLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;


@SpringBootTest
@ActiveProfiles("it")
class GbifApiServiceIT {

    @Autowired
    private GbifApiService gbifApiService;

    @SpyBean
    private GbifApiService gbifApiServiceSpy;

    @Autowired
    private InsectOrderService insectOrderService;

    @Autowired
    private InsectSpeciesService insectSpeciesService;


    // Disabled GBIF API Live Tests ------------------------------------------------------------------------------------

    @ParameterizedTest
    @ValueSource(longs = {-5L, 2147483648L})
    @Disabled(value = "This test should not be called automatically, since it relies on an external API.")
    void testGetGbifResponseItemForGbifId__invalid_gbif_id(long gbifId) {
        GbifApiException exception = assertThrows(GbifApiException.class, () -> gbifApiService.getGbifResponseItemForGbifId(gbifId));
        assertEquals(String.format("'%d' is not a valid GBIF ID. Must be a positive integer between 0 and %d.", gbifId, Integer.MAX_VALUE), exception.getMessage());
    }

    @ParameterizedTest
    @ValueSource(longs = {453457345L, Integer.MAX_VALUE})
    @Disabled(value = "This test should not be called automatically, since it relies on an external API.")
    void testGetGbifResponseItemForGbifId__not_found(long gbifId) {
        GbifApiException exception = assertThrows(GbifApiException.class, () -> gbifApiService.getGbifResponseItemForGbifId(gbifId));
        assertEquals(String.format("The GBIF API did not return any results for the GBIF ID '%s'", gbifId), exception.getMessage());
    }

    @Test
    @Disabled(value = "This test should not be called automatically, since it relies on an external API.")
    void testGetGbifResponseItemForGbifId() {
        GbifResponseItem responseItem = gbifApiService.getGbifResponseItemForGbifId(8293292L);
        assertEquals(8293292L, responseItem.getKey());
        assertEquals(8293292L, responseItem.getNubKey());
        assertEquals("Titanus giganteus", responseItem.getCanonicalName());
        assertEquals("Coleoptera", responseItem.getOrder());
        assertEquals("Cerambycidae", responseItem.getFamily());
        assertEquals("Titanus", responseItem.getGenus());
        assertEquals("Titanus giganteus", responseItem.getSpecies());
        assertEquals(1470L, responseItem.getOrderKey());
        assertEquals(5602L, responseItem.getFamilyKey());
        assertEquals(1133874L, responseItem.getGenusKey());
        assertEquals(8293292L, responseItem.getSpeciesKey());
        assertEquals(TaxonomyLevel.SPECIES.toString(), responseItem.getRank());
        assertEquals(Boolean.FALSE, responseItem.getSynonym());
    }


    // Stubbed performChecksAndResolveIds() Tests ----------------------------------------------------------------------

    @Test
    void testPerformChecksAndResolveIds__unsupported_rank_kingdom() {
        long gbifId = 1L;
        GbifResponseItem gbifResponseItem = createGbifResponseItemForId1();
        GbifApiResponseDataException exception = assertThrows(GbifApiResponseDataException.class, () -> gbifApiService.performChecksAndResolveIds(gbifResponseItem, gbifId));
        assertEquals(String.format("GBIF ID '%s' belongs to an unsupported rank: '%s'", gbifId, "KINGDOM"), exception.getMessage());
    }

    @Test
    void testPerformChecksAndResolveIds__unsupported_rank_class() {
        long gbifId = 10713444L;
        GbifResponseItem gbifResponseItem = createGbifResponseItemForId10713444();
        GbifApiResponseDataException exception = assertThrows(GbifApiResponseDataException.class, () -> gbifApiService.performChecksAndResolveIds(gbifResponseItem, gbifId));
        assertEquals(String.format("GBIF ID '%s' belongs to an unsupported rank: '%s'", gbifId, "CLASS"), exception.getMessage());
    }

    @Test
    void testPerformChecksAndResolveIds__order_does_not_belong_to_insect_class() {
        long gbifId = 10730025L;
        GbifResponseItem gbifResponseItem = createGbifResponseItemForId10730025();
        GbifApiResponseDataException exception = assertThrows(GbifApiResponseDataException.class, () -> gbifApiService.performChecksAndResolveIds(gbifResponseItem, gbifId));
        assertEquals(String.format("GBIF ID '%s' does not belong to the 'Insect' class", gbifId), exception.getMessage());
    }

    @Test
    void testPerformChecksAndResolveIds__checks_ok() {
        long gbifId = 1645047L;
        GbifResponseItem gbifResponseItem = createGbifResponseItemForId1645047();
        GbifResponseItem actualResponse = assertDoesNotThrow(() -> gbifApiService.performChecksAndResolveIds(gbifResponseItem, gbifId));
        assertEquals(gbifResponseItem, actualResponse);
    }


    // Mocked Tests ----------------------------------------------------------------------------------------------------

    @Test
    void testPerformChecksAndResolveIds__mock_reload_on_different_nub_key() {
        GbifResponseItem expectedGbifResponseItem = createGbifResponseItemForId1645047();
        when(gbifApiServiceSpy.getGbifResponseItemForGbifId(1645047L)).thenReturn(expectedGbifResponseItem);

        GbifResponseItem initialRequestGbifResponseItem = createGbifResponseItemForId123456798();
        GbifResponseItem responseItem = gbifApiService.performChecksAndResolveIds(initialRequestGbifResponseItem, 123456798L);

        assertEquals(expectedGbifResponseItem, responseItem);
    }

    @Test
    void testPerformChecksAndResolveIds__mock_reload_on_synonym() {
        GbifResponseItem expectedGbifResponseItem = createGbifResponseItemForId5039315();
        when(gbifApiServiceSpy.getGbifResponseItemForGbifId(5039315L)).thenReturn(expectedGbifResponseItem);

        GbifResponseItem initialRequestGbifResponseItem = createGbifResponseItemForId5039314();
        GbifResponseItem responseItem = gbifApiService.performChecksAndResolveIds(initialRequestGbifResponseItem, 5039314L);

        assertEquals(expectedGbifResponseItem, responseItem);
    }


    // getOrCreateInsectXxx Tests --------------------------------------------------------------------------------------

    @Test
    @Transactional
    void testGetOrCreateInsectOrder() {
        GbifResponseItem gbifResponseItem = createGbifResponseItemForId8293292();
        InsectOrder returnedInsectOrder = gbifApiService.getOrCreateInsectOrder(gbifResponseItem);
        InsectOrder storedInsectOrder = insectOrderService.getInsectOrder(returnedInsectOrder.getId());
        assertNotNull(storedInsectOrder);
        assertEquals(gbifResponseItem.getOrder(), storedInsectOrder.getName());
        assertEquals(gbifResponseItem.getOrderKey(), storedInsectOrder.getGbifId());
    }


    @Test
    @Transactional
    void testGetOrCreateInsectSpecies__all_new() {
        GbifResponseItem gbifResponseItem = createGbifResponseItemForId8293292();
        InsectSpecies returnedInsectSpecies = gbifApiService.getOrCreateInsectSpecies(gbifResponseItem);
        InsectSpecies storedInsectSpecies = insectSpeciesService.getInsectSpecies(returnedInsectSpecies.getId());
        assertNotNull(storedInsectSpecies);
        assertEquals(gbifResponseItem.getSpecies(), storedInsectSpecies.getName());
        assertEquals(gbifResponseItem.getSpeciesKey(), storedInsectSpecies.getGbifId());
        InsectGenus insectGenus = storedInsectSpecies.getGenus();
        assertNotNull(insectGenus);
        assertEquals(gbifResponseItem.getGenus(), insectGenus.getName());
        assertEquals(gbifResponseItem.getGenusKey(), insectGenus.getGbifId());
        InsectFamily insectFamily = insectGenus.getFamily();
        assertNotNull(insectFamily);
        assertEquals(gbifResponseItem.getFamily(), insectFamily.getName());
        assertEquals(gbifResponseItem.getFamilyKey(), insectFamily.getGbifId());
        InsectOrder insectOrder = insectFamily.getOrder();
        assertNotNull(insectOrder);
        assertEquals(gbifResponseItem.getOrder(), insectOrder.getName());
        assertEquals(gbifResponseItem.getOrderKey(), insectOrder.getGbifId());
    }


    @Test
    @Transactional
    void testGetOrCreateInsectSpecies__order_exists_already() {
        GbifResponseItem gbifResponseItem = createGbifResponseItemForId8293292();
        InsectOrder insectOrder = new InsectOrder();
        insectOrder.setGbifId(gbifResponseItem.getOrderKey());
        String orderName = "TestOrderName";
        insectOrder.setName(orderName);
        insectOrderService.save(insectOrder);

        InsectSpecies returnedInsectSpecies = gbifApiService.getOrCreateInsectSpecies(gbifResponseItem);
        InsectSpecies storedInsectSpecies = insectSpeciesService.getInsectSpecies(returnedInsectSpecies.getId());
        assertNotNull(storedInsectSpecies);
        assertEquals(gbifResponseItem.getSpecies(), storedInsectSpecies.getName());
        assertEquals(gbifResponseItem.getSpeciesKey(), storedInsectSpecies.getGbifId());
        InsectGenus insectGenus = storedInsectSpecies.getGenus();
        assertNotNull(insectGenus);
        assertEquals(gbifResponseItem.getGenus(), insectGenus.getName());
        assertEquals(gbifResponseItem.getGenusKey(), insectGenus.getGbifId());
        InsectFamily insectFamily = insectGenus.getFamily();
        assertNotNull(insectFamily);
        assertEquals(gbifResponseItem.getFamily(), insectFamily.getName());
        assertEquals(gbifResponseItem.getFamilyKey(), insectFamily.getGbifId());
        insectOrder = insectFamily.getOrder();
        assertNotNull(insectOrder);
        assertEquals(orderName, insectOrder.getName());
        assertEquals(gbifResponseItem.getOrderKey(), insectOrder.getGbifId());
    }


    // --- Helper Functions --------------------------------------------------------------------------------------------

    private GbifResponseItem createGbifResponseItemForId1() {
        GbifResponseItem gbifResponseItem = new GbifResponseItem();
        gbifResponseItem.setKey(1L);
        gbifResponseItem.setNubKey(1L);
        gbifResponseItem.setCanonicalName("Animalia");
        gbifResponseItem.setRank("KINGDOM");
        gbifResponseItem.setSynonym(false);
        return gbifResponseItem;
    }

    private GbifResponseItem createGbifResponseItemForId10713444() {
        GbifResponseItem gbifResponseItem = new GbifResponseItem();
        gbifResponseItem.setKey(10713444L);
        gbifResponseItem.setNubKey(10713444L);
        gbifResponseItem.setCanonicalName("Collembola");
        gbifResponseItem.setClassKey(10713444L);
        gbifResponseItem.setRank("CLASS");
        gbifResponseItem.setSynonym(false);
        return gbifResponseItem;
    }

    private GbifResponseItem createGbifResponseItemForId10730025() {
        GbifResponseItem gbifResponseItem = new GbifResponseItem();
        gbifResponseItem.setKey(10730025L);
        gbifResponseItem.setNubKey(10730025L);
        gbifResponseItem.setCanonicalName("Titanus Giganteus");
        gbifResponseItem.setOrder("Symphypleona");
        gbifResponseItem.setClassKey(10713444L);
        gbifResponseItem.setOrderKey(10730025L);
        gbifResponseItem.setRank("ORDER");
        gbifResponseItem.setSynonym(false);
        return gbifResponseItem;
    }

    private GbifResponseItem createGbifResponseItemForId1645047() {
        GbifResponseItem gbifResponseItem = new GbifResponseItem();
        gbifResponseItem.setKey(1645047L);
        gbifResponseItem.setNubKey(1645047L);
        gbifResponseItem.setCanonicalName("Simulium dolomitense");
        gbifResponseItem.setOrder("Diptera");
        gbifResponseItem.setFamily("Simuliidae");
        gbifResponseItem.setGenus("Simulium");
        gbifResponseItem.setSpecies("Simulium dolomitense");
        gbifResponseItem.setClassKey(216L);
        gbifResponseItem.setOrderKey(811L);
        gbifResponseItem.setFamilyKey(3522L);
        gbifResponseItem.setGenusKey(5734043L);
        gbifResponseItem.setSpeciesKey(1645047L);
        gbifResponseItem.setRank("SPECIES");
        gbifResponseItem.setSynonym(false);
        return gbifResponseItem;
    }

    private GbifResponseItem createGbifResponseItemForId123456798() {
        GbifResponseItem gbifResponseItem = new GbifResponseItem();
        gbifResponseItem.setKey(123456798L);
        gbifResponseItem.setNubKey(1645047L);
        gbifResponseItem.setCanonicalName("Simulium dolomitense");
        gbifResponseItem.setOrder("Diptera");
        gbifResponseItem.setFamily("Simuliidae");
        gbifResponseItem.setGenus("Simulium");
        gbifResponseItem.setSpecies("Simulium dolomitense");
        gbifResponseItem.setClassKey(123250244L);
        gbifResponseItem.setOrderKey(123394758L);
        gbifResponseItem.setFamilyKey(123455927L);
        gbifResponseItem.setGenusKey(123455970L);
        gbifResponseItem.setSpeciesKey(123456798L);
        gbifResponseItem.setRank("SPECIES");
        gbifResponseItem.setSynonym(false);
        return gbifResponseItem;
    }

    private GbifResponseItem createGbifResponseItemForId5039314() {
        GbifResponseItem gbifResponseItem = new GbifResponseItem();
        gbifResponseItem.setKey(5039314L);
        gbifResponseItem.setNubKey(5039314L);
        gbifResponseItem.setCanonicalName("Osmia rufa");
        gbifResponseItem.setOrder("Hymenoptera");
        gbifResponseItem.setFamily("Megachilidae");
        gbifResponseItem.setGenus("Osmia");
        gbifResponseItem.setSpecies("Osmia bicornis");
        gbifResponseItem.setClassKey(216L);
        gbifResponseItem.setOrderKey(1457L);
        gbifResponseItem.setFamilyKey(7911L);
        gbifResponseItem.setGenusKey(1337664L);
        gbifResponseItem.setSpeciesKey(5039315L);
        gbifResponseItem.setRank("SPECIES");
        gbifResponseItem.setSynonym(true);
        return gbifResponseItem;
    }

    private GbifResponseItem createGbifResponseItemForId5039315() {
        GbifResponseItem gbifResponseItem = new GbifResponseItem();
        gbifResponseItem.setKey(5039315L);
        gbifResponseItem.setNubKey(5039315L);
        gbifResponseItem.setCanonicalName("Osmia bicornis");
        gbifResponseItem.setOrder("Hymenoptera");
        gbifResponseItem.setFamily("Megachilidae");
        gbifResponseItem.setGenus("Osmia");
        gbifResponseItem.setSpecies("Osmia bicornis");
        gbifResponseItem.setClassKey(216L);
        gbifResponseItem.setOrderKey(1457L);
        gbifResponseItem.setFamilyKey(7911L);
        gbifResponseItem.setGenusKey(1337664L);
        gbifResponseItem.setSpeciesKey(5039315L);
        gbifResponseItem.setRank("SPECIES");
        gbifResponseItem.setSynonym(false);
        return gbifResponseItem;
    }

    private GbifResponseItem createGbifResponseItemForId8293292() {
        GbifResponseItem gbifResponseItem = new GbifResponseItem();
        gbifResponseItem.setKey(8293292L);
        gbifResponseItem.setNubKey(8293292L);
        gbifResponseItem.setCanonicalName("Titanus Giganteus");
        gbifResponseItem.setOrder("Coleoptera");
        gbifResponseItem.setFamily("Cerambycidae");
        gbifResponseItem.setGenus("Titanus");
        gbifResponseItem.setSpecies("Titanus giganteus");
        gbifResponseItem.setClassKey(216L);
        gbifResponseItem.setOrderKey(1470L);
        gbifResponseItem.setFamilyKey(5602L);
        gbifResponseItem.setGenusKey(1133874L);
        gbifResponseItem.setSpeciesKey(8293292L);
        gbifResponseItem.setRank("SPECIES");
        gbifResponseItem.setSynonym(false);
        return gbifResponseItem;
    }

}
