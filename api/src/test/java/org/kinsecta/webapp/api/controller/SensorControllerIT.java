package org.kinsecta.webapp.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.TestHelperService;
import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.model.entities.Sensor;
import org.kinsecta.webapp.api.model.entities.SensorLocation;
import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.model.mapping.SensorDtoMapper;
import org.kinsecta.webapp.api.model.mapping.SensorLocationDtoMapper;
import org.kinsecta.webapp.api.service.SensorLocationService;
import org.kinsecta.webapp.api.service.SensorService;
import org.kinsecta.webapp.api.service.UserService;
import org.kinsecta.webapp.api.v1.model.ApiViewDto;
import org.kinsecta.webapp.api.v1.model.SensorDto;
import org.kinsecta.webapp.api.v1.model.SensorLocationDto;
import org.kinsecta.webapp.api.v1.model.UserSimpleViewDto;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("it")
@SpringBootTest
@AutoConfigureMockMvc
class SensorControllerIT {

    private static final String SENSORS_PATH = "/sensors";
    private static final String SENSORS_PATH_SINGLE = SENSORS_PATH + "/1";
    private static final String SENSORS_PATH_VIEWS = SENSORS_PATH + "/_views";
    private static final String SENSORS_PATH_VIEWS_SIMPLE = SENSORS_PATH_VIEWS + "/simple";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private SensorService sensorService;

    @Autowired
    private SensorLocationService sensorLocationService;

    @Autowired
    private SensorDtoMapper sensorDtoMapper;

    @Autowired
    private SensorLocationDtoMapper sensorLocationDtoMapper;

    @Autowired
    private TestHelperService testHelperService;


    // Helper functions ------------------------------------------------------------------------------------------------

    private SensorDto createNewSensorDto(long operatorId) throws Exception {
        SensorDto dto = new SensorDto();

        SensorLocation sensorLocation = sensorLocationService.getSensorLocation(1L);
        SensorLocationDto sensorLocationDto = sensorLocationDtoMapper.sensorLocationToSensorLocationDto(sensorLocation);

        UserSimpleViewDto operator = new UserSimpleViewDto();
        operator.setId(JsonNullable.of(operatorId));
        operator.setFullName("Test Operator");
        operator.setUsername("t.operator");

        dto.setOperator(operator);
        dto.setSensorLocation(sensorLocationDto);
        dto.setShowPublic(false);
        dto.setHardwarePlatform("Test Platform");
        dto.setBokehVersion("Test Bokeh Version");
        dto.setCameraType("Test Camera Type");
        dto.setWingbeatSensorType("Test Wingbeat Sensor Type");
        dto.setOs("Test Os");

        return dto;
    }

    private SensorDto createNewSensor(long operatorId) throws Exception {
        SensorDto dto = createNewSensorDto(operatorId);

        String contentString = objectMapper.writeValueAsString(dto);
        MvcResult mvcResult = mockMvc.perform(
                post(SENSORS_PATH)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .content(contentString))
            .andExpect(status().isCreated())
            .andReturn();

        return testHelperService.convertResponseBodyToDto(mvcResult, SensorDto.class);
    }

    private SensorDto getSensorAndEditValues(long sensorId, long newOperatorId) {
        Sensor existingSensor = sensorService.getSensor(sensorId);
        SensorDto dto = sensorDtoMapper.sensorToSensorDto(existingSensor);

        User newOperator = userService.getUser(newOperatorId);
        UserSimpleViewDto newOperatorDto = new UserSimpleViewDto()
            .id(newOperator.getId())
            .username(newOperator.getUsername())
            .fullName(newOperator.getFullName());
        dto.setOperator(newOperatorDto);

        SensorLocation sensorLocation = sensorLocationService.getSensorLocation(5L);
        SensorLocationDto newSensorLocation = sensorLocationDtoMapper.sensorLocationToSensorLocationDto(sensorLocation);
        dto.setSensorLocation(newSensorLocation);

        dto.setCameraType("New Camera Type");
        dto.setWingbeatSensorType("New Wingbeat Sensor Type");

        dto.setShowPublic(true);

        return dto;
    }


    // Tests -----------------------------------------------------------------------------------------------------------

    @Test
    @Transactional
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void createSensor__as_admin() throws Exception {
        long operatorId = 1L;
        SensorDto created = createNewSensor(operatorId);

        assertEquals("Test Platform", created.getHardwarePlatform());
        assertEquals(JsonNullable.of(operatorId), created.getOperator().getId());
        assertEquals(1L, created.getSensorLocation().getId().get());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void createSensor__as_teamMember__for_themselves() throws Exception {
        long operatorId = 3L;
        SensorDto created = createNewSensor(operatorId);

        assertEquals("Test Platform", created.getHardwarePlatform());
        assertEquals(JsonNullable.of(operatorId), created.getOperator().getId());
        assertEquals(1L, created.getSensorLocation().getId().get());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void createSensor__as_teamMember__for_another_user() throws Exception {
        long operatorId = 2L;
        SensorDto sensorDto = createNewSensorDto(operatorId);

        String contentString = objectMapper.writeValueAsString(sensorDto);
        mockMvc.perform(
                post(SENSORS_PATH)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .content(contentString))
            .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllSensors() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(SENSORS_PATH))
            .andExpect(status().isOk())
            .andReturn();

        List<SensorDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, SensorDto.class);

        assertEquals(5, returnedDtoList.size());
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void getAllSensorViews() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(SENSORS_PATH_VIEWS))
            .andExpect(status().isOk())
            .andReturn();

        List<ApiViewDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, ApiViewDto.class);

        assertEquals(1, returnedDtoList.size());
        assertEquals("simple", returnedDtoList.get(0).getView());
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void getAllSensorsSimple() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(SENSORS_PATH_VIEWS_SIMPLE))
            .andExpect(status().isOk())
            .andReturn();

        List<SensorDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, SensorDto.class);

        assertEquals(5, returnedDtoList.size());
        for (SensorDto sensorDto : returnedDtoList) {
            assertTrue(sensorDto.getId().isPresent());
            assertNotNull(sensorDto.getOperator());
            assertNotNull(sensorDto.getSensorLocation());
        }
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getSensor() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(SENSORS_PATH_SINGLE))
            .andExpect(status().isOk())
            .andReturn();

        SensorDto SensorDto = testHelperService.convertResponseBodyToDto(mvcResult, SensorDto.class);
        assertEquals(1L, SensorDto.getId().get());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void editSensor__as_admin() throws Exception {
        long sensorId = 2L;
        long newOperatorId = 6L;
        SensorDto editedSensor = getSensorAndEditValues(sensorId, newOperatorId);
        editedSensor.setShowPublic(false);

        String contentString = objectMapper.writeValueAsString(editedSensor);
        MvcResult mvcResult = mockMvc.perform(
                put(SENSORS_PATH + "/" + sensorId)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .content(contentString))
            .andExpect(status().isOk())
            .andReturn();

        SensorDto result = testHelperService.convertResponseBodyToDto(mvcResult, SensorDto.class);

        assertEquals(sensorId, result.getId().get());
        assertEquals("New Camera Type", result.getCameraType());
        assertEquals("New Wingbeat Sensor Type", result.getWingbeatSensorType());
        assertEquals(6L, result.getOperator().getId().get());
        assertEquals(5L, result.getSensorLocation().getId().get());
        assertFalse(result.getShowPublic());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void editSensor__as_teamMember_for_another_operator() throws Exception {
        long sensorId = 2L;
        long newOperatorId = 3L;
        SensorDto editedSensor = getSensorAndEditValues(sensorId, newOperatorId);

        String contentString = objectMapper.writeValueAsString(editedSensor);
        mockMvc.perform(
                put(SENSORS_PATH + "/" + sensorId)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .content(contentString))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void editSensor__as_teamMember__for_themselves() throws Exception {
        long ownUserId = 3L;
        SensorDto sensorDto = createNewSensor(ownUserId);
        long sensorId = sensorDto.getId().get();

        sensorDto.setCameraType("New Camera Type");

        String contentString = objectMapper.writeValueAsString(sensorDto);
        MvcResult mvcResult = mockMvc.perform(
                put(SENSORS_PATH + "/" + sensorId)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .content(contentString))
            .andExpect(status().isOk())
            .andReturn();

        SensorDto result = testHelperService.convertResponseBodyToDto(mvcResult, SensorDto.class);

        assertEquals(sensorId, result.getId().get());
        assertEquals("New Camera Type", result.getCameraType());
        assertEquals(ownUserId, result.getOperator().getId().get());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void deleteSensor__as_admin() throws Exception {
        // Create a new Sensor for another User
        long operatorId = 2L;
        SensorDto created = createNewSensor(operatorId);
        Long sensorId = created.getId().get();

        // Immediately delete the Sensor
        mockMvc.perform(delete(SENSORS_PATH + "/" + sensorId))
            .andExpect(status().isOk());

        assertThrows(NotFoundException.class, () -> sensorService.getSensor(sensorId));
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void deleteSensor__as_teamMember__for_themselves() throws Exception {
        // Create a new Sensor for own user
        long ownUserId = 3L;
        SensorDto created = createNewSensor(ownUserId);
        Long sensorId = created.getId().get();

        // Immediately delete the Sensor
        mockMvc.perform(delete(SENSORS_PATH + "/" + sensorId))
            .andExpect(status().isOk());

        assertThrows(NotFoundException.class, () -> sensorService.getSensor(sensorId));
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void deleteSensor__as_teamMember__for_another_user() throws Exception {
        // Delete Sensor "1" which belongs to another User
        mockMvc.perform(delete(SENSORS_PATH + "/1"))
            .andExpect(status().isBadRequest());
    }

}
