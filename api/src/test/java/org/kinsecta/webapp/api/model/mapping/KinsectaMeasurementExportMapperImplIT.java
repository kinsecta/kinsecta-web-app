package org.kinsecta.webapp.api.model.mapping;

import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.TestHelperService;
import org.kinsecta.webapp.api.exception.checked.MappingException;
import org.kinsecta.webapp.api.model.entities.Dataset;
import org.kinsecta.webapp.api.service.DatasetService;
import org.kinsecta.webapp.api.v1.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@ActiveProfiles("it")
@Transactional
class KinsectaMeasurementExportMapperImplIT {

    @Autowired
    private KinsectaMeasurementExportMapper kinsectaMeasurementExportMapper;

    @Autowired
    private DatasetService datasetService;

    @Autowired
    private TestHelperService testHelperService;


    @Test
    void testDatasetToKinsectaMeasurement__with_video_and_images() throws MappingException {
        Dataset dataset = datasetService.getDataset(1L);
        KinsectaMeasurement kinsectaMeasurement = assertDoesNotThrow(() -> kinsectaMeasurementExportMapper.datasetToKinsectaMeasurement(dataset));

        // Check basic fields
        assertEquals(2L, kinsectaMeasurement.getUserId());
        assertEquals(1L, kinsectaMeasurement.getSensorId());
        assertEquals(LocalDateTime.of(2022, 1, 29, 13, 59, 12), kinsectaMeasurement.getDateTime());

        // This dataset has no wingbeat
        assertNull(kinsectaMeasurement.getWingbeat());

        // Check nested fields
        assertEquals(0, new BigDecimal("4.2").compareTo(kinsectaMeasurement.getSizeInsect().getLength().get()));
        assertEquals(0, new BigDecimal("2.6").compareTo(kinsectaMeasurement.getSizeInsect().getWidth().get()));
        assertEquals("mm", kinsectaMeasurement.getSizeInsect().getUnit());
        assertEquals("Berliner Hochschule für Technik (BHT)", kinsectaMeasurement.getSensorLocation().getName());
        assertEquals(0, new BigDecimal("13.352350").compareTo(kinsectaMeasurement.getSensorLocation().getLongitude()));
        assertFalse(kinsectaMeasurement.getSensorLocation().getGpsMasked());
        assertEquals("41d2cd7e-8103-11ec-893a-b827eb353b23.h264", kinsectaMeasurement.getVideo().getFilename());
        assertEquals(2, kinsectaMeasurement.getImages().size());
        List<String> expectedFilenames = List.of("4394cf4a-8103-11ec-893a-b827eb353b23.png", "4394aa1a-8103-11ec-893a-b827eb353b23.png");
        List<String> actualFilenames = kinsectaMeasurement.getImages().stream().map(KinsectaMeasurementImage::getFilename).toList();
        testHelperService.assertActualListContainsAllExpectedItemsIgnoreOrder(expectedFilenames, actualFilenames);

        // Check classifications
        assertEquals(1, kinsectaMeasurement.getMainClassifications().size());
        KinsectaMeasurementClassification classification = kinsectaMeasurement.getMainClassifications().get(0);
        assertEquals(ClassificationTypeDto.HUMAN, classification.getType());
        assertEquals(0, new BigDecimal("0.85000").compareTo(classification.getProbability()));
        assertEquals("Drosophila", classification.getGenus().getName().get());
    }

    @Test
    void testDatasetToKinsectaMeasurement__with_measurement_wingbeat() throws MappingException {
        Dataset dataset = datasetService.getDataset(2L);
        KinsectaMeasurement kinsectaMeasurement = assertDoesNotThrow(() -> kinsectaMeasurementExportMapper.datasetToKinsectaMeasurement(dataset));

        // Check basic fields
        assertEquals(2L, kinsectaMeasurement.getUserId());
        assertEquals(1L, kinsectaMeasurement.getSensorId());
        assertEquals(LocalDateTime.of(2022, 1, 29, 14, 31, 20), kinsectaMeasurement.getDateTime());

        // This dataset has no video or images
        assertNull(kinsectaMeasurement.getVideo());
        assertEquals(0, kinsectaMeasurement.getImages().size());

        // Check nested fields
        assertEquals(0, new BigDecimal("0.0").compareTo(kinsectaMeasurement.getSizeInsect().getLength().get()));
        assertEquals(0, new BigDecimal("0.0").compareTo(kinsectaMeasurement.getSizeInsect().getWidth().get()));
        assertEquals("mm", kinsectaMeasurement.getSizeInsect().getUnit());
        assertEquals("Berliner Hochschule für Technik (BHT)", kinsectaMeasurement.getSensorLocation().getName());
        assertEquals(0, new BigDecimal("13.352350").compareTo(kinsectaMeasurement.getSensorLocation().getLongitude()));
        assertFalse(kinsectaMeasurement.getSensorLocation().getGpsMasked());
        KinsectaMeasurementWingbeat kinsectaMeasurementWingbeat = kinsectaMeasurement.getWingbeat();
        assertEquals("be9efd38-8107-11ec-893a-b827eb353b23.wav", kinsectaMeasurementWingbeat.getFilename());
        assertEquals(96000, kinsectaMeasurementWingbeat.getSampleRate());
        assertEquals("c0ff562c-8107-11ec-893a-b827eb353b23.png", kinsectaMeasurementWingbeat.getPngFilename());

        // Check classifications
        assertEquals(1, kinsectaMeasurement.getMainClassifications().size());
        KinsectaMeasurementClassification classification = kinsectaMeasurement.getMainClassifications().get(0);
        assertEquals(ClassificationTypeDto.HUMAN, classification.getType());
        assertEquals(0, new BigDecimal("0.60000").compareTo(classification.getProbability()));
        assertEquals("Vespula", classification.getGenus().getName().get());
    }

}
