package org.kinsecta.webapp.api.security;

import nl.altindag.log.LogCaptor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.kinsecta.webapp.api.service.LoginAttemptService;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.Authentication;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class JwtAuthenticationSuccessHandlerTest {

    LogCaptor logCaptor = LogCaptor.forClass(JwtAuthenticationSuccessHandler.class);

    String username = "my_username";

    @Mock
    LoginAttemptService loginAttemptService;

    @Mock
    JwtTokenService jwtTokenService;

    MockHttpServletRequest servletRequest = new MockHttpServletRequest();
    MockHttpServletResponse servletResponse = new MockHttpServletResponse();

    @Mock
    Authentication authentication;

    @Mock
    UserDetailsImpl userDetails;


    private JwtAuthenticationSuccessHandler setupSpyJwtAuthenticationSuccessHandler() {
        return spy(new JwtAuthenticationSuccessHandler(loginAttemptService, jwtTokenService));
    }


    @Test
    void onAuthenticationSuccess() {
        JwtAuthenticationSuccessHandler successHandler = setupSpyJwtAuthenticationSuccessHandler();

        when(authentication.getPrincipal()).thenReturn(userDetails);
        when(userDetails.getUsername()).thenReturn(username);

        successHandler.onAuthenticationSuccess(servletRequest, servletResponse, authentication);

        assertTrue(logCaptor.getInfoLogs().contains(String.format("Login successful for username '%s'", username)));

    }

}
