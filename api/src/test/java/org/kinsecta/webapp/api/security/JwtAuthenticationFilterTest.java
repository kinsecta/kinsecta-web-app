package org.kinsecta.webapp.api.security;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.kinsecta.webapp.api.exception.unchecked.WrongArgumentException;
import org.kinsecta.webapp.api.model.dto.LoginRequest;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
class JwtAuthenticationFilterTest {

    @Spy
    AuthenticationManager authenticationManager;

    @Captor
    ArgumentCaptor<UsernamePasswordAuthenticationToken> usernamePasswordAuthenticationTokenArgumentCaptor;


    @Test
    void attemptAuthentication() {
        String json = """
            {
                "username": "my_username",
                "password": "my_$3cr3t_password_Ä$å"
            }
            """;
        MockHttpServletRequest servletRequest = getMockHttpServletRequest(json);
        MockHttpServletResponse servletResponse = new MockHttpServletResponse();
        JwtAuthenticationFilter jwtAuthenticationFilter = spy(new JwtAuthenticationFilter(authenticationManager));

        jwtAuthenticationFilter.attemptAuthentication(servletRequest, servletResponse);
        verify(authenticationManager).authenticate(usernamePasswordAuthenticationTokenArgumentCaptor.capture());

        // Assert that username has been saved in session for possible use in JwtAuthenticationFailureHandler
        assertDoesNotThrow(() -> servletRequest.getSession().getAttribute("auth_username"));
        assertEquals("my_username", servletRequest.getSession().getAttribute("auth_username"));

        // Assert that UsernamePasswordAuthenticationToken class has been created for the auth manager
        assertNotNull(usernamePasswordAuthenticationTokenArgumentCaptor.getValue());
        assertEquals("my_username", usernamePasswordAuthenticationTokenArgumentCaptor.getValue().getPrincipal());
        assertEquals("my_$3cr3t_password_Ä$å", usernamePasswordAuthenticationTokenArgumentCaptor.getValue().getCredentials());
    }

    @Test
    void mapRequestBodyToLoginRequest__valid() {
        String json = """
            {
                "username": "my_username",
                "password": "my_$3cr3t_password_Ä$å"
            }
            """;
        MockHttpServletRequest servletRequest = getMockHttpServletRequest(json);
        LoginRequest actualLoginRequest = JwtAuthenticationFilter.mapRequestBodyToLoginRequest(servletRequest);
        assertEquals("my_username", actualLoginRequest.getUsername());
        assertEquals("my_$3cr3t_password_Ä$å", actualLoginRequest.getPassword());
    }

    @Test
    void mapRequestBodyToLoginRequest__jsonNotWellFormed() {
        String json = """
            {
                username: "my_username",
                password: "my_$3cr3t_password_Ä$å"
            }
            """;
        MockHttpServletRequest servletRequest = getMockHttpServletRequest(json);
        assertThrows(WrongArgumentException.class, () -> JwtAuthenticationFilter.mapRequestBodyToLoginRequest(servletRequest));
    }

    @Test
    void mapRequestBodyToLoginRequest__jsonIsNotALoginRequestSchema() {
        String json = """
            {
                "user": "my_username",
                "pass": "my_$3cr3t_password_Ä$å"
            }
            """;
        MockHttpServletRequest servletRequest = getMockHttpServletRequest(json);
        assertThrows(WrongArgumentException.class, () -> JwtAuthenticationFilter.mapRequestBodyToLoginRequest(servletRequest));
    }


    private MockHttpServletRequest getMockHttpServletRequest(String json) {
        MockHttpServletRequest servletRequest = new MockHttpServletRequest();
        servletRequest.setContent(json.getBytes(StandardCharsets.UTF_8));
        return servletRequest;
    }

}
