package org.kinsecta.webapp.api.controller.roletesting;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.lingala.zip4j.ZipFile;
import org.apache.commons.io.FilenameUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.TestHelperService;
import org.kinsecta.webapp.api.mail.ExportMailService;
import org.kinsecta.webapp.api.model.entities.*;
import org.kinsecta.webapp.api.s3.S3ClientStorageService;
import org.kinsecta.webapp.api.service.DatasetImageService;
import org.kinsecta.webapp.api.service.DatasetService;
import org.kinsecta.webapp.api.service.ExportService;
import org.kinsecta.webapp.api.util.TempDirectoryHelper;
import org.kinsecta.webapp.api.v1.model.ExportDto;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileOutputStream;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("it")
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@Sql("/fixtures/datasets.sql")
class ExportControllerRoleIT {

    private static final String EXPORTS_PATH = "/exports";
    private static final String EXPORTS_PATH_SINGLE = EXPORTS_PATH + "/1";
    private static final String EXPORTS_PATH_SINGLE_EXPORT_FILES = EXPORTS_PATH_SINGLE + "/export_files";
    private static final String EXPORTS_PATH_SINGLE_EXPORT_FILES_SINGLE = EXPORTS_PATH_SINGLE_EXPORT_FILES + "/1";
    private static final String EXPORTS_PATH_SINGLE_EXPORT_FILES_SINGLE_DOWNLOAD_PATTERN = "/exports/%s/export_files/%s/download?token=%s";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private DatasetService datasetService;

    @Autowired
    private DatasetImageService datasetImageService;

    @Autowired
    private TestHelperService testHelperService;

    @Autowired
    private TempDirectoryHelper tempDirectoryHelper;

    @Autowired
    private ExportService exportService;

    @Autowired
    private S3ClientStorageService s3ClientStorageService;

    @MockBean
    private ExportMailService exportMailServiceMock;

    @Captor
    private ArgumentCaptor<Export> exportCaptor;

    @Captor
    private ArgumentCaptor<List<String>> errorMessageCaptor;

    @Value("${app.export.download.expiration}")
    private Integer expireHours;

    @Value("${app.export.max-datasets}")
    private Integer maxDatasetsPerExportLink;


    // ------ createExport ---------------------------------------------------------------------------------------------

    void createExport() throws Exception {
        try {
            Dataset dataset1 = datasetService.getDataset(1L);
            Dataset dataset2 = datasetService.getDataset(2L);

            List<DatasetImage> datasetImages1 = datasetImageService.getDatasetImagesForDataset(dataset1);
            // upload necessary s3 objects
            testHelperService.uploadTestFilesAsS3Object(
                dataset1.getVideoS3Object(),
                datasetImages1.get(0).getS3Object(),
                datasetImages1.get(1).getS3Object(),
                dataset2.getMeasurementWingbeat().getImageS3Object(),
                dataset2.getMeasurementWingbeat().getWavS3Object());

            //this should filter datasets 1 and 2

            MvcResult mvcResult = mockMvc.perform(post(EXPORTS_PATH)
                    .param("measurementStartDate", "2022-01-28")
                    .param("measurementEndDate", "2022-01-29")
                    .param("sensorFilters", "1")
                    .param("tagFilterList[0]", "-101"))
                .andExpect(status().isCreated())
                .andReturn();

            ExportDto returnedExportDto = testHelperService.convertResponseBodyToDto(mvcResult, ExportDto.class);
            Export export = exportService.getExport(returnedExportDto.getId().get());
            List<ExportFile> exportFiles = new ArrayList<>(export.getExportFiles());
            assertEquals(1, exportFiles.size());

            S3Object zipObject = exportFiles.get(0).getS3Object();
            assertTrue(testHelperService.existsS3ObjectInBucketStartingWith(zipObject.getBucket(), zipObject.getKey()));

            // assert zip file contains all the files
            File testTempDir = tempDirectoryHelper.createRandomTempSubDirectory();
            File zipFile = new File(testTempDir.getAbsolutePath(), zipObject.getFilename());
            try (FileOutputStream fos = new FileOutputStream(zipFile)) {
                s3ClientStorageService.load(zipObject.getBucket(), zipObject.getKey(), fos);
            }
            File extractedDir = new File(testTempDir, FilenameUtils.getBaseName(zipFile.getName()));
            new ZipFile(zipFile).extractAll(extractedDir.getAbsolutePath());
            List<String> fileListZipDirectory = Arrays.stream(extractedDir.list()).toList();
            assertEquals(2, fileListZipDirectory.size());

            File directory1 = new File(extractedDir.getAbsolutePath(), "1_2022-01-29_13-59-12");
            assertTrue(directory1.exists());
            assertTrue(directory1.isDirectory());
            List<String> fileList1 = Arrays.stream(directory1.list()).toList();
            assertTrue(fileList1.contains("data.json"));
            assertTrue(fileList1.contains(dataset1.getVideoS3Object().getFilename()));
            assertTrue(fileList1.contains(datasetImages1.get(0).getS3Object().getFilename()));
            assertTrue(fileList1.contains(datasetImages1.get(1).getS3Object().getFilename()));

            File directory2 = new File(extractedDir.getAbsolutePath(), "1_2022-01-29_14-31-20");
            assertTrue(directory2.exists());
            assertTrue(directory2.isDirectory());
            List<String> fileList2 = Arrays.stream(directory2.list()).toList();
            assertTrue(fileList2.contains("data.json"));
            assertTrue(fileList2.contains(dataset2.getMeasurementWingbeat().getImageS3Object().getFilename()));
            assertTrue(fileList2.contains(dataset2.getMeasurementWingbeat().getWavS3Object().getFilename()));

            // assert expires is ~24h in the future
            LocalDateTime now = LocalDateTime.now();
            Duration differenceToExpires = Duration.between(now, export.getExpires());
            assertFalse(differenceToExpires.isNegative());
            assertTrue(differenceToExpires.toMinutes() >= (expireHours * 60) - 1);
            assertTrue(differenceToExpires.toMinutes() <= (expireHours * 60) + 1);
            assertNotNull(export.getDownloadToken());
            assertEquals(TransferStatus.SUCCESSFUL, export.getStatus());

            // assert export file was saved in db
            assertEquals(1, export.getExportFiles().size());

            // assert mail is being sent
            verify(exportMailServiceMock).sendExportFinishedMessage(exportCaptor.capture(), eq(false), eq(maxDatasetsPerExportLink));
            Export capturedExport = exportCaptor.getValue();
            assertEquals(capturedExport.getId(), export.getId());

        } finally {
            testHelperService.cleanS3Buckets();
            testHelperService.cleanWorkingDirectory();
        }
    }

    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void createExport_Admin() throws Exception {
        createExport();
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void createExport_TeamMember() throws Exception {
        createExport();
    }


    @Test
    @DisplayName("Data Collectors are allowed to export datasets that are owned by them - Check if the export functions properly for datasets for which they have permission to download")
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void createExport_DataCollector_for_own_dataset() throws Exception {
        try {
            Dataset dataset = datasetService.getDataset(8L);

            List<DatasetImage> datasetImages = datasetImageService.getDatasetImagesForDataset(dataset);
            // upload necessary s3 objects
            testHelperService.uploadTestFilesAsS3Object(
                datasetImages.get(0).getS3Object());

            //this should filter dataset 8

            MvcResult mvcResult = mockMvc.perform(post(EXPORTS_PATH)
                    .param("measurementStartDate", "2022-01-28")
                    .param("measurementEndDate", "2022-01-29")
                    .param("tagFilterList[0]", "101")
                    .param("tagFilterList[1]", "103")
                    .param("sensorFilters", "5"))
                .andExpect(status().isCreated())
                .andReturn();

            ExportDto returnedExportDto = testHelperService.convertResponseBodyToDto(mvcResult, ExportDto.class);
            Export export = exportService.getExport(returnedExportDto.getId().get());
            List<ExportFile> exportFiles = new ArrayList<>(export.getExportFiles());
            assertEquals(1, exportFiles.size());

            S3Object zipObject = exportFiles.get(0).getS3Object();
            assertTrue(testHelperService.existsS3ObjectInBucketStartingWith(zipObject.getBucket(), zipObject.getKey()));

            // assert zip file contains all the files
            File testTempDir = tempDirectoryHelper.createRandomTempSubDirectory();
            File zipFile = new File(testTempDir.getAbsolutePath(), zipObject.getFilename());
            try (FileOutputStream fos = new FileOutputStream(zipFile)) {
                s3ClientStorageService.load(zipObject.getBucket(), zipObject.getKey(), fos);
            }
            File extractedDir = new File(testTempDir, FilenameUtils.getBaseName(zipFile.getName()));
            new ZipFile(zipFile).extractAll(extractedDir.getAbsolutePath());
            List<String> fileListZipDirectory = Arrays.stream(extractedDir.list()).toList();
            assertEquals(1, fileListZipDirectory.size());

            File directory = new File(extractedDir.getAbsolutePath(), "5_2022-01-29_13-00-12");
            assertTrue(directory.exists());
            assertTrue(directory.isDirectory());
            List<String> fileList = Arrays.stream(directory.list()).toList();
            assertTrue(fileList.contains("data.json"));
            assertTrue(fileList.contains(datasetImages.get(0).getS3Object().getFilename()));


            // assert expires is ~24h in the future
            LocalDateTime now = LocalDateTime.now();
            Duration differenceToExpires = Duration.between(now, export.getExpires());
            assertFalse(differenceToExpires.isNegative());
            assertTrue(differenceToExpires.toMinutes() >= (expireHours * 60) - 1);
            assertTrue(differenceToExpires.toMinutes() <= (expireHours * 60) + 1);
            assertNotNull(export.getDownloadToken());
            assertEquals(TransferStatus.SUCCESSFUL, export.getStatus());

            // assert export file was saved in db
            assertEquals(1, export.getExportFiles().size());

            // assert mail is being sent
            verify(exportMailServiceMock).sendExportFinishedMessage(exportCaptor.capture(), eq(false), eq(maxDatasetsPerExportLink));
            Export capturedExport = exportCaptor.getValue();
            assertEquals(capturedExport.getId(), export.getId());

        } finally {
            testHelperService.cleanS3Buckets();
            testHelperService.cleanWorkingDirectory();
        }
    }


    @Test
    @DisplayName("")
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void createExport_DataCollector_for_foreign_dataset() throws Exception {
        Dataset dataset = datasetService.getDataset(1L);

        List<DatasetImage> datasetImages = datasetImageService.getDatasetImagesForDataset(dataset);
        // upload necessary s3 objects
        testHelperService.uploadTestFilesAsS3Object(
            datasetImages.get(0).getS3Object());

        //this should filter dataset 1

        mockMvc.perform(post(EXPORTS_PATH)
                .param("measurementStartDate", "2022-01-28")
                .param("measurementEndDate", "2022-01-29")
                .param("tagFilterList[0]", "1")
                .param("sensorFilters", "1"))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.message").value("No exportable datasets remain after removing all datasets from filtered collection which do not belong to your user 'a.goldflam'"));


    }

    @Test
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void createExport_DataScientist() throws Exception {
        createExport();
    }

    @Test
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void createExport_DataRecipient() throws Exception {
        createExport();
    }


    // ---- createExport__export_failed_email --------------------------------------------------------------------------

    void createExport__export_failed_email() throws Exception {
        // perform POST and expect to return CREATED

        //this should filter dataset 8

        mockMvc.perform(post(EXPORTS_PATH)
                .param("measurementStartDate", "2022-01-28")
                .param("measurementEndDate", "2022-01-29")
                .param("tagFilterList[0]", "101")
                .param("tagFilterList[1]", "103")
                .param("sensorFilters", "5"))
            .andExpect(status().isCreated());

        // verify that the async export task fails with an S3 exception because the ExportFile cannot be loaded
        verify(exportMailServiceMock).sendExportFailedMessage(exportCaptor.capture(), errorMessageCaptor.capture());
        assertEquals(1, errorMessageCaptor.getValue().size());
        assertThat(errorMessageCaptor.getValue())
            .allMatch(s -> s.startsWith("Failed to load file '"));
    }

    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void createExport__export_failed_email_Admin() throws Exception {
        createExport__export_failed_email();
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void createExport__export_failed_email_TeamMember() throws Exception {
        createExport__export_failed_email();
    }

    @Test
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void createExport__export_failed_email_DataCollector() throws Exception {
        createExport__export_failed_email();
    }

    @Test
    @WithMockUser(roles = {"DATA_SCIENTIST"}, username = "d.sonneburg")
    void createExport__export_failed_email_DataScientist() throws Exception {
        createExport__export_failed_email();
    }

    @Test
    @WithMockUser(roles = {"DATA_RECIPIENT"}, username = "a.wonnleben")
    void createExport__export_failed_email_DataRecipient() throws Exception {
        createExport__export_failed_email();
    }

}
