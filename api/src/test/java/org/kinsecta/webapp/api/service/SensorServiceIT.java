package org.kinsecta.webapp.api.service;

import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.model.repositories.projections.PublicSensorProjection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.tuple;


@ActiveProfiles("it")
@SpringBootTest
class SensorServiceIT {

    @Autowired
    private SensorService sensorService;


    @Test
    void getAllSensorsPublic() {
        List<PublicSensorProjection> publicSensorProjections = sensorService.getAllSensorsPublic();

        assertThat(publicSensorProjections)
            .extracting(
                PublicSensorProjection::getId,
                PublicSensorProjection::getName,
                PublicSensorProjection::getPostcode,
                PublicSensorProjection::getCity,
                PublicSensorProjection::getLatitude,
                PublicSensorProjection::getLongitude
            )
            .containsExactlyInAnyOrder(
                tuple(1L, "Berliner Hochschule für Technik (BHT)", 13353, "Berlin", new BigDecimal("52.543762"), new BigDecimal("13.352350")),
                tuple(3L, "Umweltbildungszentrum Listhof", 72770, "Reutlingen", new BigDecimal("48.474556"), new BigDecimal("9.174845")),
                tuple(4L, "Umweltbildungszentrum Listhof", 72770, "Reutlingen", new BigDecimal("48.472000"), new BigDecimal("9.174000"))
            );
    }

}
