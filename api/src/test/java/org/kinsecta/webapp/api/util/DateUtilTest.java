package org.kinsecta.webapp.api.util;

import org.junit.jupiter.api.Test;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class DateUtilTest {

    // ---- TEST parseDateOrDateTimeAsLocalDateTime() ------------------------------------------------------------------

    @Test
    void parseDateAsLocalDateTime() {
        assertEquals(LocalDate.of(2021, 12, 2).atStartOfDay(), DateUtil.parseDateOrDateTimeAsLocalDateTime("2021-12-02", true));
        assertEquals(LocalDate.of(2021, 12, 2).atTime(LocalTime.MAX), DateUtil.parseDateOrDateTimeAsLocalDateTime("2021-12-02", false));
    }

    @Test
    void parseDateTimeAsLocalDateTime() {
        assertEquals(LocalDateTime.of(2021, 12, 2, 10, 37, 15), DateUtil.parseDateOrDateTimeAsLocalDateTime("2021-12-02 10:37:15", true));
        assertEquals(LocalDateTime.of(2021, 12, 2, 10, 37, 15), DateUtil.parseDateOrDateTimeAsLocalDateTime("2021-12-02T10:37:15", true));
    }

    @Test
    void parseDateAsLocalDateTime__unsupported_format() {
        assertThrows(DateTimeException.class, () -> DateUtil.parseDateOrDateTimeAsLocalDateTime("02.12.2012", true));
    }

}
