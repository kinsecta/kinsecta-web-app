package org.kinsecta.webapp.api.util;

import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.exception.unchecked.WrongArgumentException;
import org.kinsecta.webapp.api.v1.model.PasswordChangeDto;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;


class ValidationToolsTest {

    private final Set<String> emailDomainWhitelist = Set.of("goldflam.de");


    @Test
    void isEmailValid1() {
        assertTrue(ValidationTools.isValidEmail("d.maulat-test@goldflam.de", emailDomainWhitelist));
    }

    @Test
    void isEmailValidUnderscore() {
        assertTrue(ValidationTools.isValidEmail("david_test-1@goldflam.de", emailDomainWhitelist));
    }

    @Test
    void isEmailValid_david() {
        assertTrue(ValidationTools.isValidEmail("d.maulat@goldflam.de", emailDomainWhitelist));
    }

    @Test
    void isEmailValid_andre() {
        assertTrue(ValidationTools.isValidEmail("a.goldflam@goldflam.de", emailDomainWhitelist));
    }

    @Test
    void isEmailValid_wrongDomain() {
        assertFalse(ValidationTools.isValidEmail("d.maulat@fdsagoldflam.de", emailDomainWhitelist));
    }

    @Test
    void isEmailValid4() {
        assertFalse(ValidationTools.isValidEmail("d.maulat_at_goldflam.de", emailDomainWhitelist));
    }


    @Test
    void isNewPasswordValid_valid() {
        PasswordChangeDto pwDto = new PasswordChangeDto();
        pwDto.setNewPassword("Hello123;");
        pwDto.setConfNewPassword("Hello123;");
        ValidationTools.validateNewPassword(pwDto);
    }

    @Test
    void isNewPasswordValid_alsoValid() {
        PasswordChangeDto pwDto = new PasswordChangeDto();
        pwDto.setNewPassword("HELLöööoo 1!");
        pwDto.setConfNewPassword("HELLöööoo 1!");
        ValidationTools.validateNewPassword(pwDto);
    }


    public static void isNewPasswordValid(String password, String confirmationPassword) {
        PasswordChangeDto pwDto = new PasswordChangeDto();
        pwDto.setNewPassword(password);
        pwDto.setConfNewPassword(confirmationPassword);
        assertThrows(WrongArgumentException.class, () -> ValidationTools.validateNewPassword(pwDto));
    }

    @Test
    void isNewPasswordValid_tooLong() {
        isNewPasswordValid("HELLöööoo 1!-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------!", "HELLöööoo 1!-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------!");
    }

    @Test
    void isNewPasswordValid_notEquals() {
        isNewPasswordValid("hello", "fdshajklfdsaö");
    }

    @Test
    void isNewPasswordValid_empty() {
        isNewPasswordValid("", "");
    }

    @Test
    void isNewPasswordValid_tooShort() {
        isNewPasswordValid("hello", "hello");
    }

    @Test
    void isNewPasswordValid_noDigit() {
        isNewPasswordValid("Helloooooo!", "Helloooooo!");
    }

    @Test
    void isNewPasswordValid_noSpecialChar() {
        isNewPasswordValid("Helloooooo1", "Helloooooo1");
    }

    @Test
    void isNewPasswordValid_noUppercase() {
        isNewPasswordValid("helloooooo 1!", "helloooooo 1!");
    }

    @Test
    void isNewPasswordValid_noLowercase() {
        isNewPasswordValid("HELLOOOOOO 1!", "HELLOOOOOO 1!");
    }

    @Test
    void isValidIdListString_valid() {
        assertTrue(ValidationTools.isValidIdListString("1;2"));
    }

    @Test
    void isValidIdListString_multi_digit_numbers() {
        assertTrue(ValidationTools.isValidIdListString("12;12345"));
    }

    @Test
    void isValidIdListString_just_a_semicolon() {
        assertFalse(ValidationTools.isValidIdListString(";"));
    }

    @Test
    void isValidIdListString_starts_with_a_semicolon() {
        assertFalse(ValidationTools.isValidIdListString(";1;2"));
    }

    @Test
    void isValidIdListString_ends_with_a_semicolon() {
        assertFalse(ValidationTools.isValidIdListString("1;2;"));
    }

    @Test
    void isValidNegativeIdListString_valid() {
        assertTrue(ValidationTools.isValidNegativeIdListString("1;-2;3;-4;5"));
    }

    @Test
    void isValidNegativeIdListString_also_valid() {
        assertTrue(ValidationTools.isValidNegativeIdListString("-1;-2;-3;-4;-5"));
    }

    @Test
    void isValidNegativeIdListString_valid_just_one() {
        assertTrue(ValidationTools.isValidNegativeIdListString("-1"));
    }

    @Test
    void isValidNegativeIdListString_just_a_minus() {
        assertFalse(ValidationTools.isValidNegativeIdListString("-"));
    }

    @Test
    void isValidNegativeIdListString_double_minus() {
        assertFalse(ValidationTools.isValidNegativeIdListString("1;-2;--3"));
    }

    @Test
    void isValidNegativeIdListString_just_a_semicolon() {
        assertFalse(ValidationTools.isValidNegativeIdListString(";"));
    }

}
