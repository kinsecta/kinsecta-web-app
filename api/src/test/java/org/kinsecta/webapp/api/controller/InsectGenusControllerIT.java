package org.kinsecta.webapp.api.controller;

import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.TestHelperService;
import org.kinsecta.webapp.api.model.entities.InsectGenus;
import org.kinsecta.webapp.api.service.InsectFamilyService;
import org.kinsecta.webapp.api.service.InsectGenusService;
import org.kinsecta.webapp.api.v1.model.InsectGenusDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("it")
@SpringBootTest
@AutoConfigureMockMvc
class InsectGenusControllerIT {

    private static final String INSECT_GENUSES_PATH = "/insect_genuses";
    private static final String INSECT_GENUSES_PATH_SEARCH = INSECT_GENUSES_PATH + "/search";
    private static final String INSECT_GENUSES_PATH_SINGLE = INSECT_GENUSES_PATH + "/1";

    private final String testName = "Test InsectGenus";
    private final String testNameUrlEncoded = URLEncoder.encode(testName, StandardCharsets.UTF_8.toString());

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TestHelperService testHelperService;

    @Autowired
    private InsectGenusService insectGenusService;

    @Autowired
    private InsectFamilyService insectFamilyService;

    InsectGenusControllerIT() throws UnsupportedEncodingException {}


    // Helper Function
    private InsectGenus createNewInsectGenus() throws Exception {
        InsectGenus insectGenus = new InsectGenus();
        insectGenus.setName(testName);
        insectGenus.setGbifId(99991L);
        insectGenus.setFamily(insectFamilyService.getInsectFamily(1L));

        return insectGenusService.save(insectGenus);
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllInsectGenuses() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(INSECT_GENUSES_PATH))
            .andExpect(status().isOk())
            .andReturn();

        List<InsectGenusDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, InsectGenusDto.class);
        assertEquals(22, returnedDtoList.size());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void searchInsectGenuses__query_exact_match() throws Exception {
        InsectGenus created = createNewInsectGenus();

        MvcResult mvcResult = mockMvc.perform(get(INSECT_GENUSES_PATH_SEARCH + "?query=" + testNameUrlEncoded))
            .andExpect(status().isOk())
            .andReturn();

        List<InsectGenusDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, InsectGenusDto.class);
        assertEquals(1, returnedDtoList.size());
        InsectGenusDto resultDto = returnedDtoList.get(0);
        assertEquals(created.getName(), resultDto.getName());
        assertTrue(resultDto.getId().isPresent());
        assertEquals(created.getId(), resultDto.getId().get());
        assertTrue(resultDto.getGbifId().isPresent());
        assertEquals(created.getGbifId(), resultDto.getGbifId().get());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void searchInsectGenuses__query_partial_match_ignorecase() throws Exception {
        InsectGenus created = createNewInsectGenus();

        MvcResult mvcResult = mockMvc.perform(get(INSECT_GENUSES_PATH_SEARCH + "?query=test"))
            .andExpect(status().isOk())
            .andReturn();

        List<InsectGenusDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, InsectGenusDto.class);
        assertEquals(1, returnedDtoList.size());
        InsectGenusDto resultDto = returnedDtoList.get(0);
        assertEquals(created.getName(), resultDto.getName());
        assertTrue(resultDto.getId().isPresent());
        assertEquals(created.getId(), resultDto.getId().get());
        assertTrue(resultDto.getGbifId().isPresent());
        assertEquals(created.getGbifId(), resultDto.getGbifId().get());
    }

    @Test
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getInsectGenus() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(INSECT_GENUSES_PATH_SINGLE))
            .andExpect(status().isOk())
            .andReturn();

        InsectGenusDto resultDto = testHelperService.convertResponseBodyToDto(mvcResult, InsectGenusDto.class);
        assertEquals(1L, resultDto.getId().get());
    }

}
