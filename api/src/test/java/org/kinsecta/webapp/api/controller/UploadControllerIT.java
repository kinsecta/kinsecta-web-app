package org.kinsecta.webapp.api.controller;

import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.TestHelperService;
import org.kinsecta.webapp.api.exception.unchecked.NotFoundException;
import org.kinsecta.webapp.api.model.entities.Upload;
import org.kinsecta.webapp.api.service.UploadService;
import org.kinsecta.webapp.api.v1.model.TransferStatusDto;
import org.kinsecta.webapp.api.v1.model.UploadDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("it")
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class UploadControllerIT {

    private static final String UPLOADS_PATH = "/uploads";
    private static final String UPLOADS_PATH_SINGLE = UPLOADS_PATH + "/1";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UploadService uploadService;

    @Autowired
    private TestHelperService testHelperService;


    @Test
    @Transactional
    @WithMockUser(roles = {"DATA_COLLECTOR"}, username = "a.goldflam")
    void createUpload() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post(UPLOADS_PATH))
            .andExpect(status().isCreated())
            .andReturn();

        UploadDto uploadDto = testHelperService.convertResponseBodyToDto(mvcResult, UploadDto.class);
        assertEquals(TransferStatusDto.IN_PROGRESS, uploadDto.getStatus());
        Upload upload = uploadService.getUpload(uploadDto.getId());
        assertEquals("a.goldflam", upload.getUser().getUsername());
    }

    @Test
    @Transactional(readOnly = true)
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllUploads__forRoleTeamMember() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(UPLOADS_PATH))
            .andExpect(status().isOk())
            .andReturn();

        List<UploadDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, UploadDto.class);
        assertEquals(7, returnedDtoList.size());
    }

    @Test
    @Transactional(readOnly = true)
    @WithMockUser(roles = {"TEAM_MEMBER"}, username = "f.koeninger")
    void getAllUploads__forRoleTeamMember__paginated() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(UPLOADS_PATH + "?page=0&size=2"))
            .andExpect(status().isOk())
            .andReturn();

        List<UploadDto> returnedDtoList = testHelperService.convertResponseBodyToDtoList(mvcResult, UploadDto.class);
        assertEquals(2, returnedDtoList.size());
    }

    @Test
    @Transactional(readOnly = true)
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void getUpload() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get(UPLOADS_PATH_SINGLE))
            .andExpect(status().isOk())
            .andReturn();

        UploadDto uploadDto = testHelperService.convertResponseBodyToDto(mvcResult, UploadDto.class);
        assertEquals(1L, uploadDto.getId());
        assertEquals(TransferStatusDto.IN_PROGRESS, uploadDto.getStatus());
        assertEquals(2L, uploadDto.getUser().getId().get());
    }

    @Test
    @Transactional
    @WithMockUser(roles = {"ADMIN"}, username = "t.fischer")
    void deleteUpload() throws Exception {
        mockMvc.perform(delete(UPLOADS_PATH_SINGLE)).andExpect(status().isOk());

        assertThrows(NotFoundException.class, () -> uploadService.getUpload(1L));
    }

    // Please note: getS3ObjectForUpload(), downloadS3ObjectForUpload() and createS3ObjectForUpload() are tested thoroughly in UploadControllerTransactionIT
}
