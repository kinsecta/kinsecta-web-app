package org.kinsecta.webapp.api.service;

import org.junit.jupiter.api.Test;
import org.kinsecta.webapp.api.mail.ExportMailService;
import org.kinsecta.webapp.api.model.entities.Export;
import org.kinsecta.webapp.api.model.entities.TransferStatus;
import org.kinsecta.webapp.api.model.entities.User;
import org.kinsecta.webapp.api.model.mapping.ExportDtoMapper;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;


@SpringBootTest(properties = "app.export.max-datasets=1")
@ActiveProfiles("it")
@Transactional
class ExportServiceIT {

    @Autowired
    private ExportService exportService;

    @Autowired
    UserService userService;

    @Autowired
    SensorService sensorService;

    @Autowired
    ExportDtoMapper exportDtoMapper;

    @MockBean
    private ExportMailService exportMailServiceMock;

    @Captor
    private ArgumentCaptor<Export> exportCaptor;

    @Captor
    private ArgumentCaptor<List<String>> errorMessageCaptor;


    @Test
    void startAsyncCreationOfExportFilesForExport__export_failed_multiple_errors() {
        /*
         Filtered for:
         - startDate: LocalDate.of(2022, 1, 28)
         - endDate: LocalDate.of(2022, 1, 29)
         - tags: 1
         - sensorId: 1
         */
        Export export = new Export();
        export.setDatasetIds("1;2");

        User datasetUser = userService.findUserByUsername("f.koeninger");
        export.setUser(datasetUser);

        exportService.startAsyncCreationOfExportFilesForExport(export);

        verify(exportMailServiceMock).sendExportFailedMessage(exportCaptor.capture(), errorMessageCaptor.capture());
        assertEquals(TransferStatus.ERRONEOUS, exportCaptor.getValue().getStatus());
        assertEquals(2, errorMessageCaptor.getValue().size());
        assertThat(errorMessageCaptor.getValue()).allMatch(s -> s.startsWith("Failed to load file '"));

    }

}
