INSERT INTO sensor (id, operator, sensor_location_id, hardware_platform, os, camera_type, wingbeat_sensor_type,
                    bokeh_version)
VALUES (6, 1, 5, 'Raspberry Pi 4 Model B', 'Raspbian GNU/Linux 11 (bullseye)', 'Sony IMX477', 'Wingbeat Sensor V1.1',
        'Multisensors Bokeh V3.0');


INSERT INTO dataset (id, upload_id, user_id, sensor_id, measurement_datetime)
VALUES (3, 2, 2, 1, '2022-01-28 13:59:12.000'),
       (4, 2, 2, 5, '2022-01-28 15:00:12.000'),
       (5, 2, 2, 1, '2021-01-28 15:00:12.000'),
       (6, 2, 2, 3, '2022-01-14 12:00:00.000'),
       (7, 2, 2, 6, '2022-01-28 13:00:12.000'),
       (8, 2, 4 , 5, '2022-01-29 13:00:12.000'),  -- for Data Collector
       (9, 2, 6, 5, '2022-01-29 14:00:12.000'),  -- for Data Scientist
       (10, 2, 7, 5, '2022-01-29 15:00:12.000'); -- for Data Recipient

INSERT INTO dataset_tag (id, name, auto_tag)
VALUES (101, 'Test-Tag', false),
       (102, 'Test-Tag-2', false),
       (103, 'Test-Tag-3', false);

INSERT INTO datasets_dataset_tags(dataset_id, dataset_tag_id)
VALUES (3, 101),
       (4, 101),
       (4, 102),
       (6, 101),
       (7, 101),
       (7, 103),
       (8, 101),
       (8, 103),
       (9, 102),
       (9, 103),
       (10, 102),
       (3, 3),
       (5, 3),
       (2, 3);


INSERT INTO dataset_image (id, dataset_id, s3_object_id)
VALUES (101, 8, 3),
       (102, 9, 4),
       (103, 10, 6);

