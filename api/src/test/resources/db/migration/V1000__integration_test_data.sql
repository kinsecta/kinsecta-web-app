-- The password for these test users is always 'password'
INSERT INTO user (id, username, password, email, full_name, role, status, deleted)
VALUES (1, 't.fischer', '$2a$10$Ea4BAHiSAt.RGhrYNBRJN.ZHpcfOBOx5qPUCEir1JZGoC4JK2kfMS', 't.fischer@goldflam.de', 'Tobias Fischer', 'ADMIN', 'ACTIVE', null),
       (2, 'f.rossiar', '$2a$10$Ea4BAHiSAt.RGhrYNBRJN.ZHpcfOBOx5qPUCEir1JZGoC4JK2kfMS', 'f.rossiar@goldflam.de', 'Florian Rossiar', 'ADMIN', 'INACTIVE', null),
       (3, 'f.koeninger', '$2a$10$Ea4BAHiSAt.RGhrYNBRJN.ZHpcfOBOx5qPUCEir1JZGoC4JK2kfMS', 'f.koeninger@goldflam.de', 'Fabian Köninger', 'TEAM_MEMBER', 'ACTIVE', null),
       (4, 'a.goldflam', '$2a$10$Ea4BAHiSAt.RGhrYNBRJN.ZHpcfOBOx5qPUCEir1JZGoC4JK2kfMS', 'a.goldflam@goldflam.de', 'Andre Goldflam', 'DATA_COLLECTOR', 'ACTIVE', null),
       (5, 'deleted-user-123e4567-e89b-42d3-a456-556642440000', '$2a$10$Ea4BAHiSAt.RGhrYNBRJN.ZHpcfOBOx5qPUCEir1JZGoC4JK2kfMS', 'deleted-user-123e4567-e89b-42d3-a456-556642440000@app.kinsecta.org', 'Deleted User', 'DATA_COLLECTOR', 'DELETED', '2021-12-30 10:00:00'),
       (6, 'd.sonneburg', '$2a$10$Ea4BAHiSAt.RGhrYNBRJN.ZHpcfOBOx5qPUCEir1JZGoC4JK2kfMS', 'd.sonneburg@goldflam.de', 'David Sonneburg', 'DATA_SCIENTIST', 'ACTIVE', null),
       (7, 'a.wonnleben', '$2a$10$Ea4BAHiSAt.RGhrYNBRJN.ZHpcfOBOx5qPUCEir1JZGoC4JK2kfMS', 'a.wonnleben@goldflam.de', 'Anne Wonnleben', 'DATA_RECIPIENT', 'ACTIVE', null);

INSERT INTO s3_object (id, `key`, bucket, filename, media_type, filesize)
VALUES (1, '5e5827e9-fcf3-475d-973f-41ec614ef063', 'kinsecta-test-0', 'upload.zip', 'application/zip', 12035134),
       (2, 'd3c54a65-a8d4-4eff-b0f5-fab486dff5df', 'kinsecta-test-0', '41d2cd7e-8103-11ec-893a-b827eb353b23.h264', 'video/h264', 6193873),
       (3, 'fa8998b8-2cca-43c3-9c99-3e32671980db', 'kinsecta-test-1', '4394aa1a-8103-11ec-893a-b827eb353b23.png', 'image/jpeg', 2927066),
       (4, 'f3068172-8106-4b7a-ba01-e64e575728c1', 'kinsecta-test-1', '4394cf4a-8103-11ec-893a-b827eb353b23.png', 'image/jpeg', 2912258),
       (5, '2ac1fcbd-d2e1-41d9-9395-2c353c3f6554', 'kinsecta-test-0', 'upload.zip', 'application/zip', 63439),
       (6, 'b0e867f8-1bd1-4073-b1af-e3b4b633908e', 'kinsecta-test-0', 'be9efd38-8107-11ec-893a-b827eb353b23.wav', 'audio/wave', 98348),
       (7, 'c6eac2ea-1956-42bb-8ff6-ce53564d5e2d', 'kinsecta-test-0', 'c0ff562c-8107-11ec-893a-b827eb353b23.png', 'image/png', 41707);

INSERT INTO upload(id, user_id, s3_object_id, status, finished)
VALUES (1, 2, NULL, 'IN_PROGRESS', NULL),
       (2, 1, NULL, 'ERRONEOUS', '2022-03-02 13:43:49'),
       (3, 1, NULL, 'CANCELLED', '2022-03-02 12:00:00'),
       (4, 4, NULL, 'ERRONEOUS', '2022-03-01 09:15:00'),
       (5, 2, NULL, 'IN_PROGRESS', NULL),
       (6, 2, 1, 'SUCCESSFUL', '2022-03-01 16:12:13.395'),
       (7, 2, 5, 'SUCCESSFUL', '2022-03-02 09:18:33.077');

INSERT INTO sensor_location (id, name, postcode, city, latitude, longitude, gps_masked)
VALUES (1, 'Berliner Hochschule für Technik (BHT)', 13353, 'Berlin', 52.543762, 13.352350, 0),
       (2, 'Berliner Hochschule für Technik (BHT)|Forum Seestraße', 13347, 'Berlin', 52.553867, 13.359412, 0),
       (3, 'Umweltbildungszentrum Listhof', 72770, 'Reutlingen', 48.474556, 9.174845, 0),
       (4, 'Umweltbildungszentrum Listhof', 72770, 'Reutlingen', 48.472000, 9.174000, 1),
       (5, 'Umweltbildungszentrum Listhof', 72770, 'Reutlingen', 48.476000, 9.167000, 1);

INSERT INTO sensor (id, operator, sensor_location_id, show_public, hardware_platform, os, camera_type, wingbeat_sensor_type, bokeh_version)
VALUES (1, 1, 1, 1, 'Raspberry Pi 4 Model B', 'Raspbian GNU/Linux 10 (buster)', 'Sony IMX477', 'Wingbeat Sensor V1.0', 'Multisensors Bokeh App V2.0'),
       (2, 1, 2, 0, 'Raspberry Pi 4 Model B', 'Raspbian GNU/Linux 10 (buster)', 'Sony IMX477', 'Wingbeat Sensor V1.1', 'Multisensors Bokeh V1.0'),
       (3, 1, 3, 1, 'Raspberry Pi 3 Model B+ Rev 1.2', 'Raspbian GNU/Linux 11 (bullseye)', 'Sony IMX477', 'Wingbeat Sensor V2.0', 'Multisensors Bokeh V2.0'),
       (4, 1, 4, 1, 'Raspberry Pi 3 Model B Rev 1.2', 'Raspbian GNU/Linux 10 (buster)', 'Sony IMX477', 'Wingbeat Sensor V2.1', 'Multisensors Bokeh V3.0'),
       (5, 1, 5, 0, 'Raspberry Pi 4 Model B', 'Raspbian GNU/Linux 11 (bullseye)', 'Sony IMX477', 'Wingbeat Sensor V1.1', 'Multisensors Bokeh V3.0');

INSERT INTO measurement_wingbeat (id, wav_s3_object_id, image_s3_object_id, sample_rate)
VALUES (1, 6, 7, 96000);

INSERT INTO dataset (id, upload_id, user_id, sensor_id, video_s3_object_id, measurement_wingbeat_id,
                     measurement_datetime, measurement_size_insect_length, measurement_size_insect_width,
                     measurement_temperature, measurement_air_pressure, measurement_humidity,
                     measurement_luminosity, measurement_spectrum_channel_415nm, measurement_spectrum_channel_445nm,
                     measurement_spectrum_channel_480nm, measurement_spectrum_channel_515nm,
                     measurement_spectrum_channel_555nm, measurement_spectrum_channel_590nm,
                     measurement_spectrum_channel_630nm, measurement_spectrum_channel_680nm,
                     measurement_spectrum_near_ir, measurement_spectrum_clear_light,
                     measurement_particulate_matter_particles_03um, measurement_particulate_matter_particles_05um,
                     measurement_particulate_matter_particles_10um, measurement_particulate_matter_particles_25um,
                     measurement_particulate_matter_particles_50um, measurement_particulate_matter_particles_100um,
                     measurement_particulate_matter_pm1, measurement_particulate_matter_pm2p5, measurement_particulate_matter_pm10,
                     measurement_rainfall, measurement_wind_sensor_speed, measurement_wind_sensor_direction)
VALUES (1, 2, 2, 1, 2, null, '2022-01-29 13:59:12.000', 4.2, 2.6, 21.2, 1.024, 12.5, 325.3, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.10, 0.20, 'SSE'),
       (2, 3, 2, 1, null, 1, '2022-01-29 14:31:20.000', 0.0, 0.0, 16.7, 0.898, 19.4, 89.4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0.80, 3.80, 'S');


INSERT INTO dataset_image (id, dataset_id, s3_object_id)
VALUES (1, 1, 3),
       (2, 1, 4);


INSERT INTO datasets_dataset_tags (dataset_id, dataset_tag_id)
VALUES (1, 1),
       (1, 3);


INSERT INTO insect_order (id, name, gbif_id)
VALUES (1, 'Diptera', 811),
       (2, 'Hemiptera', 809),
       (3, 'Hymenoptera', 1457),
       (4, 'Lepidoptera', 797),
       (5, 'Mecoptera', 1000);

INSERT INTO insect_family (id, order_id, name, gbif_id)
VALUES (1, 1, 'Tachinidae', 6918),
       (2, 1, 'Calliphoridae', 3335),
       (3, 1, 'Drosophilidae', 5547),
       (4, 1, 'Syrphidae', 6920),
       (5, 1, 'Bombyliidae', 7285),
       (6, 1, 'Biblionidae', 11126326),
       (7, 1, 'Sciaridae', 3525),
       (8, 2, 'Pyrrhocoridae', 7895),
       (9, 3, 'Apidae', 4334),
       (10, 3, 'Megachilidae', 7911),
       (11, 3, 'Vespidae', 4490),
       (12, 3, 'Eumenidae', 4240),
       (13, 3, 'Formicidae', 4342),
       (14, 4, 'Pyralidae', 5336),
       (15, 5, 'Panorpidae', 7925),
       (16, 4, 'Hesperiidae', 5264);

INSERT INTO insect_genus (id, family_id, name, gbif_id)
VALUES (1, 1, 'Tachina', 1470805),
       (2, 1, 'Sarcophaga', 1463100),
       (3, 2, 'Calliphora', 1502535),
       (4, 3, 'Drosophila', 1522683),
       (5, 1, 'Musca', 8097933),
       (6, 4, 'Eristalis', 1491190),
       (7, 4, 'Episyrphus', 1536427),
       (8, 5, 'Bombylius', 1500120),
       (9, 6, 'Bibio', 1543369),
       (10, 4, 'Syrphus', 1444606),
       (11, 8, 'Pyrrhocoris', 4407618),
       (12, 9, 'Apis', 1334757),
       (13, 9, 'Bombus', 1340278),
       (14, 9, 'Xylocopa', 1342048),
       (15, 10, 'Osmia', 1337664),
       (16, 11, 'Vespa', 1311334),
       (17, 11, 'Vespula', 1311631),
       (18, 12, 'Polistes', 1310500),
       (19, 13, 'Formica', 1314840),
       (20, 14, 'Plodia', 1873074),
       (21, 15, 'Panorpa', 1031814),
       (22, 16, 'Achalarus', 2492321);

INSERT INTO insect_species (id, genus_id, name, gbif_id)
VALUES (1, 1, 'Tachina fera', 5978844),
       (2, 2, 'Sarcophaga carnaria', 4518735),
       (3, 3, 'Calliphora vicina', 1502577),
       (4, 4, 'Drosophila melanogaster', 11195063),
       (5, 5, 'Musca domestica', 11118914),
       (6, 6, 'Eristalis tenax', 1541217),
       (7, 7, 'Episyrphus balteatus', 1536449),
       (8, 8, 'Bombylius major', 1670212),
       (9, 9, 'Bibio marci', 1590998),
       (10, 10, 'Syrphus ribesii', 1537266),
       (11, 11, 'Pyrrhocoris apterus', 4486826),
       (12, 12, 'Apis mellifera', 1341976),
       (13, 13, 'Bombus terrestris', 1340503),
       (14, 13, 'Bombus lapidarius', 1340301),
       (15, 14, 'Xylocopa violacea', 1342108),
       (16, 15, 'Osmia rufa', 5039314),
       (17, 16, 'Vespa crabro', 1311527),
       (18, 17, 'Vespula vulgaris', 1311671),
       (19, 17, 'Vespula germanica', 1311649),
       (20, 18, 'Polistes dominula', 5871389),
       (21, 19, 'Formica rufa', 1315132),
       (22, 20, 'Plodia interpunctella', 1873079),
       (23, 21, 'Panorpa communis', 5742495),
       (25, 22, 'lyciades', 5231190);

INSERT INTO classification (id, dataset_id, dataset_image_id, measurement_wingbeat_id, insect_order_id, insect_family_id,
                            insect_genus_id, insect_species_id, type, probability, gender)
VALUES (1, 1, null, null, 1, 3, 4, 4, 'HUMAN', 0.85000, 'FEMALE'),
       (2, 2, null, null, 3, 11, 17, 18, 'HUMAN', 0.60000, 'UNSPECIFIED'),
       (3, null, null, null, 3, 11, 17, 18, 'HUMAN', 0.60000, 'MALE');

INSERT INTO export (id, user_id, status, finished, download_token, expires)
VALUES (1, 1, 'SUCCESSFUL', '2022-01-04 12:00:00', '12345678', '2022-01-04'),
       (2, 1, 'IN_PROGRESS', '2022-01-04 12:00:00', null, null);
