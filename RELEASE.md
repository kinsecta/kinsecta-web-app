# Release

When you are ready to release a new version, follow these steps:

1. Create a release branch from `develop` (e.g. `release-1.0.0` for the version 1.0.0)

2. Bump the Maven version number

    * Remove the `-SNAPSHOT` version suffix in all POM files on `release-1.0.0` (e.g. `1.2.3-SNAPSHOT` to `1.2.3`)
    * Run a Maven build (e.g. `mvn clean verify` to make sure everything's alright)

3. Update [CHANGELOG.md](CHANGELOG.md)

    * Replace the `[Unreleased]` section with the current date and the new version number.
    * Enter all tasks and subtasks from the GitLab milestone


4. Commit the changes (`release <version>`) and merge `release-1.0.0` into `main`

5. Add a new tag on `main` and push it

    ```bash
    git tag -a v1.2.3 -m v1.2.3
    git push --tags
    ```

6. Prepare for the next development iteration:
    * Create a new SNAPSHOT version in the POMs (e.g. `1.2.3` to `1.2.4-SNAPSHOT`)
    * Add a new `[Unreleased]` entry at the top of the CHANGELOG file.
    * Commit on `release-1.0.0` with the commit message "prepare for the next development iteration (1.2.4-SNAPSHOT)"
    * Merge into `develop`
    * Push it to the remote repository
