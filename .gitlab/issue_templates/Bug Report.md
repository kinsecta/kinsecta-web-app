## Zusammenfassung

<!-- Fassen Sie den aufgetretenen Fehler kurz und bündig zusammen. -->

XXX


## Schritte zur Reproduktion

<!--
Wie kann man das Problem reproduzieren - dies ist sehr wichtig!
Ergänzen Sie Ihre Beschreibung ggfs. um URL's zu den betroffenen Seiten.
-->

1. XXX
2. XXX


## Was ist das aktuelle fehlerhafte Verhalten?

<!-- Was tatsächlich passiert. -->

XXX


## Welches ist das erwartete korrekte Verhalten?

<!-- Was eigentlich passieren sollte. -->

XXX


## Technische Informationen

<!-- Teilen Sie uns alle relevanten Informationen zu ihrem Test-Setup mit. -->

- Webbrowser & Version: XXX
- Betriebssystem & Version: XXX
- Testsystem oder Livesystem: XXX
- Wann wurde getestet? (Datum und Uhrzeit): XXX


## Relevante Protokolle und/oder Bildschirmfotos

<!--
Fügen Sie alle relevanten Protokolle ein - bitte verwenden Sie Codeblöcke (``` über und unter dem Code),
um Konsolenausgaben, Protokolle und Code zu formatieren, da es sonst sehr schwer zu lesen ist.

Bildschirmfotos können entweder per Copy & Paste aus der Zwischenablage oder
per Drag & Drop direkt in den GitLab-Editor eingefügt und hochgeladen werden.
-->

XXX


## Mögliche Korrekturen

<!-- Wenn möglich, verlinken Sie auf die Codezeile, die für das Problem verantwortlich sein könnte. -->

XXX

---

/cc @tofi86
/label ~"Type::bug" ~"Status::investigate"
