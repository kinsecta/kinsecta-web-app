## Problembeschreibung

<!--
Bitte beschreiben Sie kurz und bündig das Problem, das mit dieser Anfrage gelöst werden soll.
Bitte machen Sie deutlich, welche Teile des Problems als "in-scope" und "out-of-scope" betrachtet werden.
-->

XXX


## Optional: Lösungsvorschlag

<!--
Eine prägnante Beschreibung der von Ihnen bevorzugten Lösung. Zu den Dingen, die Sie ansprechen sollten, gehören:
* Einzelheiten der technischen Umsetzung
* Kompromisse bei den Designentscheidungen
* Vorbehalte und Überlegungen für die Zukunft

Wenn es mehrere Lösungen gibt, stellen Sie bitte jede Lösung einzeln vor. Heben Sie sich Vergleiche für das Ende auf.
-->

XXX

---

/cc @tofi86

/label ~"Type::feature-request" ~"Status::investigate"
