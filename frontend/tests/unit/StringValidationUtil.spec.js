import { StringValidationUtil } from '@/util/StringValidationUtil'

// --- isValidEmail() -----------------------------------------------------------------------------

describe('The following email addresses should be parsed as VALID', () => {
  test('Java testcases fron ValidationToolsTest class', () => {
    expect(StringValidationUtil.isValidEmail('d.maulat-test@goldflam.de')).toBe(true)
    expect(StringValidationUtil.isValidEmail('david_test-1@goldflam.de')).toBe(true)
    expect(StringValidationUtil.isValidEmail('d.maulat@goldflam.de')).toBe(true)
    expect(StringValidationUtil.isValidEmail('a.goldflam@goldflam.de')).toBe(true)
    expect(StringValidationUtil.isValidEmail('d.maulat@fdsagoldflam.de')).toBe(true)
  })
  test('username with +', () => {
    expect(StringValidationUtil.isValidEmail('d.maulat+test@goldflam.de')).toBe(true)
  })
  test('info@google.de', () => {
    expect(StringValidationUtil.isValidEmail('info@google.de')).toBe(true)
  })
  test('5 char TLD', () => {
    expect(StringValidationUtil.isValidEmail('test@test.cloud')).toBe(true)
  })
  test('student@student.uni-tuebingen.de', () => {
    expect(StringValidationUtil.isValidEmail('student@student.uni-tuebingen.de')).toBe(true)
  })
  test('student@student.phsyics.uni-tuebingen.de', () => {
    expect(StringValidationUtil.isValidEmail('student@student.phsyics.uni-tuebingen.de')).toBe(true)
  })
})

describe('The following email addresses should be parsed as INVALID', () => {
  test('no @, no domain', () => {
    expect(StringValidationUtil.isValidEmail('d.maulat')).toBe(false)
  })
  test('no domain', () => {
    expect(StringValidationUtil.isValidEmail('d.maulat@')).toBe(false)
  })
  test('no TLD', () => {
    expect(StringValidationUtil.isValidEmail('d.maulat@test')).toBe(false)
    expect(StringValidationUtil.isValidEmail('d.maulat@localhost')).toBe(false)
  })
  test('no @', () => {
    expect(StringValidationUtil.isValidEmail('d.maulat_at_goldflam.de')).toBe(false)
  })
  test('only @', () => {
    expect(StringValidationUtil.isValidEmail('@')).toBe(false)
  })
  test('domain with whitespace', () => {
    expect(StringValidationUtil.isValidEmail('test-mail@test domain.de')).toBe(false)
  })
  test('username with whitespace', () => {
    expect(StringValidationUtil.isValidEmail('test mail@test.de')).toBe(false)
  })
  test('no username', () => {
    expect(StringValidationUtil.isValidEmail('@test.de')).toBe(false)
  })
  test('german umlauts', () => {
    expect(StringValidationUtil.isValidEmail('äöü@test.de')).toBe(false)
    expect(StringValidationUtil.isValidEmail('abcd@müller.de')).toBe(false)
  })
  test('invalid TLD', () => {
    expect(StringValidationUtil.isValidEmail('test@test.d')).toBe(false)
  })
  test('invalid TLD, 2 dots after @', () => {
    expect(StringValidationUtil.isValidEmail('student@student.uni-tuebingen.d')).toBe(false)
  })
})

// --- isValidPassword() --------------------------------------------------------------------------

describe('The following passwords should be parsed as VALID:', () => {
  test('8 chars, all groups', () => {
    expect(StringValidationUtil.isValidPassword('abcdeF1$')).toBe(true)
  })
  test('9 chars, all groups, plus an additional unicode char from none of the required groups', () => {
    expect(StringValidationUtil.isValidPassword('abcdeF1$')).toBe(true)
  })
  test('15 chars, all groups', () => {
    expect(StringValidationUtil.isValidPassword(',Ii4U$EnHYMI#l/')).toBe(true)
  })
  test('20 chars, all groups', () => {
    expect(StringValidationUtil.isValidPassword('C3m#kBidE!qx$fxC`z.!')).toBe(true)
  })
  test('31 chars, all groups, including JS escape sequences', () => {
    expect(StringValidationUtil.isValidPassword("qQ\\v[UB7y+}o:aUh'H:@<,29HtNM*FD")).toBe(true)
  })
  test('200 chars, all groups, including JS escape sequences', () => {
    expect(
      StringValidationUtil.isValidPassword(
        'c^YE_b,:Xrt-i=3d{wN2z#|?})Y\'s^$r(.;zPi$y]ED-[;dV:nbyMgw6E,i!$k1}uBmzK~jk"PK}gmkD2m>r2GC"d:|X(+h@qCKuV6cj7BKMT$w9yo_&k42:SarM#0->XB<0?Xm5isP]>dk\\fu5DibN8Y@HB!\\E)!dMk*\\0)Un|hyVwwP-G2Ug}wDTG[,J*KB#`RaB?%'
      )
    ).toBe(true)
  })
})

describe('The following passwords should be parsed as INVALID:', () => {
  test('empty string', () => {
    expect(StringValidationUtil.isValidPassword('')).toBe(false)
  })
  test('1 char', () => {
    expect(StringValidationUtil.isValidPassword('a')).toBe(false)
  })
  test('7 chars', () => {
    expect(StringValidationUtil.isValidPassword('1234567')).toBe(false)
  })
  test('8 chars, only lowercase', () => {
    expect(StringValidationUtil.isValidPassword('abcdefgh')).toBe(false)
  })
  test('8 chars, only uppercase', () => {
    expect(StringValidationUtil.isValidPassword('ABCDEFGH')).toBe(false)
  })
  test('8 chars, only numbers', () => {
    expect(StringValidationUtil.isValidPassword('12345678')).toBe(false)
  })
  test('8 chars, only special chars', () => {
    expect(StringValidationUtil.isValidPassword('$%&/@#*?')).toBe(false)
  })
  test('8 chars, only lowercase and uppercase', () => {
    expect(StringValidationUtil.isValidPassword('abcdEFGH')).toBe(false)
  })
  test('8 chars, only lowercase and numbers', () => {
    expect(StringValidationUtil.isValidPassword('abcd1234')).toBe(false)
  })
  test('8 chars, only uppercase and numbers', () => {
    expect(StringValidationUtil.isValidPassword('ABCD1234')).toBe(false)
  })
  test('8 chars, only numbers and special chars', () => {
    expect(StringValidationUtil.isValidPassword('12345!"§$')).toBe(false)
  })
  test('8 chars, only lowercase and special chars', () => {
    expect(StringValidationUtil.isValidPassword('abcd!"§$')).toBe(false)
  })
  test('8 chars, only uppercase and special chars', () => {
    expect(StringValidationUtil.isValidPassword('ABCD!"§$')).toBe(false)
  })
  test('201 chars, all groups, including JS escape sequences', () => {
    expect(
      StringValidationUtil.isValidPassword(
        'c^YE_b,:Xrt-i=3d{wN2z#|?})Y\'s^$r(.;zPi$y]ED-[;dV:nbyMgw6E,i!$k1}uBmzK~jk"PK}gmkD2m>r2GC"d:|X(+h@qCKuV6cj7BKMT$w9yo_&k42:SarM#0->XB<0?Xm5isP]>dk\\fu5DibN8Y@HB!\\E)!dMk*\\0)Un|hyVwwP-G2Ug}wDTG[,J*KB#`RaB?%1'
      )
    ).toBe(false)
  })
})

// --- isValidUsername() --------------------------------------------------------------------------

describe('The following usernames should be parsed as INVALID', () => {
  test('null value', () => {
    expect(StringValidationUtil.isValidUsername(null)).toBe(false)
  })
  test('empty string', () => {
    expect(StringValidationUtil.isValidUsername('')).toBe(false)
  })
  test('whitespace', () => {
    expect(StringValidationUtil.isValidUsername(' ')).toBe(false)
  })
  test('2 allowed chars', () => {
    expect(StringValidationUtil.isValidUsername('ab')).toBe(false)
  })
  test('disallowed chars', () => {
    expect(StringValidationUtil.isValidUsername('ToFi')).toBe(false)
  })
  test('disallowed umlauts', () => {
    expect(StringValidationUtil.isValidUsername('f.köninger')).toBe(false)
  })
})

describe('The following usernames should be parsed as VALID', () => {
  test('3 lowercase chars', () => {
    expect(StringValidationUtil.isValidUsername('abc')).toBe(true)
  })
  test('3 digits', () => {
    expect(StringValidationUtil.isValidUsername('123')).toBe(true)
  })
  test('4 chars lowercase & digits', () => {
    expect(StringValidationUtil.isValidUsername('ab12')).toBe(true)
  })
  test('real name with dot', () => {
    expect(StringValidationUtil.isValidUsername('f.koeninger')).toBe(true)
  })
  test('real name with dash', () => {
    expect(StringValidationUtil.isValidUsername('t-fischer')).toBe(true)
  })
  test('real name with underscore', () => {
    expect(StringValidationUtil.isValidUsername('t_fischer')).toBe(true)
  })
})
