import { LocalDateTimeFormatter } from '@/util/LocalDateTimeFormatter'

// TODO: What do we want if String is empty? "Invalid date" or empty String? -> new Ticket Error Handling

describe('The following tests should all return valid date strings:', () => {
  test('Standard LocalDateTime 1', () => {
    expect(LocalDateTimeFormatter.toDate('2021-02-11T20:01:00')).toBe('2021-02-11')
  })
  test('Standard LocalDateTime 2', () => {
    expect(LocalDateTimeFormatter.toDate('2019-03-05T20:01:00')).toBe('2019-03-05')
  })
  test('Just a Date String', () => {
    expect(LocalDateTimeFormatter.toDate(new Date('2019-03-05'))).toBe('2019-03-05')
  })
})

describe('The following tests must NOT return valid date strings:', () => {
  // TODO: needs to be adjusted -> just for passing
  test('Empty String', () => {
    expect(LocalDateTimeFormatter.toDate('')).toBe('')
  })
})

describe('The following tests should all return valid time strings:', () => {
  test('Standard LocalDateTime 1', () => {
    expect(LocalDateTimeFormatter.toTime('2021-02-11T20:01:00')).toBe('20:01')
  })
  test('Standard LocalDateTime 2', () => {
    expect(LocalDateTimeFormatter.toTime('2021-02-11T05:30:21')).toBe('05:30')
  })
})

describe('The following tests must NOT return valid time strings:', () => {
  // TODO: needs to be adjusted -> just for passing
  test('Empty String', () => {
    expect(LocalDateTimeFormatter.toTime('')).toBe('')
  })
})

describe('To LocalDateTime', () => {
  test('Empty Date and Time String', () => {
    expect(LocalDateTimeFormatter.toDateTime('', '')).toBe('')
  })
  test('Input is normal Date String and Time String (without seconds)', () => {
    expect(LocalDateTimeFormatter.toDateTime('2019-03-05', '20:01')).toBe('2019-03-05T20:01:00')
  })
  test('Input is new Date() and Time String (without seconds)', () => {
    expect(LocalDateTimeFormatter.toDateTime(new Date('2019-03-05'), '20:01')).toBe('2019-03-05T20:01:00')
  })
})

describe('toTimestampFromUnix', () => {
  test('With valid timestamp', () => {
    expect(LocalDateTimeFormatter.toTimestampFromUnix(1617183008)).toEqual('31.03.2021 11:30:08')
  })
})

// ---- makeDateStringsPretty ------------
describe('Test the function that makes the dates pretty', () => {
  test('Simple example', () => {
    expect(LocalDateTimeFormatter.formatLocaleDate('2021-02-01')).toStrictEqual('01.02.2021')
  })
  test('00 will be interpreted as the last day of previous month', () => {
    expect(LocalDateTimeFormatter.formatLocaleDate('2021-10-00')).toStrictEqual('30.09.2021')
  })
  test('Simple example1', () => {
    expect(LocalDateTimeFormatter.formatLocaleDate('2021-10-22')).toStrictEqual('22.10.2021')
  })
  test('Simple example2', () => {
    expect(LocalDateTimeFormatter.formatLocaleDate('2021-03-25')).toStrictEqual('25.03.2021')
  })
  test('Simple example3', () => {
    expect(LocalDateTimeFormatter.formatLocaleDate('2020-10-13')).toStrictEqual('13.10.2020')
  })
})

// minutesToTimeFormat

describe('Test the function minutesToTimeFormat to calculate a HH:mm timestamp from a minutes duration', () => {
  test('null/empty Minutes', () => {
    expect(LocalDateTimeFormatter.minutesToTimeFormat(null)).toBe('00:00')
  })
  test('0 Minutes', () => {
    expect(LocalDateTimeFormatter.minutesToTimeFormat(0)).toBe('00:00')
  })
  test('10 Minutes', () => {
    expect(LocalDateTimeFormatter.minutesToTimeFormat(10)).toBe('00:10')
  })
  test('60 Minutes', () => {
    expect(LocalDateTimeFormatter.minutesToTimeFormat(60)).toBe('01:00')
  })
  test('80 Minutes', () => {
    expect(LocalDateTimeFormatter.minutesToTimeFormat(80)).toBe('01:20')
  })
  test('120 Minutes', () => {
    expect(LocalDateTimeFormatter.minutesToTimeFormat(120)).toBe('02:00')
  })
  test('1620 Minutes', () => {
    expect(LocalDateTimeFormatter.minutesToTimeFormat(1620)).toBe('27:00')
  })
})
