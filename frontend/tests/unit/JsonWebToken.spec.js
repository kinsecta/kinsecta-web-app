import { JsonWebToken } from '@/util/JsonWebToken'
import { LocalDateTimeFormatter } from '@/util/LocalDateTimeFormatter'

// --- parseJwt() ---------------------------------------------------------------------------------

describe('The following JSON Web Tokens should be parsed as VALID', () => {
  test('valid token', () => {
    let res = JsonWebToken.parseJwt(
      'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZmlzY2hlciIsImV4cCI6MTYxNzE4MzAwOH0.wwGJjD7vgpgPcVcEWGILm5rx-IuCSwOqYfE5J9U9_RAwQjQcf5ZaIuT8ND4bELe3oSvF19Z8GKh1pPjwOc3UTg'
    )
    expect(res).toMatchObject({ exp: 1617183008, sub: 'tfischer' })
  })
})

describe('The following JSON Web Tokens should be parsed as INVALID', () => {
  test('empty token', () => {
    // Do not simply call the function, pass it as anonymous function
    // https://eloquentcode.com/expect-a-function-to-throw-an-exception-in-jest
    expect(() => JsonWebToken.parseJwt('')).toThrow()
  })
  test('invalid token', () => {
    expect(() =>
      JsonWebToken.parseJwt(
        'Bearer eyJzdWIiOiJ0Zml6MTILm5rx-IuCSwOqYfE5J9U9_RAeyXXeXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.wQjQcf5ZaIuzY2hlciIsImV4cCIT8ND4bELe3oSvF19Z8GKhYxNzE4MzAwOH0.wwGJjD7vgpgPcVcEWG1pPjwOc3UTg'
      )
    ).toThrow()
  })
})

// --- isValid() (simulated) ----------------------------------------------------------------------

describe('The following JSON Web Tokens should be parsed with a VALID expiry date', () => {
  test('Java testcases fron ValidationToolsTest class', () => {
    let exp = JsonWebToken.parseJwt(
      'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZmlzY2hlciIsImV4cCI6MTYxNzE4MzAwOH0.wwGJjD7vgpgPcVcEWGILm5rx-IuCSwOqYfE5J9U9_RAwQjQcf5ZaIuT8ND4bELe3oSvF19Z8GKh1pPjwOc3UTg'
    ).exp
    expect(exp).toEqual(1617183008)
    let formatted = LocalDateTimeFormatter.toTimestampFromUnix(exp)
    expect(formatted).toEqual('31.03.2021 11:30:08')
  })
})

// --- getUsername() ------------------------------------------------------------------------------

describe('The following JSON Web Tokens should be parsed as VALID and return a username', () => {
  test('Java testcases fron ValidationToolsTest class', () => {
    expect(
      JsonWebToken.getUsername(
        'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZmlzY2hlciIsImV4cCI6MTYxNzE4MzAwOH0.wwGJjD7vgpgPcVcEWGILm5rx-IuCSwOqYfE5J9U9_RAwQjQcf5ZaIuT8ND4bELe3oSvF19Z8GKh1pPjwOc3UTg'
      )
    ).toEqual('tfischer')
  })
})
