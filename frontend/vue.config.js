module.exports = {
  lintOnSave: true,
  outputDir: 'dist',
  pluginOptions: {
    i18n: {
      locale: 'de',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: true
    }
  },
  filenameHashing: true,
  pages: {
    kinsecta: {
      entry: `${__dirname}/src/main.js`,
      template: `${__dirname}/public/index.html`,
      filename: 'index.html'
    }
  }
}
