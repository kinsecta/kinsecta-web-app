import store from '../store/index.js'

import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc' // dependent on utc plugin
import timezone from 'dayjs/plugin/timezone'
import duration from 'dayjs/plugin/duration'
import localeData from 'dayjs/plugin/localeData'
import weekOfYear from 'dayjs/plugin/weekOfYear'
import weekday from 'dayjs/plugin/weekday'

// require day.js language files, english ('en') is included per default
require('dayjs/locale/de')

// register plugins
dayjs.extend(localeData)
dayjs.extend(utc)
dayjs.extend(timezone)
dayjs.extend(duration)
dayjs.extend(weekOfYear)
dayjs.extend(weekday)

// Set default locale from user settings
const dayjsLocale = store.getters.getLocale ? store.getters.getLocale : 'en'
dayjs.locale(dayjsLocale)

// Set default timezone from app config
// BUT this gets only applied to dayjs.tz() command as described in https://github.com/iamkun/dayjs/issues/1227#issuecomment-900816717
const dayjsTimezone = store.getters.getConfig ? store.getters.getConfig.APP_TIMEZONE : 'Europe/Berlin'
dayjs.tz.setDefault(dayjsTimezone)

export const LocalDateTimeFormatter = {
  dateFormat: 'YYYY-MM-DD',
  localDateFormat: 'DD.MM.YYYY',
  timeFormat: 'HH:mm',
  localDateTimeFormat: 'DD.MM.YYYY HH:mm:ss',

  toLocalizedDay(localDate) {
    let parsedDate = dayjs(localDate)
    return parsedDate.isValid() ? parsedDate.format('dd DD.') : ''
  },

  toDate(localDateTime) {
    let parsedDateTime = dayjs(localDateTime)
    return parsedDateTime.isValid() ? parsedDateTime.format(this.dateFormat) : ''
  },

  toTime(localDateTime) {
    let parsedDateTime = dayjs(localDateTime)
    return parsedDateTime.isValid() ? parsedDateTime.format(this.timeFormat) : ''
  },

  toDateTime(date, time) {
    let dateString = this.toDate(date)
    if (dateString !== '' && time.match('^\\d\\d:\\d\\d$')) {
      return dateString + 'T' + time + ':00'
    } else {
      return ''
    }
  },

  toTimestampFromUnix(unixTimeInSeconds) {
    return dayjs.unix(unixTimeInSeconds).tz(dayjsTimezone).format(this.localDateTimeFormat)
  },

  formatLocaleDate(localDateTime) {
    if (localDateTime) {
      return dayjs(localDateTime).format(this.localDateFormat)
    } else {
      return ''
    }
  },

  formatLocaleDateTime(localDateTime) {
    if (localDateTime) {
      return dayjs(localDateTime).format(this.localDateTimeFormat)
    } else {
      return ''
    }
  },

  minutesToTimeFormat(durationInMinutes) {
    // TODO change to dayjs duration
    if (durationInMinutes && durationInMinutes > 0) {
      let hours = Math.floor(durationInMinutes / 60)
      let minutes = Math.round(durationInMinutes % 60)
      if (hours < 10) {
        hours = '0' + hours
      }
      if (minutes < 10) {
        minutes = '0' + minutes
      }
      return hours + ':' + minutes
    } else {
      return '00:00'
    }
  },

  durationSinceStartingTime(startingTime) {
    return dayjs.duration(dayjs().diff(startingTime)).format(this.timeFormat)
  },

  transformSecondsToDays(valueInMinutes, hoursPerDay) {
    return Math.round((valueInMinutes / 60 / hoursPerDay) * 10) / 10
  },

  isWeekendDay(localDate) {
    let weekdayNum = dayjs(localDate).day()
    return weekdayNum === 6 || weekdayNum === 0
  }
}

export const LocalDate = {
  getISOTimestamp() {
    return dayjs().toISOString()
  },
  todayAsDayjs() {
    return dayjs()
  },
  today() {
    return LocalDateTimeFormatter.toDate(dayjs())
  },
  now() {
    return LocalDateTimeFormatter.formatLocaleDateTime(dayjs())
  },
  yesterday() {
    return LocalDateTimeFormatter.toDate(dayjs().subtract(1, 'day'))
  },
  currentMonth() {
    return dayjs().month() + 1
  },
  currentYear() {
    return Number(dayjs().format('YYYY'))
  },
  nextYear() {
    return Number(dayjs().add(1, 'year').format('YYYY'))
  },
  lastYear() {
    return Number(dayjs().subtract(1, 'year').format('YYYY'))
  },
  mondayInCurrentWeek() {
    let monday = 1
    let dayOfWeek = dayjs().day()
    return LocalDateTimeFormatter.toDate(dayjs().subtract(-(monday - dayOfWeek), 'day'))
  },
  mondayInLastWeek() {
    let mondayInCurrentWeek = dayjs(this.mondayInCurrentWeek(), LocalDateTimeFormatter.dateFormat)
    return LocalDateTimeFormatter.toDate(mondayInCurrentWeek.subtract(7, 'day'))
  },
  fridayInCurrentWeek() {
    let friday = 5
    let dayOfWeek = dayjs().day()
    return LocalDateTimeFormatter.toDate(dayjs().add(friday - dayOfWeek, 'day'))
  },
  fridayInLastWeek() {
    let fridayInCurrentWeek = dayjs(this.fridayInCurrentWeek(), LocalDateTimeFormatter.dateFormat)
    return LocalDateTimeFormatter.toDate(fridayInCurrentWeek.subtract(7, 'day'))
  },
  firstDayOfCurrentMonth() {
    return LocalDateTimeFormatter.toDate(dayjs().date(1))
  },
  firstDayOfLastMonth() {
    let firstDayOfCurrentMonth = dayjs(this.firstDayOfCurrentMonth(), LocalDateTimeFormatter.dateFormat)
    let daysInLastMonth = firstDayOfCurrentMonth.subtract(1, 'day').daysInMonth()
    return LocalDateTimeFormatter.toDate(firstDayOfCurrentMonth.subtract(daysInLastMonth, 'day'))
  },
  lastDayOfCurrentMonth() {
    let today = dayjs()
    return LocalDateTimeFormatter.toDate(today.date(today.daysInMonth()))
  },
  lastDayOfLastMonth() {
    let lastDayOfCurrentMonth = dayjs(this.lastDayOfCurrentMonth(), LocalDateTimeFormatter.dateFormat)
    return LocalDateTimeFormatter.toDate(lastDayOfCurrentMonth.subtract(dayjs().daysInMonth(), 'day'))
  },
  lastDayOfMonth(year, month) {
    let parsedMonth = dayjs(`${year}-${month}`, 'YYYY-MM')
    return LocalDateTimeFormatter.toDate(parsedMonth.date(parsedMonth.daysInMonth()))
  }
}

export const StaticDates = {
  yearListAsArray(minYear, maxYear) {
    let iterFromFirstYear = minYear
    let yearList = []
    while (iterFromFirstYear <= maxYear) {
      yearList.push(iterFromFirstYear)
      iterFromFirstYear++
    }
    return yearList
  },
  yearListSinceAsArray(minYear) {
    return this.yearListAsArray(minYear, LocalDate.currentYear())
  },
  yearListAsSelectArray(minYear, maxYear) {
    let yearList = []
    this.yearListAsArray(minYear, maxYear).forEach(function (year) {
      yearList.push({ value: year, text: year })
    })
    return yearList
  },
  yearListSinceAsSelectArray(minYear) {
    return this.yearListAsSelectArray(minYear, LocalDate.currentYear())
  },
  monthListAsSelectArray() {
    const monthOptions = []
    const months = dayjs.months()
    months.map((month, index) => {
      monthOptions.push({
        value: index + 1,
        text: month
      })
    })
    return monthOptions
  }
}
