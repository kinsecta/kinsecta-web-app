import { StringValidationUtil } from '@/util/StringValidationUtil'

export const GoldflamEmail = value => StringValidationUtil.isValidEmail(value)
export const GoldflamUsername = value => StringValidationUtil.isValidUsername(value)
export const GoldflamPassword = value => StringValidationUtil.isValidPassword(value)

export const GoldflamLatitude = value => StringValidationUtil.isValidLatitude(value)
export const GoldflamLongitude = value => StringValidationUtil.isValidLongitude(value)
