// https://www.digitalocean.com/community/tutorials/vuejs-global-event-bus
// https://v3.vuejs.org/guide/migration/events-api.html#_2-x-syntax

import Vue from 'vue'

export const EventBus = new Vue()
