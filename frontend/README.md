# KInsecta Webapp Frontend

The KInsecta Webapp Frontend is built with [Vue.js 2](https://v2.vuejs.org/).

## Project setup

We're using the latest Node LTS version 16.20 for this project.

On latest macOS system you will experience issues with newer Python versions.
Follow [this article](https://stackoverflow.com/a/78257644/1128689) to install Python 3.10 with Homebrew and set up
a `virtualenv` for it:

```bash
brew install python@3.10
brew install virtualenv
# make sure to use Python 3.10 as your default `python3` binary, e.g. by executing the following in your zsh shell
#echo 'export PATH="/opt/homebrew/opt/python@3.10/bin:$PATH' >> ~/.zshrc
virtualenv -p python3.10 venv
source venv/bin/activate
```

Install all dependencies with

```
npm install
```

### Port setup

The API must be up and running on `localhost:8080` or choose another port in `public/config.json` and adapt the
SpringBoot variable `server.port` in the API `application-ext.properties` file.

The vue frontend should run on port `8090` or choose another port when running `npm serve` with `npm serve -p 8090` and
adapt the SpringBoot variable `app.frontend.base-url` in the API `application-ext.properties` file.

### Compiles and hot-reloads for development

```
NODE_ENV=development npm run serve
```

Debug commands like `console.log` are accepted.

### Compiles and minifies for production

```
npm run build
```

Debug commands like `console.log` are rejected.

### Lints and fixes files

```
npm run lint
```

### GUI for managing the vue project (if vuecli is installed)

```
vue ui
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
