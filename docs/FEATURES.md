# Feature description

## Password Reset Workflow

Every user can hold exactly one reset token. If the user requests another password reset the old reset token is deleted,and a new one is created.
Only the latest reset token is considered valid. Also, the token must be redeemed within 24h.

It is required to specify the email account settings for the mail sender in the `application-ext.properties` file.

The flow is executed as follows:
1. password reset request by the user
2. email with reset link is dispatched
3. user opens the link and submits a new password
4. password is reset and a confirmation email is sent
