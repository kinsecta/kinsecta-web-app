#!/bin/bash
set -e

# build SpringBoot applications with Maven
mvn clean package

# build Vue Frontend with npm
cd frontend/
npm install
npm run build --fix
