# KInsecta Web App

Web application of the KInsecta project (https://kinsecta.org).

Available docs:

- Feature descriptions:`docs/FEATURES.md`
- Entity relationship diagram: `docs/kinsecta-er-diagram.puml`
- Changelog: `CHANGELOG.md`
- How to release: `RELEASE.md`


## Dev workflow

### Git

This project uses [Git LFS](https://git-lfs.github.com/) to store large files (e.g. test data). Run `git lfs install` after cloning this repo to initialize Git LFS for this repo.

You should never commit on `main`. Development is done on feature branches based on `develop`.

When a feature is ready, create a merge request targeting `develop` and assign a colleague for review and merging.

Before you commit Java code, please tick the box "Optimize imports" in IntelliJ.

### Docker

This project contains a Docker build workflow. Docker images are used for serving the application in staging and production environments.

For dev purposes, a local `docker compose.yml` exists which uses configuration properties from the following files and folders:

* Docker: `.env` (there's a template for that: `.env.example`)
* Springboot: `docker/config/springboot/`
* Vue: `docker/config/vue/`
* Traefik Reverse Proxy: `docker/config/traefik/`

Before you start the Docker Compose setup for the first time, run `./docker-setup-env.sh` or manually copy the `.env.example` file to `.env` and create temp folders and Docker networks as defined in the setup script.

The Docker Compose configuration launches the following services:

* `db` - a MariaDB 10.11 container for the database on port `33306`
* `api` - the API SpringBoot application on port `8080`
* `frontend` - the Vue.js application on port `8090`
* `minio` - Minio, an S3 compatible Object Storage on port `9090` and its console on port `9091`
* `traefik` - a Traefik Reverse Proxy container which handles local domains and exposes ports `80` and `443`
* `sonarqube` - an optional Sonarqube container for running code quality tests on port `9999`

If you want to launch all services, run `docker compose up -d`

If you want to run only certain services, run `docker compose up -d <service> <service>`

During development, you most likely want to run `docker compose up -d db minio createbuckets`

If you need to rebuild the images, run `docker compose up -d --build <services>`

If you started `frontend` and `api` via Docker, you can access the frontend at the URL specified in the `.env` variable `KINSECTA_FRONTEND_URL` which is per default `kinsecta.docker.localhost`.

### Database

The dev DB is served via Docker. Make sure to set up the Docker dev env as described above.

Start the MariaDB container on port `33306` with:

```
docker-compose up -d db
```

On the very first run it will create the `kinsecta` and `kinsecta_test` databases.

All tables and initial/test data will be loaded by FlyWay during Spring Boot startup from the db migration folder (`api/src/main/resources/db/migration/`) and – when running integration tests – from the fixtures migration folder (`api/src/test/resources/fixtures/migration/`).

There are a couple more SQL scripts at `api/src/main/resources/db/` which aren't applied automatically by FlyWay and can be used manually to set up a (local) test environment (e.g. `10_initial_data.sql`, which is currently also used for initial staging deployments. Note: the default user password is a random pass, currently stored in Goldflam's _"Goldflam shared"_ Keepass vault at `goldflam-shared/kinsecta/Default User Password [Staging]`).

#### Upgrading

If you need to upgrade to a newer version of MariaDB, just change the docker image tag in the `docker compose.yml`.

The `mariadb-upgrade` command is automatically executed when needed because the docker-compose configuration contains the `$MARIADB_AUTO_UPGRADE` environment variable ([docs](https://github.com/docker-library/docs/blob/master/mariadb/README.md#mariadb_auto_upgrade--mariadb_disable_upgrade_backup)).

### Vue.js

If you want to develop frontend features, use the Vue.js CLI in `frontend/`:

```
vue ui
```

### Test-Uploads

The following zip-files can be uploaded successfully (all are located at `api/src/test/resources/zipped`):
- `data_wingbeat_single.zip`
- `data_wingbeat_single_no_classification.zip`
- `data_dummie_single.zip`

----

## Testing

Run all Unit Tests with:

```
mvn clean test
```

Run all Unit and Integration Tests with:

```
mvn clean verify
```

## SonarQube checks

### Remote & CI

The project can use our Sonarqube installation for code quality checks:

```
mvn sonar:sonar -Dsonar.host.url=https://sonar.services.goldflam.de -Dsonar.login=<USERNAME> -Dsonar.password=<PASSWORD>
```

Username and password can be found in Goldflam's _"Goldflam shared"_ Keepass vault at `goldflam-shared/Shared-Services/`.

### Settings

#### Quality Gate

We have a specifically crafted *Quality Gate* called `Goldflam Quality Gate` which can be configured at https://sonar.services.goldflam.de/quality_gates/show/AXjufX1pGRetdramSeBM

#### Quality Profile

For `Java` code, we have our own set of Rules in a *Quality Profile* called `Goldflam Way` derived from the default `Sonar way` profile: https://sonar.services.goldflam.de/profiles/show?language=java&name=Goldflam+Way

This was introduced to reconfigure or disable certain rules and checks listed below:

* Disabled rule [`java:S6212`](https://sonar.services.goldflam.de/coding_rules?open=java%3AS6212&rule_key=java%3AS6212) - *Declare this local variable with "var" instead.*

### Locally

The project can also be run with a local [SonarQube Docker](https://hub.docker.com/_/sonarqube) image for code quality checks:

Because SonarQube uses an embedded Elasticsearch, make sure that your Docker host configuration complies with the [Elasticsearch production mode requirements](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html#docker-cli-run-prod-mode) and [File Descriptors configuration](https://www.elastic.co/guide/en/elasticsearch/reference/current/file-descriptors.html).

For example, on Linux, you can set the recommended values for the current session by running the following commands as root on the host:

```
sysctl -w vm.max_map_count=262144
sysctl -w fs.file-max=65536
ulimit -n 65536
ulimit -u 4096
```

Then start the SonarQube docker container and run the tests:

```
docker-compose up -d sonarqube
```

... wait a couple of minutes until you can reach http://127.0.0.1:9999 in your browser, log in with `admin` / `admin` the first time, change the password as required ...

... and then run the code quality checks:

```
mvn sonar:sonar -Dsonar.host.url=http://127.0.0.1:9999 -Dsonar.login=admin -Dsonar.password=<PASSWORD>
```
