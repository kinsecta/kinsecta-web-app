# Code Style Guide

This document lists general code design rules to follow for this project.


---

## Java

### REST API Design

* Use the plural form when designing "record" endpoints
  * Bad: `/user`
  * Good: `/users`

* Only use lowercase letters and snake case
  * Bad: `/timetrackingUnits`
  * Good: `/timetracking_units`

* Don't use trailing slashes
  * Bad: `/users/`
  * Good: `/users`

### JPA/SQL Queries

* `@NamedQuery` in _Entity_ classes should be avoided. Use _@Query_ on the method in the _Repository_ class.

### SpringBoot

* The _name_ argument must not be omitted in the _@PathVariable_ annotation
  * Bad: _@PathVariable Long id_
  * Good: _@PathVariable(name = "id") Long id_
  * Good (preferred): _@PathVariable("id") Long id_

* When specifying an API endpoint on Controller with `@RequestMapping("/xyz")` you should omit "empty" arguments on child Get/Post mappings
  * Bad: _@PostMapping("/")_
  * Bad: _@PostMapping("")_
  * Good: _@PostMapping_

* No logic must be implemented in Controller classes! The controller methods should be 'stupid' and only call Service methods to create/update/delete/etc. the entities and maybe convert between Entity and EntityDTO.


---

## SQL Database

* Columns must be named in snake case
  * Bad: `absenceStatus`
  * Good: `absence_status`

* Column names must not start with `is_` to indicate a boolean condition as this may cause trouble with the Java entity getters
  * Bad: `is_start_half_day`
  * Good: `start_is_half_day`
  * Good: `start_half_day`

* Column definitions must include `NOT NULL` wherever `null` values are not allowed.
  * Additionally, the model/entity fields must be annotated with `@Column(nullable = false)`.

* Boolean columns must be created as `TINYINT(1)` even though they can be created as `BOOLEAN`. But as this is only an alias for `TINYINT(1)` we use the original `TINYINT(1)` definition.
  * Additionally, the model/entity fields must be annotated with `@Column(nullable = false, columnDefinition = "TINYINT", length = 1)`. If not, JPA validation will fail on Spring Boot Application start.

* Creation Timestamps must be defined as `DATETIME(3) DEFAULT CURRENT_TIMESTAMP NOT NULL` so that a current timestamp is inserted when a null value is passed.
  * Additionally, the `created` entity field must be annotated with `@Column(insertable = false, updatable = false)`. If not, JPA validation will fail when trying to perform the insert query.
  * Additionally, the `created` entity field should be annotated with `@ReadOnlyProperty`.

* Update/Modification Timestamps must be defined as `DATETIME(3) DEFAULT NOW() ON UPDATE NOW() NOT NULL` so that a current timestamp is inserted when a row is updated or a null value is passed.
  * Additionally, the `modified` entity field must be annotated with `@Column(insertable = false, updatable = false)`. If not, JPA validation will fail when trying to perform the update query.
  * Additionally, the `modified` entity field should be annotated with `@ReadOnlyProperty`.


---

## Vue.js

The rules of the Vue.js styleguide should be followed wherever possible: [https://v2.vuejs.org/v2/style-guide/](https://v2.vuejs.org/v2/style-guide/)

`Priority A Rules: Essential` is a _must_ and `Priority B Rules: Strongly Recommended` should be respected.

### Routes and Pages

* Group pages in the filesystem and in the router if they belong together
  * E.g. group all "account" pages in the filesystem at `src/pages/account/xxx` and in the routes like `/account/xxx`

* Specify a route name in `router/index.js`
```
{
  path: '/manage/projects',
  name: 'manage_projects',
  component: () => import(/* webpackChunkName: "manageProjects" */ '../pages/manage/Projects.vue'),
  …
}
```

* Don't explicitly set router links as strings but with the route `name`
  * Bad: _to="/account/profile"_
  * Good: _:to="{ name: 'account_profile' }"_

### Components vs. Pages

* Components placed in _src/components/_ must be reusable and must not be used as a page

* There must be a "page" file in `src/pages/` for every route defined in `router/index.js`

* Components and pages should be grouped in the filesystem if they belong together


---

## Docker

_t.b.d._


---

## Ansible

_t.b.d._
