# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]

### Added

...

### Changed / Fixed

...

### Removed

...


## [3.2.0] - 2024-08-12

### Changed / Fixed

* [#212] Messwerte werden abgelehnt weil Datentypen nicht korrekt definiert sind
* [#211] Backend & Frontend Dependency Updates
  * Ubuntu Security Updates for test server (staging)
  * Ubuntu Security Updates für live server (production)
  * Update Maven dependencies to latest bugfix versions
  * Update Node.js dependencies to latest bugfix versions
  * Update "MariaDB" database to latest bugfix release v10.11.8
  * Update "Apache" frontend docker image to v2.4.62
  * Update "Traefik" reverse proxy to v2.11.8
  * Update "Filebeat" to v8.14.3
  * Update "Minio" S3 image to RELEASE.2024-08-03T04-33-23Z (only local dev environment)
  * Update "Minio Client" S3 image to RELEASE.2024-07-31T15-58-33Z (only local dev environment)
  * Update local "Sonarqube" to v10.6.0 (only local dev environment)


## [3.1.0] - 2024-01-03

GitLab Milestone:
* [M2.2 - Servicevertrag](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/4)

### Changed / Fixed

* [#209] Umstellung des Production-Logging von Datadog auf Goldflam-ELK-Stack
* [#210] Service & Security Updates
  * Ubuntu Security Updates for test server (staging)
  * Ubuntu Security Updates für live server (production)
  * Update Maven dependencies to latest bugfix versions
  * Update Vue.js to v2.7.16
  * Update Node.js dependencies to latest bugfix versions
  * Update "Traefik" reverse proxy to v2.10.7
  * Update "Filebeat" to v8.11.3
  * Update "Minio" S3 image to RELEASE.2024-01-01T16-36-33Z (only local dev environment)
  * Update "Minio Client" S3 image to RELEASE.2023-12-29T20-15-29Z (only local dev environment)


## [3.0.1] - 2023-12-22

GitLab Milestone:
* [M4 - Version 3.0.1](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/8)

### Added

* [#203] Wordpress: Konfiguration Charts-Plugin

### Changed / Fixed

* [#206] Bug: Public-Sensor-API: Sortierung der angezeigten Daten ist falsch
* [#207] Bug: Public-API-Requests auf "/latest" und auf "sensor/{id}" dauern recht lange


GitLab Milestone:
* [M2.2 - Servicevertrag](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/4)

### Changed / Fixed

* [#208] E-Mail-Domain-Whiteliste wurde beim Anlegen eines Users nicht geprüft


## [3.0.0] - 2023-12-15

GitLab Milestone:
* [M4 - Version 3.0](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/7)

### Added

* [#193] Public Backend-API anlegen
* [#194] Public-API: Sensors-Endpoint
* [#195] Public-API: Sensor-Details-Endpoint
* [#196] Public-API: Image-Endpoint
* [#197] Public-API: Latest-Datasets-Endpoint
* [#198] Public Stats-API: Datenreihen
* [#199] Public Stats-API: Counts
* [#200] Maps-Frontend
* [#202] Wordpress: Konfiguration "wpgetAPI"-Plugin"

### Changed / Fixed

* [#192] Infrastruktur-Setup
* [#205] Sensor erweitern um ein Feld "öffentliche Anzeige"


GitLab Milestone:
* [M2.2 - Servicevertrag](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/4)

### Changed / Fixed

* [#204] Downgrade Datadog agent to v7.45.1 (only production)


## [2.5.0] - 2023-12-01

GitLab Milestone:
* [M2.2 - Servicevertrag](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/4)

### Changed / Fixed

* [#201] Service & Security Updates
  * Ubuntu Security Updates for test server (staging)
  * Ubuntu Security Updates für live server (production)
  * Update Spring Boot to v2.7.18
  * Update Maven dependencies to latest bugfix versions
  * Update Node.js dependencies to latest bugfix versions
  * Update "MariaDB" database to latest bugfix release v10.11.6
  * Update "Traefik" reverse proxy to v2.10.6
  * Update "Datadog Agent" to v7.49.1 (only production)
  * Update "Filebeat" to v8.11.1 (only staging)
  * Update "Minio" S3 image to RELEASE.2023-11-20T22-40-07Z (only local dev environment)
  * Update "Minio Client" S3 image to RELEASE.2023-11-20T16-30-59Z (only local dev environment)
  * Update local "Sonarqube" to v10.3.0 (only local dev environment)


## [2.4.1] - 2023-11-14

GitLab Milestone:
* [M2.2 - Servicevertrag](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/4)

### Changed / Fixed

* [#191] Validierung von E-Mail-Adressen fehlerhaft


## [2.4.0] - 2023-11-06

GitLab Milestone:
* [M2.2 - Servicevertrag](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/4)

### Changed / Fixed

* [#190] Service & Security Updates
  * Ubuntu Security Updates for test server (staging)
  * Ubuntu Security Updates für live server (production)
  * Update Spring Boot to v2.7.17
  * Update Maven dependencies to latest bugfix versions
  * Update Node.js dependencies to latest bugfix versions
  * Upgrade "MariaDB" database from 10.6 to latest LTS release 10.11
  * Update "Traefik" reverse proxy to v2.10.5
  * Update "Apache" frontend docker image to v2.4.58
  * Update "Datadog Agent" to v7.49.0 (only production)
  * Update "Filebeat" to v8.10.4 (only staging)
  * Update "Minio" S3 image to RELEASE.2023-11-01T18-37-25Z (only local dev environment)
  * Update "Minio Client" S3 image to RELEASE.2023-10-30T18-43-32Z (only local dev environment)


## [2.3.0] - 2023-10-10

GitLab Milestone:
* [M2.2 - Servicevertrag](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/4)

### Changed / Fixed

* [#189] Service & Security Updates
  * Ubuntu Security Updates for test server (staging)
  * Ubuntu Security Updates für live server (production)
  * Update Spring Boot to v2.7.16
  * Update Maven dependencies to latest bugfix versions
  * Update Node.js dependencies to latest bugfix versions
  * Update "MariaDB" database to latest bugfix release v10.6.15
  * Update "Traefik" reverse proxy to v2.10.4
  * Update "Datadog Agent" to v7.48.0 (only production)
  * Update "Filebeat" to v8.10.3 (only staging)
  * Update "Minio" S3 image to RELEASE.2023-10-07T15-07-38Z (only local dev environment)
  * Update "Minio Client" S3 image to RELEASE.2023-10-04T06-52-56Z (only local dev environment)
  * Update local "Sonarqube" to v10.2.1 (only local dev environment)


## [2.2.0] - 2023-06-30

GitLab Milestone:
* [M3.1 - Version 2.2](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/6)

### Added

* [#188] New button to copy all GBIF ID's to the clipboard

### Changed / Fixed

* [#183] No more display of plain names for all roles except admin
* [#185] Jump to next record instead of back when deleting record

GitLab Milestone:
* [M2.2 - Servicevertrag](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/4)

### Changed / Fixed

* 2314827 - backend refactoring: change log message formatting
* 3aafe9b - backend refactoring: change method param order
* 6b0e38e - backend refactoring: validate S3 bucket names
* 439d9b0 - fix issue in S3Client where catching multiple exceptions together with an InterruptedException could fail
* [#184] Service & Security Updates (May 2023)
  * Ubuntu Security Updates for test server (staging)
  * Ubuntu Security Updates für live server (production)
  * Update Spring Boot to v2.7.12
  * Update Maven dependencies to latest bugfix versions
  * Update Node.js dependencies to latest bugfix versions
  * Upgrade "MariaDB" database to latest bugfix release v10.6.14
  * Update "Datadog Agent" to v7.45.0 (only production)
  * Update "Filebeat" to v8.8.1 (only staging)
  * Update "Minio" S3 image to RELEASE.2023-06-09T07-32-12Z (only local dev environment)
  * Update "Minio Client" S3 image to RELEASE.2023-06-06T13-48-56Z (only local dev environment)
  * Update local "Sonarqube" to v9.9.0 (only local dev environment)
* [#187] Service & Security Updates (June 2023)
  * Ubuntu Security Updates for test server (staging)
  * Ubuntu Security Updates für live server (production)
  * Update Spring Boot to v2.7.13
  * Update Maven dependencies to latest bugfix versions
  * Update Node.js dependencies to latest bugfix versions
  * Update "Traefik" reverse proxy to v2.10.3
  * Update "Datadog Agent" to v7.45.1 (only production)
  * Update "Filebeat" to v8.8.2 (only staging)
  * Update "Minio" S3 image to RELEASE.2023-06-29T05-12-28Z (only local dev environment)
  * Update "Minio Client" S3 image to RELEASE.2023-06-28T21-54-17Z (only local dev environment)
  * Update local "Sonarqube" to v10.1.0 (only local dev environment)
* 94049fc - not necessary to split method if it's only tested with 1 parameter, use @SpringBootTest(properties) instead


## [2.1.0] - 2023-05-02

GitLab Milestone:
* [M2.2 - Servicevertrag](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/4)

### Changed / Fixed

* [#159] fix mail sender authentication (STARTTLS)
* [#182] Service & Security Updates
  * Ubuntu Security Updates for test server (staging)
  * Ubuntu Security Updates für live server (production)
  * Update Spring Boot to v2.7.11
  * Update Maven dependencies to latest bugfix versions
  * Update Node.js dependencies to latest bugfix versions
    * Update Node.js min requirement to latest v16 LTS release (16.20.0)
  * Upgrade "MariaDB" database from 10.5 to 10.6
  * Update "Traefik" reverse proxy to v2.10.1
  * Update "Apache" frontend docker image to v2.4.57
  * Update "Datadog Agent" to v7.44.0 (only production)
  * Update "Minio" S3 image to RELEASE.2023-04-28T18-11-17Z (only local dev environment)
  * Update "Minio Client" S3 image to RELEASE.2023-04-12T02-21-51Z (only local dev environment)
  * Update local "Sonarqube" to v10.0.0 (only local dev environment)


## [2.0.0] - 2023-04-03

GitLab Milestone:
* [M3 - Version 2.0](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/5)

### Added

* [#91] Frontend: Tabelle aller Sensoren mit Bearbeitungs- und Ergänzungsmöglichkeit
* [#92] Frontend: Tabelle aller Sensor-Standorte mit Bearbeitungs- und Ergänzungsmöglichkeit
* [#94] Frontend: Tabelle aller Insekten (Ordnungen, Familien, Gattungen, Arten) mit Ergänzungsmöglichkeit
* [#108] Duplikate erkennen und löschen
* [#129] Button für nächsten/vorhergehenden Datensatz
* [#143] Erweiterung Filterfunktionen nach Datenbankeinträgen in "Datensätze" und "Export" inkl. Boolsche Operationen
* [#145] Zusätzlicher Datenbankeintrag "Letzte Änderung an Datensatz"
* [#146] Neue Rollen und Berechtigungen

### Changed / Fixed

* [#84] Backend: Funktion zur Aktualisierung von Insekten wiederherstellen
* [#131] Stückelung eines Exportes von über 200 Datensätzen zu mehreren einzelnen Downloads
* [#137] Filter/ausgewählte Seite in Reiter Datensätze per Default nicht resetten
* [#140] Angabe der Tags in Export-data.json
* [#146] Datensätze dem Upload-User zuordnen, nicht mehr dem User aus der data.json
* [#157] ER-Diagramm aktualisieren

### Removed

* [#156] Synonyme für Insekten-Spezies aus Datenbank entfernen


GitLab Milestone:
* [M2.2 - Servicevertrag](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/4)

### Changed / Fixed

* [#181] Service & Security Updates
  * Ubuntu Security Updates for test server (staging)
  * Ubuntu Security Updates für live server (production)
  * Update Spring Boot to v2.7.10
  * Update Maven dependencies to latest bugfix versions
  * Update Node.js dependencies to latest bugfix versions
  * Update "MariaDB" database to v10.5.19
  * Update "Apache" frontend docker image to v2.4.56
  * Update "Datadog Agent" to v7.43.1 (only production)
  * Update "Filebeat" to v8.7.0 (only staging)


## [1.10.0] - 2023-02-06

GitLab Milestone:
* [M3 - Version 2.0](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/5)

### Changed / Fixed

* [#128] fixed hidden datetime picker header column on small screens
* [#132] fixed missing key 'spectrum.unit' in exported 'data.json'

GitLab Milestone:
* [M2.2 - Servicevertrag](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/4)

### Changed / Fixed

* 7996476 - ci: do not run 'node:lint' job in separate stage but together with all tests
* [#155] Service & Security Updates
  * Ubuntu Security Updates for test server (staging)
  * Ubuntu Security Updates für live server (production)
  * Update Spring Boot to v2.7.8
  * Update Maven dependencies to latest bugfix versions
    * Upgrade Auth0 JWT dependency from 3.19.4 to next major version 4.2.2
  * Update Node.js dependencies to latest bugfix versions
  * Update "Apache" frontend docker image to v2.4.55
  * Update "Datadog Agent" to v7.42.1 (only production)
  * Update "Filebeat" to v8.6.1 (only staging)
  * Update "Minio" S3 image to RELEASE.2023-01-31T02-24-19Z (only local dev environment)
  * Update "Minio Client" S3 image to RELEASE.2023-01-28T20-29-38Z (only local dev environment)
  * Update local "Sonarqube" to v9.9.0 (only local dev environment)


## [1.9.0] - 2023-01-11

GitLab Milestone:
* [M3 - Version 2.0](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/5)

### Changed / Fixed

* [#125] update Spring Boot to latest v2.7.6
* 84e6920 - update Node.js requirement to latest v16 release (16.18.1)
* [#126] update Vue.js to v2.7
* 13dfb6c - refactoring: move frontend "views" to "pages" folder
* [#141] make dataset tag list in dataset table more flexible (wrap tags instead of cutting them at a fixed width)
* [#142] make dataset table (especially column widths) more flexible
* [#147] rename "back" button in dataset edit page

GitLab Milestone:
* [M2.2 - Servicevertrag](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/4)

### Changed / Fixed

* 29d3445 - fix project setup after latest minio update
* [#153] Service & Security Updates
  * Ubuntu Security Updates for test server (staging)
  * Ubuntu Security Updates für live server (production)
  * Update Spring Boot to v2.7.7
  * Update Maven dependencies to latest bugfix versions
  * Update Node.js dependencies to latest bugfix versions
  * Update "Traefik" reverse proxy to v2.9.6
  * Update "Datadog Agent" to v7.41.1 (only production)
  * Update "Filebeat" to v8.6.0 (only staging)
  * Update "Minio" S3 image to RELEASE.2023-01-06T18-11-18Z (only local dev environment)
  * Update "Minio Client" S3 image to RELEASE.2022-12-24T15-21-38Z (only local dev environment)
  * Update local "Sonarqube" to v9.8.0 (only local dev environment)


## [1.8.0] - 2022-12-01

GitLab Milestones:
* [M2.2 - Servicevertrag](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/4)

### Changed / Fixed

* [#151] Service & Security Updates
  * Ubuntu Security Updates for test server (staging)
  * Ubuntu Security Updates für live server (production)
  * Update Spring Boot to v2.6.14
  * Update Maven dependencies to latest bugfix versions
  * Update Node.js dependencies to latest bugfix versions
  * Update "MariaDB" database to v10.5.18
  * Update "Traefik" reverse proxy to v2.9.5
  * Update "Datadog Agent" to v7.40.1 (only production)
  * Update "Filebeat" to v8.5.2 (only staging)
  * Update "Minio" S3 image to RELEASE.2022-11-29T23-40-49Z (only local dev environment)
  * Update "Minio Client" S3 image to RELEASE.2022-11-17T21-20-39Z (only local dev environment)
* 4a67d57 - fix: use a named docker volume for MariaDB data instead of a local directory to avoid deadlocks on Flyway DB migrations on Windows


## [1.7.0] - 2022-11-09

GitLab Milestones:
* [M2.2 - Servicevertrag](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/4)

### Changed / Fixed

* [#150] Service & Security Updates
  * Ubuntu Security Updates for test server (staging)
  * Ubuntu Security Updates für live server (production)
  * Update Spring Boot to v2.6.13
  * Update Maven dependencies to latest bugfix versions
  * Update Node.js dependencies to latest bugfix versions
  * Update "Traefik" reverse proxy to v2.9.4
  * Update "Datadog Agent" to v7.40.0 (only production)
  * Update "Filebeat" to v8.5.0 (only staging)
  * Update "Minio" S3 image to RELEASE.2022-11-08T05-27-07Z (only local dev environment)
  * Update "Minio Client" S3 image to RELEASE.2022-11-07T23-47-39Z (only local dev environment)
  * Update local "Sonarqube" to v9.7.1 (only local dev environment)


## [1.6.0] - 2022-10-05

GitLab Milestones:
* [M2.2 - Servicevertrag](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/4)

### Changed / Fixed

* [#139] Service & Security Updates
  * Ubuntu Security Updates for test server (staging)
  * Ubuntu Security Updates für live server (production)
  * Update Spring Boot to v2.6.12
  * Update Maven dependencies to latest bugfix versions
  * Update Node.js dependencies to latest bugfix versions
  * Update "Traefik" reverse proxy to v2.9.1
  * Update "Datadog Agent" to v7.39.1 (only production)
  * Update "Filebeat" to v8.4.2 (only staging)
  * Update "Minio" S3 image to RELEASE.2022-10-02T19-29-29Z (only local dev environment)


## [1.5.0] - 2022-09-12

GitLab Milestones:
* [M2.2 - Servicevertrag](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/4)

### Changed / Fixed

* [#138] Service & Security Updates
  * Ubuntu Security Updates for test server (staging)
  * Ubuntu Security Updates für live server (production)
  * Update Spring Boot to v2.6.11
  * Update Maven dependencies to latest bugfix versions
  * Update Node.js dependencies to latest bugfix versions
  * Update "MariaDB" database to v10.5.17
  * Update "Traefik" reverse proxy to v2.8.4
  * Update "Datadog Agent" to v7.38.2 (only production)
  * Update "Filebeat" to v8.4.1 (only staging)
  * Update "Minio" S3 image to RELEASE.2022-09-07T22-25-02Z (only local dev environment)
  * Update local "Sonarqube" to v9.6.1 (only local dev environment)


## [1.4.0] - 2022-08-05

GitLab Milestones:
* [M2.2 - Servicevertrag](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/4)

### Changed / Fixed

* [#133] Aktualisierung Insektenliste und Sensorstandorte via SQL-Dump
* [#134] Spring Boot Update 2.6.10
* [#135] Service & Security Updates
  * Ubuntu Security Updates for test server (staging)
  * Ubuntu Security Updates für live server (production)
  * Update Node.js dependencies to latest bugfix versions
  * Update "Traefik" reverse proxy to v2.8.1
  * Update "Datadog Agent" to v7.38.1 (only production)
  * Update "Filebeat" to v8.3.3 (only staging)
  * Update "Minio" S3 image to RELEASE.2022-08-02T23-59-16Z (only local dev environment)
* [#136] 'docker pull' schlägt fehl, wenn Deploy Token verwendet wird


## [1.3.1] - 2022-07-07

GitLab Milestones:
* [M2.2 - Servicevertrag](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/4)

### Changed / Fixed

* [#127] Export startet nicht
* [#130] Export-Grenze auf 200 Datensätze erhöhen
* spec: add comments to OpenAPI Generator Template overrides


## [1.3.0] - 2022-07-01

GitLab Milestones:
* [M2.2 - Servicevertrag](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/4)

### Changed / Fixed

* [#123] Spring Boot Update 2.6.9
* [#124] Service & Security Updates
  * Ubuntu Security Updates for test server (staging)
  * Ubuntu Security Updates für live server (production)
  * Update Maven dependencies to latest bugfix versions
  * Update Node.js dependencies to latest bugfix versions
  * Update "Traefik" reverse proxy to v2.8.0
  * Update "Apache" frontend docker image to v2.4.54
  * Update "Datadog Agent" to v7.37.1 (only production)
  * Update "Filebeat" to v8.3.1 (only staging)
  * Update "Minio" S3 image to RELEASE.2022-06-25T15-50-16Z (only local dev environment)
  * Update local "Sonarqube" to v9.5 (only local dev environment)


## [1.2.0] - 2022-05-25

GitLab Milestones:
* [M2.2 - Servicevertrag](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/4)

### Changed / Fixed

* [#120] Ansible-Upgrade auf v5.8 (mit Ubuntu 22.04)
* [#121] Service & Security Updates
  * Ubuntu Security Updates for test server (staging)
  * Ubuntu Security Updates für live server (production)
  * Update "Traefik" reverse proxy from v2.6.3 to v2.6.7
  * Update "MariaDB" database from v10.5.15 to v10.5.16
  * Update "Filebeat" to v8.2.1 (only staging)
  * Update local "Sonarqube" to v9.4 (only local dev environment)
* [#122] Spring Boot Update 2.6.8


## [1.1.0] - 2022-05-09

GitLab Milestones:
* [v2 - Prototyp](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/2)
* [v2.1 - Infra-Setup](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/3)

### Changed / Fixed

* [#14] Datadog Hostname
* [#114] Staging auf ELK-Monitoring umstellen und testen
* [#115] Spring Boot Update 2.6.7
* [#116] Aktualisierung der Insekten und Sensoren via SQL-Dump
* [#117] Änderung der Einheit der Luftfeuchtigkeit in Datensatzbearbeitung
* [#118] Frontend - Datensatzbearbeitung: Unerwartetes Verhalten des Autocomplete-Widgets für Klassifizierungsdaten
* 7e30c0d - api dependency updates


## [1.0.0] - 2022-04-08

Initial Public Release

GitLab Milestone: [v2 - Prototyp](https://gitlab.com/kinsecta/kinsecta-web-app/-/milestones/2)
